## Contributors
- **Lucas Guillerot** (<lucas.guillerot@universite-paris-saclay.fr>):
  Contributed the `Utils.retry` decorator, which retries HDF5 function calls when a `BlockingError` occurs.
