.DEFAULT_GOAL := init
.PHONY: profile jupyter add_req_prod add_req_dev add_major_version add_minor_version add_patch_version add_premajor_version add_preminor_version add_prepatch_version add_prerelease_version prepare-dev data help lint tests coverage upload-prod-pypi update_req  pyclean tox licences check_update update_latest_main update_latest_dev show_deps_main show_deps_dev show_obsolete
VENV = ".venv"
VENV_RUN = ".py_sas_data_modeler"

define PROJECT_HELP_MSG

Usage:\n
	\n
    make help\t\t\t             show this message\n
	\n
	-------------------------------------------------------------------------\n
	\t\tInstallation\n
	-------------------------------------------------------------------------\n
	make\t\t\t\t                Install py_sas_data_modeler in the system (root)\n
	make user\t\t\t 			Install py_sas_data_modeler for non-root usage\n
	\n
	-------------------------------------------------------------------------\n
	\t\tDevelopment\n
	-------------------------------------------------------------------------\n
	make prepare-dev\t\t 		Prepare Development environment\n
	make install-dev\t\t 		Install COTS and py_sas_data_modeler for development purpose\n
	make tests\t\t\t             Run units and integration tests\n
	make release\t\t\t 			Release the package as tar.gz\n
	make pyclean\t\t\t		Clean .pyc files and __pycache__ directories\n
	make add_req_prod pkg=<name>\t Add the package in the dependencies of .toml\n
	make add_req_dev pkg=<name>\t Add the package in the DEV dependencies of .toml\n
	\n
	-------------------------------------------------------------------------\n
	\t\tVersion\n
	-------------------------------------------------------------------------\n
	make version\t\t\t		Display the version\n
	make add_major_version\t\t	Add a major version\n
	make add_minor_version\t\t	Add a minor version\n
	make add_patch_version\t\t	Add a patch version\n
	make add_premajor_version\t	Add a pre-major version\n
	make add_preminor_version\t	Add a pre-minor version\n
	make add_prepatch_version\t	Add a pre-patch version\n
	make add_prerelease_version\t	Add a pre-release version\n
	\n
	-------------------------------------------------------------------------\n
	\t\tMaintenance (use make install-dev before using these tasks)\n
	-------------------------------------------------------------------------\n
	make check_update\t\t 		Check the COTS update\n
	make update_req\t\t			Update the version of the packages in the authorized range in toml file\n
	make update_latest_main\t 	Update to the latest version for production\n
	make update_latest_dev\t\t	Update to the latest version for development\n
	make show_deps_main\t\t 	Show main COTS for production\n
	make show_deps_dev\t\t 		Show main COTS for development\n
	make show_obsolete\t\t		Show obsolete COTS\n
	\n
	-------------------------------------------------------------------------\n
	\t\tOthers\n
	-------------------------------------------------------------------------\n
	make licences\t\t\t		Display the list of licences\n
	make coverage\t\t\t 	Coverage\n
	make lint\t\t\t			Lint\n
	make tox\t\t\t 			Run all tests\n
	make jupyter\t\t\t 		Run jupyter lab\n
	make profile\t\t\t 		Run profile\n
	make doc-manual\t\t 	Generate user manual as HTML\n
	make doc-manual-pdf\t\t	Generate user manual as PDF\n
	make doc-test-plan\t\t 	Generate test plan as HTML\n
	make doc-test-plan-pdf\t\t 	Generate test plan as PDF

endef
export PROJECT_HELP_MSG


#Show help
#---------
help:
	echo $$PROJECT_HELP_MSG


#
# Sotware Installation in the system (need root access)
# -----------------------------------------------------
#
init:
	poetry install --only=main

#
# Sotware Installation for user
# -----------------------------
# This scheme is designed to be the most convenient solution for users
# that don’t have write permission to the global site-packages directory or
# don’t want to install into it.
#
user:
	poetry install --only=main


prepare-dev:
	git config --global init.defaultBranch main
	git init
	echo "echo \"Using Virtual env for py_sas_data_modeler.\"" > ${VENV_RUN}
	echo "echo \"Please, type 'deactivate' to exit this virtual env.\"" >> ${VENV_RUN}
	echo "python3 -m venv --prompt py_sas_data_modeler ${VENV} && \
	export PYTHONPATH=. && \
	export PATH=`pwd`/${VENV}/bin:${PATH}" >> ${VENV_RUN} && \
	echo "source \"`pwd`/${VENV}/bin/activate\"" >> ${VENV_RUN} && \
	scripts/install-hooks.bash && \
	echo "\nnow source this file: \033[31msource ${VENV_RUN}\033[0m"


install-dev:
	poetry install && poetry run pre-commit install && poetry run pre-commit autoupdate
	poetry run python -m ipykernel install --user --name=.venv

tox:
	poetry run tox -e py310

release:
	poetry build

version:
	poetry version -s

add_major_version:
	poetry version major
	poetry lock
	poetry run git tag `poetry version -s`

add_minor_version:
	poetry version minor
	poetry lock
	poetry run git tag `poetry version -s`

add_patch_version:
	poetry version patch
	poetry lock
	poetry run git tag `poetry version -s`

add_premajor_version:
	poetry version premajor
	poetry lock

add_preminor_version:
	poetry version preminor
	poetry lock

add_prepatch_version:
	poetry version prepatch
	poetry lock

add_prerelease_version:
	poetry version prerelease
	poetry lock

add_req_prod:
	poetry add "$(pkg)"

add_req_dev:
	poetry add -G dev "$(pkg)"

check_update:
	poetry show -l

update_req:
	poetry update

update_latest_main:
	packages=$$(poetry show -T --only=main | grep -oP "^\S+"); \
	packages_latest=$$(echo $$packages | tr '\n' ' ' | sed 's/ /@latest /g'); \
	poetry add -G main $$packages_latest

update_latest_dev:
	packages=$$(poetry show -T --only=dev | grep -oP "^\S+"); \
	packages_latest=$$(echo $$packages | tr '\n' ' ' | sed 's/ /@latest /g'); \
	poetry add -G dev $$packages_latest

show_deps_main:
	poetry show -T --only=main

show_deps_dev:
	poetry show -T --only=dev

show_obsolete:
	poetry show -o

tests:  ## Run tests
	export GENERATE_JSON_REPORT=False
	poetry run pytest -s -vv --log-cli-level=INFO

tests-report:
	poetry run pytest --json-report --json-report-file=scripts/test_result/report.json --json-report-indent=5
	poetry run python scripts/test_result/generate_report_pdf.py

pyclean:
	find . -type f -name '*.py[co]' -delete -o -type d -name __pycache__ -delete

licences:
	poetry run python3 scripts/license.py

coverage:  ## Run tests with coverage
	poetry run coverage erase
	poetry run coverage run --include=py_sas_data_modeler/* -m pytest -ra
	poetry run coverage report -m

coverage_xml:  ## Run tests with xml
	poetry run coverage xml

lint:  ## Lint and static-check
	poetry run flake8 --ignore=E203,E266,E501,W503,F403,F401 --max-line-length=71 --max-complexity=18 --select=B,C,E,F,W,T4,B9 py_sas_data_modeler
	poetry run pylint --disable C0301 py_sas_data_modeler
	poetry run mypy --install-types --non-interactive py_sas_data_modeler

doc-manual:
	make licences
	rm -rf docs/user_manual/source/_static/coverage
	poetry run pytest -ra --html=docs/user_manual/source/_static/report.html --benchmark-json=docs/user_manual/source/_static/benchmark.json --json-report --json-report-file=scripts/test_result/report.json --json-report-indent=5 --benchmark-histogram docs/user_manual/source/_static/histo
	poetry run make coverage
	poetry run coverage html -d docs/user_manual/source/_static/coverage
	make profile
	poetry run mv prof/combined.svg docs/user_manual/source/_static/
	poetry run python scripts/test_result/generate_report_pdf.py
	make pyreverse
	poetry run mv scripts/test_result/test_report.pdf docs/user_manual/source/_static/
	make html -C docs/user_manual

doc-manual-pdf:
	poetry run make doc-manual
	poetry run make latexpdf -C docs/user_manual

doc-test-plan:
	make html -C docs/test_plan

doc-test-plan-pdf:
	poetry run make doc-test-plan
	poetry run make latexpdf -C docs/test_plan

jupyter:
	jupyter lab

profile:
	poetry run pytest --profile --profile-svg tests/acceptance/pipeline_acceptance_test.py

pyreverse:
	pyreverse -o png -p architecture -d docs/user_manual/source/_static/ --ignore sas.py --colorized --all-associated py_sas_data_modeler
