## Summary

(Summarize the bug encountered concisely)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## What is the current bug behavior?

(What actually happens)

## What is the expected correct behavior?

(What you should see instead)

## Relevant logs and/or screenshots

(Paste any relevant logs - use code blocks (```) to format console output, logs, and code, as
it's very hard to read otherwise.)

## Environment

- **OS:** (e.g., macOS, Ubuntu)
- **Python version:** (e.g., 3.12)
- **Additional Information:**
  - (Add any other information about your environment, such as hardware specs, relevant services, etc.)

## Possible fixes

(If you can, link to the line of code that might be responsible for the problem)

---

/label ~"bug" ~"To Do"
/assign @jean-christophe.malapert
