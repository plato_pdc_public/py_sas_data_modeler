# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
"""
The 'hdf5' module provides the implementation of two classes, HDF5Manager and HDF5StorageSasWorkflow, for
managing HDF5 file operations and interfacing with SAS workflows.
"""
import json
import os
import sys
from enum import Enum
from typing import Any
from typing import Dict
from typing import List
from typing import Optional
from typing import TypeVar
from typing import Union

import h5py
import numpy as np
from loguru import logger

from ..dm import BaseDataClass
from ..dm import BaseRootModel
from ..dm import DataClassFactory
from ..dm import Hdf5Tree
from ..dm.io_pipeline import Hdf5NodeEnum
from ..exception import DataBlockingIOError
from ..exception import InvalidHDF5ObjectError
from ..exception import InvalidProductError
from ..exception import NoExistingDatasetError
from ..exception import NoExistingGroupError
from ..exception import ProductClassError
from ..exception import ProductNotFound
from ..exception import ProductSaveError
from ..exception import ProductTypeError
from ..exception import StoringInformationError
from ..helper import debug_log
from ..helper import retry
from ..helper import spec_coverage
from ..helper import Utils
from .interface import IStorageSasTask
from .interface import IStorageSasWorkflow

logger.remove()
logger.add(sys.stdout, level="INFO")

T = TypeVar("T", bound=Union["BaseDataClass", "BaseRootModel"])


@spec_coverage("PLATO-IAS-PDC-ICD-0001", "SAS-REQ-IF-MODULE-STO-30")
@spec_coverage("PLATO-IAS-PDC-TN-0005", "REQ-IF-HDF5-30", "REQ-IF-HDF5-40")
class HDF5Manager:
    """
    A class to manage HDF5 file operations, including linking data, saving and loading recursively,
    and checking if data exists in specific groups.

    Attributes:
        filename (str): The HDF5 file name for data management.
    """

    class NodeType(Enum):
        PRODUCT = 1
        EXTERNAL = 2
        NONE = 3

    def __init__(self, filename: str):
        """
        Initializes the HDF5Manager with the provided filename.

        Args:
            filename (str): The HDF5 file path.
        """
        self.__filename = filename
        logger.debug(f"Initialized HDF5Manager for file: {self.__filename}")

    @staticmethod
    def decode_none(value: str) -> Optional[str]:
        """Decode None value from HDF5

        Args:
            value (str): value to decode as None if needed

        Returns:
            Optional[str]: the decoded value id needed
        """
        return None if value == "None" else value

    @staticmethod
    def encode_none() -> str:
        """Encode None value as string

        Returns:
            str: None value
        """
        return "None"

    @staticmethod
    def is_group(hdf_file: h5py.File, path: str):
        """
        Check if the given path is an existing group in the HDF5 file.

        Args:
            hdf_file (h5py.File): The open HDF5 file.
            path (str): The path to check.

        Returns:
            bool: True if the path exists and is a group, False otherwise.
        """
        return (
            isinstance(hdf_file[path], h5py.Group)
            if path in hdf_file
            else False
        )

    @property
    def filename(self) -> str:
        """
        Property to retrieve the filename of the HDF5 file.

        Returns:
            str: The filename of the HDF5 file.
        """
        return self.__filename

    @retry
    def create_link_to_data(
        self, src_path: str, dest_path: str, is_required: bool = True
    ):
        """
        Creates a hard link between a source path and an input location in the HDF5 file.

        Args:
            src_path (str): The source target path where the link will be created.
            dest_path (str): The destination target path to be linked to.
            is_required (bool, optional): Indicates if the data to link is mandatory. Defaults to True.

        Raises:
            StoringInformationError: If the input location is missing and is_required is True.
            InvalidHDF5ObjectError: If dest_src exists but is neither a group nor a dataset.
        """
        with h5py.File(self.filename, "a") as hdf_file:
            # Check if the link already exists
            if dest_path in hdf_file:
                logger.debug(
                    f"Link for {dest_path} already exists in {self.filename}"
                )
                return

            # Check if dest_src exists in the file
            if src_path not in hdf_file:
                if is_required:
                    logger.error(
                        f"{src_path} is missing and is required to create the link at {dest_path}"
                    )
                    raise StoringInformationError(
                        f"Required input location '{src_path}' is missing."
                    )
                else:
                    logger.debug(
                        f"Optional data {src_path} does not exist in {self.filename}. Skipping link creation."
                    )
                    return

            # Check if input_location is a group or a dataset
            target_obj = hdf_file[src_path]
            if isinstance(target_obj, h5py.Group) or isinstance(
                target_obj, h5py.Dataset
            ):
                # If it's a group or a dataset, create a hard link
                hdf_file[dest_path] = hdf_file[src_path]
                logger.debug(
                    f"Hard link created from group/dataset {src_path} to {dest_path}"
                )
            else:
                # Raise an error if the input_location is neither a group nor a dataset
                logger.error(
                    f"{src_path} is not a valid HDF5 group or dataset"
                )
                raise InvalidHDF5ObjectError(
                    f"{src_path} is not a valid group or dataset."
                )

    @staticmethod
    def recursive_load(group: h5py.Group) -> Dict[str, Any]:
        """
        Recursively loads data from an HDF5 group into a dictionary.

        Args:
            group: The HDF5 group to load data from.

        Returns:
            Dict[str, Any]: A dictionary containing the data from the HDF5 group.

        Raises:
            NoExistingDatasetError: If an unknown item is encountered in the HDF5 file.
        """

        def process_dataset(dataset: h5py.Dataset) -> Any:
            """Process and decode a dataset."""
            data = dataset[:] if dataset.shape else dataset[()]
            if isinstance(data, np.ndarray):
                return process_ndarray(data)
            if isinstance(data, bytes):
                return HDF5Manager.decode_none(data.decode("utf-8"))
            return data

        def process_ndarray(data: np.ndarray) -> Any:
            """Decode a NumPy array, replacing NaN values with None."""
            if data.dtype.type is np.object_:
                return [
                    (
                        json.loads(d.decode("utf-8"))
                        if isinstance(d, bytes) and "{" in d.decode("utf-8")
                        else (
                            json.loads(d)
                            if isinstance(d, str) and "{" in d
                            else d
                        )
                    )
                    for d in data
                ]

            # Convert to a list, replacing np.nan with None
            return [
                None if isinstance(item, float) and np.isnan(item) else item
                for item in data.tolist()
            ]

        def process_group(item: h5py.Group) -> Dict[str, Any]:
            """Recursively load a group and its attributes."""
            subgroup_data = HDF5Manager.recursive_load(item)
            for attr_name, attr_value in item.attrs.items():
                subgroup_data[attr_name] = (
                    attr_value if attr_value != "None" else None
                )
            return subgroup_data

        data_dict = {}
        for key, item in group.items():
            if isinstance(item, h5py.Dataset):
                data_dict[key] = process_dataset(item)
            elif isinstance(item, h5py.Group):
                data_dict[key] = process_group(item)
            else:
                raise NoExistingDatasetError(f"Unknown item in HDF5: {item}")
        return data_dict

    @staticmethod
    @debug_log
    def create_dataset(group: h5py.Group, key: str, value: Any):
        def _save_value(target_group: h5py.Group, key: str, value: str):
            """
            Helper function to save a value in the HDF5 group.
            """
            if key in target_group:
                return

            if isinstance(value, np.ndarray):
                # Save numpy arrays directly as datasets
                target_group.create_dataset(key, data=value, fletcher32=True)
            elif isinstance(value, (list, tuple)):
                # Convert lists and tuples to numpy arrays
                numpy_array = Utils.convert_to_numpy(value, np.nan)
                if len(numpy_array) == 0:
                    target_group.create_dataset(key, shape=(0,))
                else:
                    target_group.create_dataset(key, data=numpy_array)
            elif value is None:
                target_group.create_dataset(
                    key, data=HDF5Manager.encode_none()
                )
            else:
                # Store other types as datasets
                target_group.create_dataset(key, data=value)

        _save_value(group, key, value)

    @spec_coverage("PLATO-IAS-PDC-TN-0005", "REQ-NF-HDF5-10")
    @staticmethod
    @debug_log
    def recursive_save(group: h5py.Group, data: Dict, is_linked: bool = True):
        """
        Recursively saves data to the HDF5 file, organizing it within groups and datasets.

        Args:
            group: The HDF5 group where the data will be stored.
            data (Dict): The data to store in the group.
        """

        def _save_value(target_group: h5py.Group, key: str, value: str):
            """
            Helper function to save a value in the HDF5 group.
            """
            if key in target_group:
                return

            if isinstance(value, np.ndarray):
                # Save numpy arrays directly as datasets
                target_group.create_dataset(key, data=value, fletcher32=True)
            elif isinstance(value, dict):
                # Recursively handle dictionaries by creating subgroups
                sub_group = target_group.create_group(key)
                HDF5Manager.recursive_save(sub_group, value)
            elif isinstance(value, (list, tuple)):
                # Convert lists and tuples to numpy arrays
                numpy_array = Utils.convert_to_numpy(value, np.nan)
                if len(numpy_array) == 0:
                    target_group.create_dataset(key, shape=(0,))
                else:
                    target_group.create_dataset(key, data=numpy_array)
            elif value is None:
                target_group.create_dataset(
                    key, data=HDF5Manager.encode_none()
                )
            else:
                # Store other types as datasets
                target_group.create_dataset(key, data=value)

        # Main recursive save loop
        for key, value in data.items():
            _save_value(group, key, value)

    @retry
    def is_data_stored_in(
        self, key: str, group_name: Optional[str] = None
    ) -> bool:
        """
        Checks if a specific key is stored within a group in the HDF5 file.

        Args:
            key (str): The key to search for.
            group_name (Optional[str]): The name of the group to search in. Defaults to None.

        Returns:
            bool: True if the key exists, False otherwise.
        """
        if not os.path.exists(self.filename):
            return False

        with h5py.File(self.filename, "r") as hdf5_file:
            is_stored: bool = False
            if group_name and group_name in hdf5_file:
                is_stored = key in hdf5_file[group_name]
            else:
                is_stored = key in hdf5_file

            logger.debug(f"{key} in {group_name}: {is_stored}")
            return is_stored

    @retry
    def load_data(self, target_node: Optional[str] = None) -> Dict:
        """
        Loads data from the HDF5 file, either fully or from a specific target node.

        Args:
            target_node (Optional[str]): The node to search for. Defaults to None.

        Returns:
            Dict: The loaded data.

        Raises:
            NoExistingGroupError: If the target node is not found or if a type error occurs during loading.
            FileNotFoundError: Could not open the file.
            RuntimeError: An error occurred
        """
        if target_node is None:
            with h5py.File(self.filename, "r") as f:
                return HDF5Manager.recursive_load(f)

        try:
            with h5py.File(self.filename, "r") as f:
                group: h5py.Group = f[target_node]
                result = HDF5Manager.recursive_load(group)
                return result
        except KeyError:
            # Handle case where the path does not exist in the HDF5 file
            raise NoExistingGroupError(
                f"Group '{target_node}' not found in the file '{self.filename}'"
            )
        except BlockingIOError as e:
            raise BlockingIOError(e)
        except OSError:
            # Handle cases where the file cannot be opened (e.g., file does not exist)
            raise FileNotFoundError(
                f"Could not open the file '{self.filename}'"
            )
        except Exception as e:
            # Handle any other exceptions that may occur
            raise RuntimeError(
                f"An error occurred while accessing '{target_node}' in '{self.filename}': {e}"
            )

    @retry
    def save_data(self, node: str, data: Any):
        """
        Saves data to a specific node in the HDF5 file.

        Args:
            node (str): The target node to store the data in.
            data (Any): The data to store.

        Raises:
            StoringInformationError: If an error occurs while saving the data.
        """
        logger.debug(f"Storing {node} {type(data)} in {self.filename}")
        if Utils.is_custom_object(data):
            data_dict = data.to_dict()
            data_dict = Utils.normalize_types(data_dict)
        else:
            data_dict = data

        with h5py.File(self.filename, "a") as f:
            try:
                if isinstance(data, BaseRootModel):
                    node_to_save = "/".join(node.rsplit("/", 1)[:-1])
                    dataset = node.rsplit("/", 1)[-1]
                    data_group = f.require_group(node_to_save)
                    HDF5Manager.create_dataset(data_group, dataset, data_dict)
                else:
                    node_to_save = node
                    data_group = f.require_group(node_to_save)
                    HDF5Manager.recursive_save(
                        data_group, data_dict, is_linked=False
                    )
                logger.debug(f"Stored {node} {type(data)} in HDF5.")
            except BlockingIOError as e:
                raise BlockingIOError(e)
            except Exception as e:
                logger.error(
                    f"Failed to store external {type(data)} in HDF5: {e}"
                )
                raise StoringInformationError(
                    f"Failed to store external {type(data)} in HDF5: {e}"
                )

    @retry
    def save_metadata(self, node: str, metadata: Dict[str, Any]):
        """
        Save metadata in the attributs within a HDF5 group.

        Args:
            node (str): Chemin vers le groupe HDF5.
            metadata (Dict[str, Any]): Dictionnaire contenant les métadonnées à enregistrer.
        """
        with h5py.File(self.filename, "a") as hdf5_file:
            # Check if the group exists
            if node in hdf5_file:
                group = hdf5_file[node]
            else:
                group = hdf5_file.create_group(node)

            # save or update attributes
            for key, value in metadata.items():
                group.attrs[key] = value

            logger.info(f"Metadata saved in the group {node}: {metadata}")

    @retry
    def load_metadata(self, node: str) -> Dict[str, Any]:
        """Load metadata from a specific group

        Args:
            node (str): node where the metadata must be loaded

        Raises:
            NoExistingGroupError: No existing group

        Returns:
            Dict[str, Any]: metadata
        """
        with h5py.File(self.filename, "r") as hdf5_file:
            if node in hdf5_file:
                group = hdf5_file[node]
                return dict(group.attrs)
            else:
                raise NoExistingGroupError(
                    f"Cannot find goup {node} in {self.filename}"
                )


@spec_coverage(
    "PLATO-IAS-PDC-TN-0005",
    "REQ-IF-HDF5-20",
    "REQ-IF-HDF5-30",
    "REQ-IF-HDF5-40",
    "REQ-IF-HDF5-50",
    "REQ-IF-HDF5-60",
    "REQ-IF-HDF5-70",
    "REQ-IF-HDF5-80",
)
class HDF5StorageSasWorkflow(IStorageSasWorkflow):
    """
    Class to handle the workflow for storing and retrieving data using the HDF5 format in a SAS workflow.
    """

    def __init__(self, filename: str, *args, **kwargs):
        """
        Initialize the HDF5StorageSasWorkflow with the given filename.

        Args:
            filename (str): The filename of the HDF5 file.
        """
        self.__module_name: Optional[str] = None
        self.__sas_hdf5 = HDF5Manager(filename)
        if kwargs:
            metadata = DataClassFactory.get_model("METADATA")(
                **kwargs
            ).to_dict()
            self.__sas_hdf5.save_metadata("METADATA", metadata)

    @property
    def filename(self) -> str:
        """Returns the filename of the HDF5 file."""
        return self.sas_hdf5.filename

    @property
    def sas_hdf5(self) -> HDF5Manager:
        """Returns the HDF5Manager instance managing the file."""
        return self.__sas_hdf5

    @property
    def module_name(self) -> Optional[str]:
        """Returns the current module name."""
        return self.__module_name

    @module_name.setter
    def module_name(self, name: str):
        """Sets the current module name."""
        self.__module_name = name

    def save_product(self, data: Union[T, Dict]):
        """
        Save a product in the HDF5 file.

        Args:
            data (Union[T, Dict]): The product data to save.

        Raises:
            ProductSaveError: If the product cannot be saved.
        """
        if Utils.is_custom_object(data):
            self._save_product_as_obj(data)
        elif isinstance(data, Dict):
            key = list(data.keys())[0]
            self._save_product_natif(key, data[key])
        else:
            raise ProductSaveError(data)

    def load_product(
        self, product_name: str
    ) -> Union[BaseDataClass, BaseRootModel]:
        """
        Load a product from the HDF5 file.

        Args:
            product_name (str): The name of the product to load.

        Returns:
            Union[BaseDataClass, BaseRootModel]: The loaded product.
        """
        path: str
        if self.sas_hdf5.is_data_stored_in(
            product_name, Hdf5NodeEnum.PRODUCTS.value
        ):
            path = Hdf5NodeEnum.PRODUCTS.value + "/" + product_name
        elif self.sas_hdf5.is_data_stored_in(
            product_name, Hdf5NodeEnum.EXTERNALS.value
        ):
            path = Hdf5NodeEnum.EXTERNALS.value + "/" + product_name
        else:
            path = Hdf5Tree.find_key_path(product_name)
        data: Dict = self.sas_hdf5.load_data(path)
        return DataClassFactory.create_instance(product_name, data)

    def is_validate_dm(self, **kwargs) -> bool:
        """
        Validate the data model in the HDF5 file by checking against the dataclass structure.

        Args:
            **kwargs: Optional arguments for specifying the module to validate.

        Returns:
            bool: True if the data model is valid, False otherwise.
        """
        if "module" in kwargs:
            module_name = kwargs["module"].upper()
            target_node = Hdf5Tree.find_key_path(module_name)
            cls_name = DataClassFactory.get_model(module_name)
        else:
            target_node = None
            cls_name = DataClassFactory.get_model("SasPipelineHdf5")

        data = self.sas_hdf5.load_data(target_node)

        try:
            cls_name.from_dict(
                data
            )  # Validate against the dataclass structure
            logger.info(
                f"Data model validation successful for module '{target_node}'."
            )
            return True
        except Exception as e:
            logger.error(
                f"Data model validation failed for module '{target_node}': {e}"
            )
            return False

    @debug_log
    def is_stored(self, product_name: str) -> bool:
        """
        Check if a product is already stored in the HDF5 file.

        Args:
            product_name (str): The name of the product to check.

        Returns:
            bool: True if the product is stored, False otherwise.
        """
        # Check in both the PRODUCTS and EXTERNALS node in the HDF5 file.
        if self.sas_hdf5.is_data_stored_in(
            product_name, Hdf5NodeEnum.PRODUCTS.value
        ):
            return True

        if self.sas_hdf5.is_data_stored_in(
            product_name, Hdf5NodeEnum.EXTERNALS.value
        ):
            return True

        return False

    @debug_log
    def is_stored_in(self, node: str, product_name: str) -> bool:
        return self.sas_hdf5.is_data_stored_in(product_name, node)

    @debug_log
    def save_external(self, data: Union[T, Dict]):
        if Utils.is_custom_object(data):
            self._save_external_as_obj(data)
        elif isinstance(data, Dict):
            key = list(data.keys())[0]
            self._save_external_natif(key, data[key])
        else:
            raise ProductSaveError(data)

    @debug_log
    def load_inputs(
        self, module_name: str
    ) -> Union[BaseDataClass, BaseRootModel]:
        """
        Load input products for a specific module.

        Args:
            module_name (str): The name of the module.

        Returns:
            Union[BaseDataClass, BaseRootModel]: The loaded input products.
        """
        cls_inputs = f"inputs_{module_name}"
        cls_inputs = cls_inputs.lower()
        path = Hdf5Tree.find_key_path(cls_inputs)
        data = self.sas_hdf5.load_data(path)
        return DataClassFactory.create_instance(cls_inputs, data)

    @debug_log
    def load_outputs(
        self, module_name: str
    ) -> Union[BaseDataClass, BaseRootModel]:
        """
        Load output products for a specific module.

        Args:
            module_name (str): The name of the module.

        Returns:
            Union[BaseDataClass, BaseRootModel]: The loaded output products.
        """
        cls_outputs = f"outputs_{module_name}"
        cls_outputs = cls_outputs.lower()
        path = Hdf5Tree.find_key_path(cls_outputs)
        data = self.sas_hdf5.load_data(path)
        return DataClassFactory.create_instance(cls_outputs, data)

    @debug_log
    def save_inputs(self, module_name: str, products: T):
        """
        Save input products for a specific module.

        Args:
            module_name (str): The name of the module.
            products (T): The input products to save.
        """
        path_module = Hdf5Tree.find_key_path(module_name)
        self.sas_hdf5.save_data(path_module, products)

    @debug_log
    def save_outputs(self, module_name: str, products: T):
        """
        Save output products for a specific module and create links in the HDF5 file.

        Args:
            module_name (str): The name of the module.
            products (T): The output products to save.
        """
        try:

            # Save the output products to HDF5
            self.sas_hdf5.save_data(Hdf5NodeEnum.PRODUCTS.value, products)

            # Discover the path for the module in HDF5
            output_path = f"outputs_{module_name.lower()}"

            # Get the names of the products
            products_names: List[str] = [
                field.alias or name
                for name, field in products.model_fields.items()
            ]  # products.list_fields()

            # Create links for each product in the HDF5 file
            for product_name in products_names:
                src_path = Hdf5NodeEnum.PRODUCTS.value + "/" + product_name
                dest_path = Hdf5Tree.find_key_path(product_name, output_path)
                self.sas_hdf5.create_link_to_data(src_path, dest_path)

            # Log a confirmation message after successful save and linking
            logger.info(
                f"Successfully saved outputs for module '{module_name}' with products: {products_names}."
            )
        except DataBlockingIOError as e:
            raise DataBlockingIOError(e)

        except Exception as e:
            # Log an error message if the save operation fails
            logger.error(
                f"Failed to save outputs for module '{module_name}': {e}"
            )
            raise StoringInformationError(
                f"Failed to save outputs for module '{module_name}': {e}"
            )

    @debug_log
    def create_link(
        self, src_path: str, dest_path: str, is_required: bool = True
    ):
        """
        Create a link to data from one path to another in the HDF5 file.

        Args:
            src_path (str): Source path for the data.
            dest_path (str): Destination path for the data link.
            is_required (bool): Whether the link is mandatory. Default is True.
        """
        self.sas_hdf5.create_link_to_data(src_path, dest_path, is_required)

    @debug_log
    def _save_product_as_obj(self, data: Any):
        """
        Save a product in the HDF5 file as a custom object.

        Args:
            data (Any): The product to save.

        Raises:
            ProductClassError: If the product class cannot be found.
            ProductTypeError: If the product type is neither internal nor external.
        """
        name = Hdf5Tree.get_key_from(data)
        if name is None:
            raise ProductClassError(data)

        src_path: str
        if Utils.is_internal_product(name):
            src_path = f"{Hdf5NodeEnum.PRODUCTS.value}/{name.upper()}"
        else:
            raise ProductTypeError(name)

        self.sas_hdf5.save_data(src_path, data)

        if self.module_name is not None:
            dest_path = Hdf5Tree.find_key_path(name, self.module_name)
            self.sas_hdf5.create_link_to_data(src_path, dest_path)

    @debug_log
    def _save_product_natif(
        self, key: str, data: Union[BaseDataClass, BaseRootModel]
    ):
        """
        Save a product in its native format (dictionary-style).

        Args:
            key (str): Key to identify the product.
            data (Union[BaseDataClass, BaseRootModel]): The product to save.

        Raises:
            InvalidProductError: If the product is invalid.
        """
        if self.module_name is None:
            if Utils.is_internal_product(key):
                self.sas_hdf5.save_data(Hdf5NodeEnum.PRODUCTS.value, data)
            else:
                raise InvalidProductError(str(data))
        else:
            logger.warning("Please set the module_name to False to save data")

    @debug_log
    def _save_external_as_obj(self, data: Any):
        name = Hdf5Tree.get_key_from(data)
        if name is None:
            raise ProductClassError(data)

        src_path: str = f"{Hdf5NodeEnum.EXTERNALS.value}/{name.upper()}"
        self.sas_hdf5.save_data(src_path, data)

        if self.module_name is not None:
            dest_path = Hdf5Tree.find_key_path(name, self.module_name)
            self.sas_hdf5.create_link_to_data(src_path, dest_path)

    @debug_log
    def _save_external_natif(self, key: str, data: T):
        if self.module_name is None:
            self.sas_hdf5.save_data(Hdf5NodeEnum.EXTERNALS.value, {key: data})
        else:
            logger.warning("Please set the module_name to False to save data")


@spec_coverage(
    "PLATO-IAS-PDC-ICD-0001",
    "SAS-REQ-IF-MODULE-STO-10",
    "SAS-REQ-IF-MODULE-STO-20",
    "SAS-REQ-IF-MODULE-STO-30",
)
@spec_coverage(
    "PLATO-IAS-PDC-TN-0005",
    "REQ-IF-HDF5-90",
    "REQ-IF-HDF5-100",
    "REQ-IF-HDF5-110",
)
class HDF5StorageSasTask(IStorageSasTask):
    """
    HDF5StorageSasTask handles input/output data management for a module using HDF5 files.

    This class implements the `IStorageSasTask` interface and manages input/output data
    by storing and loading it from HDF5 files. It uses `HDF5Manager` instances for reading
    and writing the data and a `BuilderHandler` to process the data before saving or after loading.
    """

    def __init__(self, dir_path: str, module_name: str):
        """_
        Initializes the task with a directory path and module name.
        Creates `HDF5Manager` instances for inputs and outputs, and initializes
        a builder handler to process data.

        Args:
            dir_path (str): directory path where both inputs and outputs are located
            module_name (str): module name
        """
        self.__dir_path = dir_path
        input_dir = os.path.join(dir_path, "input")
        output_dir = os.path.join(dir_path, "output")
        if not os.path.exists(input_dir):
            os.makedirs(input_dir)
        if not os.path.exists(output_dir):
            os.makedirs(output_dir)
        self.__inputs = HDF5Manager(os.path.join(input_dir, "inputs.h5"))
        self.__outputs = HDF5Manager(os.path.join(output_dir, "outputs.h5"))
        self.__module_name = module_name

    @property
    def module_name(self) -> str:
        """
        Returns the module name associated with this task.

        Returns:
            str: the module name
        """
        return self.__module_name

    @property
    def dir_path(self) -> str:
        """
        Returns the directory path where the input and output HDF5 files are located.

        Returns:
            str: the directory path
        """
        return self.__dir_path

    @property
    def inputs(self) -> HDF5Manager:
        """
        Returns the HDF5Manager instance for input data management.

        Returns:
            HDF5Manager: The HDF5Manager instance for input data management
        """
        return self.__inputs

    @property
    def outputs(self) -> HDF5Manager:
        """
        Returns the HDF5Manager instance for output data management.

        Returns:
            HDF5Manager: The HDF5Manager instance for output data management
        """
        return self.__outputs

    @spec_coverage("PLATO-IAS-PDC-ICD-0001", "SAS-REQ-IF-MODULE-STO-40")
    @spec_coverage("PLATO-IAS-PDC-TN-0005", "REQ-IF-HDF5-90")
    @debug_log
    def save_product(self, data: T):
        """
        Saves a single product to the output HDF5 file.

        Args:
            data (T): The product to be saved.
        """
        self.outputs.save_data("/", data)

    @spec_coverage("PLATO-IAS-PDC-ICD-0001", "SAS-REQ-IF-MODULE-STO-40")
    @spec_coverage("PLATO-IAS-PDC-TN-0005", "REQ-IF-HDF5-90")
    @debug_log
    def load_product(
        self, product_name: str
    ) -> Union[BaseDataClass, BaseRootModel]:
        if self.inputs.is_data_stored_in(product_name, "/"):
            data = self.inputs.load_data(product_name)
        elif self.outputs.is_data_stored_in(product_name, "/"):
            data = self.outputs.load_data(product_name)
        else:
            raise ProductNotFound(product_name)
        data = DataClassFactory.create_instance(product_name, data)
        return data

    @debug_log
    def is_validate_dm(self, **kwargs) -> bool:
        """Check if the validity of the data model in the HDF5

        Returns:
            bool: True when the data model is valid otherwise False
        """
        try:
            self.load_inputs()  # Attempt to load inputs for validation
            is_valid = True
            logger.info(
                f"Data model validation successful for module '{self.module_name}'."
            )
        except Exception as e:
            is_valid = False
            logger.error(
                f"Data model validation failed for module '{self.module_name}': {e}"
            )

        return is_valid

    @debug_log
    def is_stored(self, product_name: str) -> bool:
        """
        Check if a product is stored in either the input or output storage.

        This method takes a product name as input and checks whether the product is
        stored in either the inputs or outputs. It returns True if the product is found
        in either location, otherwise returns False.

        Args:
            product_name (str): The name of the product to check for storage.

        Returns:
            bool: True if the product is stored in either inputs or outputs, False otherwise.

        Logic:
            - First, it checks if the product is present in the input storage by calling the `is_data_stored_in` method with the product name and root path ("/").
            - If found in inputs, returns True.
            - If not, it checks the output storage similarly.
            - If found in outputs, returns True.
            - If not found in either, returns False.

        """
        if self.inputs.is_data_stored_in(product_name, "/"):
            return True

        if self.outputs.is_data_stored_in(product_name, "/"):
            return True

        return False

    @debug_log
    def is_stored_in(self, node: str, product_name: str) -> bool:
        result: bool
        if self.inputs.is_data_stored_in(product_name, node):
            result = True
        elif self.outputs.is_data_stored_in(product_name, node):
            result = True
        else:
            result = False
        return result

    @debug_log
    def load_inputs(self) -> Union[BaseDataClass, BaseRootModel]:
        """
        Loads the input data from the HDF5 file.

        Returns:
            Union[BaseDataClass, BaseRootModel]: The input data
        """
        data = self.inputs.load_data()
        cls_inputs = f"inputs_{self.module_name.lower()}"
        return DataClassFactory.create_instance(cls_inputs, data)

    @debug_log
    def load_outputs(self) -> Union[BaseDataClass, BaseRootModel]:
        """
        Loads the output data from the HDF5 file.

        Returns:
            Union[BaseDataClass, BaseRootModel]: The output data
        """
        data = self.outputs.load_data()
        cls_outputs = f"outputs_{self.module_name.lower()}"
        return DataClassFactory.create_instance(cls_outputs, data)

    @debug_log
    def save_inputs(self, products: T):
        """
        Saves the input data to the HDF5 file.

        Args:
            products (T): inputs to save
        """
        self.inputs.save_data("/", products)

    @debug_log
    def save_outputs(
        self, products: Union[Union[BaseDataClass, BaseRootModel], Dict]
    ):
        """
        Saves the output data to the HDF5 file.

        Args:
            products (Union[Union[BaseDataClass, BaseRootModel], Dict]): processing outputs to save
        """
        try:
            # Convert products to dictionary format
            if isinstance(products, BaseDataClass) or isinstance(
                products, BaseRootModel
            ):
                data_dico = products.to_dict()
            else:
                data_dico = products

            cls_outputs = f"outputs_{self.module_name.lower()}"

            # Create an instance of the output data class
            data = DataClassFactory.create_instance(cls_outputs, data_dico)

            # Save the output data to the HDF5 file
            self.outputs.save_data("/", data)

            # Log a confirmation message after successful save
            logger.info(
                f"Successfully saved outputs for module '{self.module_name}' with data: {data}."
            )
        except DataBlockingIOError as e:
            raise DataBlockingIOError(e)
        except Exception as e:
            # Log an error message if the save operation fails
            logger.error(
                f"Failed to save outputs for module '{self.module_name}': {e}"
            )
            raise StoringInformationError(
                f"Failed to save outputs for module '{self.module_name}': {e}"
            )
