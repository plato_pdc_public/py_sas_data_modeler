# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
"""
The module 'io' package a set of interfaces and classes designed for managing data storage in a workflow context. It includes the following components:

- **Interfaces**:
  - `IStorage`: The main interface for defining storage operations.
  - `IStorageSasWorkflow`: An interface specific to the storage of SAS workflows.
  - `IStorageSasTask`: An interface for storage operations related to individual tasks within a SAS workflow.

- **File Mediator**:
  - `FileMediator`: A utility class that facilitates the transfer of inputs and outputs between storage systems and task workflows.

- **HDF5 Storage Classes**:
  - `HDF5StorageSasWorkflow`: A class that implements the storage of SAS workflows in an HDF5 format, allowing for structured data management.
  - `HDF5StorageSasTask`: A class that provides functionality for storing individual task data in HDF5 format.

"""
from .hdf5 import HDF5StorageSasTask
from .hdf5 import HDF5StorageSasWorkflow
from .interface import FileMediator
from .interface import IStorage
from .interface import IStorageSasTask
from .interface import IStorageSasWorkflow


__all__ = [
    "IStorage",
    "IStorageSasWorkflow",
    "IStorageSasTask",
    "FileMediator",
    "HDF5StorageSasWorkflow",
    "HDF5StorageSasTask",
]
