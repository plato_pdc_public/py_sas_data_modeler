# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
"""
The 'interface" module defines abstract storage interfaces and implements key functionality for handling
input/output operations between different storage systems in a workflow. It includes methods to save,
load, validate, and link data across modules and tasks using abstract base classes.

Additionally, it provides utilities for discovering and managing HDF5 paths in a structured data model.
"""
from abc import ABC
from abc import abstractmethod
from typing import TypeVar
from typing import Union

from loguru import logger

from ..dm import BaseDataClass
from ..dm import BaseRootModel
from ..exception import DataBlockingIOError
from ..exception import DataTransferError
from ..helper import spec_coverage

T = TypeVar("T", bound=Union["BaseDataClass", "BaseRootModel"])


class IStorage(ABC):
    """
    Interface for storage operations in a data management system.

    This abstract base class defines the essential methods required for
    managing the storage, retrieval, and validation of products. It serves
    as a contract for any concrete implementation of storage systems.
    """

    @abstractmethod
    def save_product(self, data: Union[BaseDataClass, BaseRootModel]):
        """
        Save a product into storage.

        Args:
            data (Union[BaseDataClass, BaseRootModel]): The product data to be saved.

        Raises:
            NotImplementedError: If not implemented in the subclass.
        """
        pass

    @abstractmethod
    def load_product(self, key: str) -> Union[BaseDataClass, BaseRootModel]:
        """
        Load a product from storage.

        Args:
            key (str): The unique key or identifier of the product.

        Returns:
            Union[BaseDataClass, BaseRootModel]: The loaded product data.

        Raises:
            NotImplementedError: If not implemented in the subclass.
        """
        pass

    @abstractmethod
    def is_validate_dm(self, **kwargs) -> bool:
        """
        Validate the data model.

        Args:
            **kwargs: Additional parameters for validation.

        Returns:
            bool: True if the data model is valid, False otherwise.

        Raises:
            NotImplementedError: If not implemented in the subclass.
        """
        pass

    @abstractmethod
    def is_stored(self, product_name: str) -> bool:
        """
        Check if a product exists in storage.

        Args:
            product_name (str): The name of the product to check.

        Returns:
            bool: True if the product exists in storage, False otherwise.

        Raises:
            NotImplementedError: If not implemented in the subclass.
        """
        pass

    @abstractmethod
    def is_stored_in(self, node: str, product_name: str) -> bool:
        """
        Check if a product is stored in a specific node.

        Args:
            node (str): The node or path in storage.
            product_name (str): The name of the product to check.

        Returns:
            bool: True if the product exists in the specified node, False otherwise.

        Raises:
            NotImplementedError: If not implemented in the subclass.
        """
        pass


class IStorageSasWorkflow(IStorage):
    """
    Interface for SAS Workflow storage operations.

    This abstract class extends the IStorage interface to include additional
    methods specific to saving, loading, and linking input/output data in a
    SAS workflow context.
    """

    @abstractmethod
    def save_external(self, data: T):
        """
        Save external data into the workflow storage.

        Args:
            data (T): The external data to be saved.

        Raises:
            NotImplementedError: If not implemented in the subclass.
        """
        pass

    @abstractmethod
    def load_inputs(
        self, module_name: str
    ) -> Union[BaseDataClass, BaseRootModel]:
        """
        Load input products for a specific module.

        Args:
            module_name (str): The name of the module.

        Returns:
            Union[BaseDataClass, BaseRootModel]: The input products for the specified module.

        Raises:
            NotImplementedError: If not implemented in the subclass.
        """
        pass

    @abstractmethod
    def load_outputs(
        self, module_name: str
    ) -> Union[BaseDataClass, BaseRootModel]:
        """
        Load output products for a specific module.

        Args:
            module_name (str): The name of the module.

        Returns:
            Union[BaseDataClass, BaseRootModel]: The output products for the specified module.

        Raises:
            NotImplementedError: If not implemented in the subclass.
        """
        pass

    @abstractmethod
    def save_inputs(self, module_name: str, products: T):
        """
        Save input products for a specific module.

        Args:
            module_name (str): The name of the module.
            products (T): The input products to save.

        Raises:
            NotImplementedError: If not implemented in the subclass.
        """
        pass

    @abstractmethod
    def save_outputs(self, module_name: str, products: T):
        """
        Save output products for a specific module.

        Args:
            module_name (str): The name of the module.
            products (T): The output products to save.

        Raises:
            NotImplementedError: If not implemented in the subclass.
        """
        pass

    @abstractmethod
    def create_link(
        self, src_path: str, dest_path: str, is_required: bool = True
    ):
        """
        Create a link between a source and destination path.

        Args:
            src_path (str): The source path for the link.
            dest_path (str): The destination path for the link.
            is_required (bool, optional): Indicates if the link is mandatory. Defaults to True.

        Raises:
            NotImplementedError: If not implemented in the subclass.
        """
        pass


class IStorageSasTask(IStorage):
    """
    Interface for SAS Task storage operations.

    This abstract class extends the IStorage interface to define methods
    for saving and loading task-specific input and output products.
    """

    @abstractmethod
    def load_inputs(self) -> Union[BaseDataClass, BaseRootModel]:
        """
        Load input products for the task.

        Returns:
            Union[BaseDataClass, BaseRootModel]: The input products for the task.

        Raises:
            NotImplementedError: If not implemented in the subclass.
        """
        pass

    @abstractmethod
    def load_outputs(self) -> Union[BaseDataClass, BaseRootModel]:
        """
        Load output products for the task.

        Returns:
            Union[BaseDataClass, BaseRootModel]: The output products for the task.

        Raises:
            NotImplementedError: If not implemented in the subclass.
        """
        pass

    @abstractmethod
    def save_inputs(self, products: Union[BaseDataClass, BaseRootModel]):
        """
        Save input products for the task.

        Args:
            products (Union[BaseDataClass, BaseRootModel]): The input products to save.

        Raises:
            NotImplementedError: If not implemented in the subclass.
        """
        pass

    @abstractmethod
    def save_outputs(self, products: Union[BaseDataClass, BaseRootModel]):
        """
        Save output products for the task.

        Args:
            products (Union[BaseDataClass, BaseRootModel]): The output products to save.

        Raises:
            NotImplementedError: If not implemented in the subclass.
        """
        pass


@spec_coverage("PLATO-IAS-PDC-TN-0005", "REQ-IF-HDF5-120")
class FileMediator:
    """Facilitates the transfer of input and output data between workflow-level and task-level
    storage, ensuring smooth data flow between SAS storage and task-specific storage.
    """

    def __init__(
        self, sas_storage: IStorageSasWorkflow, task_storage: IStorageSasTask
    ):
        """
        Initializes the FileMediator with workflow and task storage.

        Args:
            sas_storage (IStorageSasWorkflow): The workflow-level SAS storage instance.
            task_storage (IStorageSasTask): The task-specific storage instance.
        """
        self.sas_storage = sas_storage
        self.task_storage = task_storage

    def transfer_inputs_to_task(self, module_name: str):
        """
        Transfers input data from SAS storage to task storage for the given module.

        Args:
            module_name (str): The name of the module for which to transfer inputs.

        Raises:
            DataTransferError: If an error occurs during the input transfer process.
            DataBlockingIOError: If an error occurs wih I/O
        """
        try:
            # Load inputs from SAS storage
            data = self.sas_storage.load_inputs(module_name)
            # Save inputs to task storage
            self.task_storage.save_inputs(data)
            # Log a confirmation message
            logger.info(
                f"Successfully transferred inputs to task '{module_name}'."
            )
        except DataBlockingIOError as e:
            raise DataBlockingIOError(e)
        except Exception as e:
            # Log an error message if the transfer fails
            logger.error(
                f"Failed to transfer inputs to task '{module_name}': {e}"
            )
            raise DataTransferError(
                f"Failed to transfer inputs to task '{module_name}': {e}"
            )

    def retrieve_outputs_from_task(self, module_name: str):
        """
        Retrieves output data from task storage and saves it to SAS storage for the given module.

        Args:
            module_name (str): The name of the module for which to retrieve outputs.

        Raises:
            DataTransferError: If an error occurs during the output retrieval process.
            DataBlockingIOError: If an error occurs wih I/O
        """
        try:
            # Load outputs from task storage
            data = self.task_storage.load_outputs()
            self.sas_storage.save_outputs(module_name, data)
            logger.info(
                f"Successfully retrieved outputs from task '{module_name}'."
            )
        except DataBlockingIOError as e:
            raise DataBlockingIOError(e)
        except Exception as e:
            # Log an error message if the retrieval fails
            logger.error(
                f"Failed to retrieve outputs from task '{module_name}': {e}"
            )
            raise DataTransferError(
                f"Failed to retrieve outputs from task '{module_name}': {e}"
            )
