# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
"""
The 'workflow' module provides classes for parsing and processing tasks and products within the SAS framework.

Classes:
--------
1. ProductParser:
   Responsible for parsing input and output products from type hints. It extracts the necessary type information and creates `Product` instances representing the parsed products.

2. TaskParser:
   Responsible for parsing tasks based on their class implementations. It creates `InputSet` instances from input hints and gathers output products, ensuring that the tasks are properly structured.

3. PipelineParser:
   Responsible for parsing a complete pipeline composed of multiple task combinations. It handles the extraction of tasks from the class implementations and organizes them into `TaskCombination` instances.

4. SasModule:
   Represents a module within the SAS framework. It initializes with a specific storage instance and module name, and it parses the associated task.

5. InputSelectionStrategy:
    Strategy to sort the sets of inputs. The order is used to define the priority to find the dataclass related to a given set of input
"""
from typing import Any
from typing import Dict
from typing import get_args
from typing import get_origin
from typing import get_type_hints
from typing import List
from typing import Optional
from typing import Type
from typing import Union

from loguru import logger

from .dm import Hdf5Tree
from .dm.io_pipeline import Hdf5NodeEnum
from .dm.io_pipeline import InputSet
from .dm.io_pipeline import Pipeline
from .dm.io_pipeline import Product
from .dm.io_pipeline import Task
from .dm.io_pipeline import TaskCombination
from .exception import InputNotStoredError
from .exception import InvalidOutputTypeError
from .exception import InvalidProductDataDictError
from .exception import ModuleNotFoundError
from .exception import ModuleTaskError
from .exception import ScientificModuleNotFound
from .exception import StoringInformationError
from .helper import debug_log
from .helper import Utils
from .io import IStorageSasWorkflow


class InputSelectionStrategy:
    """Stategy to sort the sets of inputs."""

    @staticmethod
    @debug_log
    def sort(inputs_collections: List[InputSet]) -> List[InputSet]:
        """Sort the collections according to the number of required inputs and the number of IDP_SAS in required inputs

        Args:
            inputs_collections (List[InputSet]): Sets of inputs to sort according the strategy

        Returns:
            List[InputSet]: the sets of inputs
        """
        return sorted(
            inputs_collections,
            key=lambda x: (len(x.mandatory_inputs), x.count_idp_sas),
            reverse=True,
        )


class ProductParser:
    """Responsible for parsing input and output products from type hints."""

    @staticmethod
    @debug_log
    def parse_products(
        product_hints: Union[Dict[str, Type], Type],
    ) -> List[Product]:
        """
        Parses products from type hints and returns a list of Product instances.

        Args:
            product_hints (Union[Dict[str, Type], Type]): Type hints for products.

        Returns:
            List[Product]: List of parsed Product instances.
        """
        # Resolve product hints into a consistent dictionary format
        if not isinstance(product_hints, Dict):
            product_hints = ProductParser._resolve_product_hints(product_hints)
            logger.info(f"Resolved product hints to: {product_hints}")

        products = []
        for product_name, product_cls in product_hints.items():
            product = ProductParser._parse_single_product(
                product_name, product_cls
            )
            products.append(product)
            logger.debug(
                "Parsed product: %s (required=%s, class=%s)",
                product_name,
                product.is_required,
                product.implementation_class,
            )
        return products

    @staticmethod
    def _resolve_product_hints(product_hints: Type) -> Dict[str, Type]:
        """
        Resolves product hints into a dictionary of field names and types.

        Args:
            product_hints (Type): The product hints type to resolve.

        Returns:
            Dict[str, Type]: A dictionary of field names and types.
        """
        if get_origin(product_hints) is None:
            return {}  # Return an empty dict if no fields are defined

        resolved_hints = {}
        for field_name, field in product_hints.model_fields.items():
            field_type = field.annotation
            resolved_hints[field.alias or field_name] = field_type
        return resolved_hints

    @staticmethod
    def _parse_single_product(product_name: str, product_cls: Type) -> Product:
        """
        Parses a single product definition.

        Args:
            product_name (str): Name of the product.
            product_cls (Type): The class type or type hint for the product.

        Returns:
            Product: Parsed Product instance.
        """
        origin = get_origin(product_cls)
        if origin is Union:
            cls_ = get_args(product_cls)[0]
            is_required = False
        elif origin is None:
            cls_ = product_cls
            is_required = True
        else:
            cls_ = get_args(product_cls)[0]
            is_required = True

        return Product(
            name=product_name,
            implementation_class=cls_,
            is_required=is_required,
        )


class TaskParser:
    """
    Responsible for parsing tasks from class implementations and extracting input/output products.
    """

    @debug_log
    def _create_input_set(self, inputs) -> InputSet:
        """
        Creates an InputSet from the given inputs.

        Args:
            inputs: Input hints to create the input set from.

        Returns:
            InputSet: The created input set.
        """
        input_products = ProductParser.parse_products(inputs)
        mandatory_inputs = [p for p in input_products if p.is_required]
        optional_inputs = [p for p in input_products if not p.is_required]
        input_set = InputSet(
            mandatory_inputs=mandatory_inputs, optional_inputs=optional_inputs
        )
        logger.debug("Created input set: %s", input_set)
        return input_set

    @debug_log
    def parse_task(self, task_name: str, task_cls: Type) -> Task:
        """
        Parses a single task based on its class implementation.

        Args:
            task_name (str): Name of the task.
            task_cls (Type): The task class to parse.

        Returns:
            Task: Parsed Task object.
        """
        task_hints = get_type_hints(task_cls)
        input_sets: List[InputSet] = list()

        logger.debug("Parsing task '%s' of class '%s'", task_name, task_cls)

        output_products = ProductParser.parse_products(
            self._extract_io(task_hints, "outputs", task_name)
        )
        inputs = self._extract_io(task_hints, "inputs", task_name)
        if isinstance(inputs, List):
            for input in inputs:
                input_sets.append(self._create_input_set(input))
        else:
            input_sets.append(self._create_input_set(inputs))

        return Task(
            name=task_name,
            input_sets=input_sets,
            mandatory_outputs=output_products,
        )

    @staticmethod
    @debug_log
    def _extract_io(
        task_hints: Dict[str, Type], io_type: str, task_name: str
    ) -> Union[Dict[str, Type], List[Dict[str, Type]]]:
        """
        Extracts input or output products for a task.

        Args:
            task_hints (Dict[str, Type]): Type hints for the task.
            io_type (str): Whether to extract inputs or outputs ('inputs' or 'outputs').
            task_name (str): The task name.

        Returns:
            Union[Dict[str, Type], List[Dict[str, Type]]]: Input/output product type hints.

        Raises:
            ModuleTaskError: Unsupported type hints.
        """
        io_key = f"{io_type}_{task_name.lower()}"
        io_type_hints = task_hints.get(io_key)

        # Handle single dictionary of types
        if io_type_hints is not None and get_origin(io_type_hints) is None:
            return TaskParser._resolve_model_fields(io_type_hints)

        if Utils.is_union(io_type_hints):
            return [
                TaskParser._resolve_model_fields(input_set)
                for input_set in get_args(io_type_hints)
            ]

        raise ModuleTaskError(
            f"Unsupported type hints for '{io_key}' in task '{task_name}'."
        )

    @staticmethod
    def _resolve_model_fields(model_type: Type) -> Dict[str, Type]:
        """
        Resolves model fields from a given type.

        Args:
            model_type (Type): The type to resolve fields from.

        Returns:
            Dict[str, Type]: A dictionary of field names and their types.
        """
        resolved_fields = dict()
        for name, field in model_type.model_fields.items():
            field_type = field.annotation
            field_name = field.alias or name
            resolved_fields[field_name] = field_type
        return resolved_fields


class PipelineParser:
    """
    Responsible for parsing a full pipeline with task combinations.
    """

    @debug_log
    def parse(self, class_impl: Type) -> Pipeline:
        """
        Parse a pipeline class and return a Pipeline object.

        Args:
            class_impl (Type): The pipeline class to parse.

        Returns:
            Pipeline: Parsed pipeline.
        """
        task_combinations = []
        if get_origin(class_impl) is Union:
            # Handle multiple possible input sets
            for possible_input_set in get_args(class_impl):
                task_combination = self._parse_one_input(possible_input_set)
                task_combinations.append(task_combination)
        else:
            task_combination = self._parse_one_input(class_impl)
            task_combinations.append(task_combination)

        logger.debug(
            "Parsed pipeline with %d task combinations", len(task_combinations)
        )
        return Pipeline(task_combinations=task_combinations)

    @debug_log
    def _parse_one_input(self, class_impl: Type) -> TaskCombination:
        """
        Parse tasks from a class implementation.

        Args:
            class_impl (Type): The class containing task definitions.

        Returns:
            TaskCombination: Combination of parsed tasks.
        """

        task_list = []
        tasks = get_type_hints(class_impl)

        for task_name, task_cls in tasks.items():

            if get_origin(task_cls) is not None:
                task_cls = get_args(task_cls)[0]

            io_task = get_type_hints(task_cls)
            if not any(k.startswith("inputs_") for k in io_task.keys()):
                # Handle sub-tasks
                for sub_task_name, sub_task_cls in io_task.items():
                    task = self._parse_sub_task(sub_task_name, sub_task_cls)
                    task_list.append(task)
            else:
                # Handle regular tasks
                task = TaskParser().parse_task(task_name, task_cls)
                task_list.append(task)

        logger.debug("Parsed %d tasks in this combination", len(task_list))
        return TaskCombination(tasks=task_list)

    @debug_log
    def _parse_sub_task(self, sub_task_name: str, sub_task_cls: Type) -> Task:
        """
        Parses a sub-task from a class.

        Args:
            sub_task_name (str): Name of the sub-task.
            sub_task_cls (Type): The class of the sub-task.

        Returns:
            Task: Parsed sub-task.
        """
        if get_origin(sub_task_cls) is not None:
            sub_task_cls = get_args(sub_task_cls)[0]

        sub_task_hints = get_type_hints(sub_task_cls)
        input_products = ProductParser.parse_products(
            sub_task_hints[f"inputs_{sub_task_name.lower()}"]
        )
        output_products = ProductParser.parse_products(
            sub_task_hints[f"outputs_{sub_task_name.lower()}"]
        )

        mandatory_inputs = [p for p in input_products if p.is_required]
        optional_inputs = [p for p in input_products if not p.is_required]
        input_set = InputSet(
            mandatory_inputs=mandatory_inputs, optional_inputs=optional_inputs
        )

        return Task(
            name=sub_task_name,
            input_sets=[input_set],
            mandatory_outputs=output_products,
        )


class SasModule:
    """Represents a module within the SAS framework."""

    def __init__(
        self, sas_storage: IStorageSasWorkflow, module_name: str, task: Task
    ):
        self.__sas_storage = sas_storage
        self.__module_name = module_name
        self.__task: Task = task
        logger.debug("Initialized SasModule with name: %s", module_name)

    @property
    def sas_storage(self) -> IStorageSasWorkflow:
        return self.__sas_storage

    @property
    def module_name(self) -> str:
        return self.__module_name

    @property
    def task(self) -> Task:
        return self.__task

    def __repr__(self):
        return (
            f"SasModule(module_name={self.module_name}, "
            f"sas_storage={self.sas_storage}, "
            f"task={self.task})"
        )

    @debug_log
    def _link_input_set(self, inputs: List[Product], is_mandatory: bool):
        """
        Helper function to link a set of inputs (either mandatory or optional) to the HDF5 storage.
        Args:
            inputs (List[Product]): The list of inputs to link.
            is_mandatory (bool): Whether the inputs are mandatory.
        Raises:
            InputNotStoredError: If a mandatory input is missing from storage.
        """
        for input_data in inputs:
            input_name = input_data.name

            if self.sas_storage.is_stored_in(
                Hdf5NodeEnum.PRODUCTS.value, input_name
            ):
                hdf5_node = Hdf5NodeEnum.PRODUCTS.value
            elif self.sas_storage.is_stored_in(
                Hdf5NodeEnum.EXTERNALS.value, input_name
            ):
                hdf5_node = Hdf5NodeEnum.EXTERNALS.value
            elif is_mandatory:
                logger.error(
                    f"Mandatory input {input_name} is missing. Please add it before linking."
                )
                raise InputNotStoredError(
                    f"Mandatory input {input_name} is missing."
                )
            else:
                logger.warning(
                    f"Optional input {input_name} not found in the HDF5. Skipping link creation."
                )
                continue

            hdf5_path = Hdf5Tree.find_key_path(
                input_name, f"inputs_{self.module_name.lower()}"
            )
            logger.debug(
                f"Creating link from {hdf5_path} to {hdf5_node}/{input_name}"
            )
            self.sas_storage.create_link(
                f"{hdf5_node}/{input_name}",
                hdf5_path,
                input_data.is_required,
            )

    @debug_log
    def save_product(self, data: Any):
        outputs = [
            output.implementation_class.__name__
            for output in self.task.mandatory_outputs
        ]
        if type(data) in outputs:
            try:
                self.sas_storage.save_product(data)
                logger.info(
                    f"Product of type '{type(data).__name__}' has been successfully saved."
                )
            except Exception as e:
                logger.error(
                    f"Failed to save product of type '{type(data).__name__}': {e}"
                )
                raise StoringInformationError(
                    f"Failed to save product of type '{type(data).__name__}': {e}"
                )
        else:
            logger.error(
                f"Invalid output type: '{type(data).__name__}'. Expected one of: {outputs}."
            )
            raise InvalidOutputTypeError(type(data).__name__, outputs)

    @debug_log
    def load_product(self, key: Any):
        return self.sas_storage.load_product(key)

    def add_inputs(self):
        """
        Links input sets to the current task, prioritizing mandatory inputs and providing
        feedback on the input selection process.

        This method sorts the available input sets based on the number of required elements
        and attempts to link them to the task. If multiple input sets are detected, it logs
        a warning and tries each set in order until a valid one is found or all options are exhausted.

        Raises:
            InputNotStoredError: If no valid input set can be linked.
        """
        task: Task = self.task
        input_sets: List[InputSet] = task.input_sets

        # sort the list according the number of required element
        sorted_input_sets = InputSelectionStrategy.sort(input_sets)
        if len(sorted_input_sets) > 1:
            logger.warning(
                f"Multiple input sets detected for module '{self.module_name}'. "
                "Attempting to find the best one."
            )

        for i, input_set in enumerate(sorted_input_sets):
            # Check if the last element of the list
            is_last = i == len(sorted_input_sets) - 1
            logger.warning(f"is last combination: {is_last}")

            try:
                # Gérer les inputs obligatoires et optionnels
                self._link_input_set(
                    input_set.mandatory_inputs, is_mandatory=True
                )
                self._link_input_set(
                    input_set.optional_inputs, is_mandatory=False
                )
                logger.info(
                    f"Successfully linked input set: {input_set} for Task {task.name}"
                )
                break
            except InputNotStoredError as e:
                if is_last:
                    logger.error(
                        f"Failed to link any input set for module '{self.module_name}': "
                        f"cannot find implementation for input '{e.input_name}'."
                    )
                    raise InputNotStoredError(e.input_name)
                logger.debug(
                    f"Cannot link input set: {input_set}. Trying next set..."
                )
                continue

    def is_validate_dm(self) -> bool:
        return self.sas_storage.is_validate_dm(module=self.module_name)


class SasPipeline:

    def __init__(
        self, sas_storage: IStorageSasWorkflow, cls: Optional[Type] = None
    ):
        self.__sas_storage = sas_storage
        self.__cls = cls
        self.__tasks: List[TaskCombination] = list()
        self.__modules: List[str] = list()
        self._init()

    def _init(self):
        if self.cls is not None:
            self.__pipeline: Pipeline = self._parse_pipeline()
            self.__tasks: List = self.__pipeline.task_combinations
            self.__modules = self.__pipeline.modules
        else:
            self.__pipeline = None
            self.__tasks.clear()
            self.__modules.clear()

    @property
    def sas_storage(self) -> IStorageSasWorkflow:
        return self.__sas_storage

    @property
    def cls(self) -> Optional[Type]:
        return self.__cls

    @cls.setter
    def cls(self, value: Type):
        self.__cls = value
        self._init()

    @property
    def tasks(self) -> List[TaskCombination]:
        return self.__tasks

    @property
    def modules(self) -> List[str]:
        return self.__modules

    def __repr__(self):
        cls_name = "None" if self.cls is None else self.cls.__name__
        return (
            f"SasPipeline(cls={cls_name}, "
            f"sas_storage={self.sas_storage}, "
            f"tasks={len(self.tasks)} task combinations, "
            f"modules={self.modules})"
        )

    @debug_log
    def _parse_pipeline(self) -> Pipeline:
        pipeline_parser = PipelineParser()
        pipeline = pipeline_parser.parse(self.__cls)
        return pipeline

    @debug_log
    def create_module(self, module_name: str) -> SasModule:
        if module_name not in self.modules:
            raise ModuleNotFoundError(module_name, self.modules)
        selected_task = [
            task for task in self.tasks[0].tasks if task.name == module_name
        ]
        if len(selected_task) != 1:
            raise ScientificModuleNotFound(module_name)
        return SasModule(self.sas_storage, module_name, selected_task[0])

    @debug_log
    def save_product(self, data: Any):
        try:
            self.sas_storage.save_product(data)
            logger.info(
                f"Product of type '{type(data).__name__}' has been successfully saved."
            )
        except Exception as e:
            logger.error(
                f"Failed to save product of type '{type(data).__name__}': {e}"
            )
            raise StoringInformationError(
                f"Failed to save product of type '{type(data).__name__}': {e}"
            )

    @debug_log
    def save_external(self, data: Any):
        try:
            self.sas_storage.save_external(data)
            logger.info(
                f"Product of type '{type(data).__name__}' has been successfully saved."
            )
        except Exception as e:
            logger.error(
                f"Failed to save product of type '{type(data).__name__}': {e}"
            )
            raise StoringInformationError(
                f"Failed to save product of type '{type(data).__name__}': {e}"
            )

    @debug_log
    def save_products(self, data: Dict[str, Any]):
        """
        Save multiple products to the storage.

        Args:
            data (Dict[str, Any]): A dictionary of products to be saved, where the key is the product name.

        Raises:
            InvalidProductDataError: If the data provided is not a dictionary.
        """
        if not isinstance(data, dict):
            raise InvalidProductDataDictError(
                "Expected data to be of type Dict[str, Any]."
            )

        for key, product in data.items():
            # Check if the product is a custom object
            if Utils.is_custom_object(product):
                logger.debug(f"Saving custom object for product '{key}'")
                self.save_product(product)
            else:
                logger.debug(
                    f"Saving regular product '{key}' with value '{product}'"
                )
                self.save_product({key: product})

    @debug_log
    def save_externals(self, data: Dict[str, Any]):
        if not isinstance(data, dict):
            raise InvalidProductDataDictError(
                "Expected data to be of type Dict[str, Any]."
            )

        for key, product in data.items():
            # Check if the product is a custom object
            if Utils.is_custom_object(product):
                logger.debug(f"Saving custom object for product '{key}'")
                self.save_external(product)
            else:
                logger.debug(
                    f"Saving regular product '{key}' with value '{product}'"
                )
                self.save_external({key: product})

    @debug_log
    def load_product(self, key: Any):
        return self.sas_storage.load_product(key)

    @debug_log
    def is_validate_dm(self) -> bool:
        return self.sas_storage.is_validate_dm()
