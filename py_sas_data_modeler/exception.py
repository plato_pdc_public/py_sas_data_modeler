# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
"""
The 'exception' module defines various exception classes used within the Data Modeler to handle errors related to data
processing, file handling, product management, data type validation, and task execution. The exceptions are organized
into categories to provide clarity and ease of management, making it simpler to trace, diagnose, and handle specific
errors encountered during the operation of data workflows and pipelines.

Categories:

1. Base Exception
    - DataModelError: Base exception class for all errors in the data modeler.
    - DataclassValidationError : Alias for pydantic's ValidationError

2. File-Related Errors
    - FileFormatError (DataModelError): Base class for file format errors.
        - NoExistingDatasetError: Raised when a dataset is missing.
        - NoExistingGroupError: Raised when a dataset is missing.
        - StoringInformationError: Raised when there is an error storing information.
        - DataTransferError: Raised when an issue occurs during data transfer between files.
        - DataBlockingIOError: Raised when an I/O operation is blocked or interrupted while accessing or manipulating data

3. Product-Related Errors
    - ProductError (DataModelError): Base exception class for product-related errors.
        - InvalidHDF5ObjectError: Raised when the object in the HDF5 file is neither a group nor a dataset.
        - ProductTypeError: Raised for unknown product types.
        - ProductClassError: Raised for invalid product classes.
        - ProductNotFound: Raised when a product cannot be found.
        - InputNotStoredError: Raised when a required input is not stored.
        - InvalidProductDataDictError: Raised when the input data for saving products is not a dictionary.
        - InvalidProductError: Raised when an invalid product is provided to save_product.
        - ProductSaveError: Raised for errors in saving a product.

4. Modules or tasks related Errors
    - ModuleTaskError: Base exception for errors related to modules or tasks.
        - TaskNotFoundError: Raised when a task is not found in the pipeline.
        - ModuleNotFoundError: Raised when the specified module name does not exist in the registered modules.
        - ScientificModuleNotFound: Raised when a scientific module is not found.

5. Data Type Errors
    - DataTypeError: Base class for data type errors.
        - IncompatibleDataTypeError: Raised when a list contains incompatible mixed data types.
        - InvalidDataTypeError: Raised when the provided value is not a list, dict, or numpy array.

6. Data Class Errors
    - DataclassError: Base class for dataclass errors
        - AliasRegistrationError: Exception raised when alias registration fails due to an unknown key.
        - UnknownDataclassError: Raised when attempting to retrieve an unknown dataclass.
        - DataclassInstanceCreationError: Raised when there is an error creating an instance of a dataclass from a dictionary.
        - DataclassImplementationNotFound: Raised when a dataclass cannot be instanciate based of a multiple choice.
        - KeyNotFoundError: Exception raised when a key corresponding to a dataclass is not found in the dependency tree.
        - PathNotFoundError: Exception raised when no path is found for a given key in the dependency tree.

7. General Errors
    - InvalidOutputTypeError: Raised when the provided data does not match any of the expected output types.
"""
import re
from typing import Any
from typing import List
from typing import Type
from typing import Union

from pydantic import ValidationError

DataclassValidationError = ValidationError
"""
DataclassValidationError is an alias for pydantic's ValidationError.

This exception is raised when validation fails for a dataclass instance.
It typically occurs when the provided data does not match the expected
schema or data type constraints defined in the dataclass.
"""


# 1. Base Exception
class DataModelError(Exception):
    """Base exception class for all errors in the data modeler."""


# 2. File-Related Errors
class FileFormatError(DataModelError):
    """Base class for file format errors."""


class NoExistingDatasetError(FileFormatError):
    """Exception raised when a dataset is missing."""


class NoExistingGroupError(FileFormatError):
    """Exception raised when a group is missing."""


class StoringInformationError(FileFormatError):
    """Exception raised when there is an error storing information."""


class DataTransferError(FileFormatError):
    """Raised when an issue occurs during data transfer between files."""


class DataBlockingIOError(FileFormatError):
    """Raised when an I/O operation is blocked or interrupted while accessing
    or manipulating data. This typically occurs when there is an unexpected
    read/write failure, file locking issue, or a resource contention problem
    during data transfer or storage operations."""


# 3. Product-Related Errors
class ProductError(DataModelError):
    """Base exception class for product-related errors."""


class InvalidHDF5ObjectError(ProductError):
    """Exception raised when the object in the HDF5 file is neither a group nor a dataset."""


class ProductTypeError(ProductError):
    """Exception raised for unknown product types."""

    def __init__(self, product_name: str):
        self.product_name = product_name
        super().__init__(
            f"Unknown '{product_name}' found. A product should be classified as internal or external."
        )


class ProductClassError(ProductError):
    """Exception raised for invalid product classes."""

    def __init__(self, product_class: Type):
        self.product_class = product_class
        super().__init__(
            f"Unknown product class '{product_class}' found. A product class must be defined in dm.sas module."
        )


class ProductNotFound(ProductError):
    """Raised when a product cannot be found."""

    def __init__(self, product_name: str):
        self.product_name = product_name
        super().__init__(f"Unable to find '{product_name}'")


class InputNotStoredError(ProductError):
    """Exception raised when a required input is not stored."""

    def __init__(self, input_name: Union[str, TypeError]):
        if isinstance(input_name, TypeError):
            missing_args: List[str] = re.findall(r"'(.*?)'", str(input_name))
            self.input_name = ", ".join(missing_args)
        else:
            self.input_name = input_name
        super().__init__(f"Input '{input_name}' is missing.")


class InvalidProductDataDictError(ProductError):
    """Raised when the input data for saving products is not a dictionary."""


class InvalidProductError(ProductError):
    """Raised when an invalid product is provided to save_product."""

    def __init__(self, data: str):
        super().__init__(
            f"Invalid product data provided: {data}. Data must be either an internal or an external product."
        )


class ProductSaveError(ProductError):
    """Exception raised for errors in saving a product."""

    def __init__(self, data):
        self.data = data
        super().__init__(f"An error occurred while saving the product {data}")


# 4. Modules or tasks related Errors
class ModuleTaskError(DataModelError):
    """Base exception for errors related to modules or tasks"""


class TaskNotFoundError(ModuleTaskError):
    """Raised when a task is not found in the pipeline."""

    def __init__(self, module_name: str):
        self.module_name = module_name
        super().__init__(
            f"Task with name '{module_name}' not found in the pipeline."
        )


class ModuleNotFoundError(ModuleTaskError):
    """Raised when the specified module name does not exist in the registered modules."""

    def __init__(self, module_name, registered_modules):
        self.module_name = module_name
        self.registered_modules = registered_modules
        message = f"Module '{module_name}' not found. Registered modules: {', '.join(registered_modules)}"
        super().__init__(message)


class ScientificModuleNotFound(ModuleTaskError):
    """Raised when a scientific module is not found."""

    def __init__(
        self,
        module_name: str,
        message: str = "Failed to find the scientific module",
    ):
        super().__init__(f"{message}: {module_name}")
        self.module_name = module_name


# 5. Data Type Errors
class DataTypeError(DataModelError):
    """Base exception class for data type errors."""


class IncompatibleDataTypeError(DataTypeError):
    """Exception raised when a list contains incompatible mixed data types."""


class InvalidDataTypeError(DataTypeError):
    """Exception raised when the provided value is not a list, dict, or numpy array."""


# 6. Dataclass Errors
class DataclassError(DataModelError):
    """Base exception for errors related to the dataclass"""


class AliasRegistrationError(DataclassError):
    """Exception raised when alias registration fails due to an unknown key."""

    def __init__(self, alias: str, key: str):
        self.alias = alias
        self.key = key
        super().__init__(
            f"Failed to register alias '{alias}' for key '{key}'. Key not found."
        )


class UnknownDataclassError(DataclassError):
    """Raised when attempting to retrieve an unknown dataclass."""

    def __init__(self, key: str):
        super().__init__(f"Unknown dataclass for the key: {key}")
        self.key = key


class DataclassInstanceCreationError(DataclassError):
    """Raised when there is an error creating an instance of a dataclass from a dictionary."""

    def __init__(
        self,
        key: str,
        dico: dict,
        message: str = "Failed to create dataclass instance",
    ):
        super().__init__(f"{message} for key: {key} with data: {dico}")
        self.key = key
        self.dico = dico


class DataclassImplementationNotFound(DataclassError):
    """Raised when a dataclass cannot be instanciate based of a multiple choice."""

    def __init__(
        self,
        key: str,
        dico: dict,
        message: str = "Failed to create dataclass instance",
    ):
        super().__init__(f"{message} for key: {key} with data: {dico}")
        self.key = key
        self.dico = dico


class KeyNotFoundError(DataclassError):
    """Exception raised when a key corresponding to a dataclass is not found in the dependency tree."""

    def __init__(self, dataclass_name: str):
        super().__init__(f"Key for dataclass '{dataclass_name}' not found.")
        self.dataclass_name = dataclass_name


class PathNotFoundError(DataclassError):
    """Exception raised when no path is found for a given key in the dependency tree."""

    def __init__(self, key: str, constraint=None):
        message = f"No path found for key '{key}'"
        if constraint:
            message += f" with constraint '{constraint}'"
        super().__init__(message)
        self.key = key
        self.constraint = constraint


# 6. General Errors
class InvalidOutputTypeError(DataModelError):
    """Raised when the provided data does not match any of the expected output types."""

    def __init__(self, data_type, expected_types):
        self.data_type = data_type
        self.expected_types = expected_types
        message = f"Invalid output type: {data_type}. Expected one of: {', '.join(expected_types)}"
        super().__init__(message)
