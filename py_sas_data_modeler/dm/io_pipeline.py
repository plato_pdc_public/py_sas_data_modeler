# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
"""
The 'io_pipeline' module defines classes and data structures used to represent and manage a pipeline of tasks and products
in an HDF5-based workflow system. It includes definitions for products, input sets, tasks, and task combinations,
as well as the overall pipeline structure.

Classes:

    Hdf5NodeEnum: Enumeration of HDF5 node paths for storing products and external data.

    Product: Represents an individual product used in tasks, containing its name, implementation class, and whether it is required.

    InputSet: Represents a set of inputs for a task, allowing differentiation between mandatory and optional inputs.

    Task: Represents a task within a pipeline, containing input sets and mandatory outputs.

    TaskCombination: Represents a combination of multiple tasks to be executed in sequence.

    Pipeline: Represents a pipeline of task combinations, providing methods for adding combinations, retrieving tasks,
              and managing cached task and module information.

.. uml::

    class Product {
        + name : String
        + implementation_class : Type
        + is_required : Boolean
    }

    class InputSet {
        + mandatory_inputs : List<Product>
        + optional_inputs : List<Product>
        + nb_inputs() : int
        + count_idp_sas() : int
    }

    class Task {
        + name : String
        + input_sets : List<InputSet>
        + mandatory_outputs : List<Product>
    }

    class TaskCombination {
        + tasks : List<Task>
    }

    class Pipeline {
        + task_combinations : List<TaskCombination>
        + add_combination(task_combination : TaskCombination) : void
        + modules() : List<String>
        + all_tasks() : List<Task>
        + task(module_name : String) : Task
    }

    InputSet --> Product : "contains"
    Task --> InputSet : "uses"
    Task --> Product : "produces"
    TaskCombination --> Task : "includes"
    Pipeline --> TaskCombination : "contains"



Exceptions:
    TaskNotFoundError: Raised when attempting to retrieve a task that doesn't exist in the pipeline.
"""
from enum import Enum
from typing import List
from typing import Type

from pydantic import ConfigDict
from pydantic import Field

from ..exception import TaskNotFoundError
from .dataclass_management import BaseDataClass


class Hdf5NodeEnum(Enum):
    """Node of the HDF5"""

    PRODUCTS = "/PRODUCTS"
    EXTERNALS = "/EXTERNALS"

    def __str__(self):
        return self.value


class Product(BaseDataClass):
    """
    Represents an input or output product used in tasks.

    Attributes:
        name (str): The name of the product.
        implementation_class (Type): The class responsible for handling this product.
        is_required (bool): Indicates whether the product is mandatory for the task.
    """

    model_config = ConfigDict(frozen=True)

    name: str
    implementation_class: Type  # This will refer to the class responsible for handling this product
    is_required: bool


class InputSet(BaseDataClass):
    """
    Represents a set of inputs for a task, including both mandatory and optional inputs.

    Attributes:
        nb_inputs (int): Number of inputs expected in the input set.
        mandatory_inputs (List[Product]): A list of mandatory input products.
        optional_inputs (List[Product]): A list of optional input products. Defaults to an empty list.
    """

    model_config = ConfigDict(frozen=True)

    mandatory_inputs: List[Product]  # List of mandatory inputs
    optional_inputs: List[Product] = Field(
        default_factory=list
    )  # List of optional inputs

    @property
    def nb_inputs(self) -> int:
        """
        Calculates the total number of records as the sum of mandatory and optional inputs.

        Returns:
            int: The total number of records (mandatory + optional inputs).
        """
        return len(self.mandatory_inputs) + len(self.optional_inputs)

    @property
    def count_idp_sas(self) -> int:
        """Return the number of products with the pattern IDM_SAS in mandatory_inputs."""
        return sum(
            1
            for input_name in self.mandatory_inputs
            if input_name.name.startswith("IDP_SAS")
        )


class Task(BaseDataClass):
    """
    Represents a task that is part of a pipeline, containing input sets and outputs.

    Attributes:
        name (str): The name of the task.
        input_sets (List[InputSet]): A list of possible input sets for the task.
        mandatory_outputs (List[Product]): A list of mandatory output products produced by the task.
    """

    model_config = ConfigDict(frozen=True)

    name: str
    input_sets: List[
        InputSet
    ]  # Allows multiple input sets (combinations of inputs)
    mandatory_outputs: List[Product]  # A task only has mandatory outputs


class TaskCombination(BaseDataClass):
    """
    Represents a specific combination of tasks within a pipeline.

    Attributes:
        tasks (List[Task]): A list of tasks that form this combination.
    """

    model_config = ConfigDict(frozen=True)

    tasks: List[Task]  # A combination of tasks


class Pipeline(BaseDataClass):
    """
    Represents a pipeline that consists of multiple task combinations.

    Attributes:
        task_combinations (List[TaskCombination]): A list of task combinations in the pipeline.
    """

    task_combinations: List[TaskCombination] = Field(default_factory=list)
    cached_modules: List[str] = Field(
        default_factory=list, exclude=True
    )  # Internal cache for module names
    cached_tasks: List[Task] = Field(
        default_factory=list, exclude=True
    )  # Internal cache for all tasks

    def add_combination(self, task_combination: TaskCombination) -> None:
        """
        Adds a new task combination to the pipeline.

        Args:
            task_combination (TaskCombination): The task combination to add.
        """
        self.task_combinations.append(task_combination)
        self.cached_modules.clear()  # Invalidate module cache
        self.cached_tasks.clear()  # Invalidate task cache

    @property
    def modules(self) -> List[str]:
        """
        Retrieves the unique set of task names (modules) in the pipeline.

        Returns:
            List[str]: A list of unique task names across all task combinations.
        """
        if len(self.cached_modules) == 0:
            self.cached_modules.extend(
                set(
                    task.name
                    for task_comb in self.task_combinations
                    for task in task_comb.tasks
                )
            )
        return self.cached_modules

    @property
    def all_tasks(self) -> List[Task]:
        """
        Retrieves all tasks across all task combinations.
        Caches the result to avoid recalculating it multiple times.

        Returns:
            List[Task]: A list of all tasks from all task combinations.
        """
        if len(self.cached_tasks) == 0:
            self.cached_tasks.extend(
                [
                    task
                    for task_comb in self.task_combinations
                    for task in task_comb.tasks
                ]
            )
        return self.cached_tasks

    def task(self, module_name: str) -> Task:
        """
        Retrieves a task by its name from the pipeline.

        Args:
            module_name (str): The name of the task (module) to retrieve.

        Returns:
            Task: The task corresponding to the provided module name.

        Raises:
            TaskNotFoundError: If no task with the specified name is found in the pipeline.
            DuplicateTaskError: If more than one task with the same name exists.
        """
        matching_tasks = [
            task for task in self.all_tasks if task.name == module_name
        ]

        if not matching_tasks:
            raise TaskNotFoundError(module_name)

        return matching_tasks[0]
