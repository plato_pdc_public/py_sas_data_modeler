# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
"""
The `dataclass_management` module provides a comprehensive framework for managing Python dataclasses and their
relationships, including utilities for dataclass creation, dependency analysis, and hierarchical structure management.

This module is organized into several key components:

1. **DataclassCreator**:
    A class responsible for constructing instances of dataclasses from dictionaries, facilitating the easy
    creation of dataclass objects from structured data.

2. **DataClassFactory**:
    A singleton factory class that registers and retrieves dataclass types and their aliases. It provides
    methods for retrieving dataclass instances by string keys and constructing them from dictionaries.
    This class raises custom exceptions for unknown dataclasses or errors in instance creation.

3. **DependencyTree**:
    A class that analyzes and constructs a hierarchical structure of dependencies between Python dataclasses,
    allowing recursive parsing of attributes, registration of dataclasses, and detection of recursive
    dependencies. It provides methods to query dependencies and find paths or keys within nested structures.

4. **Hdf5Tree**:
    A singleton class that extends `DependencyTree` to manage dependencies specifically in HDF5-like data
    structures. It offers functionality for creating, querying, and retrieving hierarchical paths and keys
    in dependency trees, with a focus on managing complex, nested data formats in an HDF5 context.

"""
import sys
from functools import lru_cache
from typing import Any
from typing import cast
from typing import Dict
from typing import List
from typing import Optional
from typing import Tuple
from typing import Type
from typing import TypeVar
from typing import Union

from deprecated import deprecated
from loguru import logger
from pydantic import BaseModel
from pydantic import ConfigDict
from pydantic import RootModel

from ..exception import AliasRegistrationError
from ..exception import DataclassImplementationNotFound
from ..exception import DataclassInstanceCreationError
from ..exception import KeyNotFoundError
from ..exception import PathNotFoundError
from ..exception import UnknownDataclassError
from ..helper import debug_log
from ..helper import spec_coverage
from ..helper import Utils

logger.remove()
logger.add(sys.stdout, level="INFO")

T = TypeVar("T", bound=Union["BaseDataClass", "BaseRootModel"])


class BaseDataClass(BaseModel):
    """
    Base class to encapsulate Pydantic's BaseModel.
    This abstraction allows flexibility to change the underlying implementation.
    """

    model_config = ConfigDict(
        frozen=False,  # Indicates whether the model is immutable
        arbitrary_types_allowed=True,  # Allows custom types in the fields
        validate_assignment=True,  # Validates data when attributes are assigned
    )

    @classmethod
    def from_dict(cls, data: dict):
        """Creates an instance of the class from a dictionary of data.

        This method attempts to initialize the class by unpacking the given dictionary as keyword arguments.
        If the provided data does not meet the class's validation requirements, it raises a custom
        `DataclassValidationError` to encapsulate the underlying validation issues.

        Args:
            data (dict): A dictionary containing the data to initialize the class.

        Returns:
            cls: An instance of the class initialized with the provided data.

        Raises:
            DataclassValidationError: Raised when the provided data fails validation.
        """

        return cls(**data)

    def to_dict(self) -> dict:
        """Converts the model instance to a dictionary."""
        return self.model_dump(by_alias=True)

    @classmethod
    def list_fields(cls) -> List[str]:
        """Returns a list of field names defined in the Pydantic model."""
        return list(cls.model_fields.keys())

    @classmethod
    def model_validate(cls, data: Any) -> BaseModel:
        """Validates the given data and returns an instance of the model.

        This method overrides Pydantic's `model_validate` to add custom validation behavior
        or preprocessing logic before delegating to the base implementation.

        Args:
            data (Any): The data to validate.

        Returns:
            BaseModel: An instance of the validated model.

        Raises:
            DataclassValidationError: Raised if the data fails validation.
        """
        return super().model_validate(data)


class BaseRootModel(RootModel):
    """
    Base class to encapsulate Pydantic's BaseModel.
    This abstraction allows flexibility to change the underlying implementation.
    """

    model_config = ConfigDict(
        frozen=False,  # Indicates whether the model is immutable
        arbitrary_types_allowed=True,  # Allows custom types in the fields
        validate_assignment=True,  # Validates data when attributes are assigned
    )

    @classmethod
    def from_dict(cls, data):
        """Creates an instance of the class from a dictionary of data.

        This method initializes the class by wrapping the provided dictionary as a single `root` argument.
        If the data does not meet the class's validation requirements, it raises a custom
        `DataclassValidationError` to encapsulate the underlying validation issues.

        Args:
            data (dict): A dictionary containing the data to initialize the class.

        Returns:
            cls: An instance of the class initialized with the provided data.

        Raises:
            DataclassValidationError: Raised when the provided data fails validation.
        """

        return cls(root=data)

    def to_dict(self) -> dict:
        """Converts the model instance to a dictionary."""
        return self.model_dump(by_alias=True)

    @classmethod
    def model_validate(cls, data: Any) -> BaseModel:
        """Validates the given data and returns an instance of the model.

        This method overrides Pydantic's `model_validate` to add custom validation behavior
        or preprocessing logic before delegating to the base implementation.

        Args:
            data (Any): The data to validate.

        Returns:
            BaseModel: An instance of the validated model.

        Raises:
            DataclassValidationError: Raised if the data fails validation.
        """
        return super().model_validate(data)


class DataclassCreator:
    """
    Handles creating dataclass instances from dictionaries.
    """

    def __init__(self, registry: Dict):
        self.registry = registry

    @debug_log
    def create_instance(
        self, key: str, data: Dict
    ) -> Union[BaseDataClass, BaseRootModel]:
        """Create instance

        Args:
            key (str): key related to the Type
            data (Dict): data to fill up in the object

        Raises:
            UnknownDataclassError: Unknown class
            DataclassInstanceCreationError: Instance creation error
            DataclassImplementationNotFound: Cannot find an implementation among a list of implementations

        Returns:
            Union[BaseDataClass, BaseRootModel]: created object
        """
        dataclass_type: Any
        if key.lower() in self.registry:
            dataclass_type = self.registry.get(key.lower())
        else:
            logger.error(f"Unknown dataclass for product_name '{key}'.")
            raise UnknownDataclassError(key)

        if isinstance(dataclass_type, tuple):
            for dataclass_ in dataclass_type:
                try:
                    return self._construct_dataclass(dataclass_, data)
                except Exception:
                    logger.warning(
                        f"Failed to create instance of {dataclass_} with {data} - try the next implementation if available"
                    )
            raise DataclassImplementationNotFound(key, data)
        else:
            try:
                return self._construct_dataclass(dataclass_type, data)
            except Exception as e:
                logger.exception(
                    f"Failed to create dataclass instance {dataclass_type} for key '{key}'."
                )
                raise DataclassInstanceCreationError(key, data) from e

    @staticmethod
    def _construct_dataclass(
        model_type: Union[Type[BaseDataClass], Type[BaseRootModel]], data: Dict
    ) -> Union[BaseDataClass, BaseRootModel]:
        """Constructs an instance of the dataclass using the provided dictionary.

        Args:
            model_type (Union[Type[BaseDataClass], Type[BaseRootModel]]): model type
            data (Dict): data

        Returns:
            Union[BaseDataClass, BaseRootModel]: An instance of BaseDataClass
        """
        return model_type.from_dict(data)


@spec_coverage(
    "PLATO-IAS-PDC-TN-0005",
    "REQ-IF-HDF5-20",
    "REQ-NF-HDF5-20",
    "REQ-NF-HDF5-30",
)
class DataClassFactory:
    """Singleton factory class responsible for registering and retrieving dataclass types,
    along with their aliases. This class supports retrieving dataclass instances based on
    string keys and constructing objects from dictionaries.

    Raises:
        UnknownDataclassError: If the dataclass key is not found in the registry.
        DataclassInstanceCreationError: If an error occurs during the creation of the dataclass instance.
    """

    _instance = None
    _dataclass_registry: Dict = {}
    _dataclass_creator = DataclassCreator(_dataclass_registry)
    _reserved_keys = frozenset(
        {
            "any",
            "baserootmodel",
            "basedataclass",
            "configdict",
            "dict",
            "field",
            "list",
            "optional",
            "rootmodel",
            "union",
        }
    )

    def __new__(cls):
        """
        Ensures that only one instance of the DataClassFactory is created (Singleton pattern).

        Returns:
        -------
        DataClassFactory
            The singleton instance of the DataClassFactory.
        """
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            logger.debug("Created new instance of DataClassFactory.")
        return cls._instance

    @classmethod
    def register_dataclass(
        cls, key: str, model_type: Union[Type[Any], Tuple[Type]]
    ):
        """
        Registers a new dataclass type in the registry with the specified key.

        Args:
            key (str) : The key under which the dataclass will be registered.
            model_type (Union[Type[Any], Tuple[Type]]) : The model type being registered.
        """
        if not isinstance(key, str) or not key.strip():
            raise ValueError(
                f"Invalid key: '{key}'. Key must be a non-empty string."
            )

        key_lower: str = key.lower()
        if key_lower in cls._reserved_keys:
            logger.debug(
                f"Key '{key}' is reserved and cannot be used for registration."
            )
            return

        if not isinstance(model_type, (type, tuple)):
            raise TypeError(
                f"Invalid model_type: {model_type}. "
                "Expected a class or a tuple of classes."
            )

        cls._dataclass_registry[key_lower] = Utils.resolve_type(model_type)
        logger.debug(
            f"Dataclass '{model_type}' registered under key '{key_lower}'."
        )

    @classmethod
    @debug_log
    def register_alias(cls, alias: str, key: str):
        """
        Registers an alias for an existing dataclass. The alias can be used as a key
        to retrieve the same dataclass type.

        Args:
            alias (str) : The alias to associate with the dataclass.
            key (str) : The key of the already registered dataclass.

        Raises:
            AliasRegistrationError : If the key for the dataclass does not exist in the registry.
        """
        model_type: Union[Type[BaseDataClass], Type[BaseRootModel]] = (
            cls.get_model(key)
        )
        logger.debug(f"Retrieve {model_type} from {key}")
        try:
            cls.register_dataclass(alias, model_type)
            logger.debug(
                f"Alias '{alias.lower()}' registered for dataclass '{model_type}'."
            )
        except UnknownDataclassError:
            logger.error(
                f"Failed to register alias '{alias}'. Key '{key}' not found."
            )
            raise AliasRegistrationError(alias, key)

    @classmethod
    @deprecated(reason="Use get_model instead - Will be removed in V2.1.0")
    def get_dataclass(
        cls, key: str, **kwargs
    ) -> Union[Type[Any], Tuple[Type[Any]]]:
        return cls.get_model(key, **kwargs)

    @classmethod
    @debug_log
    def get_model(
        cls, key: str, **kwargs
    ) -> Union[Type[BaseDataClass], Type[BaseRootModel]]:
        """
        Retrieves the model type associated with the given key from the registry.

        At a given key, several classes can be associated due to the the different combinations
        of entries for a scientific module.

        Args:
            key (str) : The key used to retrieve the dataclass. The key could be a schema name, a product, a scientific module ou a pipeline

        Returns:
            Union[Type[BaseDataClass], Type[BaseRootModel]] : The dataclass types associated with the given key.

        Raises:
            UnknownDataclassError : If the dataclass key is not found in the registry.
        """
        key_lower = key.lower()
        key_selected: str
        if key_lower in cls._dataclass_registry:
            key_selected = key_lower
        elif key_lower + "dm" in cls._dataclass_registry:
            key_selected = key_lower + "dm"
        else:
            logger.error(
                f"Unknown dataclass for key '{key_selected}'. List of available keys : {cls._dataclass_registry.keys()}"
            )
            raise UnknownDataclassError(key_selected)

        model_type: Union[Type[BaseDataClass], Type[BaseRootModel]] = cast(
            Union[Type[BaseDataClass], Type[BaseRootModel]],
            cls._dataclass_registry.get(key_selected),
        )
        return model_type

    @classmethod
    def create_instance(
        cls, key: str, dico: Dict
    ) -> Union[BaseDataClass, BaseRootModel]:
        """
        Retrieves the dataclass type associated with the given key and creates an instance
        of the dataclass by converting the input dictionary to a dataclass object. If no
        matching dataclass is found, raises an UnknownDataclassError. If the dataclass
        creation fails, raises a DataclassInstanceCreationError.

        Args:
            key (str) : The key used to identify the dataclass.
            dico (Dict) : The dictionary containing the data to initialize the dataclass instance.

        Returns:
            Union[BaseDataClass, BaseRootModel] : An instance of the dataclass corresponding to the key.

        Raises:
            UnknownDataclassError : If the dataclass key is not found in the registry.
            DataclassInstanceCreationError : If an error occurs during the creation of the dataclass instance.
        """
        return cls._dataclass_creator.create_instance(key, dico)


class DependencyTree:
    """
    The DependencyTree class is responsible for analyzing and constructing a hierarchical structure of
    dependencies between Python dataclasses. It recursively traverses the attributes of a given dataclass,
    resolving their types and determining the relationships between them. The primary goal is to build a
    tree-like structure that captures the dependencies between dataclasses and provides utilities to query
    this structure.

    Key Features:

    * Dependency Parsing: It recursively identifies and resolves the types of attributes within a dataclass. If an attribute is itself a dataclass or a tuple of dataclasses, it recursively processes these types.
    * Dataclass Registration: During traversal, it registers each dataclass in the DataClassFactory, allowing easy retrieval and instantiation of dataclasses later.
    * Recursion Detection: It has methods to check if a dataclass has recursive dependencies, i.e., if any of its attributes refer to the dataclass itself.
    * Dependency Discovery: It can retrieve a list of all dataclasses that a given class depends on, based on its attributes.
    * Key and Path Finding: The class also provides utilities to find the path to a specific key in a nested dictionary structure and to find a key corresponding to a specific dataclass within a dictionary.
    """

    def __init__(self) -> None:
        """
        Initializes the DependencyTree with an empty node dictionary.
        """
        self.nodes: Dict[str, Any] = {}

    def create(self, model_class: Union[Type, Tuple[Type]]) -> None:
        """
        Creates a dependency tree based on the provided class.

        Args:
            model_class (Union[Type, Tuple[Type]]): The root class from which the dependency tree is created.
        """
        logger.debug(f"Creating dependency tree for class: {model_class}")
        # Build the dependency tree starting from the provided class.
        self.nodes = self._traverse_class(Utils.resolve_type(model_class))
        logger.debug(f"Dependency tree for class {model_class}: {self.nodes}")

    @debug_log
    def _traverse_class(
        self,
        model_type: Union[Type[BaseDataClass], Type[BaseRootModel]],
        depth: int = 0,
        max_depth: int = 10,
    ) -> Dict[str, Any]:
        """
        Recursively traverses a class to build a tree of its attributes and types.

        Args:
            model_type (Union[Type[BaseDataClass], Type[BaseRootModel]]): The class to traverse.
            depth (int): Current depth of recursion.
            max_depth (int): Maximum depth of recursion to prevent stack overflow.

        Returns:
            Dict[str, Any]: A dictionary representing the structure of the class's attributes.
        """
        if depth > max_depth:
            return self._handle_max_depth(model_type, max_depth)

        logger.debug(f"Traversing class: {model_type.__name__}, depth={depth}")
        node: Dict[str, Any] = {}

        for attr_name, field_info in model_type.model_fields.items():
            field_type = cast(Type[Any], field_info.annotation)
            alias = (
                field_info.alias
                if field_info and field_info.alias
                else attr_name
            )
            self._process_attribute(alias, field_type, node, depth, max_depth)

        return node

    def _handle_max_depth(
        self,
        cls: Union[Type[BaseDataClass], Type[BaseRootModel]],
        max_depth: int,
    ) -> Dict[str, Any]:
        """Handles the case when the maximum recursion depth is reached."""
        logger.warning(
            f"Reached maximum recursion depth ({max_depth}) for class {cls.__name__}"
        )
        return {}

    def _process_attribute(
        self,
        attr_name: str,
        attr_type: Type[Any],
        node: Dict[str, Any],
        depth: int,
        max_depth: int,
    ):
        """Processes a single attribute of a class."""
        resolve_type: Type = cast(Type, self._resolve_type(attr_type))
        logger.debug(
            f"Processing attribute '{attr_name}' with resolved type: {resolve_type}"
        )
        if self._is_nested_model(resolve_type):
            self._process_dataclass_attribute(
                attr_name, resolve_type, node, depth, max_depth
            )
        elif isinstance(resolve_type, tuple):
            self._process_tuple_attribute(
                attr_name, resolve_type, node, depth, max_depth
            )
        else:
            self._process_simple_attribute(attr_name, resolve_type, node)

    def _is_nested_model(self, resolve_type: Type[Any]) -> bool:
        """Checks if the resolved type is a nested dataclass."""
        return isinstance(resolve_type, type) and (
            issubclass(resolve_type, BaseDataClass)
            or issubclass(resolve_type, BaseRootModel)
        )

    def _process_dataclass_attribute(
        self,
        attr_name: str,
        resolve_type: Union[Type[BaseDataClass], Type[BaseRootModel]],
        node: Dict[str, Any],
        depth: int,
        max_depth: int,
    ):
        """Processes a dataclass attribute."""
        node[attr_name] = {"type": resolve_type}
        child = self._traverse_class(resolve_type, depth + 1, max_depth)
        self._register_dataclass(attr_name, resolve_type)
        if child:
            node[attr_name]["child"] = child

    def _process_tuple_attribute(
        self,
        attr_name: str,
        resolve_type: Tuple[Type],
        node: Dict[str, Any],
        depth: int,
        max_depth: int,
    ):
        """Processes an attribute with a tuple type."""
        node[attr_name] = {"child": []}
        for type_ in resolve_type:
            child_node = self._traverse_class(type_, depth + 1, max_depth)
            node[attr_name]["child"].append(child_node)

        self._register_dataclass(attr_name, resolve_type)

    def _process_simple_attribute(
        self,
        attr_name: str,
        resolve_type: Type[Any],
        node: Dict[str, Any],
    ):
        """Processes a simple attribute that is not nested."""
        if Utils.is_external_product(attr_name) or Utils.is_internal_product(
            attr_name
        ):
            node[attr_name] = resolve_type
            self._register_dataclass(attr_name, resolve_type)

    @lru_cache(maxsize=None)
    def _resolve_type(self, cls: Type) -> Union[Type, Tuple[Type]]:
        """
        Caches and resolves the type of the given class.

        Args:
            cls (Type): The class to resolve.

        Returns:
            Union[Type, Tuple[Type]]: The resolved type.
        """
        return Utils.resolve_type(cls)

    def _register_dataclass(
        self, attr_name: str, resolve_type: Union[Type[Any], Tuple[Type]]
    ) -> None:
        """
        Registers a dataclass in the DataClassFactory.

        Args:
            attr_name (str): The name of the attribute to register.
            resolve_type (Type[Any]): The resolved type of the attribute.
        """
        DataClassFactory.register_dataclass(attr_name, resolve_type)
        logger.debug(
            f"Registered dataclass '{attr_name}' with type '{resolve_type}'"
        )

    @debug_log
    def find_key_path(
        self,
        d: dict,
        key: str,
        path: Optional[List] = None,
        constraint: Optional[str] = None,
    ) -> Optional[List[str]]:
        """
        Recursively searches for a key in a nested dictionary and returns the path to the key as a list.

        Args:
            d (dict): The dictionary to search.
            key (str): The key to search for.
            path (list, optional): The current path being traversed. Defaults to None.
            constraint (str, optional): A constraint to refine the key search. Defaults to None.

        Returns:
            Optional[List[str]]: The path to the key if found, otherwise None.
        """
        if path is None:
            path = []

        logger.debug(
            f"Searching for key '{key}' in dictionary at path {path}. Constraint: {constraint}"
        )

        # Base case: if the key is found at the current level of the dictionary
        if key in d:
            final_path = path + [key]
            logger.debug(f"Key '{key}' found at path: {final_path}")

            # If no constraint is specified, return the path
            if constraint is None:
                return final_path
            # If the constraint is met, return the path
            elif constraint in final_path:
                logger.debug(
                    f"Key '{key}' meets constraint '{constraint}' at path: {final_path}"
                )
                return final_path

        # Recursively search through the dictionary
        for k, v in d.items():
            if isinstance(v, dict):
                # Recursive case for dictionaries
                result = self.find_key_path(
                    v,
                    key,
                    path + [k] if k != "child" and k != "root" else path,
                    constraint,
                )
                if result is not None:
                    return result
            elif isinstance(v, list):
                # Iterate over lists and search inside each item
                for index, item in enumerate(v):
                    if isinstance(item, dict):
                        result = self.find_key_path(
                            item,
                            key,
                            (
                                path + [k, index]
                                if k != "child" and k != "root"
                                else path
                            ),
                            constraint,
                        )
                        if result is not None:
                            return result

        logger.debug(f"Key '{key}' not found at path {path}.")
        return None  # Key not found

    @debug_log
    def find_key(
        self, key: str, d: dict, model_type: Type[T]
    ) -> Optional[str]:
        """
        Searches for a key in a nested dictionary that maps to a specific target dataclass.

        Args:
            key (str): The key to search for.
            d (dict): The dictionary to search.
            model_type (Type[T]): The target dataclass instance to search for.

        Returns:
            Optional[str]: The key corresponding to the target dataclass if found, otherwise None.
        """
        # Check if the current dictionary contains the target dataclass
        if "type" in d and d["type"].__name__ == type(model_type).__name__:
            logger.debug(
                f"Target dataclass '{type(model_type).__name__}' found with key '{key}'."
            )
            return key

        # Recursively search the dictionary
        for k, v in d.items():
            if isinstance(v, dict):
                found_key = self.find_key(k, v, model_type)
                if found_key:
                    return found_key
            elif isinstance(v, list):
                for elem in v:
                    found_key = self.find_key(k, elem, model_type)
                    if found_key:
                        return found_key
            else:
                # Check if the value type matches the target dataclass
                if (
                    isinstance(v, type)
                    and v.__name__ == type(model_type).__name__
                ):
                    logger.debug(
                        f"Target dataclass '{type(model_type).__name__}' found with key '{key}'."
                    )
                    return key

        logger.debug(
            f"Target model '{type(model_type).__name__}' not found in dictionary."
        )
        return None  # Target model not found


@spec_coverage("PLATO-IAS-PDC-ICD-0001", "SAS-REQ-IF-MODULE-STO-60")
@spec_coverage("PLATO-IAS-PDC-TN-0005", "REQ-IF-HDF5-10")
class Hdf5Tree:
    """
    The Hdf5Tree class is a singleton wrapper around the DependencyTree, designed to manage a hierarchical structure of dependencies between dataclasses, with a particular focus on HDF5-like data structures. It allows for the creation, retrieval, and query of dependency relationships between dataclasses, which can be helpful in managing complex nested data formats.

    The class provides functionality for:

    * Tree Creation: Building a dependency tree for a given dataclass type.
    * Key Retrieval: Finding keys corresponding to specific dataclasses within the dependency tree.
    * Key Path Discovery: Retrieving the hierarchical path to a specific key within the tree, optionally applying constraints.
    """

    _instance = None  # Singleton instance
    _tree = DependencyTree()  # Internal DependencyTree instance

    def __new__(cls):
        """
        Ensures the class is a singleton, creating a new instance only if one doesn't already exist.

        Returns:
            The single instance of the Hdf5Tree class.
        """
        if cls._instance is None:
            cls._instance = super().__new__(cls)
            logger.debug("Created new instance of Hdf5Tree singleton.")
        return cls._instance

    @classmethod
    @debug_log
    def create(cls, dm_type: Type):
        """
        Creates a dependency tree for the given dataclass type.

        Args:
            dm_type (Type): The dataclass type to generate the dependency tree for.
        """
        cls._tree.create(dm_type)
        logger.debug(f"Dependency tree created for {dm_type.__name__}")

    @classmethod
    @debug_log
    def get_key_from(cls, model_type: Type[T]) -> str:
        """
        Finds the key corresponding to the target dataclass in the dependency tree.

        Args:
            model_type (Type[T]): The dataclass instance to search for.

        Returns:
            str: The key associated with the target dataclass.

        Raises:
            KeyNotFoundError: when a key corresponding to a dataclass is not found in the dependency tree.
        """
        key = cls._tree.find_key(None, cls._tree.nodes, model_type)

        if key is None:
            logger.error(
                f"Key for dataclass {type(model_type).__name__} not found."
            )
            raise KeyNotFoundError(type(model_type).__name__)

        logger.debug(
            f"Found key '{key}' for dataclass {type(model_type).__name__}"
        )
        return key

    @classmethod
    @debug_log
    def find_key_path(cls, key: str, constraint=None):
        """
        Finds the hierarchical path to a given key in the dependency tree.

        Args:
            key (str): The key to search for in the dependency tree.
            constraint (str, optional): A constraint to refine the search. Defaults to None.

        Returns:
            str: The hierarchical path as a string, joined by '/'.

        Raises:
            PathNotFoundError: If no path is found for the given key.
        """
        path = cls._tree.find_key_path(cls._tree.nodes, key, None, constraint)

        if path is None:
            logger.error(
                f"No path found for key '{key}' with constraint '{constraint}'."
            )
            raise PathNotFoundError(key, constraint)

        result_path = "/".join(path)
        return result_path
