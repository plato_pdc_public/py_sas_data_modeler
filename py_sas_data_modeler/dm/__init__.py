# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
"""
The 'dm' package provides business data model for working with Python dataclasses. It supports
type validation and handles complex data structures such as lists, dictionaries,
and unions. The BaseDataClass/BaseRootModel serves as a generic base class for managing these
transformations.

In addition, the DataClassFactory class operates as a singleton registry that manages
dataclass types and their aliases. This module allows for dynamic retrieval of dataclass
instances from string keys and dictionaries, facilitating seamless integration between
various dataclass formats.
"""
import inspect
from typing import Optional

from pydantic import Field

from . import sas
from .dataclass_management import BaseDataClass
from .dataclass_management import BaseRootModel
from .dataclass_management import DataClassFactory
from .dataclass_management import Hdf5Tree

IGNORED_NAMES = frozenset(
    {
        "__builtins__",
        "__cached__",
        "__doc__",
        "__file__",
        "__loader__",
        "__name__",
        "__package__",
        "__spec__",
        "annotations",
        "confloat",
        "Annotated",
    }
)

for name, class_impl in inspect.getmembers(sas):
    if name in IGNORED_NAMES:
        continue
    DataClassFactory.register_dataclass(name, class_impl)

DataClassFactory.register_alias("SAS_PIPELINE", sas.SasPipelineDM.__name__)
DataClassFactory.register_alias(
    "SAS_PIPELINE_HDF5", sas.SasPipelineHdf5DM.__name__
)

Hdf5Tree.create(sas.SasPipelineHdf5DM)


def get_data_model_version(cls) -> Optional[str]:
    """
    Retrieve the default value of the 'data_model_version' field, if it exists.

    Returns:
        Optional[str]: The default value of the 'data_model_version' field, or None if not present.
    """
    field = cls.model_fields.get("data_model_version")
    return field.default if field else None


__DM_VERSION__ = get_data_model_version(sas.MetadataDM)

__all__ = [
    "BaseDataClass",
    "BaseRootModel",
    "DataClassFactory",
    "Hdf5Tree",
    "Field",
]
