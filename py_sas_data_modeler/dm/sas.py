# -*- coding: utf-8 -*-
"""
The `business_data_model` module contains a collection of dataclasses that represent the business data model
for the SAS pipeline workflow. These dataclasses have been automatically generated from a JSON schema, ensuring
a consistent and structured representation of the various data entities involved in the SAS ecosystem.
"""
from __future__ import annotations

from typing import Annotated
from typing import Any
from typing import Dict
from typing import List
from typing import Optional
from typing import Union

from pydantic import ConfigDict
from pydantic import Field
from pydantic import RootModel

from py_sas_data_modeler.dm.dataclass_management import BaseDataClass
from py_sas_data_modeler.dm.dataclass_management import BaseRootModel


class IdpSasRegLcSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    flux: Annotated[List[float], Field(title="flux")]
    """
    Flux normalized to 1
    """
    quarter: Annotated[List[float], Field(title="quarter")]
    """
    Number giving the quarter number of the LC points
    """
    status: Annotated[List[int], Field(title="status")]
    """
    Flag giving the status of the LC points
    """
    time: Annotated[List[float], Field(title="time")]
    """
    Time regularized to cadence in BJD
    """


class IdpSasFlaresFlagSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    merged_flag: Annotated[List[int], Field(title="merged_flag")]
    """
    1 if point involved in flare, 0 otherwise
    """
    time: Annotated[List[float], Field(title="time")]
    """
    Time points of the timeseries in BJD
    """


class IdpEasQcTargetTceDetectionSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    flag: Optional[bool] = None


class EclipseDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    t0: List[float]
    """
    Eclipse centre as MBJD in day
    """
    t1: List[float]
    """
    Start of ingress as MBJD in day
    """
    t2: List[float]
    """
    End of ingress as MBJD in day
    """


class RelativeFluxCurveDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    flux: List[float]


class TCEDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    Period: Optional[List[float]] = None
    """
    Period of eclipses, if known in day
    """


class IdpEasTransitRemovalKitSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    Eclipse: Annotated[Optional[EclipseDM], Field(title="Eclipse")] = None
    """
    Parameters of eclipses
    """
    Relative_flux_curve: Optional[RelativeFluxCurveDM] = None
    TCE: Annotated[
        Optional[TCEDM], Field(title="Threshold Crossing Event")
    ] = None
    """
    A single TCE referencing set of Eclipses
    """


class IdpSasVarlcSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    flux: Annotated[List[float], Field(title="flux")]
    """
    Flux of the timeseries in normalized
    """
    time: Annotated[List[float], Field(title="time")]
    """
    Time points of the timeseries in BJD
    """


class KASOCXlongDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    flux: Annotated[List[float], Field(title="flux")]
    time: Annotated[List[float], Field(title="time")]


class KASOCXshortDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    flux: Annotated[List[float], Field(title="flux")]
    time: Annotated[List[float], Field(title="time")]


class KASOCXtransitDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    flux: Annotated[List[float], Field(title="flux")]
    time: Annotated[List[float], Field(title="time")]


class IdpSasVarlcMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    KADACS_Flags: Annotated[List[List[float]], Field(title="KADACS_Flags")]
    """
    Matrix containing the flags of the regularized vector
    """
    KASOC_K: Annotated[float, Field(title="KASOC_K")]
    """
    smoothing factor for the smoothing of the phase curve (given by P/k) in Time
    """
    KASOC_Nit: Annotated[int, Field(title="KASOC_Nit")]
    """
    number of iterations of entire filtering procedure
    """
    KASOC_Pcut: Annotated[float, Field(title="KASOC_Pcut")]
    """
    maximum period to consider in Time
    """
    KASOC_T_long: Annotated[
        float, Field(alias="KASOC_T-long", title="KASOC_T-long")
    ]
    """
    time scale giving the window width of the long-term median filter in Time
    """
    KASOC_T_short: Annotated[
        float, Field(alias="KASOC_T-short", title="KASOC_T-short")
    ]
    """
    Time
    """
    KASOC_Xlong: Annotated[KASOCXlongDM, Field(title="KASOC_Xlong")]
    """
    the long-term low-passed filtered version of the LC (flux) in Normalized flux
    """
    KASOC_Xshort: Annotated[KASOCXshortDM, Field(title="KASOC_Xshort")]
    """
    Normalized flux
    """
    KASOC_Xtransit: Annotated[KASOCXtransitDM, Field(title="KASOC_Xtransit")]
    """
    the transit light curve used in the correction (flux) in Normalized flux
    """
    Method_used: Annotated[int, Field(title="Method_used")]
    """
    method used for removing the transit: 1= Transit model; 2= KTremove; 3= KADACS; 4= RER
    """
    RER_Poly: Annotated[List[int], Field(title="RER_Poly")]
    """
    polynomial degree
    """
    RER_around: Annotated[List[float], Field(title="RER_around")]
    """
    width around the transit to be removed
    """


class IdpSasVarpsdSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    frequency: Annotated[List[float], Field(title="frequency")]
    """
    Frequency points of the varlc psd in µHz
    """
    power: Annotated[List[float], Field(title="power")]
    """
    Power points of the varlc psd in ppm²/µHz
    """


class IdpSasVarpsdMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    NT: Annotated[int, Field(title="NT")]
    Nyquist: Annotated[float, Field(title="Nyquist")]
    amplitude: Annotated[List[float], Field(title="amplitude")]
    dT: Annotated[float, Field(title="dT")]
    df: Annotated[float, Field(title="df")]
    dt: Annotated[float, Field(title="dt")]
    dutyCycle: Annotated[float, Field(title="dutyCycle")]
    fit_mean: Annotated[bool, Field(title="fit_mean")]
    freqHz: Annotated[List[float], Field(title="freqHz")]
    indx: Annotated[List[bool], Field(title="indx")]
    ls: Annotated[str, Field(title="ls")]
    normfactor: Annotated[float, Field(title="normfactor")]
    power: Annotated[List[float], Field(title="power")]
    power_standard_norm: Annotated[
        List[float], Field(title="power_standard_norm")
    ]
    timeConversion: Annotated[int, Field(title="timeConversion")]


class AdpSasLcNtSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    flux: Annotated[List[float], Field(title="flux")]
    """
    Flux of the timeseries in normalized
    """
    time: Annotated[List[float], Field(title="time")]
    """
    Time points of the timeseries in BJD
    """


class AdpSasLcNtMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    KADACS_Flags: Annotated[List[List[float]], Field(title="KADACS_Flags")]
    """
    Matrix containing the flags of the regularized vector
    """
    KASOC_K: Annotated[float, Field(title="KASOC_K")]
    """
    smoothing factor for the smoothing of the phase curve (given by P/k) in Time
    """
    KASOC_Nit: Annotated[int, Field(title="KASOC_Nit")]
    """
    number of iterations of entire filtering procedure
    """
    KASOC_Pcut: Annotated[float, Field(title="KASOC_Pcut")]
    """
    maximum period to consider in Time
    """
    KASOC_T_long: Annotated[
        float, Field(alias="KASOC_T-long", title="KASOC_T-long")
    ]
    """
    time scale giving the window width of the long-term median filter in Time
    """
    KASOC_T_short: Annotated[
        float, Field(alias="KASOC_T-short", title="KASOC_T-short")
    ]
    """
    Time
    """
    KASOC_Xlong: Annotated[List[float], Field(title="KASOC_Xlong")]
    """
    the long-term low-passed filtered version of the LC (flux) in Normalized flux
    """
    KASOC_Xshort: Annotated[List[float], Field(title="KASOC_Xshort")]
    """
    Normalized flux
    """
    KASOC_Xtransit: Annotated[List[float], Field(title="KASOC_Xtransit")]
    """
    the transit light curve used in the correction (flux) in Normalized flux
    """
    Method_used: Annotated[int, Field(title="Method_used")]
    """
    method used for removing the transit: 1= Transit model; 2= KTremove; 3= KADACS; 4= RER
    """
    RER_Poly: Annotated[List[int], Field(title="RER_Poly")]
    """
    polynomial degree
    """
    RER_around: Annotated[List[float], Field(title="RER_around")]
    """
    width around the transit to be removed
    """


class AdpSasFlaresSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    TBD: Any


class AdpSasFlaresMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    TBD: Any


class TimeItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=24000.0, le=50000.0)]


class AdpSasFlaresFlagSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    flare_flag: Annotated[List[float], Field(title="flare_flag")]
    """
    How many and which algorithms have flagged the point
    """
    time: Annotated[List[TimeItemDM], Field(title="time")]
    """
    BJD
    """


class OutputsMsap103DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_REG_LC: IdpSasRegLcSchemaDM
    """
    Time regularized LC
    """


class Dp1LcMergedSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    time: Optional[List[float]] = None
    """
    time in BJD
    """
    flux: Optional[List[float]] = None
    """
    Light curves in ppm
    """
    quarter: Optional[List[float]] = None


class Dp3SasNuMaxSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(ge=1.0, le=2500.0, title="error")]
    """
    Mean of the mutual difference of the 16th, 50th and 84th percentiles of the PDF of numax in µHz
    """
    value: Annotated[float, Field(ge=1.0, le=5000.0, title="value")]
    """
    Median of the PDF of numax from either the marginalized posterior from MSAP3 03 or, if no peakbagging is done from MSAP3 02, and if no frequency separation is observed from MSAP3 01 in µHz
    """


class IdpSasAarlcSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    flux: Annotated[List[float], Field(title="flux")]
    """
    Flux of the timeseries in normalized
    """
    time: Annotated[List[float], Field(title="time")]
    """
    Time points of the timeseries in BJD
    """


class IdpSasAarlcMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    T_long: Annotated[float, Field(title="T_long")]
    """
    width of the window used in the long-term median filter in Time
    """
    T_short: Annotated[float, Field(title="T_short")]
    """
    width of the window used in the short-term median filter in Time
    """
    quality_flags: Annotated[List[int], Field(title="quality_flags")]
    """
    vector with a bit for every timestamp indicating the source of a potential removal of the point
    """
    sigma_cliping_flag: Annotated[
        bool, Field(alias="sigma-cliping-flag", title="sigma-cliping-flag")
    ]
    """
    flag reporting if sigma clipping has been applied
    """
    sigma_cliping_limit: Annotated[
        float, Field(alias="sigma-cliping-limit", title="sigma-cliping-limit")
    ]
    """
    limit for performing sigma clipping on detrended LC
    """
    unc: Annotated[List[float], Field(title="unc")]
    """
    time-series providing the scatter of the LC (flux) in Normalized flux
    """
    weights: Annotated[List[float], Field(title="weights")]
    """
    weights used in making xfinal from xlong and xshort (value)
    """
    xfinal: Annotated[List[float], Field(title="xfinal")]
    """
    Combined filter if xlong and xshort (flux) in Normalized flux
    """
    xlong: Annotated[List[float], Field(title="xlong")]
    """
    time-series giving the long-term low-passed version of LC (flux) in Normalized flux
    """
    xshort: Annotated[List[float], Field(title="xshort")]
    """
    time-series giving the short-term high-passed version of LC (flux) in Normalized flux
    """


class IdpSasAarpsdSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    frequency: Annotated[List[float], Field(title="frequency")]
    """
    Frequency points of the aarlc psd in µHz
    """
    power: Annotated[List[float], Field(title="power")]
    """
    Power points of the aarlc psd in ppm²/µHz
    """


class IdpSasAarpsdMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    NT: Annotated[int, Field(title="NT")]
    Nyquist: Annotated[float, Field(title="Nyquist")]
    amplitude: Annotated[List[float], Field(title="amplitude")]
    dT: Annotated[float, Field(title="dT")]
    df: Annotated[float, Field(title="df")]
    dt: Annotated[float, Field(title="dt")]
    dutyCycle: Annotated[float, Field(title="dutyCycle")]
    fit_mean: Annotated[bool, Field(title="fit_mean")]
    freqHz: Annotated[List[float], Field(title="freqHz")]
    indx: Annotated[List[bool], Field(title="indx")]
    ls: Annotated[str, Field(title="ls")]
    normfactor: Annotated[float, Field(title="normfactor")]
    power: Annotated[List[float], Field(title="power")]
    power_standard_norm: Annotated[
        List[float], Field(title="power_standard_norm")
    ]
    timeConversion: Annotated[int, Field(title="timeConversion")]


class InputsMsap108DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_VARLC: IdpSasVarlcSchemaDM
    """
    VARLC: (Non-seismic) Variability Analysis-ready LC, stitched, gaps filled, detrended
    """
    IDP_SAS_VARLC_METADATA: IdpSasVarlcMetadataSchemaDM
    """
    Metadata for VARLC: Metadata of the production of the (Non-seismic) Variability Analysis-ready LC
    """


class IdpSasVarlcBinnedSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    bins: Annotated[List[float], Field(title="bins")]
    """
    Time points of the binned LC in BJD
    """
    err_flux: Annotated[List[float], Field(title="err_flux")]
    med_flux: Annotated[List[float], Field(title="med_flux")]
    """
    Flux points of the binned LC in normalized
    """


class IdpSasVarlcBinnedMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    cadence: Annotated[float, Field(title="cadence")]
    step: Annotated[int, Field(title="step")]


class IdpSasVarpsdBinnedSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    frequency: Annotated[List[float], Field(title="frequency")]
    """
    Frequency points in µHz
    """
    power: Annotated[List[float], Field(title="power")]
    """
    Power density points in ppm²/µHz
    """


class IdpSasVarpsdBinnedMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    NT: Annotated[int, Field(title="NT")]
    Nyquist: Annotated[float, Field(title="Nyquist")]
    amplitude: Annotated[List[float], Field(title="amplitude")]
    dT: Annotated[float, Field(title="dT")]
    df: Annotated[float, Field(title="df")]
    dt: Annotated[float, Field(title="dt")]
    dutyCycle: Annotated[float, Field(title="dutyCycle")]
    fit_mean: Annotated[bool, Field(title="fit_mean")]
    freqHz: Annotated[List[float], Field(title="freqHz")]
    indx: Annotated[List[bool], Field(title="indx")]
    ls: Annotated[str, Field(title="ls")]
    normfactor: Annotated[float, Field(title="normfactor")]
    power: Annotated[List[float], Field(title="power")]
    power_standard_norm: Annotated[
        List[float], Field(title="power_standard_norm")
    ]
    timeConversion: Annotated[int, Field(title="timeConversion")]


class InputsMsap109DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_VARLC_BINNED: IdpSasVarlcBinnedSchemaDM
    """
    Binned VARLC: Binned variability analysis ready light curve
    """
    IDP_SAS_VARLC_BINNED_METADATA: IdpSasVarlcBinnedMetadataSchemaDM
    """
    Metadata for the binned VARLC: Metadata of the production of the (Non-seismic) Binned Variability Analysis-ready LC metadata
    """


class IdpSasVarlcFiltBinnedSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    bins: Annotated[List[float], Field(title="bins")]
    """
    Time points of the binned LC in BJD
    """
    med_flux: Annotated[List[float], Field(title="med_flux")]
    """
    Flux points of the binned LC in normalized
    """


class IdpSasVarlcFiltBinnedMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    T_long: Annotated[float, Field(title="T_long")]
    """
    width of the window used in the long-term median filter in Time
    """
    quality_flags: Annotated[List[int], Field(title="quality_flags")]
    """
    vector with a bit for every timestamp indicating the source of a potential removal of the point
    """
    unc: Annotated[List[float], Field(title="unc")]
    """
    time-series providing the scatter of the LC (flux) in Normalized flux
    """
    weights: Annotated[List[float], Field(title="weights")]
    """
    weights used in making xfinal from xlong and xshort (value)
    """
    xfinal: Annotated[List[float], Field(title="xfinal")]
    """
    Combined filter if xlong and xshort (flux) in Normalized flux
    """
    xlong: Annotated[List[float], Field(title="xlong")]
    """
    time-series giving the long-term low-passed version of LC (flux) in Normalized flux
    """
    xshort: Annotated[List[float], Field(title="xshort")]
    """
    time-series giving the short-term high-passed version of LC (flux) in Normalized flux
    """


class IdpSasVarpsdFiltBinnedSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    frequency: Annotated[List[float], Field(title="frequency")]
    """
    Frequency points in µHz
    """
    power: Annotated[List[float], Field(title="power")]
    """
    Power density points in ppm²/µHz
    """


class IdpSasVarpsdFiltBinnedMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    NT: Annotated[int, Field(title="NT")]
    Nyquist: Annotated[float, Field(title="Nyquist")]
    amplitude: Annotated[List[float], Field(title="amplitude")]
    dT: Annotated[float, Field(title="dT")]
    df: Annotated[float, Field(title="df")]
    dt: Annotated[float, Field(title="dt")]
    dutyCycle: Annotated[float, Field(title="dutyCycle")]
    fit_mean: Annotated[bool, Field(title="fit_mean")]
    freqHz: Annotated[List[float], Field(title="freqHz")]
    indx: Annotated[List[bool], Field(title="indx")]
    ls: Annotated[str, Field(title="ls")]
    normfactor: Annotated[float, Field(title="normfactor")]
    power: Annotated[List[float], Field(title="power")]
    power_standard_norm: Annotated[
        List[float], Field(title="power_standard_norm")
    ]
    timeConversion: Annotated[int, Field(title="timeConversion")]


class GaiaBpRpDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    error: Optional[float] = None
    value: Optional[float] = None


class LGPFUOBSDATADM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    gaia_bp_rp: GaiaBpRpDM
    spec_path: List[str]
    spec_read_func: List[str]
    snr_star: List[str]
    rv_shift: List[str]
    rv_shift_err: List[str]


class IdpSasLoggFliperSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    bin_logg: Annotated[List[float], Field(title="bin_logg")]
    """
    bins of logg for posterior probability in dex
    """
    error: Annotated[float, Field(title="error")]
    """
    error on logg value in dex
    """
    nb_star_bin_logg: Annotated[List[int], Field(title="nb_star_bin_logg")]
    """
    number of artificial stars in each bin of logg for posterior probability in dex
    """
    value: Annotated[float, Field(title="value")]
    """
    Surface gravity computed from lightcurve fluctuations in dex
    """


class Dp3SasDeltaNuAvSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(ge=0.01, le=100.0, title="error")]
    """
    Mean of the mutual difference of the 16th, 50th and 84th percentiles of the PDF of delta nu in µHz
    """
    value: Annotated[float, Field(ge=1.0, le=200.0, title="value")]
    """
    Median of the PDF of delta nu from either the marginalized posterior from MSAP3 03 or, if no peakbagging is done from MSAP3 02. in µHz
    """


class IDPPFUTEFFSAPPDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    value: Optional[float] = None
    """
    Value in K
    """
    error: Optional[float] = None
    """
    Error in K
    """


class IdpSasTeffSappSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    """
    error in K
    """
    value: Annotated[float, Field(title="value")]
    """
    value in K
    """


class IDPPFULOGGSAPPDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    value: Optional[float] = None
    """
    Value in dex
    """
    error: Optional[float] = None
    """
    Error in dex
    """


class IdpSasLoggSappSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    """
    error in dex
    """
    value: Annotated[float, Field(title="value")]
    """
    value in dex
    """


class IDPPFUFEHSAPPDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    value: Optional[float] = None
    """
    Value in dex
    """
    error: Optional[float] = None
    """
    error in dex
    """


class IdpSasFeHSappSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    """
    error in dex
    """
    value: Annotated[float, Field(title="value")]
    """
    value in dex
    """


class IdpSasLoggSappTmpSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    """
    error in dex
    """
    value: Annotated[float, Field(title="value")]
    """
    value in dex
    """


class IdpSasTeffSpectroscopySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    """
    error in K
    """
    value: Annotated[float, Field(title="value")]
    """
    value in K
    """


class IdpSasLoggSpectroscopySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    """
    error in dex
    """
    value: Annotated[float, Field(title="value")]
    """
    value in dex
    """


class IdpSasMetadataStatSpectroscopySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    Degrees_of_freedom: Annotated[
        float, Field(alias="Degrees of freedom", title="Degrees of freedom")
    ]
    RV_correction: Annotated[
        float, Field(alias="RV correction", title="RV correction")
    ]
    """
    kms-1
    """
    Reduced_chi2: Annotated[
        float, Field(alias="Reduced chi2", title="Reduced chi2")
    ]


class IdpSasFeHSpectroscopySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    """
    error in dex
    """
    value: Annotated[float, Field(title="value")]
    """
    value in dex
    """


class IdpSasFlagsSpectroscopySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    flags: Annotated[List[str], Field(title="flags")]
    """
    Flags from spectroscopy
    """


class IdpSasAlphaFeSpectroscopySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    """
    error in dex
    """
    value: Annotated[float, Field(title="value")]
    """
    value in dex
    """


class IdpSasMicroturbulenceSpectroscopySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    """
    error in kms-1
    """
    value: Annotated[float, Field(title="value")]
    """
    value in kms-1
    """


class IdpSasVbrdSpectroscopySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    """
    error in kms-1
    """
    value: Annotated[float, Field(title="value")]
    """
    value in kms-1
    """


class IDPSASMGFESPECTROSCOPYDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    value: Annotated[float, Field(title="value")]


class IDPSASMNFESPECTROSCOPYDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    value: Annotated[float, Field(title="value")]


class IDPSASTIFESPECTROSCOPYDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    value: Annotated[float, Field(title="value")]


class IdpSasElem1Elem2SpectroscopySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS__MG_FE__SPECTROSCOPY: Annotated[
        IDPSASMGFESPECTROSCOPYDM,
        Field(
            alias="IDP_SAS_[MG_FE]_SPECTROSCOPY",
            title="IDP_SAS_[MG_FE]_SPECTROSCOPY",
        ),
    ]
    """
    error in dex
    """
    IDP_SAS__MN_FE__SPECTROSCOPY: Annotated[
        IDPSASMNFESPECTROSCOPYDM,
        Field(
            alias="IDP_SAS_[MN_FE]_SPECTROSCOPY",
            title="IDP_SAS_[MN_FE]_SPECTROSCOPY",
        ),
    ]
    """
    error in dex
    """
    IDP_SAS__TI_FE__SPECTROSCOPY: Annotated[
        IDPSASTIFESPECTROSCOPYDM,
        Field(
            alias="IDP_SAS_[TI_FE]_SPECTROSCOPY",
            title="IDP_SAS_[TI_FE]_SPECTROSCOPY",
        ),
    ]
    """
    error in dex
    """


class IdpSasModBestfitSpectroscopySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    mod_bestfit: Annotated[List[List[float]], Field(title="mod_bestfit")]


class IdpSasAlphaFeSappSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    """
    error in dex
    """
    value: Annotated[float, Field(title="value")]
    """
    value in dex
    """


class IdpSasCovmatSappSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    covmat: Annotated[List[List[float]], Field(title="covmat")]
    """
    Covariance matrix for classical parameters
    """
    params: Annotated[List[str], Field(title="params")]
    """
    Parameters of the covariance matrix for classical parameters
    """


class IdpSasMicroturbulenceSappSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[Optional[float], Field(title="error")] = None
    """
    error in kms-1
    """
    value: Annotated[Optional[float], Field(title="value")] = None
    """
    value in kms-1
    """


class IDPSASMGFESAPPDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    value: Annotated[float, Field(title="value")]


class IDPSASMNFESAPPDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    value: Annotated[float, Field(title="value")]


class IDPSASTIFESAPPDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[float, Field(title="error")]
    value: Annotated[float, Field(title="value")]


class IdpSasElem1Elem2SappSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS__MG_FE__SAPP: Annotated[
        Optional[IDPSASMGFESAPPDM],
        Field(alias="IDP_SAS_[MG_FE]_SAPP", title="IDP_SAS_[MG_FE]_SAPP"),
    ] = None
    """
    error in dex
    """
    IDP_SAS__MN_FE__SAPP: Annotated[
        Optional[IDPSASMNFESAPPDM],
        Field(alias="IDP_SAS_[MN_FE]_SAPP", title="IDP_SAS_[MN_FE]_SAPP"),
    ] = None
    """
    error in dex
    """
    IDP_SAS__TI_FE__SAPP: Annotated[
        Optional[IDPSASTIFESAPPDM],
        Field(alias="IDP_SAS_[TI_FE]_SAPP", title="IDP_SAS_[TI_FE]_SAPP"),
    ] = None
    """
    error in dex
    """


class IdpSasVbrdSappSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[Optional[float], Field(title="error")] = None
    """
    error in kms-1
    """
    value: Annotated[Optional[float], Field(title="value")] = None
    """
    value in kms-1
    """


class IDPPFURADIUSCLASSICALDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    value: Optional[float] = None
    """
    Value in dex
    """
    error: Optional[float] = None
    """
    Error in dex
    """


class IdpSasPowerExcessProbabilitySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP3_01_merit: Annotated[
        List[Optional[float]],
        Field(alias="MSAP3_01 merit", title="MSAP3_01 merit"),
    ]
    """
    Power excess test merit values for different frequency bins across the spectrum.
    """
    MSAP3_01_numaxpdf: Annotated[
        List[float],
        Field(alias="MSAP3_01 numaxpdf", title="MSAP3_01 numaxpdf"),
    ]
    """
    probability density function (PDF) of numax given the posterior probability of the envelope power density matching the expected scaling relations.
    """
    Test_frequency: Annotated[
        List[float], Field(alias="Test frequency", title="Test frequency")
    ]
    """
    Frequencies in the AARPS test for the presence of pmode oscillations. in µHz
    """


class IdpSasPowerExcessMetricsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    Amax: Annotated[List[List[float]], Field(title="Amax")]
    """
    Values of predicted maximum envelope amplitude as a function of test frequency with varying degrees of uncertainty. in ppm²/µHz
    """
    Binned_background: Annotated[
        List[float],
        Field(alias="Binned background", title="Binned background"),
    ]
    """
    Binned values of the PSD background level. in ppm²/µHz
    """
    Binned_power: Annotated[
        List[float], Field(alias="Binned power", title="Binned power")
    ]
    """
    Binned values of the PSD. in ppm²/µHz
    """
    H0_likelihood: Annotated[
        List[List[float]], Field(alias="H0 likelihood", title="H0 likelihood")
    ]
    """
    The likelihoood that power in a frequency bin is consistent with the background.
    """
    H1_likelihood: Annotated[
        List[List[float]], Field(alias="H1 likelihood", title="H1 likelihood")
    ]
    """
    The likelihood that the power in a frequency bin is consistent with the expectation from scaling relations.
    """
    H1_posterior: Annotated[
        List[List[float]], Field(alias="H1 posterior", title="H1 posterior")
    ]
    """
    pseudo-posterior calculation of the probability that an envelope is observed at a test frequency.
    """
    Observed_SNR: Annotated[
        List[List[float]], Field(alias="Observed SNR", title="Observed SNR")
    ]
    """
    The binned PSD divided by the binned background level.
    """
    Predicted_SNR: Annotated[
        List[List[float]], Field(alias="Predicted SNR", title="Predicted SNR")
    ]
    """
    The predicted signal-to-noise ratio based on scaling relations.
    """
    Test_frequency: Annotated[
        List[float], Field(alias="Test frequency", title="Test frequency")
    ]
    """
    Frequencies in the AARPS test for the presence of pmode oscillations. in µHz
    """
    Threshold_prior: Annotated[
        List[List[float]],
        Field(alias="Threshold prior", title="Threshold prior"),
    ]
    """
    The probability that the envelope power will exceed a given false-alarm-probability.
    """
    numax_prior: Annotated[
        List[List[float]], Field(alias="numax prior", title="numax prior")
    ]
    """
    The calculated prior on numax based on classical observables.
    """


class InputsMsap302DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_POWER_EXCESS_PROBABILITY: IdpSasPowerExcessProbabilitySchemaDM
    """
    POWER EXCESS PROBABILITY: The probability density of Nu max using MSAP3-03 to assess detection flag
    """
    IDP_SAS_POWER_EXCESS_METRICS: IdpSasPowerExcessMetricsSchemaDM
    """
    POWER EXCESS PROBABILITY: The metrics and criteria used to compute the probability density of Nu max
    """
    IDP_SAS_AARPSD: IdpSasAarpsdSchemaDM
    """
    AARPSD[WP12PDP_I32]: Asteroseismic Analysis-ready power spectrum.Frequency-power spectra of the AARLC (AARPS), including information on the spectral window function
    """
    IDP_SAS_AARPSD_METADATA: IdpSasAarpsdMetadataSchemaDM
    """
    Metadata for AARPSD[WP12PDP_I32]: Metadata for the frequency-power spectra of the AARLC (AARPS), including information on the spectral window function, and the duty cycle
    """
    IDP_PFU_TEFF_SAPP: IDPPFUTEFFSAPPDM
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: Optional[IdpSasTeffSappSchemaDM] = None
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """


class InputsMsap3021DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_POWER_EXCESS_PROBABILITY: IdpSasPowerExcessProbabilitySchemaDM
    """
    POWER EXCESS PROBABILITY: The probability density of Nu max using MSAP3-03 to assess detection flag
    """
    IDP_SAS_POWER_EXCESS_METRICS: IdpSasPowerExcessMetricsSchemaDM
    """
    POWER EXCESS PROBABILITY: The metrics and criteria used to compute the probability density of Nu max
    """
    IDP_SAS_AARPSD: IdpSasAarpsdSchemaDM
    """
    AARPSD[WP12PDP_I32]: Asteroseismic Analysis-ready power spectrum.Frequency-power spectra of the AARLC (AARPS), including information on the spectral window function
    """
    IDP_SAS_AARPSD_METADATA: IdpSasAarpsdMetadataSchemaDM
    """
    Metadata for AARPSD[WP12PDP_I32]: Metadata for the frequency-power spectra of the AARLC (AARPS), including information on the spectral window function, and the duty cycle
    """
    IDP_PFU_TEFF_SAPP: Optional[IDPPFUTEFFSAPPDM] = None
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: IdpSasTeffSappSchemaDM
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """


class IdpSasAcfProbabilitySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    ACF_dnu: Annotated[List[float], Field(alias="ACF dnu", title="ACF dnu")]
    """
    Bins of inverse ACF lag used to test for a large separation. in µHz
    """
    MSAP3_02_dnupdf: Annotated[
        List[Optional[float]],
        Field(alias="MSAP3_02 dnupdf", title="MSAP3_02 dnupdf"),
    ]
    """
    PDF of ∆ν as measured from the collapsed. ACF.
    """
    MSAP3_02_merit: Annotated[
        List[float], Field(alias="MSAP3_02 merit", title="MSAP3_02 merit")
    ]
    """
    p-mode detection metric as measured from the collapsed ACF.
    """
    MSAP3_02_numaxpdf: Annotated[
        List[float],
        Field(alias="MSAP3_02 numaxpdf", title="MSAP3_02 numaxpdf"),
    ]
    """
    PDF of νmax as measured from the collapsed ACF.
    """
    Test_frequency: Annotated[
        List[float], Field(alias="Test frequency", title="Test frequency")
    ]
    """
    Frequencies in the AARPS test for the presence of pmode oscillations. in µHz
    """


class CollapsedACFDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    dnu: Annotated[List[float], Field(title="dnu")]
    numax: Annotated[List[float], Field(title="numax")]


class IdpSasAcfMetricsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    ACF_dnu: Annotated[List[float], Field(alias="ACF dnu", title="ACF dnu")]
    """
    Bins of inverse ACF lag used to test for a large separation. in µHz
    """
    ACF_lag: Annotated[List[float], Field(alias="ACF lag", title="ACF lag")]
    """
    Bins of ACF lag used to test for a large separation. in s
    """
    Effective_Hanning_bins: Annotated[
        List[int],
        Field(alias="Effective Hanning bins", title="Effective Hanning bins"),
    ]
    """
    Effective number of bins of the Hanning filter in terms of bins of the binned AARPS frequencies, accounting for the lower and upper limits of the AARPS.
    """
    Hanning_bins: Annotated[
        List[int], Field(alias="Hanning bins", title="Hanning bins")
    ]
    """
    Number of bins of the Hanning filter in terms of bins of the binned AARPS frequencies.
    """
    Nnu: Annotated[List[int], Field(title="Nnu")]
    """
    Number of bins inside the dnu-numax filter in the frequency (ν) direction at each test lag.
    """
    Ntau: Annotated[List[int], Field(title="Ntau")]
    """
    Number of bins inside the dnu-numax filter in the lag (τ) direction at each test frequency.
    """
    PSD_background: Annotated[
        List[float], Field(alias="PSD background", title="PSD background")
    ]
    """
    Estimate of the PSD background level. in ppm²/µHz
    """
    PSD_bin_factor: Annotated[
        float, Field(alias="PSD bin factor", title="PSD bin factor")
    ]
    """
    Binning factor used to bin the PSD.
    """
    PSD_bin_width: Annotated[
        float, Field(alias="PSD bin width", title="PSD bin width")
    ]
    """
    Width of the PSD frequency bins. in µHz
    """
    Scaling_dnu_limits: Annotated[
        List[List[float]],
        Field(alias="Scaling dnu limits", title="Scaling dnu limits"),
    ]
    """
    Lower and upper bounds on ACF lag as a function of test frequency. in s
    """
    Test_frequency: Annotated[
        List[float], Field(alias="Test frequency", title="Test frequency")
    ]
    """
    Frequencies in the AARPS test for the presence of pmode oscillations. in µHz
    """
    calibration: Annotated[List[float], Field(title="calibration")]
    """
    Calibration values as a function of ACF lag to estimate the noise level.
    """
    calibration_points: Annotated[
        List[List[bool]],
        Field(alias="calibration points", title="calibration points"),
    ]
    """
    Points used to estimate the calibration values for the collapsed ACF noise level.
    """
    collapsed_ACF: Annotated[
        CollapsedACFDM, Field(alias="collapsed ACF", title="collapsed ACF")
    ]
    """
    Values of the 2D ACF collapsed along the lag axis.
    """
    collapsed_ACF_weights: Annotated[
        List[float],
        Field(alias="collapsed ACF weights", title="collapsed ACF weights"),
    ]
    """
    Weights applied along the frequency axis, based on either the power excess metric or the prior on numax.
    """
    logL_dnu: Annotated[
        List[Optional[float]], Field(alias="logL dnu", title="logL dnu")
    ]
    """
    Prior on ∆ν constructed from scaling relations. in µHz
    """
    logL_numax: Annotated[
        List[float], Field(alias="logL numax", title="logL numax")
    ]
    """
    Prior on νmax constructed from scaling relations. in µHz
    """
    numax_limits: Annotated[
        List[float], Field(alias="numax limits", title="numax limits")
    ]
    """
    Lower and upper limits on the frequencies to test for p-modes. in µHz
    """


class InputsMsap303DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_POWER_EXCESS_PROBABILITY: IdpSasPowerExcessProbabilitySchemaDM
    """
    POWER EXCESS PROBABILITY: The probability density of Nu max using MSAP3-03 to assess detection flag
    """
    IDP_SAS_POWER_EXCESS_METRICS: IdpSasPowerExcessMetricsSchemaDM
    """
    POWER EXCESS PROBABILITY: The metrics and criteria used to compute the probability density of Nu max
    """
    IDP_SAS_ACF_PROBABILITY: IdpSasAcfProbabilitySchemaDM
    """
    Autocorrelation function probability: Detection probability using ACF of the timeseries
    """
    IDP_SAS_ACF_METRICS: IdpSasAcfMetricsSchemaDM
    """
    Autocorrelation function metrics: Metrics used when determining the detection probability using ACF of the timeseries
    """
    IDP_SAS_AARPSD: IdpSasAarpsdSchemaDM
    """
    AARPSD[WP12PDP_I32]: Asteroseismic Analysis-ready power spectrum.Frequency-power spectra of the AARLC (AARPS), including information on the spectral window function
    """
    IDP_SAS_AARPSD_METADATA: IdpSasAarpsdMetadataSchemaDM
    """
    Metadata for AARPSD[WP12PDP_I32]: Metadata for the frequency-power spectra of the AARLC (AARPS), including information on the spectral window function, and the duty cycle
    """
    IDP_PFU_TEFF_SAPP: IDPPFUTEFFSAPPDM
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: Optional[IdpSasTeffSappSchemaDM] = None
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    LG_PFU_OBS_DATA: Optional[LGPFUOBSDATADM] = None
    """
    Contains Gaia BP-RP spectrophotometry from final release and Spectra as described in the PDD (under PFU Observed spectrum)
    """


class InputsMsap3031DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_POWER_EXCESS_PROBABILITY: IdpSasPowerExcessProbabilitySchemaDM
    """
    POWER EXCESS PROBABILITY: The probability density of Nu max using MSAP3-03 to assess detection flag
    """
    IDP_SAS_POWER_EXCESS_METRICS: IdpSasPowerExcessMetricsSchemaDM
    """
    POWER EXCESS PROBABILITY: The metrics and criteria used to compute the probability density of Nu max
    """
    IDP_SAS_ACF_PROBABILITY: IdpSasAcfProbabilitySchemaDM
    """
    Autocorrelation function probability: Detection probability using ACF of the timeseries
    """
    IDP_SAS_ACF_METRICS: IdpSasAcfMetricsSchemaDM
    """
    Autocorrelation function metrics: Metrics used when determining the detection probability using ACF of the timeseries
    """
    IDP_SAS_AARPSD: IdpSasAarpsdSchemaDM
    """
    AARPSD[WP12PDP_I32]: Asteroseismic Analysis-ready power spectrum.Frequency-power spectra of the AARLC (AARPS), including information on the spectral window function
    """
    IDP_SAS_AARPSD_METADATA: IdpSasAarpsdMetadataSchemaDM
    """
    Metadata for AARPSD[WP12PDP_I32]: Metadata for the frequency-power spectra of the AARLC (AARPS), including information on the spectral window function, and the duty cycle
    """
    IDP_PFU_TEFF_SAPP: Optional[IDPPFUTEFFSAPPDM] = None
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: IdpSasTeffSappSchemaDM
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    LG_PFU_OBS_DATA: Optional[LGPFUOBSDATADM] = None
    """
    Contains Gaia BP-RP spectrophotometry from final release and Spectra as described in the PDD (under PFU Observed spectrum)
    """


class MSAP302SampleDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=1.0, le=200.0)]


class Dp3SasDeltaNuAvMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP3_02_dnu: Annotated[
        List[float], Field(alias="MSAP3_02 dnu", title="MSAP3_02 dnu")
    ]
    """
    Estimate of delta nu and corresponding uncertainty from MSAP3 2. This is adopted for DP3 SAS NU MAX if no peakbagging is performed but a frequency separation is observed. in µHz
    """
    MSAP3_02_samples: Annotated[
        List[MSAP302SampleDM],
        Field(alias="MSAP3_02 samples", title="MSAP3_02 samples"),
    ]
    """
    Samples drawn from the PDF of delta nu from MSAP3 2 if a large frequency separation is observed. in µHz
    """
    MSAP3_03_samples: Annotated[
        List[float], Field(alias="MSAP3_03 samples", title="MSAP3_03 samples")
    ]
    """
    Samples drawn from the PDF of delta nu from MSAP3 3 if peakbagging is performed. in µHz
    """


class MSAP301SampleDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=1.0, le=5000.0)]


class MSAP302NumaxItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=1.0, le=5000.0)]


class MSAP302Sample1DM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=1.0, le=5000.0)]


class Dp3SasNuMaxMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP3_01_numax: Annotated[
        List[float], Field(alias="MSAP3_01 numax", title="MSAP3_01 numax")
    ]
    """
    Estimate of numax and corresponding uncertainty from MSAP3 1. This is adopted for DP3 SAS NU MAX if no peakbagging is performed and no frequency separation is observed. in µHz
    """
    MSAP3_01_samples: Annotated[
        List[MSAP301SampleDM],
        Field(alias="MSAP3_01 samples", title="MSAP3_01 samples"),
    ]
    """
    Samples drawn from the PDF of numax from MSAP3 1 if a power excess is observed. in µHz
    """
    MSAP3_02_numax: Annotated[
        List[MSAP302NumaxItemDM],
        Field(alias="MSAP3_02 numax", title="MSAP3_02 numax"),
    ]
    """
    Estimate of numax and corresponding uncertainty from MSAP3 2. This is adopted for DP3 SAS NU MAX if no peakbagging is performed but a frequency separation is observed. in µHz
    """
    MSAP3_02_samples: Annotated[
        List[MSAP302Sample1DM],
        Field(alias="MSAP3_02 samples", title="MSAP3_02 samples"),
    ]
    """
    Samples drawn from the PDF of numax from MSAP3 2 if a large frequency separation is observed. in µHz
    """
    MSAP3_03_samples: Annotated[
        List[float], Field(alias="MSAP3_03 samples", title="MSAP3_03 samples")
    ]
    """
    Samples drawn from the PDF of numax from MSAP3 3 if peakbagging is performed. in µHz
    """


class HarveyExponent1DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(title="median")]
    samples: Annotated[List[float], Field(title="samples")]
    uncertainty: Annotated[List[float], Field(title="uncertainty")]


class HarveyExponent2DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(title="median")]
    samples: Annotated[List[float], Field(title="samples")]
    uncertainty: Annotated[List[float], Field(title="uncertainty")]


class HarveyFrequency1DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(title="median")]
    samples: Annotated[List[float], Field(title="samples")]
    uncertainty: Annotated[List[float], Field(title="uncertainty")]


class HarveyFrequency2DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(title="median")]
    samples: Annotated[List[float], Field(title="samples")]
    uncertainty: Annotated[List[float], Field(title="uncertainty")]


class HarveyPowerDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(title="median")]
    samples: Annotated[List[float], Field(title="samples")]
    uncertainty: Annotated[List[float], Field(title="uncertainty")]


class InstrumentalFrequencyDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(title="median")]
    samples: Annotated[List[float], Field(title="samples")]
    uncertainty: Annotated[List[float], Field(title="uncertainty")]


class InstrumentalPowerDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(title="median")]
    samples: Annotated[List[float], Field(title="samples")]
    uncertainty: Annotated[List[float], Field(title="uncertainty")]


class WhiteNoiseLevelDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(title="median")]
    samples: Annotated[List[float], Field(title="samples")]
    uncertainty: Annotated[List[float], Field(title="uncertainty")]


class IdpSasBackgroundFitSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    Harvey_exponent_1: Annotated[
        HarveyExponent1DM,
        Field(alias="Harvey exponent 1", title="Harvey exponent 1"),
    ]
    """
    16th, 50th and 84th percentile differences of the highfrequency granulation term exponent posterior.
    """
    Harvey_exponent_2: Annotated[
        HarveyExponent2DM,
        Field(alias="Harvey exponent 2", title="Harvey exponent 2"),
    ]
    """
    16th, 50th and 84th percentile differences of the lowfrequency granulation term exponent posterior.
    """
    Harvey_frequency_1: Annotated[
        HarveyFrequency1DM,
        Field(alias="Harvey frequency 1", title="Harvey frequency 1"),
    ]
    """
    16th, 50th and 84th percentile differences of the highfrequency granulation term frequency posterior. in µHz
    """
    Harvey_frequency_2: Annotated[
        HarveyFrequency2DM,
        Field(alias="Harvey frequency 2", title="Harvey frequency 2"),
    ]
    """
    16th, 50th and 84th percentile differences of the lowfrequency granulation term frequency posterior. in µHz
    """
    Harvey_power: Annotated[
        HarveyPowerDM, Field(alias="Harvey power", title="Harvey power")
    ]
    """
    16th, 50th and 84th percentile differences of the granulation amplitude posterior. in ppm²/µHz
    """
    Instrumental_frequency: Annotated[
        InstrumentalFrequencyDM,
        Field(alias="Instrumental frequency", title="Instrumental frequency"),
    ]
    """
    16th, 50th and 84th percentile differences of the frequency dependent instrumental term frequency posterior. in µHz
    """
    Instrumental_power: Annotated[
        InstrumentalPowerDM,
        Field(alias="Instrumental power", title="Instrumental power"),
    ]
    """
    16th, 50th and 84th percentile differences of the frequency dependent instrumental term amplitude posterior. in ppm²/µHz
    """
    White_noise_level: Annotated[
        WhiteNoiseLevelDM,
        Field(alias="White noise level", title="White noise level"),
    ]
    """
    16th, 50th and 84th percentile differences of the frequency independent instrumental offset posterior. in ppm²/µHz
    """


class ModeFrequencySamplesDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    l0: Annotated[List[List[float]], Field(title="l0")]
    l1: Annotated[List[List[float]], Field(title="l1")]
    l2: Annotated[List[List[float]], Field(title="l2")]


class IdpSasFrequenciesFirstGuessSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    mode_frequency: Annotated[
        List[float], Field(alias="mode frequency", title="mode frequency")
    ]
    """
    Median of the marginalized frequency posterior distribution of modes observed in the AARPS, ordered according to the frequencies in IDP SAS FREQUENCIES FIRST GUESS in µHz
    """
    mode_frequency_samples: Annotated[
        ModeFrequencySamplesDM,
        Field(alias="mode frequency samples", title="mode frequency samples"),
    ]
    """
    Samples of the frequency posterior distribution, from which mode frequency and uncertainty columns are derived in µHz
    """
    mode_frequency_uncertainty: Annotated[
        List[float],
        Field(
            alias="mode frequency uncertainty",
            title="mode frequency uncertainty",
        ),
    ]
    """
    Median absolute deviation of the marginalized frequency posterior distribution, scaled to be 1 sigma for a normal distribution in µHz
    """


class IdpSasFrequenciesFirstGuessMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    angular_degree: Annotated[
        List[int], Field(alias="angular degree", title="angular degree")
    ]
    """
    Angular degree of the modes observed in the AARPS, ordered according to the frequencies in IDP SAS FREQUENCIES FIRST GUESS.
    """
    mixedModeFlag: Annotated[bool, Field(title="mixedModeFlag")]
    """
    Flag for whether or not mixed modes are present in the set of identified modes.
    """
    p_mode_radial_order: Annotated[
        List[float],
        Field(alias="p mode radial order", title="p mode radial order"),
    ]
    """
    p-mode radial order of the modes observed in the AARPS, ordered according to the frequencies in IDP SAS FREQUENCIES FIRST GUESS.
    """
    zeta: Annotated[List[float], Field(title="zeta")]
    """
    Degree of mixing of the modes in the AARPS, ordered according to the frequencies in IDP SAS FREQUENCIES FIRST GUESS
    """


class ModeHeightSamplesDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    l0: Annotated[List[List[float]], Field(title="l0")]
    l1: Annotated[List[List[float]], Field(title="l1")]
    l2: Annotated[List[List[float]], Field(title="l2")]


class IdpSasOscModeHeightsFirstGuessSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    mode_height: Annotated[
        List[float], Field(alias="mode height", title="mode height")
    ]
    """
    Median of the marginalized height posterior distribution of modes observed in the AARPS, ordered according to the frequencies in IDP SAS FREQUENCIES FIRST GUESS in ppm²/µHz
    """
    mode_height_samples: Annotated[
        ModeHeightSamplesDM,
        Field(alias="mode height samples", title="mode height samples"),
    ]
    """
    Samples of the height posterior distribution, from which mode height and uncertainty columns are derived in ppm²/µHz
    """
    mode_height_uncertainty: Annotated[
        List[float],
        Field(
            alias="mode height uncertainty", title="mode height uncertainty"
        ),
    ]
    """
    Median absolute deviation of the marginalized height posterior distribution, scaled to be 1 sigma for a normal distribution in ppm²/µHz
    """


class ModeWidthSamplesDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    l0: Annotated[List[List[float]], Field(title="l0")]
    l1: Annotated[List[List[float]], Field(title="l1")]
    l2: Annotated[List[List[float]], Field(title="l2")]


class IdpSasOscModeWidthsFirstGuessSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    mode_width: Annotated[
        List[float], Field(alias="mode width", title="mode width")
    ]
    """
    Median of the marginalized width posterior distribution of modes observed in the AARPS, ordered according to the frequencies in IDP SAS FREQUENCIES FIRST GUESS in µHz
    """
    mode_width_samples: Annotated[
        ModeWidthSamplesDM,
        Field(alias="mode width samples", title="mode width samples"),
    ]
    """
    Samples of the width posterior distribution, from which mode width and uncertainty columns are derived in µHz
    """
    mode_width_uncertainty: Annotated[
        List[float],
        Field(alias="mode width uncertainty", title="mode width uncertainty"),
    ]
    """
    Median absolute deviation of the marginalized width posterior distribution, scaled to be 1 sigma for a normal distribution in µHz
    """


class IdpSasMultidetectionMetricsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(title="median")]
    """
    Posterior median of the multidetection metric
    """
    samples: Annotated[List[float], Field(title="samples")]
    """
    Posterior samples of the multidetection metric
    """
    uncertainty: Annotated[List[float], Field(title="uncertainty")]
    """
    16th, 50th and 84th percentile differences of the multidetection metric
    """


class MSAP301ThresholdsDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    double: Annotated[float, Field(title="double")]
    single: Annotated[float, Field(title="single")]


class MSAP302ThresholdsDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    double: Annotated[float, Field(title="double")]
    single: Annotated[float, Field(title="single")]


class IdpSasDetectionMetricsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP3_01_merit: Annotated[
        List[float], Field(alias="MSAP3_01 merit", title="MSAP3_01 merit")
    ]
    """
    Power excess test merit values for different frequency bins across the spectrum.
    """
    MSAP3_01_thresholds: Annotated[
        MSAP301ThresholdsDM,
        Field(alias="MSAP3_01 thresholds", title="MSAP3_01 thresholds"),
    ]
    """
    Required threshold for the power excess merit to yield a detection if used without MSAP3 02.
    """
    MSAP3_02_merit: Annotated[
        List[float], Field(alias="MSAP3_02 merit", title="MSAP3_02 merit")
    ]
    """
    Large separation test merit values for different frequency bins across the spectrum.
    """
    MSAP3_02_thresholds: Annotated[
        MSAP302ThresholdsDM,
        Field(alias="MSAP3_02 thresholds", title="MSAP3_02 thresholds"),
    ]
    """
    Required threshold for the large separation merit to yield a detection if used without MSAP3 01.
    """


class IdpSasSeismicDetectionFlagSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP3_01_flag: Annotated[bool, Field(title="MSAP3_01_flag")]
    """
    Power excess detection flag. True, if a threshold crossing event is detected using the single-module threshold for MSAP3 01.
    """
    MSAP3_02_flag: Annotated[bool, Field(title="MSAP3_02_flag")]
    """
    Large separation detection flag. True, if a threshold crossing event is detected using the single-module threshold for MSAP3 02.
    """
    osc_detection_flag: Annotated[bool, Field(title="osc_detection_flag")]
    """
    MSAP3 01 flag, MSAP3 02 flag combination. True, if a threshold crossing event is detected using the double-module thresholds for MSAP3 01 and MSAP3 02.
    """
    peakbagging_flag: Annotated[bool, Field(title="peakbagging_flag")]
    """
    Peak-bagging flag
    """


class InputsMsap304DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_AARPSD: IdpSasAarpsdSchemaDM
    """
    AARPSD[WP12PDP_I32]: Asteroseismic Analysis-ready power spectrum.Frequency-power spectra of the AARLC (AARPS), including information on the spectral window function
    """
    IDP_SAS_AARPSD_METADATA: IdpSasAarpsdMetadataSchemaDM
    """
    Metadata for AARPSD[WP12PDP_I32]: Metadata for the frequency-power spectra of the AARLC (AARPS), including information on the spectral window function, and the duty cycle
    """
    DP3_SAS_DELTA_NU_AV: Dp3SasDeltaNuAvSchemaDM
    """
    Average large frequency separation: Average large frequency separation from global analysis of Asteroseismic Analysis Ready Power spectrum (AARPS), or from peak bagging wen available (optional)
    """
    DP3_SAS_DELTA_NU_AV_METADATA: Dp3SasDeltaNuAvMetadataSchemaDM
    """
    Metadata on the average large frequency separation: Metadata on the average large frequency separation, including model and range for estimate (optional)
    """
    DP3_SAS_NU_MAX: Dp3SasNuMaxSchemaDM
    """
    Frequency of maximum oscillations power: Frequency of maximum oscillations power from global analysis of AARPS, or from peak bagging if available (optional)
    """
    DP3_SAS_NU_MAX_METADATA: Dp3SasNuMaxMetadataSchemaDM
    """
    Metadata on the frequency of maximum oscillations power: Metadata on frequency of maximum oscillations power, including model for estimate (optional)
    """
    IDP_SAS_BACKGROUND_FIT: IdpSasBackgroundFitSchemaDM
    """
    Back-ground fit : Background fit (model and parameter)
    """
    IDP_SAS_FREQUENCIES_FIRST_GUESS: IdpSasFrequenciesFirstGuessSchemaDM
    """
    IDP_SAS_FREQUENCIES_FIRST_GUESS
    """
    IDP_SAS_FREQUENCIES_FIRST_GUESS_METADATA: (
        IdpSasFrequenciesFirstGuessMetadataSchemaDM
    )
    """
    First-guess frequency list metadata: Metadata on the first-guess frequency list from peak-bagging preparation (optional)
    """
    IDP_SAS_OSC_MODE_HEIGHTS_FIRST_GUESS: (
        IdpSasOscModeHeightsFirstGuessSchemaDM
    )
    """
    First-guess oscillation modes heights: First-guess oscillation modes heights from peak-bagging preparation (optional)
    """
    IDP_SAS_OSC_MODE_WIDTHS_FIRST_GUESS: IdpSasOscModeWidthsFirstGuessSchemaDM
    """
    First-guess oscillation modes widths: First-guess oscillation modes widths from peak-bagging preparation (optional)
    """


class Dp3SasOscFreqSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[List[float], Field(title="median")]
    """
    Posterior median of the mode frequencies in ppm2/µHz
    """
    samples: Annotated[List[List[float]], Field(title="samples")]
    """
    Posterior samples of the mode frequencies in ppm2/µHz
    """
    uncertainty: Annotated[List[List[float]], Field(title="uncertainty")]
    """
    16th, 50th and 84th percentile differences of the mode frequencies posteriors in ppm²/µHz
    """


class Dp3SasOscFreqCovmatSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    ocs_freq_covmat: Annotated[
        List[List[float]], Field(title="ocs_freq_covmat")
    ]
    """
    Covariance matrix for the fitted frequencies in µHz²
    """


class Dp3SasOscFreqMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    angular_degrees: Annotated[
        List[int], Field(alias="angular degrees", title="angular degrees")
    ]
    """
    Angular degrees of the modes included in the peakbagging
    """
    mixedModeFlag: Annotated[bool, Field(title="mixedModeFlag")]
    """
    Flag for whether or not mixed modes are present in the set of identified modes.
    """
    p_mode_radial_order: Annotated[
        List[float],
        Field(alias="p mode radial order", title="p mode radial order"),
    ]
    """
    p-mode radial order of the modes observed in the AARPS, ordered according to the frequencies in IDP SAS FREQUENCIES FIRST GUESS.
    """
    zeta: Annotated[List[float], Field(title="zeta")]
    """
    Degree of mixing of the modes in the AARPS, ordered according to the frequencies in IDP SAS FREQUENCIES FIRST GUESS
    """


class SampleDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10.0)]


class UncertaintyItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10.0)]


class HarveyExponent11DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(ge=0.0, le=10.0, title="median")]
    samples: Annotated[List[SampleDM], Field(title="samples")]
    uncertainty: Annotated[List[UncertaintyItemDM], Field(title="uncertainty")]


class HarveyExponent21DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(ge=0.0, le=10.0, title="median")]
    samples: Annotated[List[SampleDM], Field(title="samples")]
    uncertainty: Annotated[List[UncertaintyItemDM], Field(title="uncertainty")]


class HarveyFrequency21DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(ge=0.0, le=10.0, title="median")]
    samples: Annotated[List[SampleDM], Field(title="samples")]
    uncertainty: Annotated[List[float], Field(title="uncertainty")]


class Sample3DM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10000.0)]


class UncertaintyItem2DM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10000.0)]


class HarveyPower1DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(ge=0.0, le=10000.0, title="median")]
    samples: Annotated[List[Sample3DM], Field(title="samples")]
    uncertainty: Annotated[
        List[UncertaintyItem2DM], Field(title="uncertainty")
    ]


class Sample4DM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=6000.0)]


class InstrumentalFrequency1DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(ge=0.0, le=6000.0, title="median")]
    samples: Annotated[List[Sample4DM], Field(title="samples")]
    uncertainty: Annotated[List[float], Field(title="uncertainty")]


class Sample5DM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10000.0)]


class InstrumentalPower1DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(ge=0.0, le=10000.0, title="median")]
    samples: Annotated[List[Sample5DM], Field(title="samples")]
    uncertainty: Annotated[
        List[UncertaintyItem2DM], Field(title="uncertainty")
    ]


class WhiteNoiseLevel1DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(ge=0.0, le=10000.0, title="median")]
    samples: Annotated[List[Sample5DM], Field(title="samples")]
    uncertainty: Annotated[
        List[UncertaintyItem2DM], Field(title="uncertainty")
    ]


class AdpSasBackgroundBestfitSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    Harvey_exponent_1: Annotated[
        HarveyExponent11DM,
        Field(alias="Harvey exponent 1", title="Harvey exponent 1"),
    ]
    """
    16th, 50th and 84th percentile differences of the highfrequency granulation term exponent posterior.
    """
    Harvey_exponent_2: Annotated[
        HarveyExponent21DM,
        Field(alias="Harvey exponent 2", title="Harvey exponent 2"),
    ]
    """
    16th, 50th and 84th percentile differences of the lowfrequency granulation term exponent posterior.
    """
    Harvey_frequency_1: Annotated[
        HarveyFrequency1DM,
        Field(alias="Harvey frequency 1", title="Harvey frequency 1"),
    ]
    """
    16th, 50th and 84th percentile differences of the highfrequency granulation term frequency posterior. in µHz
    """
    Harvey_frequency_2: Annotated[
        HarveyFrequency21DM,
        Field(alias="Harvey frequency 2", title="Harvey frequency 2"),
    ]
    """
    16th, 50th and 84th percentile differences of the lowfrequency granulation term frequency posterior. in µHz
    """
    Harvey_power: Annotated[
        HarveyPower1DM, Field(alias="Harvey power", title="Harvey power")
    ]
    """
    16th, 50th and 84th percentile differences of the granulation amplitude posterior. in ppm²/µHz
    """
    Instrumental_frequency: Annotated[
        InstrumentalFrequency1DM,
        Field(alias="Instrumental frequency", title="Instrumental frequency"),
    ]
    """
    16th, 50th and 84th percentile differences of the frequency dependent instrumental term frequency posterior. in µHz
    """
    Instrumental_power: Annotated[
        InstrumentalPower1DM,
        Field(alias="Instrumental power", title="Instrumental power"),
    ]
    """
    16th, 50th and 84th percentile differences of the frequency dependent instrumental term amplitude posterior. in ppm²/µHz
    """
    White_noise_level: Annotated[
        WhiteNoiseLevel1DM,
        Field(alias="White noise level", title="White noise level"),
    ]
    """
    16th, 50th and 84th percentile differences of the frequency independent instrumental offset posterior. in ppm²/µHz
    """


class AdpSasModeHeightsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[List[float], Field(title="median")]
    """
    Posterior median of the mode heights in ppm²/µHz
    """
    samples: Annotated[List[List[float]], Field(title="samples")]
    """
    Posterior samples of the mode heights in ppm²/µHz
    """
    uncertainty: Annotated[List[List[float]], Field(title="uncertainty")]
    """
    16th, 50th and 84th percentile differences of the mode height posteriors in ppm²/µHz
    """


class AdpSasModeWidthsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[List[float], Field(title="median")]
    """
    Posterior median of the mode widths in µHz
    """
    samples: Annotated[List[List[float]], Field(title="samples")]
    """
    Posterior samples of the mode widths in µHz
    """
    uncertainty: Annotated[List[List[float]], Field(title="uncertainty")]
    """
    16th, 50th and 84th percentile differences of the mode width posteriors in µHz
    """


class AdpSasModePowersSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[List[float], Field(title="median")]
    """
    Posterior median of the mode power in ppm²
    """
    samples: Annotated[List[List[float]], Field(title="samples")]
    """
    Posterior samples of the mode power in ppm²
    """
    uncertainty: Annotated[List[List[float]], Field(title="uncertainty")]
    """
    16th, 50th and 84th percentile differences of the mode power posteriors in ppm²
    """


class AdpSasPeakAsymmetriesSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[List[float], Field(title="median")]
    """
    Posterior median of the mode asymmetry
    """
    samples: Annotated[List[List[float]], Field(title="samples")]
    """
    Posterior samples of the mode asymmetry
    """
    uncertainty: Annotated[List[List[float]], Field(title="uncertainty")]
    """
    16th, 50th and 84th percentile differences of the mode asymmetry posterior
    """


class IdpSasSeismicQualityMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    Quality_control_chains: Annotated[
        List[List[float]],
        Field(alias="Quality control chains", title="Quality control chains"),
    ]
    """
    Position in parameter space of each walker at each step in the MCMC chain.
    """
    Total_steps: Annotated[
        int, Field(alias="Total steps", title="Total steps")
    ]
    """
    Total number of steps taken by the MCMC sampler
    """
    fit_PSD_range: Annotated[
        List[float], Field(alias="fit PSD range", title="fit PSD range")
    ]
    """
    PSD for the frequency bins in the range considered in the fit
    """
    fit_frequency_range: Annotated[
        List[float],
        Field(alias="fit frequency range", title="fit frequency range"),
    ]
    """
    Frequency bins in the range considered in the fit.
    """
    nmodes: Annotated[int, Field(title="nmodes")]
    """
    Total number of modes in the fit
    """


class IdpSasCovmatAllParametersSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    covmat_from_peakbagging: Annotated[
        List[List[float]], Field(title="covmat_from_peakbagging")
    ]
    """
    Covariance matrix for all parameters from peakbagging
    """


class IdpSasSplittingsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(title="median")]
    """
    Posterior median of the frequency splittings in µHz
    """
    samples: Annotated[List[float], Field(title="samples")]
    """
    Posterior samples of the frequency splittings in µHz
    """
    uncertainty: Annotated[List[float], Field(title="uncertainty")]
    """
    16th, 50th and 84th percentile differences of the frequency splitting posteriors in µHz
    """


class IdpSasInclinationAnglePeakbaggingSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(title="median")]
    """
    Posterior median of the inclination angle. in radians
    """
    samples: Annotated[List[float], Field(title="samples")]
    """
    Posterior samples of the inclination angle. in radians
    """
    uncertainty: Annotated[List[float], Field(title="uncertainty")]
    """
    16th, 50th and 84th percentile differences of inclination angle posterior. in radians
    """


class InputsMsap401DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_VARLC: IdpSasVarlcSchemaDM
    """
    VARLC: (Non-seismic) Variability Analysis-ready LC, stitched, gaps filled, detrended
    """
    IDP_SAS_VARLC_METADATA: IdpSasVarlcMetadataSchemaDM
    """
    Metadata for VARLC: Metadata of the production of the (Non-seismic) Variability Analysis-ready LC
    """
    IDP_SAS_VARPSD_BINNED: IdpSasVarpsdBinnedSchemaDM
    """
    Variability analysis ready Power spectrum from binned lightcurve
    """
    IDP_SAS_VARPSD_FILT_BINNED: IdpSasVarpsdFiltBinnedSchemaDM
    """
    Variability analysis ready Power spectrum from filtered and binned lightcurve
    """
    IDP_SAS_VARPSD_BINNED_METADATA: IdpSasVarpsdBinnedMetadataSchemaDM
    """
    Metadata for the VARPSD binned
    """
    IDP_SAS_VARPSD_FILT_BINNED_METADATA: IdpSasVarpsdFiltBinnedMetadataSchemaDM
    """
    Metadata for the VARPSD from filtered and binned
    """
    IDP_PFU_TEFF_SAPP: IDPPFUTEFFSAPPDM
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_PFU_LOGG_SAPP: IDPPFULOGGSAPPDM
    """
    Value of log g selected after statistical analysis: combination of values yielded from the SAPP pipeline
    """
    IDP_SAS_TEFF_SAPP: Optional[IdpSasTeffSappSchemaDM] = None
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_SAS_LOGG_SAPP: Optional[IdpSasLoggSappSchemaDM] = None
    """
    Combined value for log g: Value of log g selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """


class InputsMsap4011DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_VARLC: IdpSasVarlcSchemaDM
    """
    VARLC: (Non-seismic) Variability Analysis-ready LC, stitched, gaps filled, detrended
    """
    IDP_SAS_VARLC_METADATA: IdpSasVarlcMetadataSchemaDM
    """
    Metadata for VARLC: Metadata of the production of the (Non-seismic) Variability Analysis-ready LC
    """
    IDP_SAS_VARPSD_BINNED: IdpSasVarpsdBinnedSchemaDM
    """
    Variability analysis ready Power spectrum from binned lightcurve
    """
    IDP_SAS_VARPSD_FILT_BINNED: IdpSasVarpsdFiltBinnedSchemaDM
    """
    Variability analysis ready Power spectrum from filtered and binned lightcurve
    """
    IDP_SAS_VARPSD_BINNED_METADATA: IdpSasVarpsdBinnedMetadataSchemaDM
    """
    Metadata for the VARPSD binned
    """
    IDP_SAS_VARPSD_FILT_BINNED_METADATA: IdpSasVarpsdFiltBinnedMetadataSchemaDM
    """
    Metadata for the VARPSD from filtered and binned
    """
    IDP_PFU_TEFF_SAPP: Optional[IDPPFUTEFFSAPPDM] = None
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_PFU_LOGG_SAPP: Optional[IDPPFULOGGSAPPDM] = None
    """
    Value of log g selected after statistical analysis: combination of values yielded from the SAPP pipeline
    """
    IDP_SAS_TEFF_SAPP: IdpSasTeffSappSchemaDM
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_SAS_LOGG_SAPP: IdpSasLoggSappSchemaDM
    """
    Combined value for log g: Value of log g selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """


class Dp4SasHarvey1AmplitudeSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(ge=0.0, le=1000000.0, title="median")]
    """
    median value in ppm
    """
    uncertainty: Annotated[
        float, Field(ge=0.0, le=1000000.0, title="uncertainty")
    ]
    """
    1-sigma uncertainty in ppm
    """


class Dp4SasHarvey1TimeSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(ge=0.0, le=100000000.0, title="median")]
    """
    median value in sec
    """
    uncertainty: Annotated[
        float, Field(ge=0.0, le=100000000.0, title="uncertainty")
    ]
    """
    1-sigma uncertainty in sec
    """


class Dp4SasHarvey1ExponentSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    uncertainty: Annotated[float, Field(ge=0.0, le=10.0, title="uncertainty")]
    """
    1-sigma uncertainty
    """
    value: Annotated[float, Field(ge=0.0, le=10.0, title="value")]
    """
    value of the exponent
    """


class Dp4SasHarvey2AmplitudeSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(ge=0.0, le=1000000.0, title="median")]
    """
    median value in ppm
    """
    uncertainty: Annotated[
        float, Field(ge=0.0, le=1000000.0, title="uncertainty")
    ]
    """
    1-sigma uncertainty in ppm
    """


class Dp4SasHarvey2TimeSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(ge=0.0, le=100000000.0, title="median")]
    """
    median value in sec
    """
    uncertainty: Annotated[
        float, Field(ge=0.0, le=100000000.0, title="uncertainty")
    ]
    """
    1-sigma uncertainty in sec
    """


class Dp4SasHarvey2ExponentSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    uncertainty: Annotated[float, Field(ge=0.0, le=10.0, title="uncertainty")]
    """
    1-sigma uncertainty
    """
    value: Annotated[float, Field(ge=0.0, le=10.0, title="value")]
    """
    value of the exponent
    """


class Dp4SasHarvey3AmplitudeSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(ge=0.0, le=1000000.0, title="median")]
    """
    median value in ppm
    """
    uncertainty: Annotated[
        float, Field(ge=0.0, le=1000000.0, title="uncertainty")
    ]
    """
    1-sigma uncertainty in ppm
    """


class Dp4SasHarvey3TimeSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(ge=0.0, le=1000000.0, title="median")]
    """
    median value in sec
    """
    uncertainty: Annotated[
        float, Field(ge=0.0, le=1000000.0, title="uncertainty")
    ]
    """
    1-sigma uncertainty in sec
    """


class Dp4SasHarvey3ExponentSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    uncertainty: Annotated[float, Field(ge=0.0, le=10.0, title="uncertainty")]
    """
    1-sigma uncertainty
    """
    value: Annotated[float, Field(ge=0.0, le=10.0, title="value")]
    """
    value of the exponent
    """


class Dp4SasWhiteNoiseFourierSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    median: Annotated[float, Field(ge=0.0, le=100.0, title="median")]
    """
    median value in ppm²/µHz
    """
    uncertainty: Annotated[float, Field(ge=0.0, le=100.0, title="uncertainty")]
    """
    1-sigma uncertainty in ppm²/µHz
    """


class Dp4SasHarveyMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    flag_error1: Annotated[int, Field(title="flag_error1")]
    """
    a flag specifying whether a likelihood evaluation error occurred during the computation of the sampling
    """
    flag_error2: Annotated[int, Field(title="flag_error2")]
    """
    a flag specifying whether an ellipsoidal matrix decomposition error occurred during the computation of the sampling
    """
    flag_harvey1: Annotated[int, Field(title="flag_harvey1")]
    """
    a flag specifying whether the first (low-frequency) Harvey component is detected
    """
    flag_harvey2: Annotated[int, Field(title="flag_harvey2")]
    """
    a flag specifying whether the second (mid-frequency) Harvey component is detected
    """
    flag_harvey3: Annotated[int, Field(title="flag_harvey3")]
    """
    a flag specifying whether the third (high-frequency) Harvey component is detected
    """
    high_frequency_threshold_cut: Annotated[
        float,
        Field(
            alias="high-frequency threshold cut",
            title="high-frequency threshold cut",
        ),
    ]
    """
    value of the high-frequency threshold cut (10 mHz by default) in mHz
    """
    low_frequency_threshold_cut: Annotated[
        float,
        Field(
            alias="low-frequency threshold cut",
            title="low-frequency threshold cut",
        ),
    ]
    """
    value of the low-frequency threshold cut (0.01 µHz by default) in µHz
    """
    model_name: Annotated[str, Field(title="model_name")]
    """
    Name of the background model fitted by DIAMONDS+Background
    """
    nuMax: Annotated[float, Field(title="nuMax")]
    """
    input guess for ν max or equivalent in case of no oscillations in µHz
    """
    ratio: Annotated[float, Field(title="ratio")]
    """
    ratio of the remainder to collected evidence provided by DIAMONDS
    """


class IdpSasLongtermModulationFourierSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    lower_uncertainty: Annotated[List[float], Field(title="lower_uncertainty")]
    """
    lower uncertainty in sec
    """
    pFA: Annotated[List[float], Field(title="pFA")]
    """
    Corresponding False Alarm Probability
    """
    power: Annotated[List[float], Field(title="power")]
    """
    Corresponding power
    """
    upper_uncertainty: Annotated[List[float], Field(title="upper_uncertainty")]
    """
    upper uncertainty in sec
    """
    values: Annotated[List[float], Field(title="values")]
    """
    Periods of long-term flux modulations in sec
    """


class IdpSasProtFourierSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    lower_uncertainty: Annotated[List[float], Field(title="lower_uncertainty")]
    """
    lower uncertainty in sec
    """
    pFA: Annotated[List[float], Field(title="pFA")]
    """
    Corresponding False Alarm Probability
    """
    power: Annotated[List[float], Field(title="power")]
    """
    Corresponding power
    """
    upper_uncertainty: Annotated[List[float], Field(title="upper_uncertainty")]
    """
    upper uncertainty in sec
    """
    values: Annotated[List[float], Field(title="values")]
    """
    Periods likely arising from rotation in sec
    """


class InputsMsap402DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_VARLC_BINNED: IdpSasVarlcBinnedSchemaDM
    """
    Binned VARLC: Binned variability analysis ready light curve
    """
    IDP_SAS_VARLC_BINNED_METADATA: IdpSasVarlcBinnedMetadataSchemaDM
    """
    Metadata for the binned VARLC: Metadata of the production of the (Non-seismic) Binned Variability Analysis-ready LC metadata
    """
    IDP_SAS_VARLC_FILT_BINNED: IdpSasVarlcFiltBinnedSchemaDM
    """
    Filtered and binned VARLC, long term filtering for removing variabilities larger than 55 days
    """
    IDP_SAS_VARPSD_FILT_BINNED_METADATA: IdpSasVarpsdFiltBinnedMetadataSchemaDM
    """
    Metadata for the VARPSD from filtered and binned
    """


class IdpSasProtTimeseriesSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    G_ACF: Annotated[List[float], Field(title="G_ACF")]
    """
    Corresponding global maximum of the smoothed ACF
    """
    H_ACF: Annotated[List[float], Field(title="H_ACF")]
    """
    Corresponding height of the peak in the smoothed ACF
    """
    lower_uncertainty: Annotated[List[float], Field(title="lower_uncertainty")]
    """
    lower uncertainty in sec
    """
    peak_sequential_number: Annotated[
        List[float], Field(title="peak_sequential_number")
    ]
    """
    Peak sequential number by which to divide column 1 to get the cycle length
    """
    upper_uncertainty: Annotated[List[float], Field(title="upper_uncertainty")]
    """
    upper uncertainty in sec
    """
    values: Annotated[List[float], Field(title="values")]
    """
    Periods corresponding to the local maxima below a period of 45 days of the ACF in sec
    """


class IdpSasAcfFiltTimeseriesSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    ACF: Annotated[List[float], Field(title="ACF")]
    """
    AutoCorrelation Function
    """
    time_lag: Annotated[List[float], Field(title="time_lag")]
    """
    Time lag in sec
    """


class IdpSasLongtermModulationTimeseriesSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    G_ACF: Annotated[List[float], Field(title="G_ACF")]
    """
    Corresponding global maximum of the smoothed ACF
    """
    H_ACF: Annotated[List[float], Field(title="H_ACF")]
    """
    Corresponding height of the peak in the smoothed ACF
    """
    lower_uncertainty: Annotated[List[float], Field(title="lower_uncertainty")]
    """
    lower uncertainty in sec
    """
    peak_sequential_number: Annotated[
        List[float], Field(title="peak_sequential_number")
    ]
    """
    Peak sequential number by which to divide column 1 to get the rotation period
    """
    upper_uncertainty: Annotated[List[float], Field(title="upper_uncertainty")]
    """
    upper uncertainty in sec
    """
    values: Annotated[List[float], Field(title="values")]
    """
    Periods corresponding to the local maxima below a period of 45 days of the ACF in sec
    """


class IdpSasAcfTimeseriesSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    ACF: Annotated[List[float], Field(title="ACF")]
    """
    AutoCorrelation Function
    """
    time_lag: Annotated[List[float], Field(title="time_lag")]
    """
    Time lag in sec
    """


class InputsMsap403DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_PROT_FOURIER: IdpSasProtFourierSchemaDM
    """
    Rotation period: from Fourier analysis
    """
    IDP_SAS_PROT_TIMESERIES: IdpSasProtTimeseriesSchemaDM
    """
    Rotation period: from time-series analysis ( below a period of 45 days)
    """
    IDP_SAS_ACF_FILT_TIMESERIES: IdpSasAcfFiltTimeseriesSchemaDM
    """
    Autocorrelation Function of the filtered binned LC
    """
    IDP_SAS_VARLC_FILT_BINNED: IdpSasVarlcFiltBinnedSchemaDM
    """
    Filtered and binned VARLC, long term filtering for removing variabilities larger than 55 days
    """
    IDP_SAS_VARLC_FILT_BINNED_METADATA: IdpSasVarlcFiltBinnedMetadataSchemaDM
    """
    Metadata for the binned filtered VARLC
    """
    IDP_SAS_VARPSD_FILT_BINNED: IdpSasVarpsdFiltBinnedSchemaDM
    """
    Variability analysis ready Power spectrum from filtered and binned lightcurve
    """
    IDP_SAS_VARPSD_FILT_BINNED_METADATA: IdpSasVarpsdFiltBinnedMetadataSchemaDM
    """
    Metadata for the VARPSD filtered and binned
    """
    IDP_SAS_TEFF_SAPP: IdpSasTeffSappSchemaDM
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_PFU_TEFF_SAPP: Optional[IDPPFUTEFFSAPPDM] = None
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """


class InputsMsap4031DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_PROT_FOURIER: IdpSasProtFourierSchemaDM
    """
    Rotation period: from Fourier analysis
    """
    IDP_SAS_PROT_TIMESERIES: IdpSasProtTimeseriesSchemaDM
    """
    Rotation period: from time-series analysis ( below a period of 45 days)
    """
    IDP_SAS_ACF_FILT_TIMESERIES: IdpSasAcfFiltTimeseriesSchemaDM
    """
    Autocorrelation Function of the filtered binned LC
    """
    IDP_SAS_VARLC_FILT_BINNED: IdpSasVarlcFiltBinnedSchemaDM
    """
    Filtered and binned VARLC, long term filtering for removing variabilities larger than 55 days
    """
    IDP_SAS_VARLC_FILT_BINNED_METADATA: IdpSasVarlcFiltBinnedMetadataSchemaDM
    """
    Metadata for the binned filtered VARLC
    """
    IDP_SAS_VARPSD_FILT_BINNED: IdpSasVarpsdFiltBinnedSchemaDM
    """
    Variability analysis ready Power spectrum from filtered and binned lightcurve
    """
    IDP_SAS_VARPSD_FILT_BINNED_METADATA: IdpSasVarpsdFiltBinnedMetadataSchemaDM
    """
    Metadata for the VARPSD filtered and binned
    """
    IDP_SAS_TEFF_SAPP: Optional[IdpSasTeffSappSchemaDM] = None
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_PFU_TEFF_SAPP: IDPPFUTEFFSAPPDM
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """


class Dp4SasProtSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    Rossby_number: Annotated[
        float, Field(ge=0.0, le=5.0, title="Rossby_number")
    ]
    """
    the Rossby number computed from the rotation period
    """
    err_lower: Annotated[
        float, Field(ge=0.0, le=10000000.0, title="err_lower")
    ]
    """
    lower error in sec
    """
    err_upper: Annotated[
        float, Field(ge=0.0, le=10000000.0, title="err_upper")
    ]
    """
    upper uncertainty in sec
    """
    rotation_score: Annotated[
        float, Field(ge=0.0, le=10.0, title="rotation_score")
    ]
    """
    rotation score
    """
    value: Annotated[float, Field(ge=0.0, le=10000000.0, title="value")]
    """
    Value of the stellar rotation period in sec
    """


class Dp4SasProtMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    method: Annotated[str, Field(title="method")]
    """
    method that was used to measure the selected rotation period (GLS, ACF, CS, or none,if missing)
    """
    rotation_class: Annotated[str, Field(title="rotation_class")]
    """
    the rotation class, rotation or no rotation
    """


class ErrsLowerItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10000000.0)]


class PeriodDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10000000.0)]


class ShearValueItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=-1.0, le=1.0)]


class Dp4SasDeltaProtSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    errs_lower: Annotated[List[ErrsLowerItemDM], Field(title="errs_lower")]
    """
    corresponding lower uncertainties in sec
    """
    errs_upper: Annotated[List[float], Field(title="errs_upper")]
    """
    corresponding upper uncertainties in sec
    """
    periods: Annotated[List[PeriodDM], Field(title="periods")]
    """
    Candidate periods in sec
    """
    shear_value: Annotated[List[ShearValueItemDM], Field(title="shear_value")]
    """
    absolute shear value |P_SDR - P_rot |/P_rot
    """


class Dp4SasDeltaProtMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    dr_flag: Annotated[int, Field(title="dr_flag")]
    """
    The differential rotation flag status
    """


class Dp4SasSphSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    mean_Sph: Annotated[float, Field(ge=0.0, le=1000000.0, title="mean_Sph")]
    """
    Mean value of the Sph timeseries, computed using the stellar rotation period, standard deviation of th Sph in ppm
    """
    std_Sph: Annotated[float, Field(ge=0.0, le=1000000.0, title="std_Sph")]
    """
    Standard deviation of the Sph time series in ppm
    """


class Dp4SasSphMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    nsegment: Annotated[int, Field(title="nsegment")]
    """
    umber of light curve segments that were considered to compute the Sph mean value
    """


class IdpSasSPhotoIndexSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    sph_series: Annotated[List[float], Field(title="sph_series")]
    """
    Temporal Sph in ppm
    """
    time: Annotated[List[float], Field(title="time")]
    """
    time in BJD
    """


class InputsMsap404DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP4_SAS_WHITE_NOISE_FOURIER: Dp4SasWhiteNoiseFourierSchemaDM
    """
    White noise of the Fourier spectrum: White noise yielded from the Harvey fit
    """
    IDP_SAS_VARPSD: IdpSasVarpsdSchemaDM
    """
    Variability analysis ready Power spectrum
    """
    IDP_SAS_VARPSD_METADATA: Optional[IdpSasVarpsdMetadataSchemaDM] = None
    """
    Metadata of the variability analysis ready Power spectrum: Including the duty cycle
    """


class OutputsMsap404DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_LOGG_FLIPER: IdpSasLoggFliperSchemaDM
    """
    Log g from variability[WP12PDP_I33]: Log g determined from the variability of the light curve
    """


class Msap404SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap4_04: Annotated[
        InputsMsap404DM, Field(title="Inputs for MSAP4_04 module")
    ]
    outputs_msap4_04: Annotated[
        OutputsMsap404DM, Field(title="Outputs for MSAP4_04 module")
    ]


class InputsMsap406DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    additionalProperties: Optional[Any] = None
    IDP_SAS_LONGTERM_MODULATION_FOURIER: (
        IdpSasLongtermModulationFourierSchemaDM
    )
    """
    Periods of long-term flux modulations extracted from binned PSD that may arise either from activity cycles or from beating of close rotation frequencies, that are longer than a fixed threshold value
    """
    IDP_SAS_LONGTERM_MODULATION_TIMESERIES: (
        IdpSasLongtermModulationTimeseriesSchemaDM
    )
    """
    Periods corresponding to the local maxima above a period of 45 days of the ACF of binned LC, likely arising from either activity cycles or beating of close rotation frequencies
    """
    IDP_SAS_S_PHOTO_INDEX: IdpSasSPhotoIndexSchemaDM
    """
    Measurement of a temporal photometric activity index Sph
    """
    DP4_SAS_PROT: Optional[Dp4SasProtSchemaDM] = None
    """
    Rotation Period: Rotation period determined either from the Fourier analysis, from the timeseries analysis, or else from spot modelling
    """


class AcfPeriodItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10000000.0)]


class AcfPeriodLowerUncertaintyItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10000000.0)]


class AcfPeriodUpperUncertaintyItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10000000.0)]


class AcfSphPeriodItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=-1.0, le=10000000.0)]


class AcfSphPeriodLowerUncertaintyItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10000000.0)]


class AcfSphPeriodUpperUncertaintyItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10000000.0)]


class GlsSphPeriodItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=-1.0, le=10000000.0)]


class GlsSphPeriodLowerUncertaintyItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10000000.0)]


class GlsSphPeriodUpperUncertaintyItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10000000.0)]


class LowerUncertaintyItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10000000.0)]


class UpperUncertaintyItemDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[float, Field(ge=0.0, le=10000000.0)]


class Dp4SasLongtermModulationSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    acf_period: Annotated[List[AcfPeriodItemDM], Field(title="acf_period")]
    """
    compatible period measured in the ACF in sec
    """
    acf_period_lower_uncertainty: Annotated[
        List[AcfPeriodLowerUncertaintyItemDM],
        Field(title="acf_period_lower_uncertainty"),
    ]
    """
    sec
    """
    acf_period_upper_uncertainty: Annotated[
        List[AcfPeriodUpperUncertaintyItemDM],
        Field(title="acf_period_upper_uncertainty"),
    ]
    """
    sec
    """
    acf_sph_period: Annotated[
        List[AcfSphPeriodItemDM], Field(title="acf_sph_period")
    ]
    """
    compatible period measured in the ACF of the Sph , or -1 in sec
    """
    acf_sph_period_lower_uncertainty: Annotated[
        List[AcfSphPeriodLowerUncertaintyItemDM],
        Field(title="acf_sph_period_lower_uncertainty"),
    ]
    """
    sec
    """
    acf_sph_period_upper_uncertainty: Annotated[
        List[AcfSphPeriodUpperUncertaintyItemDM],
        Field(title="acf_sph_period_upper_uncertainty"),
    ]
    """
    sec
    """
    gls_sph_period: Annotated[
        List[GlsSphPeriodItemDM], Field(title="gls_sph_period")
    ]
    """
    compatible period measured in the GLS of the Sph , or -1 in sec
    """
    gls_sph_period_lower_uncertainty: Annotated[
        List[GlsSphPeriodLowerUncertaintyItemDM],
        Field(title="gls_sph_period_lower_uncertainty"),
    ]
    """
    sec
    """
    gls_sph_period_upper_uncertainty: Annotated[
        List[GlsSphPeriodUpperUncertaintyItemDM],
        Field(title="gls_sph_period_upper_uncertainty"),
    ]
    """
    sec
    """
    lower_uncertainty: Annotated[
        List[LowerUncertaintyItemDM], Field(title="lower_uncertainty")
    ]
    """
    sec
    """
    upper_uncertainty: Annotated[
        List[UpperUncertaintyItemDM], Field(title="upper_uncertainty")
    ]
    """
    sec
    """
    values: Annotated[List[float], Field(title="values")]
    """
    Long term modulation periods identified in the Fourier spectrum, or -1 in sec
    """


class Dp4SasLongtermModulationMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    modulation_class: Annotated[str, Field(title="modulation_class")]
    """
    Detection status: modulation or no modulation
    """


class IdpSasLongtermModulationPhotoIndexFourierSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    lower_uncertainty: Annotated[List[float], Field(title="lower_uncertainty")]
    """
    lower uncertainty in sec
    """
    pFA: Annotated[List[float], Field(title="pFA")]
    """
    Corresponding false alarm probability
    """
    peak_power: Annotated[List[float], Field(title="peak_power")]
    """
    Corresponding power of the selected peak
    """
    upper_uncertainty: Annotated[List[float], Field(title="upper_uncertainty")]
    """
    upper uncertainty in sec
    """
    values: Annotated[List[float], Field(title="values")]
    """
    periods extracted from GLS periodogram of the Sph timeseries in sec
    """


class IdpSasLongtermModulationPhotoIndexTimeseriesSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    G_ACF: Annotated[List[float], Field(title="G_ACF")]
    """
    Corresponding global maximum of the smoothed ACF
    """
    H_ACF: Annotated[List[float], Field(title="H_ACF")]
    """
    Corresponding height of the peak in the smoothed ACF
    """
    lower_uncertainty: Annotated[List[float], Field(title="lower_uncertainty")]
    """
    sec
    """
    peak_sequential_number: Annotated[
        List[float], Field(title="peak_sequential_number")
    ]
    """
    Peak sequential number by which to divide column 1 to get the cycle length
    """
    upper_uncertainty: Annotated[List[float], Field(title="upper_uncertainty")]
    """
    sec
    """
    values: Annotated[List[float], Field(title="values")]
    """
    Period corresponding to the local maxima of ACF in sec
    """


class IDPPFUALPHAFESAPPDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    value: Optional[float] = None
    """
    Value in dex
    """
    error: Optional[float] = None
    """
    error in dex
    """


class IDPPFULIRFMDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    error: Optional[float] = None
    value: Optional[float] = None


class PDPWP12MODEVOLDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: List[Dict[str, Any]]
    """
Stellar evolution tracks. They contain: mass, age, Teff, L, radius, surface chemical composition, central temperature, density, boundary of convective envelopes (both in radius and mass), surface rotation velocity (if rotation is included in the calculations)
    """


class IDPPFUCOVMATSAPPDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    covmat: Optional[List[List[float]]] = None
    """
    Covariance matrix for classical parameters
    """
    params: Optional[List[str]] = None
    """
    Parameters of the covariance matrix for classical parameters
    """


class InfoDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    description: Annotated[str, Field(title="description")]
    plotname: Annotated[str, Field(title="plotname")]
    unit: Annotated[str, Field(title="unit")]


class PdfDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    kde: Annotated[List[float], Field(title="kde")]
    values: Annotated[List[float], Field(title="values")]


class PosteriorDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class SummaryDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    err_minus: Annotated[float, Field(title="err_minus")]
    err_plus: Annotated[float, Field(title="err_plus")]
    mean: Annotated[float, Field(title="mean")]
    median: Annotated[float, Field(title="median")]
    quant_16: Annotated[float, Field(title="quant_16")]
    quant_84: Annotated[float, Field(title="quant_84")]
    stdev: Annotated[float, Field(title="stdev")]


class IdpSasMassScalingGridsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[InfoDM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[PosteriorDM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior1DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasRadiusScalingGridsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[InfoDM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior1DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior2DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasAgeScalingGridsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[InfoDM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior2DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior3DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasLoggScalingGridsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[InfoDM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior3DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior4DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasDensityScalingGridsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[InfoDM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior4DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class FitQualityDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    max_likelihood: Annotated[
        float, Field(alias="max-likelihood", title="max-likelihood")
    ]
    min_chi2: Annotated[float, Field(alias="min-chi2", title="min-chi2")]


class IdpSasMetadataScalingGridsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[List[str], Field(title="error")]
    """
    error from the run
    """
    fit_quality: Annotated[
        FitQualityDM, Field(alias="fit-quality", title="fit-quality")
    ]
    """
    chi2 value of the model with the lowest chi2
    """
    posterior_warning: Annotated[List[str], Field(title="posterior_warning")]
    """
    warning on posteriors
    """
    warning: Annotated[List[str], Field(title="warning")]
    """
    warning from the run
    """


class InputsMsap512DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP3_SAS_DELTA_NU_AV_METADATA: Dp3SasDeltaNuAvMetadataSchemaDM
    """
    Metadata on the average large frequency separation: Metadata on the average large frequency separation, including model and range for estimate (optional)
    """
    DP3_SAS_NU_MAX_METADATA: Dp3SasNuMaxMetadataSchemaDM
    """
    Metadata on the frequency of maximum oscillations power: Metadata on frequency of maximum oscillations power, including model for estimate (optional)
    """
    IDP_SAS_TEFF_SAPP: IdpSasTeffSappSchemaDM
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_PFU_TEFF_SAPP: Optional[IDPPFUTEFFSAPPDM] = None
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """


class InputsMsap5121DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP3_SAS_DELTA_NU_AV_METADATA: Dp3SasDeltaNuAvMetadataSchemaDM
    """
    Metadata on the average large frequency separation: Metadata on the average large frequency separation, including model and range for estimate (optional)
    """
    DP3_SAS_NU_MAX_METADATA: Dp3SasNuMaxMetadataSchemaDM
    """
    Metadata on the frequency of maximum oscillations power: Metadata on frequency of maximum oscillations power, including model for estimate (optional)
    """
    IDP_SAS_TEFF_SAPP: Optional[IdpSasTeffSappSchemaDM] = None
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_PFU_TEFF_SAPP: IDPPFUTEFFSAPPDM
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """


class DataDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    err_minus: Annotated[float, Field(title="err_minus")]
    err_plus: Annotated[float, Field(title="err_plus")]
    mean: Annotated[float, Field(title="mean")]
    median: Annotated[float, Field(title="median")]
    quant_16: Annotated[float, Field(title="quant_16")]
    quant_84: Annotated[float, Field(title="quant_84")]
    samples: Annotated[List[float], Field(title="samples")]
    std: Annotated[float, Field(title="std")]


class Info5DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    description: Annotated[str, Field(title="description")]
    latex_name: Annotated[str, Field(title="latex_name")]
    latex_unit: Annotated[str, Field(title="latex_unit")]


class IdpSasMassScalingOnlySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    data: Annotated[DataDM, Field(title="data")]
    """
    uncertainty on mean value
    """
    info: Annotated[Info5DM, Field(title="info")]
    """
    Latex formatted string for unit
    """


class IdpSasRadiusScalingOnlySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    data: Annotated[DataDM, Field(title="data")]
    """
    uncertainty on mean value
    """
    info: Annotated[Info5DM, Field(title="info")]
    """
    Latex formatted string for unit
    """


class IdpSasMetadataScalingOnlySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    errors: Annotated[List[str], Field(title="errors")]
    """
    Errors encountered during code execution
    """
    warnings: Annotated[List[str], Field(title="warnings")]
    """
    Warnings encountered during code execution: divide by zero or negative square root
    """


class PDPWP12MODOSCFREQDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: List[Dict[str, Any]]
    """
Stellar oscillations grids: For all models in PDP_B_SAS_MOD-EVOL, adiabatic frequencies will be provided
    """


class Info7DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    description: Annotated[str, Field(title="description")]
    plotname: Annotated[str, Field(title="plotname")]
    unit: Annotated[str, Field(title="unit")]


class Posterior5DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasMassGridMixedSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior5DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior6DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasRadiusGridMixedSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior6DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior7DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasAgeGridMixedSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior7DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior8DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasLoggGridMixedSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior8DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior9DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasTeffGridMixedSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior9DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior10DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasLGridMixedSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior10DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior11DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasDensityGridMixedSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior11DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class ModelNNDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    chi2: Annotated[float, Field(title="chi2")]
    index: Annotated[int, Field(title="index")]
    likelihood: Annotated[float, Field(title="likelihood")]
    name: Annotated[float, Field(title="name")]
    source_coefs: Annotated[List[float], Field(title="source_coefs")]
    source_models: Annotated[List[str], Field(title="source_models")]


class IntpolInfoDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    model_NN: Annotated[ModelNNDM, Field(title="model_NN")]


class IdpSasMetadataGridMixedSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[List[str], Field(title="error")]
    """
    error from the run
    """
    fit_quality: Annotated[
        FitQualityDM, Field(alias="fit-quality", title="fit-quality")
    ]
    """
    chi2 value of the model with the lowest chi2
    """
    intpol_info: Annotated[IntpolInfoDM, Field(title="intpol_info")]
    """
    weighting coefficients
    """
    posterior_warning: Annotated[List[str], Field(title="posterior_warning")]
    """
    warning on posteriors
    """
    warning: Annotated[List[str], Field(title="warning")]
    """
    warning from the run
    """


class InputsMsap514DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP3_SAS_DELTA_NU_AV: Dp3SasDeltaNuAvSchemaDM
    """
    Average large frequency separation: Average large frequency separation from global analysis of Asteroseismic Analysis Ready Power spectrum (AARPS), or from peak bagging wen available (optional)
    """
    DP3_SAS_NU_MAX: Dp3SasNuMaxSchemaDM
    """
    Frequency of maximum oscillations power: Frequency of maximum oscillations power from global analysis of AARPS, or from peak bagging if available (optional)
    """
    IDP_PFU_TEFF_SAPP: Optional[IDPPFUTEFFSAPPDM] = None
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: IdpSasTeffSappSchemaDM
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_PFU__FE_H__SAPP: Annotated[
        Optional[IDPPFUFEHSAPPDM], Field(alias="IDP_PFU_[FE_H]_SAPP")
    ] = None
    """
    Iron over hydrogen abundance ratio by number relative to the solar value.
    """
    IDP_SAS__FE_H__SAPP: Annotated[
        IdpSasFeHSappSchemaDM, Field(alias="IDP_SAS_[FE_H]_SAPP")
    ]
    """
    Combined value for [Fe/H]: Value of Fe/H selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """
    IDP_PFU__ALPHA_FE__SAPP: Annotated[
        Optional[IDPPFUALPHAFESAPPDM], Field(alias="IDP_PFU_[ALPHA_FE]_SAPP")
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value in dex
    """
    IDP_SAS__ALPHA_FE__SAPP: Annotated[
        Optional[IdpSasAlphaFeSappSchemaDM],
        Field(alias="IDP_SAS_[ALPHA_FE]_SAPP"),
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value
    """
    IDP_PFU_L_IRFM: Optional[IDPPFULIRFMDM] = None
    """
    Luminosity from infrared flux method (IRFM) in L⊙
    """
    PDP_WP12_MOD_EVOL: PDPWP12MODEVOLDM
    """
    Stellar evolution tracks. They contain: mass, age, Teff, L, radius, surface chemical composition, central temperature, density, boundary of convective envelopes (both in radius and mass), surface rotation velocity (if rotation is included in the calculations)
    """
    PDP_WP12_MOD_OSC_FREQ: PDPWP12MODOSCFREQDM
    """
    Stellar oscillations grids: For all models in PDP_B_SAS_MOD-EVOL, adiabatic frequencies will be provided for low angular degree, l=0, 1, 2, 3, modes.
    """
    IDP_PFU_COVMAT_SAPP: Optional[IDPPFUCOVMATSAPPDM] = None
    """
    Covariance matrix for classical parameters from the SAPP pipeline
    """
    IDP_SAS_COVMAT_SAPP: IdpSasCovmatSappSchemaDM
    """
    COVARIANCE matrix classical parameters: Covariance matrix for classical parameters from the SAPP module
    """
    DP3_SAS_OSC_FREQ: Dp3SasOscFreqSchemaDM
    """
    Oscillation mode frequencies: Inferred from the power spectra. Individual frequencies from peakbagging
    """
    DP3_SAS_OSC_FREQ_COVMAT: Dp3SasOscFreqCovmatSchemaDM
    """
    Covariance matrix: Covariance matrix for the fitted frequencies
    """
    DP3_SAS_OSC_FREQ_METADATA: Dp3SasOscFreqMetadataSchemaDM
    """
    Metadata on frequencies: Metadata on individual frequencies, including radial p-mode order and angular degree (latter to include separate explicit entry for mixed modes)
    """
    IDP_SAS_FREQUENCIES_FIRST_GUESS_METADATA: (
        IdpSasFrequenciesFirstGuessMetadataSchemaDM
    )
    """
    First-guess frequency list metadata: Metadata on the first-guess frequency list from peak-bagging preparation (optional)
    """


class InputsMsap5141DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP3_SAS_DELTA_NU_AV: Dp3SasDeltaNuAvSchemaDM
    """
    Average large frequency separation: Average large frequency separation from global analysis of Asteroseismic Analysis Ready Power spectrum (AARPS), or from peak bagging wen available (optional)
    """
    DP3_SAS_NU_MAX: Dp3SasNuMaxSchemaDM
    """
    Frequency of maximum oscillations power: Frequency of maximum oscillations power from global analysis of AARPS, or from peak bagging if available (optional)
    """
    IDP_PFU_TEFF_SAPP: IDPPFUTEFFSAPPDM
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: Optional[IdpSasTeffSappSchemaDM] = None
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_PFU__FE_H__SAPP: Annotated[
        IDPPFUFEHSAPPDM, Field(alias="IDP_PFU_[FE_H]_SAPP")
    ]
    """
    Iron over hydrogen abundance ratio by number relative to the solar value.
    """
    IDP_SAS__FE_H__SAPP: Annotated[
        Optional[IdpSasFeHSappSchemaDM], Field(alias="IDP_SAS_[FE_H]_SAPP")
    ] = None
    """
    Combined value for [Fe/H]: Value of Fe/H selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """
    IDP_PFU__ALPHA_FE__SAPP: Annotated[
        Optional[IDPPFUALPHAFESAPPDM], Field(alias="IDP_PFU_[ALPHA_FE]_SAPP")
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value in dex
    """
    IDP_SAS__ALPHA_FE__SAPP: Annotated[
        Optional[IdpSasAlphaFeSappSchemaDM],
        Field(alias="IDP_SAS_[ALPHA_FE]_SAPP"),
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value
    """
    IDP_PFU_L_IRFM: Optional[IDPPFULIRFMDM] = None
    """
    Luminosity from infrared flux method (IRFM) in L⊙
    """
    PDP_WP12_MOD_EVOL: PDPWP12MODEVOLDM
    """
    Stellar evolution tracks. They contain: mass, age, Teff, L, radius, surface chemical composition, central temperature, density, boundary of convective envelopes (both in radius and mass), surface rotation velocity (if rotation is included in the calculations)
    """
    PDP_WP12_MOD_OSC_FREQ: PDPWP12MODOSCFREQDM
    """
    Stellar oscillations grids: For all models in PDP_B_SAS_MOD-EVOL, adiabatic frequencies will be provided for low angular degree, l=0, 1, 2, 3, modes.
    """
    IDP_PFU_COVMAT_SAPP: IDPPFUCOVMATSAPPDM
    """
    Covariance matrix for classical parameters from the SAPP pipeline
    """
    IDP_SAS_COVMAT_SAPP: Optional[IdpSasCovmatSappSchemaDM] = None
    """
    COVARIANCE matrix classical parameters: Covariance matrix for classical parameters from the SAPP module
    """
    DP3_SAS_OSC_FREQ: Dp3SasOscFreqSchemaDM
    """
    Oscillation mode frequencies: Inferred from the power spectra. Individual frequencies from peakbagging
    """
    DP3_SAS_OSC_FREQ_COVMAT: Dp3SasOscFreqCovmatSchemaDM
    """
    Covariance matrix: Covariance matrix for the fitted frequencies
    """
    DP3_SAS_OSC_FREQ_METADATA: Dp3SasOscFreqMetadataSchemaDM
    """
    Metadata on frequencies: Metadata on individual frequencies, including radial p-mode order and angular degree (latter to include separate explicit entry for mixed modes)
    """
    IDP_SAS_FREQUENCIES_FIRST_GUESS_METADATA: (
        IdpSasFrequenciesFirstGuessMetadataSchemaDM
    )
    """
    First-guess frequency list metadata: Metadata on the first-guess frequency list from peak-bagging preparation (optional)
    """


class Posterior12DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasMassGridFreqsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior12DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior13DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasRadiusGridFreqsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior13DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior14DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasAgeGridFreqsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior14DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior15DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasLoggGridFreqsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior15DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior16DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasTeffGridFreqsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior16DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior17DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasLGridFreqsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior17DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior18DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasDensityGridFreqsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior18DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class IntpolInfo1DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    model_NN: Annotated[ModelNNDM, Field(title="model_NN")]


class IdpSasMetadataGridFreqsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[List[str], Field(title="error")]
    """
    error from the run
    """
    fit_quality: Annotated[
        FitQualityDM, Field(alias="fit-quality", title="fit-quality")
    ]
    """
    chi2 value of the model with the lowest chi2
    """
    intpol_info: Annotated[IntpolInfo1DM, Field(title="intpol_info")]
    """
    weighting coefficients
    """
    posterior_warning: Annotated[List[str], Field(title="posterior_warning")]
    """
    warning on posteriors
    """
    warning: Annotated[List[str], Field(title="warning")]
    """
    warning from the run
    """


class InputsMsap515DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP3_SAS_DELTA_NU_AV: Dp3SasDeltaNuAvSchemaDM
    """
    Average large frequency separation: Average large frequency separation from global analysis of Asteroseismic Analysis Ready Power spectrum (AARPS), or from peak bagging wen available (optional)
    """
    DP3_SAS_NU_MAX: Dp3SasNuMaxSchemaDM
    """
    Frequency of maximum oscillations power: Frequency of maximum oscillations power from global analysis of AARPS, or from peak bagging if available (optional)
    """
    IDP_PFU_TEFF_SAPP: Optional[IDPPFUTEFFSAPPDM] = None
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: IdpSasTeffSappSchemaDM
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_PFU__FE_H__SAPP: Annotated[
        Optional[IDPPFUFEHSAPPDM], Field(alias="IDP_PFU_[FE_H]_SAPP")
    ] = None
    """
    Iron over hydrogen abundance ratio by number relative to the solar value.
    """
    IDP_SAS__FE_H__SAPP: Annotated[
        IdpSasFeHSappSchemaDM, Field(alias="IDP_SAS_[FE_H]_SAPP")
    ]
    """
    Combined value for [Fe/H]: Value of Fe/H selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """
    IDP_PFU__ALPHA_FE__SAPP: Annotated[
        Optional[IDPPFUALPHAFESAPPDM], Field(alias="IDP_PFU_[ALPHA_FE]_SAPP")
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value in dex
    """
    IDP_SAS__ALPHA_FE__SAPP: Annotated[
        Optional[IdpSasAlphaFeSappSchemaDM],
        Field(alias="IDP_SAS_[ALPHA_FE]_SAPP"),
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value
    """
    IDP_PFU_L_IRFM: Optional[IDPPFULIRFMDM] = None
    """
    Luminosity from infrared flux method (IRFM) in L⊙
    """
    PDP_WP12_MOD_EVOL: PDPWP12MODEVOLDM
    """
    Stellar evolution tracks. They contain: mass, age, Teff, L, radius, surface chemical composition, central temperature, density, boundary of convective envelopes (both in radius and mass), surface rotation velocity (if rotation is included in the calculations)
    """
    PDP_WP12_MOD_OSC_FREQ: PDPWP12MODOSCFREQDM
    """
    Stellar oscillations grids: For all models in PDP_B_SAS_MOD-EVOL, adiabatic frequencies will be provided for low angular degree, l=0, 1, 2, 3, modes.
    """
    IDP_PFU_COVMAT_SAPP: Optional[IDPPFUCOVMATSAPPDM] = None
    """
    Covariance matrix for classical parameters from the SAPP pipeline
    """
    IDP_SAS_COVMAT_SAPP: IdpSasCovmatSappSchemaDM
    """
    COVARIANCE matrix classical parameters: Covariance matrix for classical parameters from the SAPP module
    """
    DP3_SAS_OSC_FREQ: Dp3SasOscFreqSchemaDM
    """
    Oscillation mode frequencies: Inferred from the power spectra. Individual frequencies from peakbagging
    """
    DP3_SAS_OSC_FREQ_COVMAT: Dp3SasOscFreqCovmatSchemaDM
    """
    Covariance matrix: Covariance matrix for the fitted frequencies
    """
    DP3_SAS_OSC_FREQ_METADATA: Dp3SasOscFreqMetadataSchemaDM
    """
    Metadata on frequencies: Metadata on individual frequencies, including radial p-mode order and angular degree (latter to include separate explicit entry for mixed modes)
    """
    IDP_SAS_FREQUENCIES_FIRST_GUESS_METADATA: (
        IdpSasFrequenciesFirstGuessMetadataSchemaDM
    )
    """
    First-guess frequency list metadata: Metadata on the first-guess frequency list from peak-bagging preparation (optional)
    """


class InputsMsap5151DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP3_SAS_DELTA_NU_AV: Dp3SasDeltaNuAvSchemaDM
    """
    Average large frequency separation: Average large frequency separation from global analysis of Asteroseismic Analysis Ready Power spectrum (AARPS), or from peak bagging wen available (optional)
    """
    DP3_SAS_NU_MAX: Dp3SasNuMaxSchemaDM
    """
    Frequency of maximum oscillations power: Frequency of maximum oscillations power from global analysis of AARPS, or from peak bagging if available (optional)
    """
    IDP_PFU_TEFF_SAPP: IDPPFUTEFFSAPPDM
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: Optional[IdpSasTeffSappSchemaDM] = None
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_PFU__FE_H__SAPP: Annotated[
        IDPPFUFEHSAPPDM, Field(alias="IDP_PFU_[FE_H]_SAPP")
    ]
    """
    Iron over hydrogen abundance ratio by number relative to the solar value.
    """
    IDP_SAS__FE_H__SAPP: Annotated[
        Optional[IdpSasFeHSappSchemaDM], Field(alias="IDP_SAS_[FE_H]_SAPP")
    ] = None
    """
    Combined value for [Fe/H]: Value of Fe/H selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """
    IDP_PFU__ALPHA_FE__SAPP: Annotated[
        Optional[IDPPFUALPHAFESAPPDM], Field(alias="IDP_PFU_[ALPHA_FE]_SAPP")
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value in dex
    """
    IDP_SAS__ALPHA_FE__SAPP: Annotated[
        Optional[IdpSasAlphaFeSappSchemaDM],
        Field(alias="IDP_SAS_[ALPHA_FE]_SAPP"),
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value
    """
    IDP_PFU_L_IRFM: Optional[IDPPFULIRFMDM] = None
    """
    Luminosity from infrared flux method (IRFM) in L⊙
    """
    PDP_WP12_MOD_EVOL: PDPWP12MODEVOLDM
    """
    Stellar evolution tracks. They contain: mass, age, Teff, L, radius, surface chemical composition, central temperature, density, boundary of convective envelopes (both in radius and mass), surface rotation velocity (if rotation is included in the calculations)
    """
    PDP_WP12_MOD_OSC_FREQ: PDPWP12MODOSCFREQDM
    """
    Stellar oscillations grids: For all models in PDP_B_SAS_MOD-EVOL, adiabatic frequencies will be provided for low angular degree, l=0, 1, 2, 3, modes.
    """
    IDP_PFU_COVMAT_SAPP: IDPPFUCOVMATSAPPDM
    """
    Covariance matrix for classical parameters from the SAPP pipeline
    """
    IDP_SAS_COVMAT_SAPP: Optional[IdpSasCovmatSappSchemaDM] = None
    """
    COVARIANCE matrix classical parameters: Covariance matrix for classical parameters from the SAPP module
    """
    DP3_SAS_OSC_FREQ: Dp3SasOscFreqSchemaDM
    """
    Oscillation mode frequencies: Inferred from the power spectra. Individual frequencies from peakbagging
    """
    DP3_SAS_OSC_FREQ_COVMAT: Dp3SasOscFreqCovmatSchemaDM
    """
    Covariance matrix: Covariance matrix for the fitted frequencies
    """
    DP3_SAS_OSC_FREQ_METADATA: Dp3SasOscFreqMetadataSchemaDM
    """
    Metadata on frequencies: Metadata on individual frequencies, including radial p-mode order and angular degree (latter to include separate explicit entry for mixed modes)
    """
    IDP_SAS_FREQUENCIES_FIRST_GUESS_METADATA: (
        IdpSasFrequenciesFirstGuessMetadataSchemaDM
    )
    """
    First-guess frequency list metadata: Metadata on the first-guess frequency list from peak-bagging preparation (optional)
    """


class Posterior19DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasMassGridSurfaceIndependentSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior19DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior20DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasRadiusGridSurfaceIndependentSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior20DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior21DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasAgeGridSurfaceIndependentSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior21DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior22DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasLoggGridSurfaceIndependentSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior22DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior23DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasTeffGridSurfaceIndependentSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior23DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior24DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasLGridSurfaceIndependentSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior24DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior25DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasDensityGridSurfaceIndependentSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior25DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class IntpolInfo2DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    model_NN: Annotated[ModelNNDM, Field(title="model_NN")]


class IdpSasMetadataGridSurfaceIndependentSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[List[str], Field(title="error")]
    """
    error from the run
    """
    fit_quality: Annotated[
        FitQualityDM, Field(alias="fit-quality", title="fit-quality")
    ]
    """
    chi2 value of the model with the lowest chi2
    """
    intpol_info: Annotated[IntpolInfo2DM, Field(title="intpol_info")]
    """
    weighting coefficients
    """
    posterior_warning: Annotated[List[str], Field(title="posterior_warning")]
    """
    warning on posteriors
    """
    warning: Annotated[List[str], Field(title="warning")]
    """
    warning from the run
    """


class InputsMsap521DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP4_SAS_PROT: Dp4SasProtSchemaDM
    """
    Rotation Period: Rotation period determined either from the Fourier analysis, from the timeseries analysis, or else from spot modelling
    """
    LG_PFU_OBS_DATA: LGPFUOBSDATADM
    """
    Contains Gaia BP-RP spectrophotometry from final release and Spectra as described in the PDD (under PFU Observed spectrum)
    """


class IdpSasAgeGyroSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    posterior: Annotated[List[float], Field(title="posterior")]
    """
    posterior distribution obtained from a Gaussian probability density function in Myr
    """
    uncertainty: Annotated[float, Field(title="uncertainty")]
    """
    uncertainty in Myr
    """
    value: Annotated[float, Field(title="value")]
    """
    value of age computed from the gyrochronology relation in Myr
    """


class IdpSasMetadataGyroSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    quality_flag: Annotated[int, Field(title="quality_flag")]
    """
    Flag indicating if the age estimate computed can be adopted (0) or not (1) for scientific purpose
    """


class InputsMsap522DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP4_SAS_PROT: Dp4SasProtSchemaDM
    """
    Rotation Period: Rotation period determined either from the Fourier analysis, from the timeseries analysis, or else from spot modelling
    """
    IDP_PFU_TEFF_SAPP: Optional[IDPPFUTEFFSAPPDM] = None
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_PFU__FE_H__SAPP: Annotated[
        Optional[IDPPFUFEHSAPPDM], Field(alias="IDP_PFU_[FE_H]_SAPP")
    ] = None
    """
    Iron over hydrogen abundance ratio by number relative to the solar value.
    """
    IDP_PFU_COVMAT_SAPP: Optional[IDPPFUCOVMATSAPPDM] = None
    """
    Covariance matrix for classical parameters from the SAPP pipeline.
    """
    IDP_SAS_TEFF_SAPP: IdpSasTeffSappSchemaDM
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_SAS__FE_H__SAPP: Annotated[
        IdpSasFeHSappSchemaDM, Field(alias="IDP_SAS_[FE_H]_SAPP")
    ]
    """
    Combined value for [Fe/H]: Value of Fe/H selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """
    IDP_SAS_COVMAT_SAPP: IdpSasCovmatSappSchemaDM
    """
    Covariance matrix classical parameters: Covariance matrix for classical parameters from the SAPP pipeline
    """
    DP4_SAS_SPH: Dp4SasSphSchemaDM
    """
    Activity index
    """
    IDP_PFU_L_IRFM: IDPPFULIRFMDM
    """
    Luminosity from infrared flux method (IRFM) in L⊙
    """


class InputsMsap5221DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP4_SAS_PROT: Dp4SasProtSchemaDM
    """
    Rotation Period: Rotation period determined either from the Fourier analysis, from the timeseries analysis, or else from spot modelling
    """
    IDP_PFU_TEFF_SAPP: IDPPFUTEFFSAPPDM
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_PFU__FE_H__SAPP: Annotated[
        IDPPFUFEHSAPPDM, Field(alias="IDP_PFU_[FE_H]_SAPP")
    ]
    """
    Iron over hydrogen abundance ratio by number relative to the solar value.
    """
    IDP_PFU_COVMAT_SAPP: IDPPFUCOVMATSAPPDM
    """
    Covariance matrix for classical parameters from the SAPP pipeline.
    """
    IDP_SAS_TEFF_SAPP: Optional[IdpSasTeffSappSchemaDM] = None
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_SAS__FE_H__SAPP: Annotated[
        Optional[IdpSasFeHSappSchemaDM], Field(alias="IDP_SAS_[FE_H]_SAPP")
    ] = None
    """
    Combined value for [Fe/H]: Value of Fe/H selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """
    IDP_SAS_COVMAT_SAPP: Optional[IdpSasCovmatSappSchemaDM] = None
    """
    Covariance matrix classical parameters: Covariance matrix for classical parameters from the SAPP pipeline
    """
    DP4_SAS_SPH: Dp4SasSphSchemaDM
    """
    Activity index
    """
    IDP_PFU_L_IRFM: IDPPFULIRFMDM
    """
    Luminosity from infrared flux method (IRFM) in L⊙
    """


class IdpSasAgeActivitySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    uncertainty: Annotated[float, Field(title="uncertainty")]
    """
    uncertainty in Gyr
    """
    value: Annotated[float, Field(title="value")]
    """
    value of age computed from the activity relations in Gyr
    """


class IdpSasAgeActivityMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    quality_flag: Annotated[int, Field(title="quality_flag")]
    """
    Flag indicating if the age estimate computed can be adopted for scientific purpose, 0: within applicability limits, 1: outside effective temperature range, 2: outside age range
    """


class IDPPFURADIUSSAPPDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: float
    """
Stellar radius combined by MSetSci1 from various methods in R⊙
    """


class IdpSasMassGranulationSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    Mass: Annotated[float, Field(title="Mass")]
    """
    value of mass computed from surface gravity and photometric radius in g
    """
    Mass_pdf: Annotated[List[float], Field(title="Mass_pdf")]
    """
    mass posterior distribution
    """
    Mass_std: Annotated[float, Field(title="Mass_std")]
    """
    mass standard deviation in g
    """


class IdpSasMetadataGranulationSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    Flag: Annotated[int, Field(title="Flag")]
    """
    flags from IDP_SAS_LOGG_FLIPER and/or IDP_PFU_RADIUS_SAPP
    """


class InputsMsap524DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    additionalProperties: Optional[Any] = None
    IDP_SAS_LOGG_FLIPER: IdpSasLoggFliperSchemaDM
    """
    Log g from variability[WP12PDP_I33]: Log g determined from the variability of the light curve
    """
    IDP_PFU_TEFF_SAPP: Optional[IDPPFUTEFFSAPPDM] = None
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_PFU__FE_H__SAPP: Annotated[
        Optional[IDPPFUFEHSAPPDM], Field(alias="IDP_PFU_[FE_H]_SAPP")
    ] = None
    """
    Iron over hydrogen abundance ratio by number relative to the solar value.
    """
    IDP_PFU_COVMAT_SAPP: Optional[IDPPFUCOVMATSAPPDM] = None
    """
    Covariance matrix for classical parameters from the SAPP pipeline
    """
    IDP_SAS_TEFF_SAPP: IdpSasTeffSappSchemaDM
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_SAS__FE_H__SAPP: Annotated[
        IdpSasFeHSappSchemaDM, Field(alias="IDP_SAS_[FE_H]_SAPP")
    ]
    """
    Combined value for [Fe/H]: Value of Fe/H selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """
    IDP_SAS_COVMAT_SAPP: IdpSasCovmatSappSchemaDM
    """
    COVARIANCE matrix classical parameters: Covariance matrix for classical parameters from the SAPP module
    """
    IDP_PFU_L_IRFM: Optional[IDPPFULIRFMDM] = None
    """
    Luminosity from infrared flux method (IRFM) in L⊙
    """
    PDP_WP12_MOD_EVOL: PDPWP12MODEVOLDM
    """
    Stellar evolution tracks. They contain: mass, age, Teff, L, radius, surface chemical composition, central temperature, density, boundary of convective envelopes (both in radius and mass), surface rotation velocity (if rotation is included in the calculations)
    """


class InputsMsap5241DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    additionalProperties: Optional[Any] = None
    IDP_SAS_LOGG_FLIPER: IdpSasLoggFliperSchemaDM
    """
    Log g from variability[WP12PDP_I33]: Log g determined from the variability of the light curve
    """
    IDP_PFU_TEFF_SAPP: IDPPFUTEFFSAPPDM
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_PFU__FE_H__SAPP: Annotated[
        IDPPFUFEHSAPPDM, Field(alias="IDP_PFU_[FE_H]_SAPP")
    ]
    """
    Iron over hydrogen abundance ratio by number relative to the solar value.
    """
    IDP_PFU_COVMAT_SAPP: IDPPFUCOVMATSAPPDM
    """
    Covariance matrix for classical parameters from the SAPP pipeline
    """
    IDP_SAS_TEFF_SAPP: Optional[IdpSasTeffSappSchemaDM] = None
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_SAS__FE_H__SAPP: Annotated[
        Optional[IdpSasFeHSappSchemaDM], Field(alias="IDP_SAS_[FE_H]_SAPP")
    ] = None
    """
    Combined value for [Fe/H]: Value of Fe/H selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """
    IDP_SAS_COVMAT_SAPP: Optional[IdpSasCovmatSappSchemaDM] = None
    """
    COVARIANCE matrix classical parameters: Covariance matrix for classical parameters from the SAPP module
    """
    IDP_PFU_L_IRFM: Optional[IDPPFULIRFMDM] = None
    """
    Luminosity from infrared flux method (IRFM) in L⊙
    """
    PDP_WP12_MOD_EVOL: PDPWP12MODEVOLDM
    """
    Stellar evolution tracks. They contain: mass, age, Teff, L, radius, surface chemical composition, central temperature, density, boundary of convective envelopes (both in radius and mass), surface rotation velocity (if rotation is included in the calculations)
    """


class Posterior26DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasMassGranulationCgbmSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior26DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior27DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasRadiusGranulationCgbmSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior27DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class Posterior28DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    pdf: Annotated[PdfDM, Field(title="pdf")]
    samples: Annotated[List[float], Field(title="samples")]


class IdpSasAgeGranulationCgbmSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    info: Annotated[Info7DM, Field(title="info")]
    """
    Unit
    """
    posterior: Annotated[Posterior28DM, Field(title="posterior")]
    """
    full selection of drawn random samples of the posterior based on the likelihood computation
    """
    summary: Annotated[SummaryDM, Field(title="summary")]
    """
    uncertainty on mean
    """


class IdpSasMetadataGranulationCgbmSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    error: Annotated[List[str], Field(title="error")]
    """
    error from the run
    """
    fit_quality: Annotated[
        FitQualityDM, Field(alias="fit-quality", title="fit-quality")
    ]
    """
    chi2 value of the model with the lowest chi2
    """
    posterior_warning: Annotated[List[str], Field(title="posterior_warning")]
    """
    warning on posteriors
    """
    warning: Annotated[List[str], Field(title="warning")]
    """
    warning from the run
    """


class InputsMsap531DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    additionalProperties: Optional[Any] = None
    IDP_SAS_MASS_SCALING_GRIDS: Optional[IdpSasMassScalingGridsSchemaDM] = None
    """
    Mass scaling on grids: Mass inferred from scaling relations constrained on stellar evolution tracks
    """
    IDP_SAS_RADIUS_SCALING_GRIDS: Optional[
        IdpSasRadiusScalingGridsSchemaDM
    ] = None
    """
    Radius scaling on grids: Radius inferred from scaling relations constrained on stellar evolution tracks
    """
    IDP_SAS_AGE_SCALING_GRIDS: Optional[IdpSasAgeScalingGridsSchemaDM] = None
    """
    Age scaling on grids: Age inferred from scaling relations constrained on stellar evolution tracks
    """
    IDP_SAS_MASS_SCALING_ONLY: Optional[IdpSasMassScalingOnlySchemaDM] = None
    """
    Mass scaling only: Mass inferred from scaling relations
    """
    IDP_SAS_RADIUS_SCALING_ONLY: Optional[IdpSasRadiusScalingOnlySchemaDM] = (
        None
    )
    """
    Radius scaling only: Radius inferred from scaling relations
    """
    IDP_SAS_MASS_GRID_MIXED: Optional[IdpSasMassGridMixedSchemaDM] = None
    """
    Mass from grid-based method using mixed modes: Mass inferred from grid based methods using mixed modes
    """
    IDP_SAS_RADIUS_GRID_MIXED: Optional[IdpSasRadiusGridMixedSchemaDM] = None
    """
    Radius from grid-based method using mixed modes: Radius inferred from grid based methods using mixed modes
    """
    IDP_SAS_AGE_GRID_MIXED: Optional[IdpSasAgeGridMixedSchemaDM] = None
    """
    Age from grid-based method using mixed modes: Age inferred from grid based methods using mixed modes
    """
    IDP_SAS_MASS_GRID_FREQS: Optional[IdpSasMassGridFreqsSchemaDM] = None
    """
    Mass from grid-based method using frequencies: Mass inferred from grid based methods using individual frequencies
    """
    IDP_SAS_RADIUS_GRID_FREQS: Optional[IdpSasRadiusGridFreqsSchemaDM] = None
    """
    Radius from grid-based method using frequencies: Radius inferred from grid based methods using individual frequencies
    """
    IDP_SAS_AGE_GRID_FREQS: Optional[IdpSasAgeGridFreqsSchemaDM] = None
    """
    Age from grid-based method using frequencies: Age inferred from grid based methods using individual frequencies
    """
    IDP_SAS_MASS_GRID_SURFACE_INDEPENDENT: Optional[
        IdpSasMassGridSurfaceIndependentSchemaDM
    ] = None
    """
    Mass from grid-based method using surface independent methods: Mass inferred from grid based methods using surface independent methods
    """
    IDP_SAS_RADIUS_GRID_SURFACE_INDEPENDENT: Optional[
        IdpSasRadiusGridSurfaceIndependentSchemaDM
    ] = None
    """
    Radius from grid-based method using surface independent methods: Radius inferred from grid based methods using surface independent methods
    """
    IDP_SAS_AGE_GRID_SURFACE_INDEPENDENT: Optional[
        IdpSasAgeGridSurfaceIndependentSchemaDM
    ] = None
    """
    Age from grid-based method using surface independent methods: Age inferred from grid based methods using surface independent methods
    """
    IDP_SAS_AGE_GYRO: Optional[IdpSasAgeGyroSchemaDM] = None
    """
    Age from gyrochronology: Age determined from gyrochronology
    """
    IDP_SAS_AGE_ACTIVITY: Optional[IdpSasAgeActivitySchemaDM] = None
    """
    Age from age-activity relations: Age determined from activity-age relations
    """
    IDP_SAS_MASS_GRANULATION: Optional[IdpSasMassGranulationSchemaDM] = None
    """
    Mass from granulation: Mass determined using the granulation log g and the photometric radius
    """
    IDP_SAS_MASS_GRANULATION_CGBM: Optional[
        IdpSasMassGranulationCgbmSchemaDM
    ] = None
    """
    Mass from non-seismic grid based methods and granulation: Mass determined from grid-based modelling with classical methods using granulation log g as a constraint
    """
    IDP_SAS_RADIUS_GRANULATION_CGBM: Optional[
        IdpSasRadiusGranulationCgbmSchemaDM
    ] = None
    """
    Radius from non-seismic grid based methods and granulation: Radius determined from grid-based modelling with classical methods using granulation log g as a constraint
    """
    IDP_SAS_AGE_GRANULATION_CGBM: Optional[
        IdpSasAgeGranulationCgbmSchemaDM
    ] = None
    """
    Age from non-seismic grid based methods and granulation: Age determined from grid-based modelling with classical methods using granulation log g as a constraint
    """


class IdpSasConsistencyFlagsSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    age_consistency_flag: Annotated[bool, Field(title="age_consistency_flag")]
    """
    consistency flag for all age values
    """
    mass_consistency_flag: Annotated[
        bool, Field(title="mass_consistency_flag")
    ]
    """
    consistency flag for all mass values
    """
    radius_consistency_flag: Annotated[
        bool, Field(title="radius_consistency_flag")
    ]
    """
    consistency flag for all radius values
    """


class InputsMsap532DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_MASS_SCALING_GRIDS: Optional[IdpSasMassScalingGridsSchemaDM] = None
    """
    Mass scaling on grids: Mass inferred from scaling relations constrained on stellar evolution tracks
    """
    IDP_SAS_RADIUS_SCALING_GRIDS: Optional[
        IdpSasRadiusScalingGridsSchemaDM
    ] = None
    """
    Radius scaling on grids: Radius inferred from scaling relations constrained on stellar evolution tracks
    """
    IDP_SAS_AGE_SCALING_GRIDS: Optional[IdpSasAgeScalingGridsSchemaDM] = None
    """
    Age scaling on grids: Age inferred from scaling relations constrained on stellar evolution tracks
    """
    IDP_SAS_MASS_SCALING_ONLY: Optional[IdpSasMassScalingOnlySchemaDM] = None
    """
    Mass scaling only: Mass inferred from scaling relations
    """
    IDP_SAS_RADIUS_SCALING_ONLY: Optional[IdpSasRadiusScalingOnlySchemaDM] = (
        None
    )
    """
    Radius scaling only: Radius inferred from scaling relations
    """
    IDP_SAS_MASS_GRID_MIXED: Optional[IdpSasMassGridMixedSchemaDM] = None
    """
    Mass from grid-based method using mixed modes: Mass inferred from grid based methods using mixed modes
    """
    IDP_SAS_RADIUS_GRID_MIXED: Optional[IdpSasRadiusGridMixedSchemaDM] = None
    """
    Radius from grid-based method using mixed modes: Radius inferred from grid based methods using mixed modes
    """
    IDP_SAS_AGE_GRID_MIXED: Optional[IdpSasAgeGridMixedSchemaDM] = None
    """
    Age from grid-based method using mixed modes: Age inferred from grid based methods using mixed modes
    """
    IDP_SAS_MASS_GRID_FREQS: Optional[IdpSasMassGridFreqsSchemaDM] = None
    """
    Mass from grid-based method using frequencies: Mass inferred from grid based methods using individual frequencies
    """
    IDP_SAS_RADIUS_GRID_FREQS: Optional[IdpSasRadiusGridFreqsSchemaDM] = None
    """
    Radius from grid-based method using frequencies: Radius inferred from grid based methods using individual frequencies
    """
    IDP_SAS_AGE_GRID_FREQS: Optional[IdpSasAgeGridFreqsSchemaDM] = None
    """
    Age from grid-based method using frequencies: Age inferred from grid based methods using individual frequencies
    """
    IDP_SAS_MASS_GRID_SURFACE_INDEPENDENT: Optional[
        IdpSasMassGridSurfaceIndependentSchemaDM
    ] = None
    """
    Mass from grid-based method using surface independent methods: Mass inferred from grid based methods using surface independent methods
    """
    IDP_SAS_RADIUS_GRID_SURFACE_INDEPENDENT: Optional[
        IdpSasRadiusGridSurfaceIndependentSchemaDM
    ] = None
    """
    Radius from grid-based method using surface independent methods: Radius inferred from grid based methods using surface independent methods
    """
    IDP_SAS_AGE_GRID_SURFACE_INDEPENDENT: Optional[
        IdpSasAgeGridSurfaceIndependentSchemaDM
    ] = None
    """
    Age from grid-based method using surface independent methods: Age inferred from grid based methods using surface independent methods
    """
    IDP_SAS_AGE_GYRO: Optional[IdpSasAgeGyroSchemaDM] = None
    """
    Age from gyrochronology: Age determined from gyrochronology
    """
    IDP_SAS_AGE_ACTIVITY: Optional[IdpSasAgeActivitySchemaDM] = None
    """
    Age from age-activity relations: Age determined from activity-age relations
    """
    IDP_SAS_MASS_GRANULATION: Optional[IdpSasMassGranulationSchemaDM] = None
    """
    Mass from granulation: Mass determined using the granulation log g and the photometric radius
    """
    IDP_SAS_MASS_GRANULATION_CGBM: Optional[
        IdpSasMassGranulationCgbmSchemaDM
    ] = None
    """
    Mass from non-seismic grid based methods and granulation: Mass determined from grid-based modelling with classical methods using granulation log g as a constraint
    """
    IDP_SAS_RADIUS_GRANULATION_CGBM: Optional[
        IdpSasRadiusGranulationCgbmSchemaDM
    ] = None
    """
    Radius from non-seismic grid based methods and granulation: Radius determined from grid-based modelling with classical methods using granulation log g as a constraint
    """
    IDP_SAS_AGE_GRANULATION_CGBM: Optional[
        IdpSasAgeGranulationCgbmSchemaDM
    ] = None
    """
    Age from non-seismic grid based methods and granulation: Age determined from grid-based modelling with classical methods using granulation log g as a constraint
    """
    IDP_SAS_CONSISTENCY_FLAGS: Optional[IdpSasConsistencyFlagsSchemaDM] = None
    """
    Consistency flags (TBD): For control purposes, when anomalous values are detected when combining the parameters values into the final ones
    """


class IdpSasMassPrioritySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    mass_selection: Annotated[str, Field(title="mass_selection")]
    """
    mass selected from priority list
    """


class IdpSasRadiusPrioritySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    radius_selection: Annotated[str, Field(title="radius_selection")]
    """
    radius selected from priority list
    """


class IdpSasAgePrioritySchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    age_selection: Annotated[str, Field(title="age_selection")]
    """
    age selected from priority list
    """


class InputsMsap534DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_MASS_SCALING_GRIDS: Optional[IdpSasMassScalingGridsSchemaDM] = None
    """
    Mass scaling on grids: Mass inferred from scaling relations constrained on stellar evolution tracks
    """
    IDP_SAS_RADIUS_SCALING_GRIDS: Optional[
        IdpSasRadiusScalingGridsSchemaDM
    ] = None
    """
    Radius scaling on grids: Radius inferred from scaling relations constrained on stellar evolution tracks
    """
    IDP_SAS_AGE_SCALING_GRIDS: Optional[IdpSasAgeScalingGridsSchemaDM] = None
    """
    Age scaling on grids: Age inferred from scaling relations constrained on stellar evolution tracks
    """
    IDP_SAS_MASS_SCALING_ONLY: Optional[IdpSasMassScalingOnlySchemaDM] = None
    """
    Mass scaling only: Mass inferred from scaling relations
    """
    IDP_SAS_RADIUS_SCALING_ONLY: Optional[IdpSasRadiusScalingOnlySchemaDM] = (
        None
    )
    """
    Radius scaling only: Radius inferred from scaling relations
    """
    IDP_SAS_MASS_GRID_MIXED: Optional[IdpSasMassGridMixedSchemaDM] = None
    """
    Mass from grid-based method using mixed modes: Mass inferred from grid based methods using mixed modes
    """
    IDP_SAS_RADIUS_GRID_MIXED: Optional[IdpSasRadiusGridMixedSchemaDM] = None
    """
    Radius from grid-based method using mixed modes: Radius inferred from grid based methods using mixed modes
    """
    IDP_SAS_AGE_GRID_MIXED: Optional[IdpSasAgeGridMixedSchemaDM] = None
    """
    Age from grid-based method using mixed modes: Age inferred from grid based methods using mixed modes
    """
    IDP_SAS_MASS_GRID_FREQS: Optional[IdpSasMassGridFreqsSchemaDM] = None
    """
    Mass from grid-based method using frequencies: Mass inferred from grid based methods using individual frequencies
    """
    IDP_SAS_RADIUS_GRID_FREQS: Optional[IdpSasRadiusGridFreqsSchemaDM] = None
    """
    Radius from grid-based method using frequencies: Radius inferred from grid based methods using individual frequencies
    """
    IDP_SAS_AGE_GRID_FREQS: Optional[IdpSasAgeGridFreqsSchemaDM] = None
    """
    Age from grid-based method using frequencies: Age inferred from grid based methods using individual frequencies
    """
    IDP_SAS_MASS_GRID_SURFACE_INDEPENDENT: Optional[
        IdpSasMassGridSurfaceIndependentSchemaDM
    ] = None
    """
    Mass from grid-based method using surface independent methods: Mass inferred from grid based methods using surface independent methods
    """
    IDP_SAS_RADIUS_GRID_SURFACE_INDEPENDENT: Optional[
        IdpSasRadiusGridSurfaceIndependentSchemaDM
    ] = None
    """
    Radius from grid-based method using surface independent methods: Radius inferred from grid based methods using surface independent methods
    """
    IDP_SAS_AGE_GRID_SURFACE_INDEPENDENT: Optional[
        IdpSasAgeGridSurfaceIndependentSchemaDM
    ] = None
    """
    Age from grid-based method using surface independent methods: Age inferred from grid based methods using surface independent methods
    """
    IDP_SAS_AGE_GYRO: Optional[IdpSasAgeGyroSchemaDM] = None
    """
    Age from gyrochronology: Age determined from gyrochronology
    """
    IDP_SAS_AGE_ACTIVITY: Optional[IdpSasAgeActivitySchemaDM] = None
    """
    Age from age-activity relations: Age determined from activity-age relations
    """
    IDP_SAS_MASS_GRANULATION: Optional[IdpSasMassGranulationSchemaDM] = None
    """
    Mass from granulation: Mass determined using the granulation log g and the photometric radius
    """
    IDP_SAS_MASS_GRANULATION_CGBM: Optional[
        IdpSasMassGranulationCgbmSchemaDM
    ] = None
    """
    Mass from non-seismic grid based methods and granulation: Mass determined from grid-based modelling with classical methods using granulation log g as a constraint
    """
    IDP_SAS_RADIUS_GRANULATION_CGBM: Optional[
        IdpSasRadiusGranulationCgbmSchemaDM
    ] = None
    """
    Radius from non-seismic grid based methods and granulation: Radius determined from grid-based modelling with classical methods using granulation log g as a constraint
    """
    IDP_SAS_AGE_GRANULATION_CGBM: Optional[
        IdpSasAgeGranulationCgbmSchemaDM
    ] = None
    """
    Age from non-seismic grid based methods and granulation: Age determined from grid-based modelling with classical methods using granulation log g as a constraint
    """
    IDP_SAS_MASS_PRIORITY: Optional[IdpSasMassPrioritySchemaDM] = None
    """
    Mass value selection: combining all mass determinations
    """
    IDP_SAS_RADIUS_PRIORITY: Optional[IdpSasRadiusPrioritySchemaDM] = None
    """
    Radius value selection: combining all radius determinations
    """
    IDP_SAS_AGE_PRIORITY: Optional[IdpSasAgePrioritySchemaDM] = None
    """
    Age value selection: combining all age determinations
    """


class Dp5SasMassSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    mean: Annotated[float, Field(ge=0.0, le=50.0, title="mean")]
    """
    Mean value of the stellar mass in M⊙
    """
    std: Annotated[float, Field(ge=0.0, le=50.0, title="std")]
    """
    Standard deviation in M⊙
    """


class Dp5SasMassMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    source: Annotated[str, Field(title="source")]
    """
    source of measurement
    """


class Dp5SasRadiusSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    mean: Annotated[float, Field(ge=0.0, le=100.0, title="mean")]
    """
    Mean value of the stellar radius in R⊙
    """
    std: Annotated[float, Field(ge=0.0, le=100.0, title="std")]
    """
    Standard deviation in R⊙
    """


class Dp5SasRadiusMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    source: Annotated[str, Field(title="source")]
    """
    source of measurement
    """


class Dp5SasAgeSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    mean: Annotated[float, Field(ge=0.0, le=14.0, title="mean")]
    """
    Mean value of the stellar age in Gyr
    """
    std: Annotated[float, Field(ge=0.0, le=14.0, title="std")]
    """
    Standard deviation in Gyr
    """


class Dp5SasAgeMetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    source: Annotated[str, Field(title="source")]
    """
    source of measurement
    """


class Dp1MetadataSchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    metadata: Optional[Dict[str, Any]] = None


class EasSchemaDM(
    IdpEasQcTargetTceDetectionSchemaDM, IdpEasTransitRemovalKitSchemaDM
):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )


class LGPFUOBSDATA1DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    gaia_bp_rp: GaiaBpRpDM
    spec_path: List[str]
    spec_read_func: List[str]
    snr_star: List[str]
    rv_shift: List[str]
    rv_shift_err: List[str]


class Mstesci1SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    IDP_PFU_TEFF_SAPP: IDPPFUTEFFSAPPDM
    """
    Value of Teff selected after statistical analysis: combination of values yielded from the SAPP pipeline, after validation by WP125200 in K
    """
    IDP_PFU_LOGG_SAPP: IDPPFULOGGSAPPDM
    """
    Value of log g selected after statistical analysis: combination of values yielded from the SAPP pipeline in dex
    """
    IDP_PFU__FE_H__SAPP: Annotated[
        IDPPFUFEHSAPPDM, Field(alias="IDP_PFU_[FE_H]_SAPP")
    ]
    """
    Iron over hydrogen abundance ratio by number relative to the solar value in dex
    """
    IDP_PFU_COVMAT_SAPP: IDPPFUCOVMATSAPPDM
    """
    Covariance matrix for classical parameters from the SAPP pipeline
    """
    IDP_PFU_RADIUS_SAPP: float
    """
    Stellar radius combined by MSetSci1 from various methods in R⊙
    """
    IDP_PFU__ALPHA_FE__SAPP: Annotated[
        IDPPFUALPHAFESAPPDM, Field(alias="IDP_PFU_[ALPHA_FE]_SAPP")
    ]
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value in dex
    """
    IDP_PFU__ALPHA_FE__SPECTROSCOPY: Annotated[
        float, Field(alias="IDP_PFU_[ALPHA_FE]_SPECTROSCOPY")
    ]
    """
    Alpha elements abundances relative to iron from spectroscopy in dex
    """
    IDP_PFU_VSINI_SPECTROSCOPY: float
    """
    Projected rotational velocity from spectroscopy (only for spectral synthesis) in Km/s
    """
    IDP_PFU__ELEM1_ELEM2__SPECTROSCOPY: Annotated[
        List[List[float]], Field(alias="IDP_PFU_[ELEM1_ELEM2]_SPECTROSCOPY")
    ]
    """
    Chemical abundance of element1 (TBD) relative to element2 (TBD) from spectroscopy in dex
    """
    LG_PFU_OBS_DATA: LGPFUOBSDATA1DM
    """
    Contains Gaia BP-RP spectrophotometry from final release and Spectra as described in the PDD (under PFU Observed spectrum)
    """
    IDP_PFU_LOGG_PHOTOMETRY: float
    """
    Photometric surface gravity in dex
    """
    IDP_PFU_TEFF_PHOTOMETRY: float
    """
    Effective temperature from photometry in K
    """
    IDP_PFU_METADATA_PHOTOMETRY: Dict[str, Any]
    """
    Metadata of the photometry
    """
    IDP_PFU_EXTINCTION: float
    """
    Interstellar extinction
    """
    IDP_PFU_L_IRFM: IDPPFULIRFMDM
    """
    Luminosity from infrared flux method (IRFM) in L⊙
    """
    IDP_PFU_THETA_IRFM: float
    """
    Angular diameter from IRFM
    """
    IDP_PFU_TEFF_IRFM: float
    """
    Effective temperature from IRFM in K
    """
    IDP_PFU_RADIUS_IRFM: float
    """
    Radius from IRFM in R⊙
    """
    IDP_PFU_METADATA_IRFM: Dict[str, Any]
    """
    Metadata of the IRFM
    """
    IDP_PFU_TEFF_SBCR: float
    """
    Limb-darkened effective temperature from surface brightness relations in K
    """
    IDP_PFU_THETA_SBCR: float
    """
    Limb-darkened angular diameter from surface brightness relations
    """
    IDP_PFU_RADIUS_SBCR: float
    """
    Limb-darkened radius from surface brightness relations in R⊙
    """
    IDP_PFU_METADATA_SBCR: Dict[str, Any]
    """
    Metadata of the SBCR
    """
    IDP_PFU_LD_INTERFEROMETRY: float
    """
    Limb-darkened coefficients from interferometry in K
    """
    IDP_PFU_THETA_INTERFEROMETRY: float
    """
    Limb-darkened angular diameter from interferometry in K
    """
    IDP_PFU_TEFF_INTERFEROMETRY: float
    """
    Limb-darkened effective temperature from interferometry in K
    """
    IDP_PFU_RADIUS_INTERFEROMETRY: float
    """
    Limb-darkened radius from interferometry in R⊙
    """
    IDP_PFU_METADATA_INTERFEROMETRY: Dict[str, Any]
    """
    Metadata for the interferometry
    """


class Mstesci2SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    IDP_PFU_MASS_CLASSICAL: float
    """
    Mass determined from classical methods (non seismic) in M⊙
    """
    IDP_PFU_RADIUS_CLASSICAL: IDPPFURADIUSCLASSICALDM
    """
    Radius determined from classical methods (non seismic) in R⊙
    """
    IDP_PFU_AGE_CLASSICAL: float
    """
    Age determined from classical methods (non seismic) in Myr
    """
    IDP_PFU_METADATA_CLASSICAL: Dict[str, Any]
    """
    Metadata of the production of the classical parameters, allowing to retrieve mass_classical, radius_classical, age_classical from the outputs of MSteSci1
    """


class Wp121SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    PDP_WP12_MOD_STRUCTURE: List[Dict[str, Any]]
    """
    Stellar internal structure models
    """
    PDP_WP12_MOD_OSC_FREQ: List[Dict[str, Any]]
    """
    Stellar oscillations grids: For all models in PDP_B_SAS_MOD-EVOL, adiabatic frequencies will be provided
    """
    PDP_WP12_MOD_EVOL: List[Dict[str, Any]]
    """
    Stellar evolution tracks. They contain: mass, age, Teff, L, radius, surface chemical composition, central temperature, density, boundary of convective envelopes (both in radius and mass), surface rotation velocity (if rotation is included in the calculations)
    """


class Wp122SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    PDP_WP12_MOD_STELLAR_SPECTRA_1D: List[Dict[str, Any]]
    """
    Synthetic spectra from 1D models: LTE and non-LTE synthetic spectra, fluxes and intensities from 1D stellar atmospheres models. 1D MARCS theoretical synthetic spectra computed by WP122100.
    """
    PDP_WP12_MOD_STELLAR_SPECTRA_3D: List[Dict[str, Any]]
    """
    Synthetic spectra from 3D models: LTE and non-LTE synthetic spectra, fluxes and intensities from 3D stellar atmospheres models. 3D STAGGER theoretical synthetic spectra computed by WP122200.
    """


class MetadataDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    data_segment: int
    """
    Number of data segment, updated at every repointing and every field rotation, counted from 1. (data_segment would be a better and more-general name, but we'll use the same name as the other sub-system.) Assumed to be unique through the whole mission, rather than restarting from 1 for each new field.
    """
    plato_id: int
    """
    Catalogue number of the target, from the PIC.
    """
    processing_id: str
    """
    Identifier for a single run of the entire pipeline. This identifier includes the version of the pipeline, derived from the versions of the modules used.
    """
    sas_modules: str
    """
    List of SAS modules, and their release versions, performed to compute the Data Products from the current plato_id
    """
    lineage: str
    """
    Data traceability, including the list of inputs used during the processing_id
    """
    date_created: str
    """
    The date on which this version of the data was created. The ISO 8601:2004 extended date format is used.
    """
    creator_name: str
    """
    The name of the person principally responsible for creating this data.
    """
    data_model_version: Optional[str] = "2.0.0"
    """
    The data model version.
    """


class OutputsMsap101DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_VARLC: IdpSasVarlcSchemaDM
    """
    VARLC: (Non-seismic) Variability Analysis-ready LC, stitched, gaps filled, detrended
    """
    IDP_SAS_VARLC_METADATA: IdpSasVarlcMetadataSchemaDM
    """
    Metadata for VARLC: Metadata of the production of the (Non-seismic) Variability Analysis-ready LC
    """
    IDP_SAS_VARPSD: IdpSasVarpsdSchemaDM
    """
    Variability analysis ready Power spectrum
    """
    IDP_SAS_VARPSD_METADATA: IdpSasVarpsdMetadataSchemaDM
    """
    Metadata of the variability analysis ready Power spectrum: Including the duty cycle
    """
    ADP_SAS_LC_NT: AdpSasLcNtSchemaDM
    """
    Lightcurve corrected for the transit: LC after the transit have been removed and the gaps filled
    """
    ADP_SAS_LC_NT_METADATA: AdpSasLcNtMetadataSchemaDM


class OutputsMsap102DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_FLARES_FLAG: Annotated[
        IdpSasFlaresFlagSchemaDM, Field(title="IDP_SAS_FLARES_FLAG")
    ]
    """
    Flare flag: Indications allowing to identify the points of the timeseries involved in a flare. Total length containing all the quarters.
    """
    ADP_SAS_FLARES_FLAG: Annotated[
        AdpSasFlaresFlagSchemaDM, Field(title="ADP_SAS_FLARES_FLAG")
    ]
    """
    Metadata on flares detection.
    """


class OutputsMsap107DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_AARLC: IdpSasAarlcSchemaDM
    """
    AARLC[WP12PDP_I32]: Asteroseismic Analysis-ready LC
    """
    IDP_SAS_AARLC_METADATA: IdpSasAarlcMetadataSchemaDM
    """
    Metadata for AARLC[WP12PDP_I32]: Metadata for AARLC: processing flag (was DP2 model for transit removal applied or not)
    """
    IDP_SAS_AARPSD: IdpSasAarpsdSchemaDM
    """
    AARPSD[WP12PDP_I32]: Asteroseismic Analysis-ready power spectrum.Frequency-power spectra of the AARLC (AARPS), including information on the spectral window function
    """
    IDP_SAS_AARPSD_METADATA: IdpSasAarpsdMetadataSchemaDM
    """
    Metadata for AARPSD[WP12PDP_I32]: Metadata for the frequency-power spectra of the AARLC (AARPS), including information on the spectral window function, and the duty cycle
    """


class OutputsMsap108DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_VARLC_BINNED: IdpSasVarlcBinnedSchemaDM
    """
    Binned VARLC: Binned variability analysis ready light curve
    """
    IDP_SAS_VARLC_BINNED_METADATA: IdpSasVarlcBinnedMetadataSchemaDM
    """
    Metadata for the binned VARLC: Metadata of the production of the (Non-seismic) Binned Variability Analysis-ready LC metadata
    """
    IDP_SAS_VARPSD_BINNED: IdpSasVarpsdBinnedSchemaDM
    """
    Variability analysis ready Power spectrum from binned lightcurve
    """
    IDP_SAS_VARPSD_BINNED_METADATA: IdpSasVarpsdBinnedMetadataSchemaDM
    """
    Metadata for the VARPSD binned
    """


class OutputsMsap109DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_VARLC_FILT_BINNED: IdpSasVarlcFiltBinnedSchemaDM
    """
    Filtered and binned VARLC, long term filtering for removing variabilities larger than 55 days (TBC)
    """
    IDP_SAS_VARLC_FILT_BINNED_METADATA: IdpSasVarlcFiltBinnedMetadataSchemaDM
    """
    Metadata for the binned filtered VARLC
    """
    IDP_SAS_VARPSD_FILT_BINNED: IdpSasVarpsdFiltBinnedSchemaDM
    """
    Variability analysis ready Power spectrum from filtered and binned lightcurve
    """
    IDP_SAS_VARPSD_FILT_BINNED_METADATA: IdpSasVarpsdFiltBinnedMetadataSchemaDM
    """
    Metadata for the VARPSD filtered and binned
    """


class OutputsMsap2123DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_LOGG_SAPP_TMP: IdpSasLoggSappTmpSchemaDM
    """
    Log g for spectroscopy: Log g from variability or seismic analysis for spectroscopy
    """
    IDP_SAS_TEFF_SPECTROSCOPY: IdpSasTeffSpectroscopySchemaDM
    """
    Log g for spectroscopy: Log g from variability or seismic analysis for spectroscopy
    """
    IDP_SAS_LOGG_SPECTROSCOPY: IdpSasLoggSpectroscopySchemaDM
    """
    Log g from spectroscopy: Log g from spectroscopy after iteration
    """
    IDP_SAS_METADATA_STAT_SPECTROSCOPY: IdpSasMetadataStatSpectroscopySchemaDM
    """
    Stat_Spec: metadata from spectral synthesis: statistics of fit
    """
    IDP_SAS__FE_H__SPECTROSCOPY: Annotated[
        IdpSasFeHSpectroscopySchemaDM,
        Field(alias="IDP_SAS_[FE_H]_SPECTROSCOPY"),
    ]
    """
    Value for [Fe/H] from spectroscopy
    """
    IDP_SAS_FLAGS_SPECTROSCOPY: IdpSasFlagsSpectroscopySchemaDM
    """
    Flags from spectroscopy
    """
    IDP_SAS__ALPHA_FE__SPECTROSCOPY: Annotated[
        IdpSasAlphaFeSpectroscopySchemaDM,
        Field(alias="IDP_SAS_[ALPHA_FE]_SPECTROSCOPY"),
    ]
    """
    Alpha elements: Abundances in alpha elements relative to iron
    """
    IDP_SAS_MICROTURBULENCE_SPECTROSCOPY: Optional[
        IdpSasMicroturbulenceSpectroscopySchemaDM
    ] = None
    """
    ξspec: Microturbulence from spectroscopy
    """
    IDP_SAS_VBRD_SPECTROSCOPY: Optional[IdpSasVbrdSpectroscopySchemaDM] = None
    """
    Total spectral line broadening
    """
    IDP_SAS__ELEM1_ELEM2__SPECTROSCOPY: Annotated[
        Optional[IdpSasElem1Elem2SpectroscopySchemaDM],
        Field(alias="IDP_SAS_[ELEM1_ELEM2]_SPECTROSCOPY"),
    ] = None
    """
    [element1/element2]spec: Chemical abundance of element 1 (TBD) relative to element 2 (TBD) from spectroscopy fixing log g to the seismic value, or to the granulation value
    """
    IDP_SAS_MOD_BESTFIT_SPECTROSCOPY: Optional[
        IdpSasModBestfitSpectroscopySchemaDM
    ] = None
    """
    BestModel_Spec: Best-fitting theoretical model from spectral synthesis
    """
    IDP_SAS_TEFF_SAPP: IdpSasTeffSappSchemaDM
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_SAS_LOGG_SAPP: IdpSasLoggSappSchemaDM
    """
    Combined value for log g: Value of log g selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """
    IDP_SAS__FE_H__SAPP: Annotated[
        IdpSasFeHSappSchemaDM, Field(alias="IDP_SAS_[FE_H]_SAPP")
    ]
    """
    Combined value for [Fe/H]: Value of Fe/H selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """
    DP_SAS__ALPHA_FE__SAPP: Annotated[
        Optional[IdpSasAlphaFeSappSchemaDM],
        Field(alias="DP_SAS_[ALPHA_FE]_SAPP"),
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value
    """
    IDP_SAS_COVMAT_SAPP: IdpSasCovmatSappSchemaDM
    """
    COVARIANCE matrix classical parameters: Covariance matrix for classical parameters from the SAPP module
    """
    IDP_SAS_MICROTURBULENCE_SAPP: Optional[
        IdpSasMicroturbulenceSappSchemaDM
    ] = None
    """
    ξspec: Microturbulence
    """
    IDP_SAS__ELEM1_ELEM2__SAPP: Annotated[
        Optional[IdpSasElem1Elem2SappSchemaDM],
        Field(alias="IDP_SAS_[ELEM1_ELEM2]_SAPP"),
    ] = None
    """
    Chemical abundance of element 1 relative to element 2 fixing log g to the seismic value, or to the granulation value
    """
    IDP_SAS_VBRD_SAPP: Optional[IdpSasVbrdSappSchemaDM] = None
    """
    Total spectral line broadening
    """


class OutputsMsap301DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_POWER_EXCESS_PROBABILITY: IdpSasPowerExcessProbabilitySchemaDM
    """
    POWER EXCESS PROBABILITY: The probability density of Nu max using MSAP3-03 to assess detection flag
    """
    IDP_SAS_POWER_EXCESS_METRICS: IdpSasPowerExcessMetricsSchemaDM
    """
    POWER EXCESS PROBABILITY: The metrics and criteria used to compute the probability density of Nu max
    """


class OutputsMsap302DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_ACF_PROBABILITY: IdpSasAcfProbabilitySchemaDM
    """
    Autocorrelation function probability: Detection probability using ACF of the timeseries
    """
    IDP_SAS_ACF_METRICS: IdpSasAcfMetricsSchemaDM
    """
    Autocorrelation function metrics: Metrics used when determining the detection probability using ACF of the timeseries
    """


class OutputsMsap303DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP3_SAS_DELTA_NU_AV: Optional[Dp3SasDeltaNuAvSchemaDM] = None
    """
    Average large frequency separation: Average large frequency separation from global analysis of Asteroseismic Analysis Ready Power spectrum (AARPS), or from peak bagging wen available (optional)
    """
    DP3_SAS_DELTA_NU_AV_METADATA: Optional[Dp3SasDeltaNuAvMetadataSchemaDM] = (
        None
    )
    """
    Metadata on the average large frequency separation: Metadata on the average large frequency separation, including model and range for estimate (optional)
    """
    DP3_SAS_NU_MAX: Optional[Dp3SasNuMaxSchemaDM] = None
    """
    Frequency of maximum oscillations power: Frequency of maximum oscillations power from global analysis of AARPS, or from peak bagging if available (optional)
    """
    DP3_SAS_NU_MAX_METADATA: Optional[Dp3SasNuMaxMetadataSchemaDM] = None
    """
    Metadata on the frequency of maximum oscillations power: Metadata on frequency of maximum oscillations power, including model for estimate (optional)
    """
    IDP_SAS_BACKGROUND_FIT: Optional[IdpSasBackgroundFitSchemaDM] = None
    """
    Back-ground fit : Background fit (model and parameter)
    """
    IDP_SAS_FREQUENCIES_FIRST_GUESS: Optional[
        IdpSasFrequenciesFirstGuessSchemaDM
    ] = None
    """
    IDP_SAS_FREQUENCIES_FIRST_GUESS
    """
    IDP_SAS_FREQUENCIES_FIRST_GUESS_METADATA: Optional[
        IdpSasFrequenciesFirstGuessMetadataSchemaDM
    ] = None
    """
    First-guess frequency list metadata: Metadata on the first-guess frequency list from peak-bagging preparation (optional)
    """
    IDP_SAS_OSC_MODE_HEIGHTS_FIRST_GUESS: Optional[
        IdpSasOscModeHeightsFirstGuessSchemaDM
    ] = None
    """
    First-guess oscillation modes heights: First-guess oscillation modes heights from peak-bagging preparation (optional)
    """
    IDP_SAS_OSC_MODE_WIDTHS_FIRST_GUESS: Optional[
        IdpSasOscModeWidthsFirstGuessSchemaDM
    ] = None
    """
    First-guess oscillation modes widths: First-guess oscillation modes widths from peak-bagging preparation (optional)
    """
    IDP_SAS_MULTIDETECTION_METRICS: IdpSasMultidetectionMetricsSchemaDM
    """
    Multiple Detection Flag: Results and metadata from detection stage of module: Multiple Detection Flag (single or multiple spectra, number)
    """
    IDP_SAS_DETECTION_METRICS: IdpSasDetectionMetricsSchemaDM
    """
    Detection metrics: Results and metadata from detection stage of module: Detection metrics (SNR, integrated power) (metadata)
    """
    IDP_SAS_SEISMIC_DETECTION_FLAG: IdpSasSeismicDetectionFlagSchemaDM
    """
    Detection flag: Results and metadata from detection stage of module: Detection Flag (4 entries: power excess, large separation, a combination of the two, and peak bagging detection flag)
    """


class OutputsMsap304DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP3_SAS_OSC_FREQ: Dp3SasOscFreqSchemaDM
    """
    Oscillation mode frequencies: Inferred from the power spectra. Individual frequencies from peakbagging
    """
    DP3_SAS_OSC_FREQ_COVMAT: Dp3SasOscFreqCovmatSchemaDM
    """
    Covariance matrix: Covariance matrix for the fitted frequencies
    """
    DP3_SAS_OSC_FREQ_METADATA: Dp3SasOscFreqMetadataSchemaDM
    """
    Metadata on frequencies: Metadata on individual frequencies, including radial p-mode order and angular degree (latter to include separate explicit entry for mixed modes)
    """
    ADP_SAS_BACKGROUND_BESTFIT: AdpSasBackgroundBestfitSchemaDM
    """
    Background best fitting parameters: From peak bagging: Background parameters (various, various units)
    """
    ADP_SAS_MODE_HEIGHTS: AdpSasModeHeightsSchemaDM
    """
    Mode heights: From peak-bagging
    """
    ADP_SAS_MODE_WIDTHS: AdpSasModeWidthsSchemaDM
    """
    Mode widths: From peak-bagging
    """
    ADP_SAS_MODE_POWERS: AdpSasModePowersSchemaDM
    """
    Mode powers: From peak-bagging
    """
    ADP_SAS_PEAK_ASYMMETRIES: AdpSasPeakAsymmetriesSchemaDM
    """
    Final mode assymmetry estimates
    """
    IDP_SAS_SEISMIC_QUALITY_METADATA: IdpSasSeismicQualityMetadataSchemaDM
    """
    Quality control metadata produced by the seismic analysis: Quality control metadata (numbers and flags for every parameters)
    """
    IDP_SAS_COVMAT_ALL_PARAMETERS: IdpSasCovmatAllParametersSchemaDM
    """
    Covariances for all parameters: Covariance matrix for all parameters
    """
    IDP_SAS_SPLITTINGS: IdpSasSplittingsSchemaDM
    """
    Oscillation modes splittings: Oscillation frequency splittings from peak-bagging
    """
    IDP_SAS_INCLINATION_ANGLE_PEAKBAGGING: (
        IdpSasInclinationAnglePeakbaggingSchemaDM
    )
    """
    Inclination angle: From peak-bagging
    """


class OutputsMsap401DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP4_SAS_HARVEY1_AMPLITUDE: Dp4SasHarvey1AmplitudeSchemaDM
    """
    Harvey profile1 (activity): amplitude: Amplitude of the Harvey profile modelling the activity in the Fourier background
    """
    DP4_SAS_HARVEY1_TIME: Dp4SasHarvey1TimeSchemaDM
    """
    Harvey profile 1 (activity): characteristic time: Characteristic time of the Harvey profile 1
    """
    DP4_SAS_HARVEY1_EXPONENT: Dp4SasHarvey1ExponentSchemaDM
    """
    Harvey profile 1 (activity) : exponent: Exponent of the Harvey profile 1
    """
    DP4_SAS_HARVEY2_AMPLITUDE: Dp4SasHarvey2AmplitudeSchemaDM
    """
    Harvey profile 2 (mesogranulation) : amplitude: Amplitude of the Harvey profile modelling the mesogranulation in the Fourier background
    """
    DP4_SAS_HARVEY2_TIME: Dp4SasHarvey2TimeSchemaDM
    """
    Harvey profile 2 (mesogranulation) : characteristic time: Characteristic time of the Harvey profile 2
    """
    DP4_SAS_HARVEY2_EXPONENT: Dp4SasHarvey2ExponentSchemaDM
    """
    Harvey profile 2 (mesogranulation) : exponent: Exponent of the Harvey profile 2
    """
    DP4_SAS_HARVEY3_AMPLITUDE: Dp4SasHarvey3AmplitudeSchemaDM
    """
    Harvey profile 3 (granulation) : amplitude: Amplitude of the Harvey profile modelling the granulation in the Fourier background
    """
    DP4_SAS_HARVEY3_TIME: Dp4SasHarvey3TimeSchemaDM
    """
    Harvey profile 3 (granulation) : characteristic time: Characteristic time of the Harvey profile 3
    """
    DP4_SAS_HARVEY3_EXPONENT: Dp4SasHarvey3ExponentSchemaDM
    """
    Harvey profile 3 (granulation) : exponent: Exponent of the Harvey profile 3
    """
    DP4_SAS_WHITE_NOISE_FOURIER: Dp4SasWhiteNoiseFourierSchemaDM
    """
    White noise of the Fourier spectrum: White noise yielded from the Harvey fit
    """
    DP4_SAS_HARVEY_METADATA: Dp4SasHarveyMetadataSchemaDM
    """
    Harvey profiles metadata: Containing the process by which the Harvey profiles are fitted
    """
    IDP_SAS_LONGTERM_MODULATION_FOURIER: (
        IdpSasLongtermModulationFourierSchemaDM
    )
    """
    Period of long-term flux modulations extracted from binned PSD that may arise either from activity cycles or from beating of close rotation frequencies, that are longer than a fixed threshold value
    """
    IDP_SAS_PROT_FOURIER: IdpSasProtFourierSchemaDM
    """
    Rotation period: from Fourier analysis
    """


class OutputsMsap402DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_PROT_TIMESERIES: IdpSasProtTimeseriesSchemaDM
    """
    Rotation period: from time-series analysis ( below a period of 45 days)
    """
    IDP_SAS_ACF_FILT_TIMESERIES: IdpSasAcfFiltTimeseriesSchemaDM
    """
    Autocorrelation Function of the filtered binned LC
    """
    IDP_SAS_LONGTERM_MODULATION_TIMESERIES: (
        IdpSasLongtermModulationTimeseriesSchemaDM
    )
    """
    Periods corresponding to the local maxima above a period of 45 days of the ACF of binned LC, likely arising from either activity cycles or beating of close rotation frequencies
    """
    IDP_SAS_ACF_TIMESERIES: IdpSasAcfTimeseriesSchemaDM
    """
    Autocorrelation Function of the binned LC
    """


class OutputsMsap403DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP4_SAS_PROT: Dp4SasProtSchemaDM
    """
    Rotation Period: Rotation period determined either from the Fourier analysis, from the timeseries analysis, or else from spot modelling
    """
    DP4_SAS_PROT_METADATA: Dp4SasProtMetadataSchemaDM
    """
    Rotation period determination metadata: Containing the process by which the rotation is generated
    """
    DP4_SAS_DELTA_PROT: Dp4SasDeltaProtSchemaDM
    """
    Differential rotation: Selected final value of the surface differential rotation’s amplitude (inferred from Fourier analysis, time series analysis, or spot modelling).
    """
    DP4_SAS_DELTA_PROT_METADATA: Dp4SasDeltaProtMetadataSchemaDM
    """
    Differential rotation determination metadata: Containing the process by which the differential rotation is generated
    """
    DP4_SAS_SPH: Dp4SasSphSchemaDM
    """
    Activity index
    """
    DP4_SAS_SPH_METADATA: Dp4SasSphMetadataSchemaDM
    """
    Metadata for activity index: Containing the number of segments used to compute the index.
    """
    IDP_SAS_S_PHOTO_INDEX: IdpSasSPhotoIndexSchemaDM
    """
    Measurement of a temporal photometric activity index Sph
    """


class OutputsMsap406DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP4_SAS_LONGTERM_MODULATION: Dp4SasLongtermModulationSchemaDM
    """
    Final periods identified relatively to the long-term cyclical variation
    """
    DP4_SAS_LONGTERM_MODULATION_METADATA: (
        Dp4SasLongtermModulationMetadataSchemaDM
    )
    """
    Additional information on the process by which the long-term cyclical variation is identified
    """
    IDP_SAS_LONGTERM_MODULATION_PHOTO_INDEX_FOURIER: (
        IdpSasLongtermModulationPhotoIndexFourierSchemaDM
    )
    """
    Periods extracted from GLS periodogram of the Sph timeseries
    """
    IDP_SAS_LONGTERM_MODULATION_PHOTO_INDEX_TIMESERIES: (
        IdpSasLongtermModulationPhotoIndexTimeseriesSchemaDM
    )
    """
    Period corresponding to the local maxima of ACF computed with the Sph time series
    """


class OutputsMsap511DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_MASS_SCALING_GRIDS: IdpSasMassScalingGridsSchemaDM
    """
    Mass scaling on grids: Mass inferred from scaling relations constrained on stellar evolution tracks
    """
    IDP_SAS_RADIUS_SCALING_GRIDS: IdpSasRadiusScalingGridsSchemaDM
    """
    Radius scaling on grids: Radius inferred from scaling relations constrained on stellar evolution tracks
    """
    IDP_SAS_AGE_SCALING_GRIDS: IdpSasAgeScalingGridsSchemaDM
    """
    Age scaling on grids: Age inferred from scaling relations constrained on stellar evolution tracks
    """
    IDP_SAS_LOGG_SCALING_GRIDS: IdpSasLoggScalingGridsSchemaDM
    """
    Log g scaling on grids: Log g inferred from scaling relations constrained on stellar evolution tracks
    """
    IDP_SAS_DENSITY_SCALING_GRIDS: IdpSasDensityScalingGridsSchemaDM
    """
    Density scaling on grids: Density inferred from scaling relations constrained on stellar evolution tracks
    """
    IDP_SAS_METADATA_SCALING_GRIDS: IdpSasMetadataScalingGridsSchemaDM
    """
    Metadata for the scaling grids method: We store in the metadata information to retrieve the optimal model, together with information on the quality of the fit
    """


class OutputsMsap512DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_MASS_SCALING_ONLY: IdpSasMassScalingOnlySchemaDM
    """
    Mass scaling only: Mass inferred from scaling relations
    """
    IDP_SAS_RADIUS_SCALING_ONLY: IdpSasRadiusScalingOnlySchemaDM
    """
    Radius scaling only: Radius inferred from scaling relations
    """
    IDP_SAS_METADATA_SCALING_ONLY: IdpSasMetadataScalingOnlySchemaDM


class OutputsMsap513DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_MASS_GRID_MIXED: IdpSasMassGridMixedSchemaDM
    """
    Mass from grid-based method using mixed modes: Mass inferred from grid based methods using mixed modes
    """
    IDP_SAS_RADIUS_GRID_MIXED: IdpSasRadiusGridMixedSchemaDM
    """
    Radius from grid-based method using mixed modes: Radius inferred from grid based methods using mixed modes
    """
    IDP_SAS_AGE_GRID_MIXED: IdpSasAgeGridMixedSchemaDM
    """
    Age from grid-based method using mixed modes: Age inferred from grid based methods using mixed modes
    """
    IDP_SAS_LOGG_GRID_MIXED: IdpSasLoggGridMixedSchemaDM
    """
    Log g from grid-based method using mixed modes: Log g inferred from grid based methods using mixed modes
    """
    IDP_SAS_TEFF_GRID_MIXED: IdpSasTeffGridMixedSchemaDM
    """
    Teff from grid-based method using mixed modes: Teff inferred from grid based methods using mixed modes
    """
    IDP_SAS_L_GRID_MIXED: IdpSasLGridMixedSchemaDM
    """
    Luminosity from grid-based method using mixed modes: L inferred from grid based methods using mixed modes
    """
    IDP_SAS_DENSITY_GRID_MIXED: IdpSasDensityGridMixedSchemaDM
    """
    Density from grid-based method using mixed modes: Mean density inferred from grid based methods using mixed modes
    """
    IDP_SAS_METADATA_GRID_MIXED: IdpSasMetadataGridMixedSchemaDM
    """
    Metadata of the optimal model with grid based methods using mixed modes: We store in the metadata information to retrieve the optimal model to be used as reference model for inversions with mixed modes, together with information on the quality of the fit we store the metadata to retrieve the optimal model
    """


class OutputsMsap514DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_MASS_GRID_FREQS: IdpSasMassGridFreqsSchemaDM
    """
    Mass from grid-based method using frequencies: Mass inferred from grid based methods using individual frequencies
    """
    IDP_SAS_RADIUS_GRID_FREQS: IdpSasRadiusGridFreqsSchemaDM
    """
    Radius from grid-based method using frequencies: Radius inferred from grid based methods using individual frequencies
    """
    IDP_SAS_AGE_GRID_FREQS: IdpSasAgeGridFreqsSchemaDM
    """
    Age from grid-based method using frequencies: Age inferred from grid based methods using individual frequencies
    """
    IDP_SAS_LOGG_GRID_FREQS: IdpSasLoggGridFreqsSchemaDM
    """
    Log g from grid-based method using frequencies: Log g inferred from grid based methods using individual frequencies
    """
    IDP_SAS_TEFF_GRID_FREQS: IdpSasTeffGridFreqsSchemaDM
    """
    Teff from grid-based method using frequencies: Teff of the inferred from grid-based methods using the individual frequencies fit
    """
    IDP_SAS_L_GRID_FREQS: IdpSasLGridFreqsSchemaDM
    """
    Luminosity from grid-based method using frequencies: Luminosity of the inferred from grid-based methods using the individual frequencies fit
    """
    IDP_SAS_DENSITY_GRID_FREQS: IdpSasDensityGridFreqsSchemaDM
    """
    Density from grid-based method using frequencies: The value for the mean density will be inferred from the pdf distribution of the mean density. The pdf, in turn, is derived through forward modelling, from mean density values provided for each model in the grid.
    """
    IDP_SAS_METADATA_GRID_FREQS: IdpSasMetadataGridFreqsSchemaDM
    """
    Metadata of the optimal model with grid based methods using frequencies: We store in the metadata information to retrieve the optimal model to be used as reference model for the sophisticated methods (inversion, glitch), together with information on the quality of the fit
    """


class OutputsMsap515DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_MASS_GRID_SURFACE_INDEPENDENT: (
        IdpSasMassGridSurfaceIndependentSchemaDM
    )
    """
    Mass from grid-based method using surface independent methods: Mass inferred from grid based methods using surface independent methods
    """
    IDP_SAS_RADIUS_GRID_SURFACE_INDEPENDENT: (
        IdpSasRadiusGridSurfaceIndependentSchemaDM
    )
    """
    Radius from grid-based method using surface independent methods: Radius inferred from grid based methods using surface independent methods
    """
    IDP_SAS_AGE_GRID_SURFACE_INDEPENDENT: (
        IdpSasAgeGridSurfaceIndependentSchemaDM
    )
    """
    Age from grid-based method using surface independent methods: Age inferred from grid based methods using surface independent methods
    """
    IDP_SAS_LOGG_GRID_SURFACE_INDEPENDENT: (
        IdpSasLoggGridSurfaceIndependentSchemaDM
    )
    """
    Log g from grid-based method using surface independent methods: Log g inferred from grid based methods using surface independent methods
    """
    IDP_SAS_TEFF_GRID_SURFACE_INDEPENDENT: (
        IdpSasTeffGridSurfaceIndependentSchemaDM
    )
    """
    Teff from grid-based method using surface independent methods: Teff inferred from grid-based methods using surface independent methods
    """
    IDP_SAS_L_GRID_SURFACE_INDEPENDENT: IdpSasLGridSurfaceIndependentSchemaDM
    """
    Luminosity from grid-based method using surface independent methods: Luminosity of the inferred from grid based method using surface-independent methods
    """
    IDP_SAS_DENSITY_GRID_SURFACE_INDEPENDENT: (
        IdpSasDensityGridSurfaceIndependentSchemaDM
    )
    """
    Density from grid-based method using surface independent methods : The value for the mean density will be inferred from the pdf distribution of the mean density. The pdf, in turn, is derived through forward modelling, from mean density values provided for each model in the grid
    """
    IDP_SAS_METADATA_GRID_SURFACE_INDEPENDENT: (
        IdpSasMetadataGridSurfaceIndependentSchemaDM
    )
    """
    Metadata of the optimal model with grid based methods using surface independent methods: We store in the metadata information to retrieve the optimal model to be used as reference model for the sophisticated methods (inversion, glitch), together with information on the quality of the fit
    """


class OutputsMsap521DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_AGE_GYRO: IdpSasAgeGyroSchemaDM
    """
    Age from gyrochronology: Age determined from gyrochronology
    """
    IDP_SAS_METADATA_GYRO: IdpSasMetadataGyroSchemaDM
    """
    Metadata from gyrochronology
    """


class OutputsMsap522DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_AGE_ACTIVITY: IdpSasAgeActivitySchemaDM
    """
    Age from age-activity relations: Age determined from activity-age relations
    """
    IDP_SAS_AGE_ACTIVITY_METADATA: IdpSasAgeActivityMetadataSchemaDM
    """
    Metadata age-activity relations
    """


class OutputsMsap523DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_MASS_GRANULATION: IdpSasMassGranulationSchemaDM
    """
    Mass from granulation: Mass determined using the granulation log g and the photometric radius
    """
    IDP_SAS_METADATA_GRANULATION: IdpSasMetadataGranulationSchemaDM
    """
    Metadata for non-seismic grid based methods and granulation
    """


class OutputsMsap524DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_MASS_GRANULATION_CGBM: IdpSasMassGranulationCgbmSchemaDM
    """
    Mass from non-seismic grid based methods and granulation: Mass determined from grid-based modelling with classical methods using granulation log g as a constraint
    """
    IDP_SAS_RADIUS_GRANULATION_CGBM: IdpSasRadiusGranulationCgbmSchemaDM
    """
    Radius from non-seismic grid based methods and granulation: Radius determined from grid-based modelling with classical methods using granulation log g as a constraint
    """
    IDP_SAS_AGE_GRANULATION_CGBM: IdpSasAgeGranulationCgbmSchemaDM
    """
    Age from non-seismic grid based methods and granulation: Age determined from grid-based modelling with classical methods using granulation log g as a constraint
    """
    IDP_SAS_METADATA_GRANULATION_CGBM: IdpSasMetadataGranulationCgbmSchemaDM
    """
    Metadata for non-seismic grid based methods and granulation
    """


class OutputsMsap531DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_CONSISTENCY_FLAGS: IdpSasConsistencyFlagsSchemaDM
    """
    Consistency flags (TBD): For control purposes, when anomalous values are detected when combining the parameters values into the final ones
    """


class OutputsMsap532DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_MASS_PRIORITY: IdpSasMassPrioritySchemaDM
    """
    Mass value selection: combining all mass determinations
    """
    IDP_SAS_RADIUS_PRIORITY: IdpSasRadiusPrioritySchemaDM
    """
    Radius value selection: combining all radius determinations
    """
    IDP_SAS_AGE_PRIORITY: IdpSasAgePrioritySchemaDM
    """
    Age value selection: combining all age determinations
    """


class OutputsMsap534DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP5_SAS_MASS: Dp5SasMassSchemaDM
    """
    Stellar mass: Value of the stellar mass. If relevant, selected after comparison of the different masses determined by the module
    """
    DP5_SAS_MASS_METADATA: Dp5SasMassMetadataSchemaDM
    """
    Stellar mass metadata: Contains the whole history of generation of the stellar mass
    """
    DP5_SAS_RADIUS: Dp5SasRadiusSchemaDM
    """
    Stellar radius: Value of the stellar radius. If relevant, selected after comparison of the different masses determined by the module
    """
    DP5_SAS_RADIUS_METADATA: Dp5SasRadiusMetadataSchemaDM
    """
    Stellar radius metadata: Contains the whole history of generation of the stellar radius
    """
    DP5_SAS_AGE: Dp5SasAgeSchemaDM
    """
    Stellar age: Value of the stellar age. If relevant, selected after comparison of the different masses determined by the module
    """
    DP5_SAS_AGE_METADATA: Dp5SasAgeMetadataSchemaDM
    """
    Stellar age metadata: Contains the whole history of generation of the stellar age
    """


class InputsMsap101DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_REG_LC: IdpSasRegLcSchemaDM
    """
    Time regularized LC
    """
    IDP_SAS_FLARES_FLAG: IdpSasFlaresFlagSchemaDM
    """
    Flare flag: Indications allowing to identify the points of the timeseries involved in a flare. Total length containing all the quarters
    """
    IDP_EAS_QC_TARGET_TCE_DETECTION: Annotated[
        IdpEasQcTargetTceDetectionSchemaDM, Field(title="TCE DETECTION")
    ]
    """
    EAS Quality-control-metrics per target, detection
    """
    IDP_EAS_TRANSIT_REMOVAL_KIT: Annotated[
        IdpEasTransitRemovalKitSchemaDM, Field(title="L2 transit-removal kit")
    ]
    """
    A collection of information that aids SAS in removing transit-like features from light curves. It is produced for every Plato target in each processing cycle and is made available in two versions per cycle.
    """


class Msap101SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap1_01: Annotated[
        InputsMsap101DM, Field(title="Inputs for MSAP1_01 module")
    ]
    outputs_msap1_01: Annotated[
        OutputsMsap101DM, Field(title="Outputs for MSAP1_01 module")
    ]


class InputsMsap102DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_REG_LC: IdpSasRegLcSchemaDM
    """
    Time regularized LC
    """
    ADP_SAS_FLARES: Annotated[
        Optional[AdpSasFlaresSchemaDM], Field(title="ADP_SAS_FLARES_FLAG")
    ] = None
    """
    Flares parameters : Flares parameters from preparation of the LC
    """
    ADP_SAS_FLARES_METADATA: Annotated[
        Optional[AdpSasFlaresMetadataSchemaDM],
        Field(title="ADP_SAS_FLARES_METADATA"),
    ] = None
    """
    Flares parameters metadata: Metadata for flares parameters from preparation of the LC
    """
    ADP_SAS_FLARES_FLAG: Annotated[
        Optional[AdpSasFlaresFlagSchemaDM], Field(title="ADP_SAS_FLARES_FLAG")
    ] = None
    """
    Metadata on flares detection.
    """
    IDP_SAS_FLARES_FLAG: Annotated[
        Optional[IdpSasFlaresFlagSchemaDM], Field(title="IDP_SAS_FLARES_FLAG")
    ] = None
    """
    Flare flag: Indications allowing to identify the points of the timeseries involved in a flare. Total length containing all the quarters.
    """


class InputsMsap1021DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_REG_LC: IdpSasRegLcSchemaDM
    """
    Time regularized LC
    """
    ADP_SAS_FLARES: Annotated[
        Optional[AdpSasFlaresSchemaDM], Field(title="ADP_SAS_FLARES_FLAG")
    ] = None
    """
    Flares parameters : Flares parameters from preparation of the LC
    """
    ADP_SAS_FLARES_METADATA: Annotated[
        Optional[AdpSasFlaresMetadataSchemaDM],
        Field(title="ADP_SAS_FLARES_METADATA"),
    ] = None
    """
    Flares parameters metadata: Metadata for flares parameters from preparation of the LC
    """
    ADP_SAS_FLARES_FLAG: Annotated[
        AdpSasFlaresFlagSchemaDM, Field(title="ADP_SAS_FLARES_FLAG")
    ]
    """
    Metadata on flares detection.
    """
    IDP_SAS_FLARES_FLAG: Annotated[
        IdpSasFlaresFlagSchemaDM, Field(title="IDP_SAS_FLARES_FLAG")
    ]
    """
    Flare flag: Indications allowing to identify the points of the timeseries involved in a flare. Total length containing all the quarters.
    """


class Msap102SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap1_02: Annotated[
        Union[InputsMsap102DM, InputsMsap1021DM],
        Field(title="Inputs for MSAP1_02 module"),
    ]
    outputs_msap1_02: Annotated[
        OutputsMsap102DM, Field(title="Outputs for MSAP1_02 module")
    ]


class InputsMsap103DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP1_LC_MERGED: Annotated[Dp1LcMergedSchemaDM, Field(title="DP1 LC Merged")]
    """
    DP1 data
    """


class Msap103SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap1_03: Annotated[
        InputsMsap103DM, Field(title="Inputs for MSAP1_03 module")
    ]
    outputs_msap1_03: Annotated[
        OutputsMsap103DM, Field(title="Outputs for MSAP1_03 module")
    ]


class InputsMsap107DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_VARLC: IdpSasVarlcSchemaDM
    """
    VARLC: (Non-seismic) Variability Analysis-ready LC, stitched, gaps filled, detrended
    """
    IDP_SAS_VARLC_METADATA: IdpSasVarlcMetadataSchemaDM
    """
    Metadata for VARLC: Metadata of the production of the (Non-seismic) Variability Analysis-ready LC
    """
    DP3_SAS_NU_MAX: Optional[Dp3SasNuMaxSchemaDM] = None
    """
    Frequency of maximum oscillations power: Frequency of maximum oscillations power from global analysis of AARPS, or from peak bagging if available (optional)
    """


class Msap107SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap1_07: Annotated[
        InputsMsap107DM, Field(title="Inputs for MSAP1_07 module")
    ]
    outputs_msap1_07: Annotated[
        OutputsMsap107DM, Field(title="Outputs for MSAP1_07 module")
    ]


class Msap108SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap1_08: Annotated[
        InputsMsap108DM, Field(title="Inputs for MSAP1_08 module")
    ]
    outputs_msap1_08: Annotated[
        OutputsMsap108DM, Field(title="Outputs for MSAP1_08 module")
    ]


class Msap109SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap1_09: Annotated[
        InputsMsap109DM, Field(title="Inputs for MSAP1_01 module")
    ]
    outputs_msap1_09: Annotated[
        OutputsMsap109DM, Field(title="Outputs for MSAP1_09 module")
    ]


class InputsMsap2123DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    LG_PFU_OBS_DATA: LGPFUOBSDATADM
    """
    Contains Gaia BP-RP spectrophotometry from final release and Spectra as described in the PDD (under PFU Observed spectrum)
    """
    IDP_SAS_LOGG_FLIPER: IdpSasLoggFliperSchemaDM
    """
    Log g from variability[WP12PDP_I33]: Log g determined from the variability of the light curve
    """
    DP3_SAS_DELTA_NU_AV: Optional[Dp3SasDeltaNuAvSchemaDM] = None
    """
    Average large frequency separation: Average large frequency separation from global analysis of Asteroseismic Analysis Ready Power spectrum (AARPS), or from peak bagging wen available (optional)
    """
    DP3_SAS_NU_MAX: Dp3SasNuMaxSchemaDM
    """
    Frequency of maximum oscillations power: Frequency of maximum oscillations power from global analysis of AARPS, or from peak bagging if available (optional)
    """
    IDP_PFU_TEFF_SAPP: IDPPFUTEFFSAPPDM
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: Optional[IdpSasTeffSappSchemaDM] = None
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_PFU_LOGG_SAPP: IDPPFULOGGSAPPDM
    """
    Value of log g selected after statistical analysis: combination of values yielded from the SAPP pipeline
    """
    IDP_SAS_LOGG_SAPP: Optional[IdpSasLoggSappSchemaDM] = None
    """
    Combined value for log g: Value of log g selected after statistical analysis : combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_PFU__FE_H__SAPP: Annotated[
        IDPPFUFEHSAPPDM, Field(alias="IDP_PFU_[FE_H]_SAPP")
    ]
    """
    Iron over hydrogen abundance ratio by number relative to the solar value
    """
    IDP_SAS__FE_H__SAPP: Annotated[
        Optional[IdpSasFeHSappSchemaDM], Field(alias="IDP_SAS_[FE_H]_SAPP")
    ] = None
    """
    Combined value for [Fe/H]: Value of Fe/H selected after statistical analysis: combination of values yielded from the SAPP pipeline, after validation by WP12500
    """


class InputsMsap21231DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    LG_PFU_OBS_DATA: LGPFUOBSDATADM
    """
    Contains Gaia BP-RP spectrophotometry from final release and Spectra as described in the PDD (under PFU Observed spectrum)
    """
    IDP_SAS_LOGG_FLIPER: IdpSasLoggFliperSchemaDM
    """
    Log g from variability[WP12PDP_I33]: Log g determined from the variability of the light curve
    """
    DP3_SAS_DELTA_NU_AV: Optional[Dp3SasDeltaNuAvSchemaDM] = None
    """
    Average large frequency separation: Average large frequency separation from global analysis of Asteroseismic Analysis Ready Power spectrum (AARPS), or from peak bagging wen available (optional)
    """
    DP3_SAS_NU_MAX: Dp3SasNuMaxSchemaDM
    """
    Frequency of maximum oscillations power: Frequency of maximum oscillations power from global analysis of AARPS, or from peak bagging if available (optional)
    """
    IDP_PFU_TEFF_SAPP: Optional[IDPPFUTEFFSAPPDM] = None
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: IdpSasTeffSappSchemaDM
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_PFU_LOGG_SAPP: Optional[IDPPFULOGGSAPPDM] = None
    """
    Value of log g selected after statistical analysis: combination of values yielded from the SAPP pipeline
    """
    IDP_SAS_LOGG_SAPP: IdpSasLoggSappSchemaDM
    """
    Combined value for log g: Value of log g selected after statistical analysis : combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_PFU__FE_H__SAPP: Annotated[
        Optional[IDPPFUFEHSAPPDM], Field(alias="IDP_PFU_[FE_H]_SAPP")
    ] = None
    """
    Iron over hydrogen abundance ratio by number relative to the solar value
    """
    IDP_SAS__FE_H__SAPP: Annotated[
        IdpSasFeHSappSchemaDM, Field(alias="IDP_SAS_[FE_H]_SAPP")
    ]
    """
    Combined value for [Fe/H]: Value of Fe/H selected after statistical analysis: combination of values yielded from the SAPP pipeline, after validation by WP12500
    """


class Msap2123SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap2_123: Annotated[
        Union[InputsMsap2123DM, InputsMsap21231DM],
        Field(title="Inputs for MSAP2_123 module"),
    ]
    outputs_msap2_123: Annotated[
        OutputsMsap2123DM, Field(title="Outputs for MSAP2_123 module")
    ]


class InputsMsap301DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_AARPSD: IdpSasAarpsdSchemaDM
    """
    AARPSD[WP12PDP_I32]: Asteroseismic Analysis-ready power spectrum.Frequency-power spectra of the AARLC (AARPS), including information on the spectral window function
    """
    IDP_SAS_AARPSD_METADATA: IdpSasAarpsdMetadataSchemaDM
    """
    Metadata for AARPSD[WP12PDP_I32]: Metadata for the frequency-power spectra of the AARLC (AARPS), including information on the spectral window function, and the duty cycle
    """
    IDP_PFU_RADIUS_CLASSICAL: IDPPFURADIUSCLASSICALDM
    """
    Radius determined from classical methods (non seismic) in R⊙
    """
    IDP_PFU_TEFF_SAPP: IDPPFUTEFFSAPPDM
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: Optional[IdpSasTeffSappSchemaDM] = None
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """


class InputsMsap3011DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_AARPSD: IdpSasAarpsdSchemaDM
    """
    AARPSD[WP12PDP_I32]: Asteroseismic Analysis-ready power spectrum.Frequency-power spectra of the AARLC (AARPS), including information on the spectral window function
    """
    IDP_SAS_AARPSD_METADATA: IdpSasAarpsdMetadataSchemaDM
    """
    Metadata for AARPSD[WP12PDP_I32]: Metadata for the frequency-power spectra of the AARLC (AARPS), including information on the spectral window function, and the duty cycle
    """
    IDP_PFU_RADIUS_CLASSICAL: IDPPFURADIUSCLASSICALDM
    """
    Radius determined from classical methods (non seismic) in R⊙
    """
    IDP_PFU_TEFF_SAPP: Optional[IDPPFUTEFFSAPPDM] = None
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: IdpSasTeffSappSchemaDM
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """


class Msap301SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap3_01: Annotated[
        Union[InputsMsap301DM, InputsMsap3011DM],
        Field(title="Inputs for MSAP3_01 module"),
    ]
    outputs_msap3_01: Annotated[
        OutputsMsap301DM, Field(title="Outputs for MSAP3_01 module")
    ]


class Msap302SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap3_02: Annotated[
        Union[InputsMsap302DM, InputsMsap3021DM],
        Field(title="Inputs for MSAP3_02 module"),
    ]
    outputs_msap3_02: Annotated[
        OutputsMsap302DM, Field(title="Outputs for MSAP3_02 module")
    ]


class Msap303SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap3_03: Annotated[
        Union[InputsMsap303DM, InputsMsap3031DM],
        Field(title="Inputs for MSAP3_03 module"),
    ]
    outputs_msap3_03: Annotated[
        OutputsMsap303DM, Field(title="Outputs for MSAP3_03 module")
    ]


class Msap304SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap3_04: Annotated[
        InputsMsap304DM, Field(title="Inputs for MSAP3_04 module")
    ]
    outputs_msap3_04: Annotated[
        OutputsMsap304DM, Field(title="Outputs for MSAP3_04 module")
    ]


class Msap401SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap4_01: Annotated[
        Union[InputsMsap401DM, InputsMsap4011DM],
        Field(title="Inputs for MSAP4_01 module"),
    ]
    outputs_msap4_01: Annotated[
        OutputsMsap401DM, Field(title="Outputs for MSAP4_01 module")
    ]


class Msap402SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap4_02: Annotated[
        InputsMsap402DM, Field(title="Inputs for MSAP4_02 module")
    ]
    outputs_msap4_02: Annotated[
        OutputsMsap402DM, Field(title="Outputs for MSAP4_02 module")
    ]


class Msap403SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap4_03: Annotated[
        Union[InputsMsap403DM, InputsMsap4031DM],
        Field(title="Inputs for MSAP4_03 module"),
    ]
    outputs_msap4_03: Annotated[
        OutputsMsap403DM, Field(title="Outputs for MSAP4_03 module")
    ]


class Msap406SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap4_06: Annotated[
        InputsMsap406DM, Field(title="Inputs for MSAP4_06 module")
    ]
    outputs_msap4_06: Annotated[
        OutputsMsap406DM, Field(title="Outputs for MSAP4_06 module")
    ]


class InputsMsap511DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP3_SAS_DELTA_NU_AV: Dp3SasDeltaNuAvSchemaDM
    """
    Average large frequency separation: Average large frequency separation from global analysis of Asteroseismic Analysis Ready Power spectrum (AARPS), or from peak bagging wen available (optional)
    """
    DP3_SAS_NU_MAX: Dp3SasNuMaxSchemaDM
    """
    Frequency of maximum oscillations power: Frequency of maximum oscillations power from global analysis of AARPS, or from peak bagging if available (optional)
    """
    IDP_PFU_TEFF_SAPP: Optional[IDPPFUTEFFSAPPDM] = None
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: IdpSasTeffSappSchemaDM
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_PFU__FE_H__SAPP: Annotated[
        Optional[IDPPFUFEHSAPPDM], Field(alias="IDP_PFU_[FE_H]_SAPP")
    ] = None
    """
    Iron over hydrogen abundance ratio by number relative to the solar value.
    """
    IDP_SAS__FE_H__SAPP: Annotated[
        IdpSasFeHSappSchemaDM, Field(alias="IDP_SAS_[FE_H]_SAPP")
    ]
    """
    Combined value for [Fe/H]: Value of Fe/H selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """
    IDP_PFU__ALPHA_FE__SAPP: Annotated[
        Optional[IDPPFUALPHAFESAPPDM], Field(alias="IDP_PFU_[ALPHA_FE]_SAPP")
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value in dex
    """
    IDP_SAS__ALPHA_FE__SAPP: Annotated[
        Optional[IdpSasAlphaFeSappSchemaDM],
        Field(alias="IDP_SAS_[ALPHA_FE]_SAPP"),
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value
    """
    IDP_PFU_L_IRFM: Optional[IDPPFULIRFMDM] = None
    """
    Luminosity from infrared flux method (IRFM) in L⊙
    """
    PDP_WP12_MOD_EVOL: PDPWP12MODEVOLDM
    """
    Stellar evolution tracks. They contain: mass, age, Teff, L, radius, surface chemical composition, central temperature, density, boundary of convective envelopes (both in radius and mass), surface rotation velocity (if rotation is included in the calculations)
    """
    IDP_PFU_COVMAT_SAPP: Optional[IDPPFUCOVMATSAPPDM] = None
    """
    Covariance matrix for classical parameters from the SAPP pipeline
    """
    IDP_SAS_COVMAT_SAPP: IdpSasCovmatSappSchemaDM
    """
    COVARIANCE matrix classical parameters: Covariance matrix for classical parameters from the SAPP module
    """


class InputsMsap5111DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP3_SAS_DELTA_NU_AV: Dp3SasDeltaNuAvSchemaDM
    """
    Average large frequency separation: Average large frequency separation from global analysis of Asteroseismic Analysis Ready Power spectrum (AARPS), or from peak bagging wen available (optional)
    """
    DP3_SAS_NU_MAX: Dp3SasNuMaxSchemaDM
    """
    Frequency of maximum oscillations power: Frequency of maximum oscillations power from global analysis of AARPS, or from peak bagging if available (optional)
    """
    IDP_PFU_TEFF_SAPP: IDPPFUTEFFSAPPDM
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: Optional[IdpSasTeffSappSchemaDM] = None
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_PFU__FE_H__SAPP: Annotated[
        IDPPFUFEHSAPPDM, Field(alias="IDP_PFU_[FE_H]_SAPP")
    ]
    """
    Iron over hydrogen abundance ratio by number relative to the solar value.
    """
    IDP_SAS__FE_H__SAPP: Annotated[
        Optional[IdpSasFeHSappSchemaDM], Field(alias="IDP_SAS_[FE_H]_SAPP")
    ] = None
    """
    Combined value for [Fe/H]: Value of Fe/H selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """
    IDP_PFU__ALPHA_FE__SAPP: Annotated[
        Optional[IDPPFUALPHAFESAPPDM], Field(alias="IDP_PFU_[ALPHA_FE]_SAPP")
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value in dex
    """
    IDP_SAS__ALPHA_FE__SAPP: Annotated[
        Optional[IdpSasAlphaFeSappSchemaDM],
        Field(alias="IDP_SAS_[ALPHA_FE]_SAPP"),
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value
    """
    IDP_PFU_L_IRFM: Optional[IDPPFULIRFMDM] = None
    """
    Luminosity from infrared flux method (IRFM) in L⊙
    """
    PDP_WP12_MOD_EVOL: PDPWP12MODEVOLDM
    """
    Stellar evolution tracks. They contain: mass, age, Teff, L, radius, surface chemical composition, central temperature, density, boundary of convective envelopes (both in radius and mass), surface rotation velocity (if rotation is included in the calculations)
    """
    IDP_PFU_COVMAT_SAPP: IDPPFUCOVMATSAPPDM
    """
    Covariance matrix for classical parameters from the SAPP pipeline
    """
    IDP_SAS_COVMAT_SAPP: Optional[IdpSasCovmatSappSchemaDM] = None
    """
    COVARIANCE matrix classical parameters: Covariance matrix for classical parameters from the SAPP module
    """


class Msap511SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap5_11: Annotated[
        Union[InputsMsap511DM, InputsMsap5111DM],
        Field(title="Inputs for MSAP5_11 submodule"),
    ]
    outputs_msap5_11: Annotated[
        OutputsMsap511DM, Field(title="Outputs for MSAP5_11 submodule")
    ]


class Msap512SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap5_12: Annotated[
        Union[InputsMsap512DM, InputsMsap5121DM],
        Field(title="Inputs for MSAP5_12 submodule"),
    ]
    outputs_msap5_12: Annotated[
        OutputsMsap512DM, Field(title="Outputs for MSAP5_12 submodule")
    ]


class InputsMsap513DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP3_SAS_DELTA_NU_AV: Dp3SasDeltaNuAvSchemaDM
    """
    Average large frequency separation: Average large frequency separation from global analysis of Asteroseismic Analysis Ready Power spectrum (AARPS), or from peak bagging wen available (optional)
    """
    DP3_SAS_NU_MAX: Dp3SasNuMaxSchemaDM
    """
    Frequency of maximum oscillations power: Frequency of maximum oscillations power from global analysis of AARPS, or from peak bagging if available (optional)
    """
    IDP_PFU_TEFF_SAPP: Optional[IDPPFUTEFFSAPPDM] = None
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: IdpSasTeffSappSchemaDM
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_PFU__FE_H__SAPP: Annotated[
        Optional[IDPPFUFEHSAPPDM], Field(alias="IDP_PFU_[FE_H]_SAPP")
    ] = None
    """
    Iron over hydrogen abundance ratio by number relative to the solar value.
    """
    IDP_SAS__FE_H__SAPP: Annotated[
        IdpSasFeHSappSchemaDM, Field(alias="IDP_SAS_[FE_H]_SAPP")
    ]
    """
    Combined value for [Fe/H]: Value of Fe/H selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """
    IDP_PFU__ALPHA_FE__SAPP: Annotated[
        Optional[IDPPFUALPHAFESAPPDM], Field(alias="IDP_PFU_[ALPHA_FE]_SAPP")
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value in dex
    """
    IDP_SAS__ALPHA_FE__SAPP: Annotated[
        Optional[IdpSasAlphaFeSappSchemaDM],
        Field(alias="IDP_SAS_[ALPHA_FE]_SAPP"),
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value
    """
    IDP_PFU_L_IRFM: Optional[IDPPFULIRFMDM] = None
    """
    Luminosity from infrared flux method (IRFM) in L⊙
    """
    PDP_WP12_MOD_EVOL: PDPWP12MODEVOLDM
    """
    Stellar evolution tracks. They contain: mass, age, Teff, L, radius, surface chemical composition, central temperature, density, boundary of convective envelopes (both in radius and mass), surface rotation velocity (if rotation is included in the calculations)
    """
    PDP_WP12_MOD_OSC_FREQ: PDPWP12MODOSCFREQDM
    """
    Stellar oscillations grids: For all models in PDP_B_SAS_MOD-EVOL, adiabatic frequencies will be provided for low angular degree, l=0, 1, 2, 3, modes.
    """
    IDP_PFU_COVMAT_SAPP: Optional[IDPPFUCOVMATSAPPDM] = None
    """
    Covariance matrix for classical parameters from the SAPP pipeline
    """
    IDP_SAS_COVMAT_SAPP: IdpSasCovmatSappSchemaDM
    """
    COVARIANCE matrix classical parameters: Covariance matrix for classical parameters from the SAPP module
    """
    DP3_SAS_OSC_FREQ: Dp3SasOscFreqSchemaDM
    """
    Oscillation mode frequencies: Inferred from the power spectra. Individual frequencies from peakbagging
    """
    DP3_SAS_OSC_FREQ_COVMAT: Dp3SasOscFreqCovmatSchemaDM
    """
    Covariance matrix: Covariance matrix for the fitted frequencies
    """
    DP3_SAS_OSC_FREQ_METADATA: Dp3SasOscFreqMetadataSchemaDM
    """
    Metadata on frequencies: Metadata on individual frequencies, including radial p-mode order and angular degree (latter to include separate explicit entry for mixed modes)
    """
    IDP_SAS_FREQUENCIES_FIRST_GUESS_METADATA: (
        IdpSasFrequenciesFirstGuessMetadataSchemaDM
    )
    """
    First-guess frequency list metadata: Metadata on the first-guess frequency list from peak-bagging preparation (optional)
    """


class InputsMsap5131DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    DP3_SAS_DELTA_NU_AV: Dp3SasDeltaNuAvSchemaDM
    """
    Average large frequency separation: Average large frequency separation from global analysis of Asteroseismic Analysis Ready Power spectrum (AARPS), or from peak bagging wen available (optional)
    """
    DP3_SAS_NU_MAX: Dp3SasNuMaxSchemaDM
    """
    Frequency of maximum oscillations power: Frequency of maximum oscillations power from global analysis of AARPS, or from peak bagging if available (optional)
    """
    IDP_PFU_TEFF_SAPP: IDPPFUTEFFSAPPDM
    """
    Value of Teff selected after statistical analysis in K: combination of values yielded from the SAPP pipeline, after validation by WP125200
    """
    IDP_SAS_TEFF_SAPP: Optional[IdpSasTeffSappSchemaDM] = None
    """
    Combined value for Teff: Value of Teff selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200.
    """
    IDP_PFU__FE_H__SAPP: Annotated[
        IDPPFUFEHSAPPDM, Field(alias="IDP_PFU_[FE_H]_SAPP")
    ]
    """
    Iron over hydrogen abundance ratio by number relative to the solar value.
    """
    IDP_SAS__FE_H__SAPP: Annotated[
        Optional[IdpSasFeHSappSchemaDM], Field(alias="IDP_SAS_[FE_H]_SAPP")
    ] = None
    """
    Combined value for [Fe/H]: Value of Fe/H selected after statistical analysis: combination of values yielded from the SAPP module, after validation by WP125200
    """
    IDP_PFU__ALPHA_FE__SAPP: Annotated[
        Optional[IDPPFUALPHAFESAPPDM], Field(alias="IDP_PFU_[ALPHA_FE]_SAPP")
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value in dex
    """
    IDP_SAS__ALPHA_FE__SAPP: Annotated[
        Optional[IdpSasAlphaFeSappSchemaDM],
        Field(alias="IDP_SAS_[ALPHA_FE]_SAPP"),
    ] = None
    """
    Mean abundance ratio by number of the α elements over iron relative to the solar value
    """
    IDP_PFU_L_IRFM: Optional[IDPPFULIRFMDM] = None
    """
    Luminosity from infrared flux method (IRFM) in L⊙
    """
    PDP_WP12_MOD_EVOL: PDPWP12MODEVOLDM
    """
    Stellar evolution tracks. They contain: mass, age, Teff, L, radius, surface chemical composition, central temperature, density, boundary of convective envelopes (both in radius and mass), surface rotation velocity (if rotation is included in the calculations)
    """
    PDP_WP12_MOD_OSC_FREQ: PDPWP12MODOSCFREQDM
    """
    Stellar oscillations grids: For all models in PDP_B_SAS_MOD-EVOL, adiabatic frequencies will be provided for low angular degree, l=0, 1, 2, 3, modes.
    """
    IDP_PFU_COVMAT_SAPP: IDPPFUCOVMATSAPPDM
    """
    Covariance matrix for classical parameters from the SAPP pipeline
    """
    IDP_SAS_COVMAT_SAPP: Optional[IdpSasCovmatSappSchemaDM] = None
    """
    COVARIANCE matrix classical parameters: Covariance matrix for classical parameters from the SAPP module
    """
    DP3_SAS_OSC_FREQ: Dp3SasOscFreqSchemaDM
    """
    Oscillation mode frequencies: Inferred from the power spectra. Individual frequencies from peakbagging
    """
    DP3_SAS_OSC_FREQ_COVMAT: Dp3SasOscFreqCovmatSchemaDM
    """
    Covariance matrix: Covariance matrix for the fitted frequencies
    """
    DP3_SAS_OSC_FREQ_METADATA: Dp3SasOscFreqMetadataSchemaDM
    """
    Metadata on frequencies: Metadata on individual frequencies, including radial p-mode order and angular degree (latter to include separate explicit entry for mixed modes)
    """
    IDP_SAS_FREQUENCIES_FIRST_GUESS_METADATA: Optional[
        IdpSasFrequenciesFirstGuessMetadataSchemaDM
    ] = None
    """
    First-guess frequency list metadata: Metadata on the first-guess frequency list from peak-bagging preparation (optional)
    """


class Msap513SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap5_13: Annotated[
        Union[InputsMsap513DM, InputsMsap5131DM],
        Field(title="Inputs for MSAP5_13 submodule"),
    ]
    outputs_msap5_13: Annotated[
        OutputsMsap513DM, Field(title="Outputs for MSAP5_13 submodule")
    ]


class Msap514SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap5_14: Annotated[
        Union[InputsMsap514DM, InputsMsap5141DM],
        Field(title="Inputs for MSAP5_14 submodule"),
    ]
    outputs_msap5_14: Annotated[
        OutputsMsap514DM, Field(title="Outputs for MSAP5_14 submodule")
    ]


class Msap515SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap5_15: Annotated[
        Union[InputsMsap515DM, InputsMsap5151DM],
        Field(title="Inputs for MSAP5_15 submodule"),
    ]
    outputs_msap5_15: Annotated[
        OutputsMsap515DM, Field(title="Outputs for MSAP5_15 submodule")
    ]


class Msap521SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap5_21: Annotated[
        InputsMsap521DM, Field(title="Inputs for MSAP5_21 submodule")
    ]
    outputs_msap5_21: Annotated[
        OutputsMsap521DM, Field(title="Outputs for MSAP5_21 submodule")
    ]


class Msap522SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap5_22: Annotated[
        Union[InputsMsap522DM, InputsMsap5221DM],
        Field(title="Inputs for MSAP5_22 submodule"),
    ]
    outputs_msap5_22: Annotated[
        OutputsMsap522DM, Field(title="Outputs for MSAP5_22 submodule")
    ]


class InputsMsap523DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    IDP_SAS_LOGG_FLIPER: Optional[IdpSasLoggFliperSchemaDM] = None
    """
    Log g from variability[WP12PDP_I33]: Log g determined from the variability of the light curve
    """
    IDP_PFU_RADIUS_SAPP: IDPPFURADIUSSAPPDM
    """
    Stellar radius combined by MSetSci1 from various methods in R⊙
    """


class Msap523SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap5_23: Annotated[
        InputsMsap523DM, Field(title="Inputs for MSAP5_23 submodule")
    ]
    outputs_msap5_23: Annotated[
        OutputsMsap523DM, Field(title="Outputs for MSAP5_23 submodule")
    ]


class Msap524SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap5_24: Annotated[
        Union[InputsMsap524DM, InputsMsap5241DM],
        Field(title="Inputs for MSAP5_24 submodule"),
    ]
    outputs_msap5_24: Annotated[
        OutputsMsap524DM, Field(title="Outputs for MSAP5_24 submodule")
    ]


class Msap531SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap5_31: Annotated[
        InputsMsap531DM, Field(title="Inputs for MSAP5_31 submodule")
    ]
    outputs_msap5_31: Annotated[
        OutputsMsap531DM, Field(title="Outputs for MSAP5_31 submodule")
    ]


class Msap532SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap5_32: Annotated[
        InputsMsap532DM, Field(title="Inputs for MSAP5_32 submodule")
    ]
    outputs_msap5_32: Annotated[
        OutputsMsap532DM, Field(title="Outputs for MSAP5_32 submodule")
    ]


class Msap534SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    inputs_msap5_34: Annotated[
        InputsMsap534DM, Field(title="Inputs for MSAP5_34 submodule")
    ]
    outputs_msap5_34: Annotated[
        OutputsMsap534DM, Field(title="Outputs for MSAP5_34 submodule")
    ]


class Dp1SchemaDM(Dp1LcMergedSchemaDM, Dp1MetadataSchemaDM):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )


class MstesciSchemaDM(Mstesci1SchemaDM, Mstesci2SchemaDM):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )


class Wp12SchemaDM(Wp121SchemaDM, Wp122SchemaDM):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )


class SasMsap1SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP1_01: Annotated[OutputsMsap101DM, Field(title="MSAP1-01")]
    """
    Identification and flag of flares
    """
    MSAP1_02: Annotated[OutputsMsap102DM, Field(title="MSAP1-02")]
    """
    Removes the transit and/or flares from the LC. Fills the gaps in the L1 light-curve, induced by transit removal, and by flares removal.
    """
    MSAP1_03: Annotated[OutputsMsap103DM, Field(title="MSAP1_03 module")]
    """
    03 Time regulation
    """
    MSAP1_07: Annotated[OutputsMsap107DM, Field(title="MSAP1-07")]
    """
    Filters long-term variability for asteroseismic analysis.
    """
    MSAP1_08: Annotated[OutputsMsap108DM, Field(title="MSAP1-08")]
    """
    Binning the light curves for rotation and activity analysis.
    """
    MSAP1_09: Annotated[OutputsMsap109DM, Field(title="MSAP1_09 module")]
    """
    09 69 days filter PSD computation
    """


class SasMsap2SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    SAS_PIP_MSAP2_123: Annotated[
        Optional[OutputsMsap2123DM], Field(title="SAS-PIP-MSAP2-123")
    ] = None
    """
    Computes the seismic log g from DP3 (if available). If both available, selects between the log g inferred from seismic analysis or rotation/activity analysis.
    """


class SasMsap3SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP3_01: Annotated[OutputsMsap301DM, Field(title="MSAP3-01")]
    """
    Estimates the probability that an oscillation envelope is present in the power spectrum of the ligth curve.
    """
    MSAP3_02: Annotated[OutputsMsap302DM, Field(title="MSAP3-02")]
    """
    Estimates the probability density function for the large separation.
    """
    MSAP3_03: Annotated[OutputsMsap303DM, Field(title="MSAP3-03")]
    """
    Where detection is positive, provides global asteroseismic parameters and associated uncertainties and prepares for peak-bagging procedure by doing mode identification and by estimating background noise level.
    """
    MSAP3_04: Annotated[
        Optional[OutputsMsap304DM], Field(title="MSAP3-04")
    ] = None
    """
    In cases with both an oscillation detection and sufficient signal to noise ratio, this sub-component should provide individual frequencies and other individual mode parameters, covariance matrices, and associated uncertainties.
    """


class SasMsap4SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP4_01: Annotated[OutputsMsap401DM, Field(title="MSAP4-01")]
    """
    Using a Fourier analysis, measures the stellar Rotation Period, the length of photometric cyclical long-term variations and extracts and characterizes the components that constitute the background signal.
    """
    MSAP4_02: Annotated[OutputsMsap402DM, Field(title="MSAP4-02")]
    """
    This module has the purpose of measuring the stellar Rotation Period and photometric Activity Cycle length in the time domain from the flux timeserie.
    """
    MSAP4_03: Annotated[OutputsMsap403DM, Field(title="MSAP4-03")]
    """
    Determines the mean rotation period and differential rotation, together with their 1-sigma uncertainty and False-Alarm probability.
    """
    MSAP4_04: Annotated[OutputsMsap404DM, Field(title="MSAP4-04")]
    """
    Determines the surface gravity from the amplitude of the light fluctuations produced by convective motions together with its 1-sigma uncertainty.
    """
    MSAP4_06: Annotated[OutputsMsap406DM, Field(title="MSAP4-06")]
    """
    Estimates the mean activity level and the range of activity level from the time-series of a target.
    """


class Msap51SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP5_11: Annotated[
        Optional[OutputsMsap511DM], Field(title="MSAP5-11")
    ] = None
    """
    Computes an estimate of stellar mass, radius, and surface gravity from seismic scaling relations, effective temperature, as well as grids of stellar models.
    """
    MSAP5_12: Annotated[
        Optional[OutputsMsap512DM], Field(title="MSAP5-12")
    ] = None
    """
    Computes an estimate of stellar mass, radius, and surface gravity from seismic scaling relations and effective temperature.
    """
    MSAP5_13: Annotated[
        Optional[OutputsMsap513DM], Field(title="MSAP5-13")
    ] = None
    """
    For stars with detected individual frequencies and mixed modes, it computes an estimate of stellar mass, radius, and age from a seismic forward inference procedure using classical stellar parameters and individual frequencies as constraints.
    """
    MSAP5_14: Annotated[
        Optional[OutputsMsap514DM], Field(title="MSAP5-14")
    ] = None
    """
    For stars with detected individual frequencies, it computes an estimate of stellar mass, radius, and age from a seismic forward inference using classical stellar parameters and individual frequencies as constraints.
    """
    MSAP5_15: Annotated[
        Optional[OutputsMsap515DM], Field(title="MSAP5-15")
    ] = None
    """
    For stars with detected individual frequencies, it computes an estimate of stellar mass, radius, and age from a seismic forward inference using classical stellar parameters and combination of individual frequencies using a surface-independent method.
    """


class Msap52SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP5_21: Annotated[
        Optional[OutputsMsap521DM], Field(title="MSAP5-21")
    ] = None
    """
    This sub-component selects and applies a Rotation-Mass-Age relationship, using the mean stellar rotation period, to derive a gyrochronology age.
    """
    MSAP5_22: Annotated[
        Optional[OutputsMsap522DM], Field(title="MSAP5-22")
    ] = None
    """
    When stellar activity measurements are available from ground-based observations of stellar activity, it selects an age-activity relation and determines the stellar age.
    """
    MSAP5_23: Annotated[
        Optional[OutputsMsap523DM], Field(title="MSAP5-23")
    ] = None
    """
    This sub-component derives an estimate for the stellar mass from surface gravity inferred using flickering and the radius inferred using the luminosity and effective temperature.
    """
    MSAP5_24: Annotated[
        Optional[OutputsMsap524DM], Field(title="MSAP5-24")
    ] = None
    """
    This sub-component computes an estimate for stellar mass, radius, and age from a model fitting of classical constraints (stellar effective temperature, luminosity, surface gravity, radius –if available– and chemical composition) and surface gravity obtained using flickering.
    """


class Msap53SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP5_31: Annotated[OutputsMsap531DM, Field(title="MSAP5-31")]
    """
    Compares stellar properties from seismic and non-seismic inferences, and flags inconsistencies if any.
    """
    MSAP5_32: Annotated[
        Optional[OutputsMsap532DM], Field(title="MSAP5-32")
    ] = None
    """
    Produces the 'best set of parameters' from the assessment of the different types of parameter determination (seismic and/or non-seismic) taking into account their statistical properties.
    """
    MSAP5_34: Annotated[OutputsMsap534DM, Field(title="MSAP5-34")]
    """
    Verifies that the set of stellar parameters and properties selected as the ‘best set of parameters’ for each PLATO target falls within the range of expected values (as provided by, but not only, benchmark stars).
    """


class SasMsap513SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP5_1: Annotated[Msap51SchemaDM, Field(title="MSAP5-1")]
    """
    MRA determination from seismic data
    """
    MSAP5_3: Annotated[Msap53SchemaDM, Field(title="MSAP5-3")]
    """
    Selection and validation
    """


class SasMsap523SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP5_2: Annotated[Msap52SchemaDM, Field(title="MSAP5-2")]
    """
    MRA determination from non seismic data
    """
    MSAP5_3: Annotated[Msap53SchemaDM, Field(title="MSAP5-3")]
    """
    Selection and validation
    """


class SasMsap1SchemaModelDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP1_01: Annotated[Msap101SchemaDM, Field(title="MSAP1_01 module")]
    """
    01 Transit/Flare removal and gaps filling PDS computation
    """
    MSAP1_02: Annotated[Msap102SchemaDM, Field(title="MSAP1_02 module")]
    """
    02 Flares detection
    """
    MSAP1_03: Annotated[Msap103SchemaDM, Field(title="MSAP1_03 module")]
    """
    03 Time regulation
    """
    MSAP1_07: Annotated[Msap107SchemaDM, Field(title="MSAP1_07 module")]
    """
    07 Long term filter PSD computation
    """
    MSAP1_08: Annotated[Msap108SchemaDM, Field(title="MSAP1_08 module")]
    """
    08 Binning PSD computation
    """
    MSAP1_09: Annotated[Msap109SchemaDM, Field(title="MSAP1_09 module")]
    """
    09 69 days filter PSD computation
    """


class SasMsap2SchemaModelDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP2_123: Annotated[Msap2123SchemaDM, Field(title="MSAP2_123 module")]
    """
    Classical stellar parameters determination
    """


class SasMsap3SchemaModelDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP3_01: Annotated[Msap301SchemaDM, Field(title="MSAP3_01 module")]
    """
    01 Power excess detection
    """
    MSAP3_02: Annotated[Msap302SchemaDM, Field(title="MSAP3_02 module")]
    """
    02 Large separation detection
    """
    MSAP3_03: Annotated[Msap303SchemaDM, Field(title="MSAP3_03 module")]
    """
    03 Global asteroseismic parameters & mode detection
    """
    MSAP3_04: Annotated[
        Optional[Msap304SchemaDM], Field(title="MSAP3_04 module")
    ] = None
    """
    01 Peak-bagging
    """


class SasMsap4SchemaModelDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP4_01: Annotated[Msap401SchemaDM, Field(title="MSAP4_01 module")]
    """
    01 Fourier analysis
    """
    MSAP4_02: Annotated[Msap402SchemaDM, Field(title="MSAP4_02 module")]
    """
    02 Time series analysis
    """
    MSAP4_03: Annotated[Msap403SchemaDM, Field(title="MSAP4_03 module")]
    """
    03 Rotation period determination
    """
    MSAP4_04: Annotated[Msap404SchemaDM, Field(title="MSAP4_04 module")]
    """
    04 Gravity from Flicker/FliPer
    """
    MSAP4_06: Annotated[Msap406SchemaDM, Field(title="MSAP4_06 module")]
    """
    06 Activity cycle determination
    """


class Msap51SchemaModelDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP5_11: Annotated[Msap511SchemaDM, Field(title="MSAP5_11 submodule")]
    """
    MSAP5_11 submodule
    """
    MSAP5_12: Annotated[Msap512SchemaDM, Field(title="MSAP5_12 submodule")]
    """
    MSAP5_12 submodule
    """
    MSAP5_13: Annotated[
        Optional[Msap513SchemaDM], Field(title="MSAP5_13 submodule")
    ] = None
    """
    MSAP5_13 submodule
    """
    MSAP5_14: Annotated[
        Optional[Msap514SchemaDM], Field(title="MSAP5_14 submodule")
    ] = None
    """
    MSAP5_14 submodule
    """
    MSAP5_15: Annotated[
        Optional[Msap515SchemaDM], Field(title="MSAP5_15 submodule")
    ] = None
    """
    MSAP5_15 submodule Inputs/Outputs
    """


class Msap52SchemaModelDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP5_21: Annotated[
        Optional[Msap521SchemaDM], Field(title="MSAP5_21 submodule")
    ] = None
    """
    MSAP5_21 submodule Inputs/Outputs
    """
    MSAP5_22: Annotated[
        Optional[Msap522SchemaDM], Field(title="MSAP5_22 submodule")
    ] = None
    """
    MSAP5_22 submodule Inputs/Outputs
    """
    MSAP5_23: Annotated[
        Optional[Msap523SchemaDM], Field(title="MSAP5_23 submodule")
    ] = None
    """
    MSAP5_23 submodule Inputs/Outputs
    """
    MSAP5_24: Annotated[
        Optional[Msap524SchemaDM], Field(title="MSAP5_24 submodule")
    ] = None
    """
    MSAP5_24 submodule Inputs/Outputs
    """


class Msap53SchemaModelDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP5_31: Annotated[Msap531SchemaDM, Field(title="MSAP5_31 submodule")]
    """
    MSAP5_31 submodule Inputs/Outputs
    """
    MSAP5_32: Annotated[
        Optional[Msap532SchemaDM], Field(title="MSAP5_32 submodule")
    ] = None
    """
    MSAP5_32 submodule Inputs/Outputs
    """
    MSAP5_34: Annotated[Msap534SchemaDM, Field(title="MSAP5_34 submodule")]
    """
    MSAP5_34 submodule Inputs/Outputs
    """


class SasExternalInputsSchemaDM(
    Dp1SchemaDM, EasSchemaDM, MstesciSchemaDM, Wp12SchemaDM
):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )


class SasMsap5123SchemaDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP5_1: Annotated[Msap51SchemaDM, Field(title="MSAP5-1")]
    """
    MRA determination from seismic data
    """
    MSAP5_2: Annotated[Msap52SchemaDM, Field(title="MSAP5-2")]
    """
    MRA determination from non seismic data
    """
    MSAP5_3: Annotated[Msap53SchemaDM, Field(title="MSAP5-3")]
    """
    Selection and validation
    """


class SasMsap5Schema1DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP5_1: Annotated[Msap51SchemaModelDM, Field(title="MSAP5_1 module")]
    """
    MRA determination from seismic data
    """
    MSAP5_2: Annotated[Msap52SchemaModelDM, Field(title="MSAP5_2 module")]
    """
    MRA determination from non-seismic data
    """
    MSAP5_3: Annotated[Msap53SchemaModelDM, Field(title="MSAP5_3 module")]
    """
    Selection and validation
    """


class SasMsap5Schema2DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP5_1: Annotated[Msap51SchemaModelDM, Field(title="MSAP5_1 module")]
    """
    MRA determination from seismic data
    """
    MSAP5_2: Annotated[
        Optional[Msap52SchemaModelDM], Field(title="MSAP5_2 module")
    ] = None
    """
    MRA determination from non-seismic data
    """
    MSAP5_3: Annotated[Msap53SchemaModelDM, Field(title="MSAP5_3 module")]
    """
    Selection and validation
    """


class SasMsap5Schema3DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP5_1: Annotated[
        Optional[Msap51SchemaModelDM], Field(title="MSAP5_1 module")
    ] = None
    """
    MRA determination from seismic data
    """
    MSAP5_2: Annotated[Msap52SchemaModelDM, Field(title="MSAP5_2 module")]
    """
    MRA determination from non-seismic data
    """
    MSAP5_3: Annotated[Msap53SchemaModelDM, Field(title="MSAP5_3 module")]
    """
    Selection and validation
    """


class SasMsap5SchemaDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[
        Union[SasMsap5Schema1DM, SasMsap5Schema2DM, SasMsap5Schema3DM],
        Field(title="MSAP5 pipeline"),
    ]
    """
The objective of this module is to determine stellar properties, specifically radius, mass, and age, as precisely and as accurately as possible. This module is thus expected to provide DP5. Several optimization and grid searching processes will be used simultaneously in order to assess and remove biases and systematic uncertainties. Other information, if available, will be used as well; examples of such information include cluster membership, interferometric radius from MsteSci1, and binarity. Both seismic and non-seismic inferences will be done to eventually apply quality assessments and decision proceduresthat will permit to derive DP5, which comprisesfor a given target the mass, radius, and age (also called as MRA).
    """


class SasMsap5SchemaModelDM(BaseRootModel):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
    root: Annotated[
        Union[SasMsap5123SchemaDM, SasMsap513SchemaDM, SasMsap523SchemaDM],
        Field(title="MSAP5 Data Model"),
    ]
    """
MSAP5 has to determine stellar properties, specifically radius, mass and age, and their associated 1-sigma uncertainties, of PLATO targets
    """


class SasPipelineHdf5DM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    METADATA: Annotated[Optional[MetadataDM], Field(title="METADATA")] = None
    """
    Metadata for SAS HDF5
    """
    MSAP1: Annotated[Optional[SasMsap1SchemaModelDM], Field(title="MSAP1")] = (
        None
    )
    """
    MSAP1 component encompasses all the necessary preliminary processes on the Level 1 light curves so as to produce analysis-ready light curves for deriving DP3, DP4 and DP5
    """
    MSAP2: Annotated[Optional[SasMsap2SchemaModelDM], Field(title="MSAP2")] = (
        None
    )
    """
    If the log g inferred from the seismic analysis and/or the log g inferred from the activity/rotation analysis is available, MSAP2 has to determine, with the necessary level of accuracy and precision, classical stellar parameters and their associated 1-sigma uncertainties.
    """
    MSAP3: Annotated[Optional[SasMsap3SchemaModelDM], Field(title="MSAP3")] = (
        None
    )
    """
    MSAP3 has to detect oscillations and, if detected, has to determine the asteroseismic mode parameters for stars, with their associated 1-sigma uncertainties.
    """
    MSAP4: Annotated[Optional[SasMsap4SchemaModelDM], Field(title="MSAP4")] = (
        None
    )
    """
    MSAP4 has to determine surface rotation periods and activity levels from Level 1 light curves and science preparatory data.
    """
    MSAP5: Annotated[Optional[SasMsap5SchemaDM], Field(title="MSAP5")] = None
    """
    MSAP5 has to determine stellar properties, specifically radius, mass and age, and their associated 1-sigma uncertainties, of PLATO targets.
    """


class SasPipelineDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    MSAP1: Annotated[SasMsap1SchemaModelDM, Field(title="MSAP1 pipeline")]
    """
    This pipeline encompasses all the necessary preliminary processes on the Level 1 merged and stitched light curves so as to produce analysis-ready light curves for deriving DP3, DP4, and DP5. In a first step, a dedicated sub-module will detect and flag flares. Also, in the current mission design for PLATO, the detrending of systematics (on all timescales) is expected to be part of the Level 1 module. However, it is anticipated that there will be residuals from this filtering process, and it will be necessary to filter these out to unambiguously detect astrophysical signals related to stellar oscillations and activity. In the case that those methods are not suitable for these purposes, specific methods will be applied for the detrending of the light curves. In a second step, filtering and modelling of transits and/or eclipses will be present, if relevant. Transit models are expected to be produced by the EAS module for modelling of planetary candidates. This same sub-module must also include the capability of removing flares and then treat any gaps in the light curve, if necessary. Finally, the power spectrum density is computed for each corrected light curve.
    """
    MSAP2: Annotated[
        Optional[SasMsap2SchemaModelDM], Field(title="MSAP2 pipeline")
    ] = None
    """
    Although the determination of classical stellar parameters will primarily rely on science preparatory data, it will benefit during operations from the availability of quantities inferred from the PLATO light curve. Where available, precise surface gravities derived from seismic modelling and/or from convectively-driven brightness fluctuations will be used as input for the determination of the classical stellar parameters. Enforcing a prior on the surface gravity leads to improved classical parameters from spectroscopy and significantly increasesthe precision and accuracy of classical stellar parameters
    """
    MSAP3: Annotated[SasMsap3SchemaModelDM, Field(title="MSAP3 pipeline")]
    """
    The objective of this pipeline is to measure the seismic mode parameters with the precision required for satisfying PLATO specifications. This module is thus expected to provide DP3. The module will include all of the standard procedures for detecting and measuring signatures of solar-like oscillations based on the legacy of CoRoT and Kepler. For cases where there is positive detection of oscillations, the module will provide average / global asteroseismic parameters and associated uncertainties. Whether or not the SAS can identify individual modes will depend on the quality of the power spectrum, and whether or not oscillations have been detected. In cases with both a detection and sufficient SNR, the module should provide individual frequencies and other individual mode parameters, covariance matrices, and associated uncertainties.
    """
    MSAP4: Annotated[SasMsap4SchemaModelDM, Field(title="MSAP4 pipeline")]
    """
    The objective is to provide measurements of surface rotation periods and, when possible, information on the latitudinal differential rotation and the activity level of the target stars. This module is thus expected to provide DP4. The module will then include standard procedures for providing such information, adapted to accommodate the characteristics of PLATO Level 1 light curves.
    """
    MSAP5: Annotated[SasMsap5SchemaDM, Field(title="MSAP5 pipeline")]
    """
    The objective of this module is to determine stellar properties, specifically radius, mass, and age, as precisely and as accurately as possible. This module is thus expected to provide DP5. Several optimization and grid searching processes will be used simultaneously in order to assess and remove biases and systematic uncertainties. Other information, if available, will be used as well; examples of such information include cluster membership, interferometric radius from MsteSci1, and binarity. Both seismic and non-seismic inferences will be done to eventually apply quality assessments and decision proceduresthat will permit to derive DP5, which comprisesfor a given target the mass, radius, and age (also called as MRA).
    """


class SasHdf5DeliveryDM(BaseDataClass):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        extra="forbid",
        populate_by_name=True,
    )
    METADATA: Annotated[MetadataDM, Field(title="METADATA")]
    """
    Metadata for SAS HDF5
    """
    MSAP1: Annotated[SasMsap1SchemaDM, Field(title="MSAP1")]
    """
    MSAP1 component encompasses all the necessary preliminary processes on the Level 1 light curves so as to produce analysis-ready light curves for deriving DP3, DP4 and DP5
    """
    MSAP2: Annotated[Optional[SasMsap2SchemaDM], Field(title="MSAP2")] = None
    """
    If the log g inferred from the seismic analysis and/or the log g inferred from the activity/rotation analysis is available, MSAP2 has to determine, with the necessary level of accuracy and precision, classical stellar parameters and their associated 1-sigma uncertainties.
    """
    MSAP3: Annotated[SasMsap3SchemaDM, Field(title="MSAP3")]
    """
    MSAP3 has to detect oscillations and, if detected, has to determine the asteroseismic mode parameters for stars, with their associated 1-sigma uncertainties.
    """
    MSAP4: Annotated[SasMsap4SchemaDM, Field(title="MSAP4")]
    """
    MSAP4 has to determine surface rotation periods and activity levels from Level 1 light curves and science preparatory data.
    """
    MSAP5: Annotated[SasMsap5SchemaModelDM, Field(title="MSAP5")]
    """
    MSAP5 has to determine stellar properties, specifically radius, mass and age, and their associated 1-sigma uncertainties, of PLATO targets.
    """


class StellarAnalysisSystemInputsAndOutputsDM(
    SasPipelineDM,
    SasExternalInputsSchemaDM,
    SasHdf5DeliveryDM,
    SasPipelineHdf5DM,
):
    model_config = ConfigDict(
        arbitrary_types_allowed=True,
        protected_namespaces=(),
        populate_by_name=True,
    )
