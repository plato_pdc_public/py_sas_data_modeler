# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
"""
The py_sas_data_modeler software facilitates the management and processing of tasks and products within
an HDF5-based workflow system. It is designed for users who need to model, validate, and manipulate
complex data pipelines using Python dataclasses.

This software leverages auto-generated business classes derived from the SAS framework data model defined
in a JSON schema. These business classes enable the creation of objects and the serialization of various
SAS products and pipelines into JSON format.

Additionally, the software supports serialization and deserialization of these objects into two distinct
HDF5 files: one for storing all input/output products for each pipeline associated with a given star, and
two separate files, inputs.h5 and outputs.h5, used for scientific module processing. The HDF5 storage is
optimized through the use of symbolic links, minimizing redundancy in the representation of products across
different inputs and outputs.

The software is usefull for two user profiles: workflow developers for SAS and developers of scientific
modules. It abstracts information storage and provides validation mechanisms.
"""
