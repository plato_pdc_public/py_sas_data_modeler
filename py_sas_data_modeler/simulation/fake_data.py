# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
"""
The 'fake_data' module provides utilities for generating fake data, particularly for
dataclasses. It extends the Faker library with custom providers that leverage
NumPy for generating various data types such as floats, integers, and booleans.
"""
import json
import random
import secrets
from types import NoneType
from typing import Any
from typing import Dict
from typing import get_args
from typing import get_origin
from typing import List
from typing import Optional
from typing import Tuple
from typing import Type
from typing import TypeVar
from typing import Union

import numpy as np
from faker import Faker
from faker.providers import BaseProvider
from loguru import logger

from ..dm import BaseDataClass
from ..dm import BaseRootModel
from ..dm import Field
from ..exception import ProductTypeError
from ..helper import debug_log

T = TypeVar("T", bound=Union[BaseDataClass, BaseRootModel])


class NumPyProvider(BaseProvider):
    def npfloat(
        self,
        left_digits=3,
        right_digits=2,
        seed=None,
        min_value=None,
        max_value=None,
    ) -> np.float64:
        """
        Generate a floating-point number using NumPy with specified precision.

        Args:
            left_digits (int, optional): Number of digits before the decimal point. Defaults to 3.
            right_digits (int, optional): Number of digits after the decimal point. Defaults to 2.
            seed (int or None): A seed for reproducibility. If None, the RNG will be non-deterministic.
            min_value (float, optional) : Minimum value
            max_value (float, optional) : Maximim vlue

        Returns:
            np.float64: A NumPy float64 value.
        """
        # Initialize random number generator with seed if provided
        rng = np.random.default_rng(seed)

        # Define range for integer number and decimal part
        integer_part = 10**left_digits

        # Determine range_min and range_max
        if min_value is not None and max_value is not None:
            if min_value >= max_value:
                raise ValueError("min_value must be less than max_value.")
            range_min = min_value
            range_max = max_value
        elif min_value is not None:  # Only min_value provided
            range_min = min_value
            range_max = integer_part if max_value is None else max_value
        elif max_value is not None:  # Only max_value provided
            range_min = 0 if max_value > 0 else -integer_part
            range_max = max_value
        else:  # Neither min_value nor max_value provided
            range_min = -integer_part
            range_max = integer_part

        # Arrondir à la précision souhaitée
        number = rng.uniform(range_min, range_max)
        rounded_number = round(number, right_digits)

        return np.float64(rounded_number)

    def nprandom_int(self, min=1, max=100, seed=None) -> np.int64:
        """
        Generate a random integer using NumPy within the specified range.

        Args:
            min (int, optional): Minimum value of the integer. Defaults to 1.
            max (int, optional): Maximum value of the integer. Defaults to 100.
            seed (int or None): A seed for reproducibility. If None, the RNG will be non-deterministic.

        Returns:
            np.int64: A NumPy int64 value.
        """
        rng = np.random.default_rng(seed)
        return np.int64(rng.integers(low=min, high=max))

    def npword(self) -> bytes:
        """
        Generate a fake word encoded in UTF-8.

        Returns:
            bytes: A word encoded as UTF-8 bytes.
        """
        return Faker().word().encode("utf-8")

    def npboolean(self, seed=None) -> bool:
        """
        Generate a random boolean value using NumPy.

        Args:
            seed (int or None): A seed for reproducibility. If None, the RNG will be non-deterministic.

        Returns:
            bool: A NumPy boolean value.
        """
        rng = np.random.default_rng(seed)
        boolean_value = rng.choice(a=[True, False])
        return bool(boolean_value)


# Étendre Faker avec le fournisseur NumPy
class CustomFaker(Faker):
    """
    Initialize the CustomFaker instance and add the NumPyProvider.
    """

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_provider(NumPyProvider)


class FakeData:
    def __init__(self, use_numpy: bool = True, has_optional: bool = True):
        """
        Initialize the FakeData instance with no initial data.
        """
        self.__fake_data: Dict = dict()
        self.__faker = CustomFaker()
        self.__use_numpy: bool = use_numpy
        self.__has_optional: bool = has_optional
        self.__cache: Dict = dict()

    @property
    def fake_data(self) -> Dict:
        """
        Get the generated fake data.

        Returns:
            Any: The generated fake data.
        """
        return self.__fake_data

    @property
    def use_numpy(self) -> bool:
        return self.__use_numpy

    @property
    def faker(self) -> CustomFaker:
        return self.__faker

    def _create_instance_from_dataclass(
        self, dataclass_: Type[BaseDataClass]
    ) -> Union[BaseDataClass, BaseRootModel]:
        """
        Create an instance of the dataclass with fake data.
        """
        if "root" in dataclass_.model_fields:
            return dataclass_(root=self.fake_data)
        return dataclass_(**self.__fake_data)

    def _is_dataclass(self, model_class: Type) -> bool:
        """
        Helper method to check if the class is a valid dataclass type.
        """
        return isinstance(model_class, type) and (
            issubclass(model_class, BaseDataClass)
            or issubclass(model_class, BaseRootModel)
        )

    def _handle_generate_simple_type_or_dataclass(
        self, model_class: Union[Type[BaseDataClass], Type[BaseRootModel]]
    ) -> Union[BaseDataClass, BaseRootModel, Dict]:
        """
        Handle the case where the model class is a simple type or a dataclass without annotations.
        """
        if self._is_dataclass(model_class):
            if issubclass(model_class, BaseRootModel):
                return model_class(root=self.fake_data)
            return model_class(**self.__fake_data)
        return self.__fake_data

    def _handle_generate_union_type(
        self, model_class: Union[Type[BaseDataClass], Type[BaseRootModel]]
    ) -> Union[BaseDataClass, BaseRootModel]:
        """
        Handle the Union/Optional case for generating fake data.
        """
        for dataclass_ in get_args(model_class):
            try:
                return self._create_instance_from_dataclass(dataclass_)
            except Exception:
                continue
        raise TypeError("Cannot find the class")

    @debug_log
    def generate_fake_data(
        self, model_class: Union[Type[BaseDataClass], Type[BaseRootModel]]
    ) -> Union[BaseDataClass, BaseRootModel, Dict]:
        """
        Generate fake data for an instance of the given dataclass type.

        Args:
            model_class (Union[Type[BaseDataClass], Type[BaseRootModel]]): The dataclass type for which to generate data.

        Returns:
            Union[Type[BaseDataClass], Type[BaseRootModel], Dict]: An instance of the dataclass with generated data or a simple type if the class is a simple Type
        """
        self.__fake_data = self._generate_fake_data(model_class, limits={})

        # Check for a simple type or dataclass type
        if get_origin(model_class) is None:
            return self._handle_generate_simple_type_or_dataclass(model_class)

        if get_origin(model_class) is Union:
            # Union/optional case
            return self._handle_generate_union_type(model_class)

        return self.__fake_data

    def get_field_limits(
        self,
        cls: Union[Type[BaseDataClass], Type[BaseRootModel]],
        field_name: str,
    ) -> Dict[str, Any]:
        """
        Retrieves the minimum and maximum limits (ge and le) of a field in a Pydantic model.

        Args:
            cls (Union[Type[BaseDataClass], Type[BaseRootModel]]): The Pydantic BaseModel class to inspect.
            field_name (str): The name of the field.

        Returns:
            Dict[str, Any]: A dictionary containing the limits {"min": ..., "max": ...}.
        """
        # Check if the field exists in the model
        if field_name not in cls.model_fields:
            raise ProductTypeError(
                f"Field '{field_name}' does not exist in the class."
            )

        # Retrieve the field metadata
        field_metadata = cls.model_fields[field_name]

        limits = {}
        for meta in field_metadata.metadata:
            if hasattr(meta, "le"):
                limits["maxe"] = meta.le  # Less than or Equal to
            elif hasattr(meta, "ge"):
                limits["mine"] = meta.ge  # Greater than or Equal to
            elif hasattr(meta, "lt"):
                limits["max"] = meta.le  # Less than
            elif hasattr(meta, "gt"):
                limits["min"] = meta.ge  # Greater than
            elif hasattr(meta, "min_length"):
                limits["min_length"] = meta.min_length  # min_length
            elif hasattr(meta, "max_length"):
                limits["max_length"] = meta.max_length  # max_length

        # Return the limits or {} if no limits are defined
        return limits if limits else {}

    @debug_log
    def _generate_fake_data(self, model_class: Type[T], limits: Dict) -> Dict:
        """
        Internal method to generate fake data, either for a dataclass or other types.

        Args:
            model_class (Type[T]): The type for which to generate data.
            limits (Dict): Limits on the generated value

        Returns:
            Dict: Generated data.
        """
        logger.debug(f"Generate Fake data for {model_class}")
        if not (
            isinstance(model_class, type)
            and (
                issubclass(model_class, BaseDataClass)
                or issubclass(model_class, BaseRootModel)
            )
        ):
            return self._handle_non_dataclass_type(model_class, limits)

        instance_result = self._generate_dataclass_instance(model_class)
        return (
            instance_result["root"]
            if "root" in instance_result
            else instance_result
        )

    @debug_log
    def _handle_non_dataclass_type(self, cls_type: Type[Any], limits) -> Union[
        None,
        List[Any],
        Dict[Any, Any],
        str,
        int,
        float,
        bool,
        bytes,
        np.int64,
        np.float64,
    ]:
        """
        Handle generation of fake data for non-dataclass types.

        Args:
            cls_type (Type[Any]): The type to handle.
            limits (Dict): Limits on the generated value

        Returns:
            Union[None, List[Any], Dict[Any, Any], str, int, float, bool, bytes, np.int64, np.float64]: Generated data.
        """

        type_ = get_origin(cls_type)
        if cls_type is Union:
            return self._handle_union_type(cls_type, limits)
        if type_ in {list, List}:  # depends on the python version
            return self._handle_list_type(cls_type, limits)
        if type_ in {dict, Dict}:  # depends on the python version
            return self._handle_dict_type(cls_type, limits)
        if type_ is None:
            # Simple type
            return self._generate_default_value(cls_type, limits)

        raise NotImplementedError(f"Type {cls_type} not supported")

    @debug_log
    def _handle_tuple_type(self, tuple_type: Tuple[Type[Any]]) -> Dict:
        """
        Handle Tuple types by choosing a random type from the Tuple.

        Args:
            tuple_type (Type[Any]): The Tuple type to handle.

        Returns:
            Dict: Generated data of one of the possible types.
        """
        chosen_type = secrets.choice(tuple_type)
        return self._generate_fake_data(chosen_type)

    @debug_log
    def _handle_union_type(self, union_type: Type[Any], limits: Dict) -> Dict:
        """
        Handle Union types by choosing a random type from the Union.

        Args:
            union_type (Type[Any]): The Union type to handle.
            limits (Dict): Limits on the generated value

        Returns:
            Dict: Generated data of one of the possible types.
        """
        possible_types = get_args(union_type)
        # Check if it's an Optional (Union with NoneType)
        if not self.__has_optional and NoneType in possible_types:
            chosen_type = possible_types[0]
        else:
            chosen_type = secrets.choice(possible_types)
        return self._generate_fake_data(chosen_type, limits)

    @debug_log
    def _handle_list_type(self, list_type: Type[Any], limits: Dict) -> List:
        """
        Handle list types by generating a list of the appropriate type.

        Args:
            list_type (Type[Any]): The list type to handle.
            limits (Dict): Constraints on the generated value

        Returns:
            List: A list of generated data.
        """
        item_type = get_args(list_type)[0]
        dimensions = self._get_list_dimensions(list_type)
        return self._generate_nested_list(item_type, dimensions, limits)

    @debug_log
    def _handle_dict_type(self, dict_type: Type[Any], limits: Dict) -> Dict:
        """
        Handle dictionary types by generating a dictionary with fake keys and values.

        Args:
            dict_type (Type[Any]): The dictionary type to handle.
            limits (Dict): Limits on the generated value

        Returns:
            Dict: A dictionary of generated data.
        """
        key_type, value_type = get_args(dict_type)
        return self._generate_fake_dict(key_type, value_type, 3, limits)

    @debug_log
    def _is_product(self, key: str) -> bool:
        """
        Checks if a given key should be a product.

        Args:
            key (str): The key to check.

        Returns:
            bool: True if the key is a, False otherwise.
        """
        return any(
            prefix in key
            for prefix in ["DP", "IDP", "ADP", "EAS", "LG", "PDP"]
        )

    @debug_log
    def _generate_dataclass_instance(
        self, model_class: Union[Type[BaseDataClass], Type[BaseRootModel]]
    ) -> Dict:
        """
        Generate an instance of the given class type with fake data.

        Args:
            model_class (Union[Type[BaseDataClass], Type[BaseRootModel]]): The model class for which to generate an instance.

        Returns:
            Dict: An instance of the dataclass with generated data.
        """
        data = {}

        for field_name, field_info in model_class.model_fields.items():
            field_alias = field_info.alias or field_name
            field_type = field_info.annotation
            limits: Dict = self.get_field_limits(model_class, field_name)
            logger.debug(
                f"Generate field value {field_alias} for {field_type} with limits {limits}"
            )
            if self._is_product(field_name):
                if field_name in self.__cache:
                    data[field_alias] = self.__cache[field_alias]
                else:
                    data[field_alias] = self._generate_field_value(
                        field_type, limits
                    )

                    if data[field_alias] is not None:
                        # The output products always exist but not the inputs.
                        # We cache only existing objects
                        self.__cache[field_alias] = data[field_alias]
            else:
                data[field_alias] = self._generate_field_value(
                    field_type, limits
                )
        return data

    @debug_log
    def _generate_field_value(
        self, field_type: Type[Any], limits: Dict
    ) -> Dict:
        """
        Generate a fake value for a given field type.

        Args:
            field_type (Type[Any]): The type of the field for which to generate a value.
            limits (Dict): constraints on the generated value

        Returns:
            Dict: Generated value.
        """
        origin = get_origin(field_type)
        if origin in [list, List]:
            return self._handle_list_type(field_type, limits)
        if origin in [dict, Dict]:
            return self._handle_dict_type(field_type, limits)
        if origin is Union:
            return self._handle_union_type(field_type, limits)
        if isinstance(field_type, type) and (
            issubclass(field_type, BaseDataClass)
            or issubclass(field_type, BaseRootModel)
        ):
            return self._generate_fake_data(field_type, limits)

        return self._generate_default_value(field_type, limits)

    @debug_log
    def _get_list_dimensions(self, field_type: Type[Any]) -> int:
        """
        Determine the number of dimensions in a nested list type.

        Args:
            field_type (Type[Any]): The list type to analyze.

        Returns:
            int: The number of dimensions in the list.
        """
        dimensions = 0
        while get_origin(field_type) in [list, List]:
            field_type = get_args(field_type)[0]
            dimensions += 1
        return dimensions

    @debug_log
    def _generate_nested_list(
        self, item_type: Type[Any], dimensions: int, limits
    ) -> List:
        """
        Generate a nested list with the specified dimensions.

        Args:
            item_type (Type[Any]): The type of items in the list.
            dimensions (int): The number of dimensions for the list.
            limits: Dict: Constraints on the generated value

        Returns:
            List: A nested list of generated data.
        """
        if dimensions == 1:
            return [
                self._generate_default_value(item_type, limits)
                for _ in range(3)
            ]

        return [
            self._generate_nested_list(
                get_args(item_type)[0], dimensions - 1, limits
            )
            for _ in range(3)
        ]

    @debug_log
    def _generate_fake_dict(
        self, key_type: Type[Any], value_type: Type[Any], size: int, limits
    ) -> Dict[Any, Any]:
        """
        Generate a dictionary with the specified key and value types.

        Args:
            key_type (Type[Any]): The type of the keys in the dictionary.
            value_type (Type[Any]): The type of the values in the dictionary.
            size (int): The number of key-value pairs in the dictionary.
            limits (Dict): Limits on the generated value

        Returns:
            Dict: A dictionary of generated data.
        """
        return {
            self._generate_default_value(
                key_type, limits
            ): self._generate_default_value(value_type, limits)
            for _ in range(size)
        }

    @debug_log
    def _generate_default_value(self, type_: Type[Any], limits: Dict) -> Union[
        None,
        List[Any],
        Dict[Any, Any],
        str,
        int,
        float,
        bool,
        bytes,
        np.int64,
        np.float64,
    ]:
        """
        Generate a default fake value for the given type.

        Args:
            type_ (Type[Any]): The type for which to generate a value.
            limits (Dict): Limits on the generated value

        Returns:
            Union[None, List[Any], Dict[Any, Any], str, int, float, bool, bytes, np.int64, np.float64]: Generated value.
        """
        logger.debug(f"Use Numpy : {self.use_numpy} for {type_}")
        if self.use_numpy:
            return self._generate_default_value_numpy(type_, limits)
        else:
            return self._generate_default_value_python(type_, limits)

    def _get_min_max_limits(self, limits: Dict) -> Tuple:
        """
        Extract and return the minimum and maximum limits from the provided dictionary.
        """
        min_val = limits.get("min", None) or limits.get("mine", None)
        max_val = limits.get("max", None) or limits.get("maxe", None)
        return min_val, max_val

    def _generate_default_int(self, min_val: int, max_val: int) -> int:
        """
        Generate a default integer value within the specified range.
        """
        min_val = min_val or 0
        max_val = max_val or 9999
        return self.faker.random_int(min=min_val, max=max_val)

    def _generate_default_numpy_int(
        self, min_val: int, max_val: int
    ) -> np.int64:
        """
        Generate a default numpy integer value within the specified range.
        """
        min_val = min_val or 0
        max_val = max_val or 9999
        return self.faker.nprandom_int(min=min_val, max=max_val)

    def _generate_default_float(self, min_val: float, max_val: float) -> float:
        """
        Generate a default float value within the specified range.
        """
        if max_val is not None and max_val > 1e15:
            max_val = 1e14  # To avoid overly large float values
        return self.faker.pyfloat(min_value=min_val, max_value=max_val)

    def _generate_default_numpy_float(
        self, min_val: float, max_val: float
    ) -> np.float64:
        """
        Generate a default numpy float value within the specified range.
        """
        if max_val is not None and max_val > 1e14:
            max_val = 1e15  # To avoid overly large float values
        return self.faker.npfloat(min_value=min_val, max_value=max_val)

    def _generate_default_list(self, types: Tuple[Type], limits: Dict) -> List:
        """
        Generate a list of default values based on the given type.
        """
        return [
            self._generate_default_value(types[0], limits) for _ in range(3)
        ]

    def _generate_default_numpy_list(
        self, types: Tuple[Type], limits: Dict
    ) -> List:
        """
        Generate a list of default NumPy values based on the given type.
        """
        return [
            self._generate_default_value_numpy(types[0], limits)
            for _ in range(3)
        ]

    def _generate_default_union(self, type_: Type, limits: Dict) -> Any:
        """
        Handle the Union type and return a generated value from one of the possible types.
        """
        possible_types = get_args(type_)
        selected_type = possible_types[secrets.randbelow(len(possible_types))]
        return self._generate_default_value(selected_type, limits)

    def _generate_default_numpy_union(self, type_: Type, limits: Dict) -> Any:
        """
        Handle the Union type and return a generated value from one of the possible types.
        """
        possible_types = get_args(type_)
        selected_type = possible_types[secrets.randbelow(len(possible_types))]
        return self._generate_default_value_numpy(selected_type, limits)

    @debug_log
    def _generate_default_value_python(
        self, type_: Type[Any], limits: Dict
    ) -> Union[None, str, int, float, bool, List[Any], Dict[Any, Any]]:
        """
        Generate a default fake value for the given type.

        Args:
            type_ (Type[Any]): The type for which to generate a value.
            limits (Dict): Limits on the generated value

        Returns:
            Union[None, str, int, float, bool, List[Any], Dict[Any, Any]]: Generated value.
        """
        origin = get_origin(type_)
        min_val, max_val = self._get_min_max_limits(limits)

        if type_ == type(None):  # noqa: E721
            return None

        if type_ == Any:
            return self.faker.word()

        if type_ == int:
            return self._generate_default_int(min_val, max_val)

        if type_ == float:
            return self._generate_default_float(min_val, max_val)

        if type_ == str:
            return self.faker.word()

        if type_ == bool:
            return self.faker.boolean()

        if origin in [list, List]:
            return self._generate_default_list(get_args(type_), limits)

        if origin in [dict, Dict]:
            return self._generate_fake_dict(
                get_args(type_)[0], get_args(type_)[1], 3, limits
            )

        if origin is Union:
            return self._generate_default_union(type_, limits)

        if isinstance(type_, type) and (
            issubclass(type_, BaseDataClass)
            or issubclass(type_, BaseRootModel)
        ):
            return self._generate_fake_data(type_, limits)

        raise NotImplementedError(f"Type {type_} not supported")

    @debug_log
    def _generate_default_value_numpy(
        self, type_: Type[Any], limits: Dict
    ) -> Union[
        None,
        bytes,
        np.int64,
        np.float64,
        str,
        bool,
        List[Any],
        Dict[Any, Any],
        Any,
    ]:
        """
        Generate a default fake value for the given type.

        Args:
            type_ (Type[Any]): The type for which to generate a value.
            limits (Dict): Limits on the generated value

        Returns:
            Union[None, bytes, np.int64, np.float64, str, bool, List[Any], Dict[Any, Any], Any]: Generated value.
        """
        origin = get_origin(type_)
        min_val, max_val = self._get_min_max_limits(limits)

        if type_ == type(None):  # noqa: E721
            return None

        if type_ == Any:
            return self.faker.npword()

        if type_ == int:
            return self._generate_default_numpy_int(min_val, max_val)

        if type_ == float:
            return self._generate_default_numpy_float(min_val, max_val)

        if type_ == str:
            return self.faker.word()

        if type_ == bool:
            return self.faker.npboolean()

        if origin in [list, List]:
            return self._generate_default_numpy_list(get_args(type_), limits)

        if origin in [dict, Dict]:
            return self._generate_fake_dict(
                get_args(type_)[0], get_args(type_)[1], 3, limits
            )

        if origin is Union:
            return self._generate_default_numpy_union(type_, limits)

        if isinstance(type_, type) and (
            issubclass(type_, BaseDataClass)
            or issubclass(type_, BaseRootModel)
        ):
            return self._generate_fake_data(type_, limits)

        raise NotImplementedError(f"Type {type_} not supported")

    def format_dict(self, indent: int = 4) -> str:
        """
        Format the dictionary representation of the fake data as a JSON string.

        Args:
            indent (int, optional): The indentation level for formatting. Defaults to 4.

        Returns:
            str: A JSON-formatted string.
        """
        return json.dumps(self.fake_data, indent=indent)
