# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
"""Project metadata."""
from importlib.metadata import metadata

pkg_metadata = metadata("PySasDataModeler")

__name_soft__ = pkg_metadata.get("name", "unknown")
__version__ = pkg_metadata.get("version", "0.0.0")
__title__ = pkg_metadata.get("name", "unknown")
__description__ = pkg_metadata.get("summary", "")
__url__ = pkg_metadata.get("homepage", "")
__author__ = pkg_metadata.get("author", "unknown")
__author_email__ = pkg_metadata.get("author-email", "unknown")
__license__ = pkg_metadata.get("license", "Unknown")
__copyright__ = "2024-2025, CNES (Jean-Christophe Malapert for PLATO/SAS)"
