# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
"""
Utility class uses in the others modules.
"""
import inspect
import json
from functools import wraps
from time import sleep
from typing import Any
from typing import get_args
from typing import get_origin
from typing import Tuple
from typing import Type
from typing import Union

import numpy as np
from loguru import logger

from .exception import DataBlockingIOError
from .exception import IncompatibleDataTypeError
from .exception import InvalidDataTypeError

MAX_ATTEMPT = 10


class Utils:
    """Utility class"""

    @staticmethod
    def convert_to_numpy(value, default_value=None) -> np.ndarray:
        """
        Converts nested lists, dictionaries, or arrays to numpy arrays, handling different data types.

        Args:
            value: The list, nested list, dictionary, or array to be converted to a numpy array.
            default_value: The value to replace None with (default: None).

        Returns:
            np.ndarray: The converted numpy array, list of numpy arrays, or dictionary with numpy arrays.

        Raises:
            IncompatibleDataTypeError: The list contains incompatible mixed data types.
            InvalidDataTypeError: Value should be a list or numpy array.
        """
        if isinstance(value, np.ndarray):
            return value

        if not isinstance(value, list):
            raise InvalidDataTypeError("Value must be a list or numpy array.")

        if default_value is not None:
            value = [default_value if item is None else item for item in value]

        # Handlers for homogeneous data types
        handlers = [
            (
                all(isinstance(item, dict) for item in value),
                lambda v: np.array(
                    [json.dumps(d) for d in v], dtype=np.object_
                ),
            ),
            (
                all(isinstance(item, list) for item in value),
                lambda v: np.array(
                    [Utils.convert_to_numpy(item, default_value) for item in v]
                ),
            ),
            (
                all(
                    isinstance(item, np.ndarray)
                    and item.dtype.type is np.bytes_
                    for item in value
                ),
                lambda v: np.concatenate([item.flatten() for item in v]),
            ),
            (
                all(
                    isinstance(item, (str, bytes, np.bytes_)) for item in value
                ),
                lambda v: np.array(v, dtype=np.object_),
            ),
            (
                all(isinstance(item, (float, np.float64)) for item in value),
                lambda v: np.array(v, dtype=np.float64),
            ),
            (
                all(isinstance(item, bool) for item in value),
                lambda v: np.array(v, dtype=bool),
            ),
            (
                all(isinstance(item, (int, np.integer)) for item in value),
                lambda v: np.array(v, dtype=int),
            ),
        ]

        # Apply the first matching handler
        for condition, handler in handlers:
            if condition:
                return handler(value)

        # If no handler matches, raise an exception for incompatible types
        raise IncompatibleDataTypeError(
            f"The list contains incompatible mixed data types: {value} - "
            f"type: {type(value)} - types found: {[type(item) for item in value]}"
        )

    @staticmethod
    def _handle_dict_list(value):
        json_data = [json.dumps(d) for d in value]
        return np.array(json_data, dtype=np.object_)

    @staticmethod
    def _handle_nested_lists(value, default_value=None):
        converted_nested = [
            Utils.convert_to_numpy(item, default_value) for item in value
        ]
        return np.array(converted_nested, dtype=converted_nested[0].dtype)

    @staticmethod
    def _handle_byte_arrays(value):
        flat_items = [item.flatten() for item in value]
        return np.concatenate(flat_items)

    @staticmethod
    def _handle_strings(value):
        return np.array(value, dtype=np.object_)

    @staticmethod
    def _handle_floats(value):
        return np.array(value, dtype=np.float64)

    @staticmethod
    def _handle_booleans(value):
        return np.array(value, dtype=bool)

    @staticmethod
    def _handle_integers(value):
        return np.array(value, dtype=int)

    @staticmethod
    def normalize_types(d) -> Any:
        """Recursively convert numpy types to native Python types."""
        if isinstance(d, dict):
            return {k: Utils.normalize_types(v) for k, v in d.items()}
        elif isinstance(
            d, np.ndarray
        ):  # Convert numpy types to native Python types
            if len(d) > 0 and isinstance(
                d[0], (bytes, np.bytes_)
            ):  # Check if the numpy array contains bytes
                return [
                    Utils.normalize_types(x) for x in d
                ]  # Convert bytes to strings
            else:
                return d.tolist()  # Convert other numpy arrays to lists
        elif isinstance(d, list):
            return [Utils.normalize_types(item) for item in d]
        elif isinstance(d, (np.bytes_, bytes)):
            try:
                return json.loads(d.decode("utf-8"))
            except json.JSONDecodeError:
                return d.decode("utf-8")
        elif isinstance(d, (np.int64, np.int32, np.int16)):
            return int(d)
        elif isinstance(d, bool):
            return bool(d)
        elif isinstance(
            d, np.generic
        ):  # Convert numpy types to native Python types
            return d.item()
        else:
            return d

    @staticmethod
    def is_external_product(product_name: str) -> bool:
        prefixes = ("DP1", "IDP_PFU", "LG_PFU", "IDP_EAS", "PDP_WP12")
        return product_name.upper().startswith(prefixes)

    @staticmethod
    def is_internal_product(product_name: str) -> bool:
        prefixes = ("IDP_SAS", "ADP_SAS", "DP3_SAS", "DP4_SAS", "DP5_SAS")
        return product_name.upper().startswith(prefixes)

    @staticmethod
    def is_custom_object(variable) -> bool:
        return isinstance(variable, object) and not isinstance(
            variable, (int, float, str, list, dict, tuple, bool)
        )

    @staticmethod
    def is_union(type_) -> bool:
        # Check if the origin of the type is Union
        return get_origin(type_) is Union

    @staticmethod
    def is_optional(typ) -> bool:
        # An Optional is a Union that includes NoneType (type(None))
        if Utils.is_union(typ):
            return type(None) in get_args(typ)
        return False

    @staticmethod
    def is_class(typ) -> bool:
        # Check if it is a class while excluding Union or Optional
        return isinstance(typ, type) and not Utils.is_union(typ)

    @staticmethod
    def resolve_type(type_: Any) -> Union[Type, Tuple[Type]]:
        """
        Resolves the provided type and returns the base type or types.

        - If it's a class, returns the class itself.
        - If it's an Optional, returns the wrapped type.
        - If it's a Union, returns a tuple of types.
        - Otherwise, returns the type as is.
        """
        if Utils.is_class(type_):
            return type_
        elif Utils.is_optional(type_):
            return get_args(type_)[
                0
            ]  # Extract the first non-None type from Optional
        elif Utils.is_union(type_):
            return get_args(type_)  # Return all types in the Union as a tuple
        else:
            return type_


def spec_coverage(doc_id, *specs):

    def decorator(cls_or_func):
        if inspect.isclass(cls_or_func):
            # Si c'est une classe, on applique le décorateur à toutes les méthodes
            for name, method in inspect.getmembers(
                cls_or_func, predicate=inspect.isfunction
            ):
                method._specs = specs
                method._doc_id = doc_id
            return cls_or_func
        else:
            # Si c'est une fonction, on applique directement le décorateur
            cls_or_func._specs = specs
            cls_or_func._doc_id = doc_id
            return cls_or_func

    return decorator


def debug_log(func):
    @wraps(func)
    def wrapper(*args, **kwargs):
        logger.debug(f"Call {func.__name__} with args={args} kwargs={kwargs}")
        try:
            result = func(*args, **kwargs)
            logger.debug(f"{func.__name__} has returned {result}")
            return result
        except Exception as e:
            logger.exception(f"Exception in {func.__name__}: {e}")
            raise

    return wrapper


def retry(func):
    """Retry given function when BlockingIOError exception"""

    @wraps(func)
    def wrapper(*args, **kwargs):
        for i in range(0, MAX_ATTEMPT):
            try:
                result = func(*args, **kwargs)
                return result
            except BlockingIOError:
                logger.warning(
                    f"Resource temporarily unavailable: attempt {i}"
                )
                sleep(1)
        raise DataBlockingIOError("Cannot open file (locked)")

    return wrapper
