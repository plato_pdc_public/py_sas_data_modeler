# -*- coding: utf-8 -*-
from typing import Tuple

import pytest

from py_sas_data_modeler.dm import DataClassFactory
from py_sas_data_modeler.simulation import FakeData


@pytest.mark.class_description(
    "A test suite for validating the creation, validation, and compatibility of dataclasses registered in the DataClassFactory using generated fake data."
)
class TestDataModel:

    @pytest.fixture
    def fake_data(self):
        """Fixture to create a FakeData instance."""
        return FakeData(use_numpy=False)

    @pytest.mark.id("TI_DATAMODEL_001")
    @pytest.mark.title("Test Validation of Registered Dataclasses")
    @pytest.mark.description(
        "Ensure that all registered dataclasses in the DataClassFactory can be "
        "successfully validated by generating fake data and creating instances."
    )
    @pytest.mark.inputs(
        "Registered dataclasses in DataClassFactory, and fake data generated for each dataclass."
    )
    @pytest.mark.expected(
        "Each registered dataclass is successfully instantiated and validated without errors."
    )
    @pytest.mark.steps(
        "1. Iterate through all registered dataclasses in DataClassFactory.\n"
        "2. For each dataclass, generate fake data using the FakeData generator.\n"
        "3. Convert the fake data into the dataclass using the `from_dict` method.\n"
        "4. Verify that the created instance is of the correct dataclass type.\n"
        "5. Assert no validation errors occur during the process."
    )
    @pytest.mark.coverage_spec(
        [
            [
                "PDC-UC-GDP-SAS-2664",
                "Validation of data designated for ingestion",
            ]
        ]
    )
    @pytest.mark.parametrize(
        "cls",
        [
            pytest.param(cls if not isinstance(cls, Tuple) else cls[0], id=key)
            for key, cls in DataClassFactory._dataclass_registry.items()
        ],
    )
    def test_validate_registered_dataclasses(self, cls, fake_data):
        """
        Test that all registered dataclasses in the DataClassFactory can be
        successfully validated with fake data.
        """
        try:
            # Generate fake data and validate
            data = fake_data.generate_fake_data(cls)
            obj = cls.from_dict(data.to_dict())
            assert isinstance(
                obj, cls
            ), f"Validation failed for {cls.__name__}."
        except Exception as e:
            pytest.fail(f"Failed to validate {cls.__name__}: {str(e)}")
