# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
import logging
from dataclasses import is_dataclass
from itertools import repeat
from unittest import mock

import pytest

from py_sas_data_modeler.dm import DataClassFactory
from py_sas_data_modeler.exception import DataBlockingIOError
from py_sas_data_modeler.helper import spec_coverage
from py_sas_data_modeler.io.hdf5 import HDF5StorageSasWorkflow
from py_sas_data_modeler.simulation import FakeData
from py_sas_data_modeler.workflow import PipelineParser

pipelines = [
    "MSAP1",
    "MSAP2",
    "MSAP3",
    "MSAP4",
    "MSAP5_1",
    "MSAP5_2",
    "MSAP5_3",
]


@pytest.fixture(scope="class")
def hdf5_file(tmp_path_factory):
    """Fixture to create a temporary HDF5 file."""
    tmp_dir = tmp_path_factory.mktemp("data")
    return tmp_dir / "test.h5"


@pytest.fixture(scope="class")
def storage_hdf5(hdf5_file):
    hdf5 = HDF5StorageSasWorkflow(hdf5_file)
    return hdf5


@pytest.fixture(scope="class")
def fake_data():
    """Fixture to create a FakeData instance."""
    return FakeData(has_optional=False)


@pytest.fixture(scope="class")
def generate_fake_data_msap1(storage_hdf5, fake_data):
    storage_hdf5.module_name = "MSAP1_01"
    msap1_01_model = DataClassFactory.get_model("MSAP1_01")
    msap1_01 = fake_data.generate_fake_data(msap1_01_model)
    return msap1_01


@pytest.fixture(scope="class")
def setup_hdf5_data(storage_hdf5, fake_data):
    """Fixture to generate and save fake data in the HDF5 file."""
    # Generate fake data using the provided schema
    data = fake_data.generate_fake_data(
        DataClassFactory.get_model("SasPipelineHdf5")
    )

    # Save the generated fake data
    storage_hdf5.sas_hdf5.save_data("/", data)


@pytest.mark.usefixtures("setup_hdf5_data")
@pytest.mark.parametrize("sas_pipeline", pipelines)
@spec_coverage(
    "PLATO-IAS-PDC-TN-0005",
    "test_datasets_in_hdf5_groups_implementation",
    "test_root_group_naming",
)
@pytest.mark.class_description(
    "Test loading and saving pipeline information from/to HDF5 storage."
)
class TestMsap:

    @pytest.fixture
    def products_in_msap(self, sas_pipeline):
        pipeline_clss = DataClassFactory.get_model(sas_pipeline)
        if isinstance(pipeline_clss, tuple):
            pipeline_cls = pipeline_clss[0]
        else:
            pipeline_cls = pipeline_clss

        products = set()
        pipeline_parser = PipelineParser()
        pipeline = pipeline_parser.parse(pipeline_cls)

        for task_comb in pipeline.task_combinations:
            for task in task_comb.tasks:
                for input_set in task.input_sets:
                    products.update(
                        input.name for input in input_set.mandatory_inputs
                    )
                products.update(
                    output.name for output in task.mandatory_outputs
                )

        return list(products)

    @pytest.mark.id("TI_WORKFLOW_001")
    @pytest.mark.title("Test Load Input Schema Object")
    @pytest.mark.description(
        "Test that the correct schema object is loaded from the HDF5 file."
    )
    @pytest.mark.inputs("Products in MSAP pipeline, HDF5 storage.")
    @pytest.mark.expected(
        "The correct schema object should be loaded, and its type should match the expected product class."
    )
    @pytest.mark.steps(
        "1. Iterate over the list of products in the MSAP pipeline.\n"
        "2. For each product, load it from the HDF5 storage.\n"
        "3. Verify that the loaded object is an instance of the expected class.\n"
        "4. Measure the performance using a benchmark."
    )
    def test_load_input(
        self, storage_hdf5, products_in_msap, sas_pipeline, benchmark
    ):
        """Test that the correct schema object is loaded from the HDF5 file."""

        logging.debug(f"test_load_input: Test {sas_pipeline}")

        def load_all_products():
            for product in products_in_msap:

                # Charger le produit depuis le stockage HDF5
                if not is_dataclass(product):
                    continue

                obj = storage_hdf5.load_product(product)

                # Vérifier que le type de l'objet chargé est le bon
                assert isinstance(
                    obj, product
                ), f"Expected instance of {product} but got {type(obj)}"

        # Utiliser le benchmark pour mesurer le temps d'exécution de load_all_products
        benchmark(load_all_products)

    @pytest.mark.id("TI_WORKFLOW_002")
    @pytest.mark.title("Test Save Input Schema Object")
    @pytest.mark.description(
        "Test that the correct schema object is saved to the HDF5 file."
    )
    @pytest.mark.inputs("Products in MSAP pipeline, HDF5 storage.")
    @pytest.mark.expected(
        "The correct schema object should be saved to HDF5 without errors or exceptions."
    )
    @pytest.mark.steps(
        "1. Iterate over the list of products in the MSAP pipeline.\n"
        "2. For each product, load it from the HDF5 storage.\n"
        "3. Attempt to save the loaded product to the HDF5 file.\n"
        "4. If an exception occurs during the save process, fail the test.\n"
        "5. Measure the performance using a benchmark."
    )
    def test_save_input(
        self, tmp_path_factory, products_in_msap, sas_pipeline, benchmark
    ):
        """Test that the correct schema object is loaded from the HDF5 file."""

        logging.info(f"test_save_input: Test {sas_pipeline}")

        def save_all_products():
            for product in products_in_msap:

                # Load product from HDF5 storage
                if not is_dataclass(product):
                    continue

                hdf5 = HDF5StorageSasWorkflow(hdf5_file)

                cls = DataClassFactory.get_model(product)
                try:
                    hdf5.save_product(cls)
                except Exception as e:
                    pytest.fail(
                        f"Saving the product {cls} failed with exception: {e}"
                    )

        benchmark(save_all_products)

    @pytest.mark.id("TI_WORKFLOW_003")
    @pytest.mark.title("Test if Product is Stored")
    @pytest.mark.description(
        "Test the functionality of checking if a product is stored in the HDF5 storage."
    )
    @pytest.mark.inputs("Products in MSAP pipeline, HDF5 storage.")
    @pytest.mark.expected(
        "The `is_stored` method should return `True` for all stored products."
    )
    @pytest.mark.steps(
        "1. Iterate over the list of products in the MSAP pipeline.\n"
        "2. For each product, check if it is stored in the HDF5 storage.\n"
        "3. Assert that the result of the `is_stored` check is `True` for each product."
    )
    def test_is_stored(self, storage_hdf5, products_in_msap, sas_pipeline):
        """Test is stored."""

        logging.info(f"test_is_stored: Test {sas_pipeline}")

        for product in products_in_msap:

            # Load product from HDF5 storage
            if not is_dataclass(product):
                continue

            obj = storage_hdf5.is_stored(product)

            assert obj is True

    @pytest.mark.id("TI_WORKFLOW_004")
    @pytest.mark.title("Test if Data Model is Validated")
    @pytest.mark.description(
        "Test the functionality of validating the data model in HDF5 storage."
    )
    @pytest.mark.inputs("SAS pipeline, HDF5 storage.")
    @pytest.mark.expected(
        "The `is_validate_dm` method should return `True` if the data model is validated correctly."
    )
    @pytest.mark.steps(
        "1. Call the `is_validate_dm` method on the HDF5 storage.\n"
        "2. Assert that the result is `True`, indicating the data model is valid."
    )
    def test_is_validate_dm(self, storage_hdf5, sas_pipeline):
        is_valid = storage_hdf5.is_validate_dm()
        assert is_valid is True


class TestIO:

    def test_retry_on_blocking_io_error_load_metadata(self, storage_hdf5):

        # Mock of h5py.File with BlockingIOError
        with mock.patch(
            "h5py.File",
            side_effect=[*repeat(BlockingIOError("File locked"), 10)],
        ) as patched_file:

            # Function to test
            def load_metadata_with_retry():
                return storage_hdf5.sas_hdf5.load_metadata("/some_group")

            # Call the function
            with pytest.raises(DataBlockingIOError):
                load_metadata_with_retry()

            # Check h5py.File has been called 10 times
            assert (
                patched_file.call_count == 10
            ), f"Expected 10 calls, but got {patched_file.call_count}"

    def test_retry_on_blocking_io_error_load_data(self, storage_hdf5):

        # Mock of h5py.File with BlockingIOError
        with mock.patch(
            "h5py.File",
            side_effect=[*repeat(BlockingIOError("File locked"), 10)],
        ) as patched_file:

            # Function to test
            def load_data_with_retry():
                return storage_hdf5.sas_hdf5.load_data(
                    "MSAP1/MSAP1_01/inputs_msap1_01"
                )

            # Call the function
            with pytest.raises(DataBlockingIOError):
                load_data_with_retry()

            # Check h5py.File has been called 10 times
            assert (
                patched_file.call_count == 10
            ), f"Expected 10 calls, but got {patched_file.call_count}"

    def test_retry_on_blocking_io_error_save_outputs(
        self, storage_hdf5, generate_fake_data_msap1
    ):

        # Mock of h5py.File with BlockingIOError
        with mock.patch(
            "h5py.File",
            side_effect=[*repeat(BlockingIOError("File locked"), 10)],
        ) as patched_file:

            # Function to test
            def save_outputs_with_retry():
                return storage_hdf5.save_outputs(
                    "MSAP1_01", generate_fake_data_msap1
                )

            # Call the function
            with pytest.raises(DataBlockingIOError):
                save_outputs_with_retry()

            # Check h5py.File has been called 10 times
            assert (
                patched_file.call_count == 10
            ), f"Expected 10 calls, but got {patched_file.call_count}"
