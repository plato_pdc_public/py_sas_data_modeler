# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
import os
from pathlib import Path

import pytest
from loguru import logger

from py_sas_data_modeler.dm import DataClassFactory
from py_sas_data_modeler.helper import spec_coverage
from py_sas_data_modeler.io import FileMediator
from py_sas_data_modeler.io import HDF5StorageSasTask
from py_sas_data_modeler.io import HDF5StorageSasWorkflow
from py_sas_data_modeler.simulation import (
    FakeData,
)  # Assuming a module for fake data generation
from py_sas_data_modeler.workflow import SasPipeline


class BaseTestPipeline:

    @pytest.fixture(scope="session", autouse=True)
    def cleanup_test_h5(self):
        # Utilisation d'un chemin de répertoire fixe pour tous les tests
        self.data_path = Path("./test_data")
        test_h5 = self.data_path / "test.h5"

        # Nettoyage du fichier HDF5 de test avant de commencer tous les tests
        if test_h5.exists():
            os.remove(test_h5)

        # Création du répertoire de test si nécessaire
        self.data_path.mkdir(parents=True, exist_ok=True)

        yield

    @pytest.fixture(autouse=True)
    def setup(self):
        # Setup for all pipelines
        self.faker = FakeData(use_numpy=False, has_optional=False)

        # Use a fixed directory path for all tests
        self.data_path = Path("./test_data")

        test_h5 = self.data_path / "test.h5"

        self.hdf5 = HDF5StorageSasWorkflow(
            test_h5,
            data_segment=1,
            plato_id=1234,
            processing_id="SAS-1234-5678",
            sas_modules="MSAP1(v1.0), MSAP2(v1.0), MSAP3(v1.0), MSAP4(v1.0), MSAP5(v1.0)",
            lineage="https://foo.com/SAS-1234-5678",
            date_created="2024-10-21T00:00:00Z",
            creator_name="SAS",
        )

        yield

    @pytest.fixture(autouse=True)
    def setup_teardown(self):
        yield  # Let the test run

        # Teardown: Remove temporary files after each test
        self.remove_local_files()

    def remove_local_files(self):
        inputs_file = self.data_path / "input" / "inputs.h5"
        outputs_file = self.data_path / "output" / "outputs.h5"
        if inputs_file.exists():
            os.remove(inputs_file)
        if outputs_file.exists():
            os.remove(outputs_file)

    def simulate_task(self, task_name):
        task_module = self.sas.create_module(task_name)
        task_module.add_inputs()

        task_hdf5 = HDF5StorageSasTask(str(self.data_path), task_name)
        file_transfer = FileMediator(
            sas_storage=self.hdf5, task_storage=task_hdf5
        )

        # Transfer inputs to local storage
        file_transfer.transfer_inputs_to_task(task_name)

        # Simulate output generation
        outputs_cls_name = f"OUTPUTS_{task_name}"
        types_ = DataClassFactory.get_model(outputs_cls_name)
        if isinstance(types_, tuple):
            class_name = types_[0]
        else:
            class_name = types_
        output_data = self.faker.generate_fake_data(class_name)

        task_hdf5.save_outputs(output_data)

        # Transfer outputs back to SAS storage
        file_transfer.retrieve_outputs_from_task(task_name)

        # Remove local files
        self.remove_local_files()

    def check_data(self, task_name):
        # Load inputs/outputs to verify correctness
        inputs = self.hdf5.load_inputs(task_name)
        outputs = self.hdf5.load_outputs(task_name)
        return inputs, outputs


@spec_coverage(
    "PLATO-IAS-PDC-TN-0005",
    "test_HDF5_standards",
    "test_work_data_flows_implementation",
    "test_hard_links_for_pipeline_inputs_and_outputs",
    "test_external_product_storage_path",
    "test_product_representation_HDF5_groups",
    "test_hdf5_hierarchy_and_group_structure",
)
@pytest.mark.class_description(
    "Tests the implementation of the MSAP pipeline workflow, "
    "including input/output management, HDF5 storage, and task execution."
)
class TestMSAPPipeline(BaseTestPipeline):

    @pytest.mark.id("TI_PIPELINE_001")
    @pytest.mark.title("Test MSAP1 Pipeline Execution")
    @pytest.mark.description(
        "Test the execution of the MSAP1 pipeline, including external data preparation, task simulation, and data integrity verification."
    )
    @pytest.mark.inputs(
        "Fake data generated for IDP_SAS_REG_LC, IDP_SAS_FLARES_FLAG, IDP_EAS_QC_TARGET_TCE_DETECTION, IDP_EAS_TRANSIT_REMOVAL_KIT; SAS pipeline initialized with MSAP1."
    )
    @pytest.mark.expected(
        "All simulated tasks (MSAP1_02, MSAP1_01, MSAP1_07, MSAP1_08) should pass, and data integrity should be verified."
    )
    @pytest.mark.steps(
        "1. Generate fake data for IDP_SAS_REG_LC, IDP_SAS_FLARES_FLAG, IDP_EAS_QC_TARGET_TCE_DETECTION, IDP_EAS_TRANSIT_REMOVAL_KIT.\n"
        "2. Store the generated fake data in SAS storage.\n"
        "3. Initialize a SasPipeline instance with MSAP1.\n"
        "4. Save the generated external data using `save_externals`.\n"
        "5. Simulate the execution of tasks MSAP1_02, MSAP1_01, MSAP1_07, and MSAP1_08.\n"
        "6. Verify the data integrity after each task simulation."
    )
    @pytest.mark.coverage_spec(
        [
            ("PDC-UC-GDP-SAS-2352", "Retrieving inputs from the SAS-PIP-STO"),
            ("PDC-UC-GDP-SAS-2353", "Transfer the outputs to SAS-PIP-STO"),
            (
                "PDC-UC-GDP-SAS-2914",
                "SAS product definition interface for data format validation",
            ),
        ]
    )
    def test_msap1_pipeline(self):
        # External data preparation
        dp1 = self.faker.generate_fake_data(
            DataClassFactory.get_model("DP1_LC_MERGED")
        )
        flares_flag = self.faker.generate_fake_data(
            DataClassFactory.get_model("IDP_SAS_FLARES_FLAG")
        )
        qc = self.faker.generate_fake_data(
            DataClassFactory.get_model("IDP_EAS_QC_TARGET_TCE_DETECTION")
        )
        transit = self.faker.generate_fake_data(
            DataClassFactory.get_model("IDP_EAS_TRANSIT_REMOVAL_KIT")
        )

        # Store external data in SAS storage
        data = {
            "DP1_LC_MERGED": dp1,
            "IDP_SAS_FLARES_FLAG": flares_flag,
            "IDP_EAS_QC_TARGET_TCE_DETECTION": qc,
            "IDP_EAS_TRANSIT_REMOVAL_KIT": transit,
        }
        self.sas = SasPipeline(self.hdf5, DataClassFactory.get_model("MSAP1"))
        self.sas.save_externals(data)

        # Simulate task MSAP1_03
        self.simulate_task("MSAP1_03")

        # Simulate task MSAP1_02
        self.simulate_task("MSAP1_02")

        # Simulate task MSAP1_01
        self.simulate_task("MSAP1_01")

        # Simulate task MSAP1_07
        self.simulate_task("MSAP1_07")

        # Simulate task MSAP1_08
        self.simulate_task("MSAP1_08")

        # Simulate task MSAP1_09
        self.simulate_task("MSAP1_09")

        # Verifying data integrity
        assert self.check_data("MSAP1_03")
        assert self.check_data("MSAP1_02")
        assert self.check_data("MSAP1_01")
        assert self.check_data("MSAP1_07")
        assert self.check_data("MSAP1_08")
        assert self.check_data("MSAP1_09")

    @pytest.mark.id("TI_PIPELINE_002")
    @pytest.mark.title("Test MSAP3 Pipeline Execution")
    @pytest.mark.description(
        "Test the execution of the MSAP3 pipeline, including external data preparation, task simulation, and data integrity verification."
    )
    @pytest.mark.inputs(
        "Fake data generated for IDP_PFU_TEFF_SAPP, IDP_PFU_RADIUS_CLASSICAL; SAS pipeline initialized with MSAP3."
    )
    @pytest.mark.expected(
        "All simulated tasks (MSAP3_01, MSAP3_02, MSAP3_03, MSAP3_04) should pass, and data integrity should be verified."
    )
    @pytest.mark.steps(
        "1. Generate fake data for IDP_PFU_TEFF_SAPP and IDP_PFU_RADIUS_CLASSICAL.\n"
        "2. Store the generated fake data in SAS storage.\n"
        "3. Initialize a SasPipeline instance with MSAP3.\n"
        "4. Save the generated external data using `save_externals`.\n"
        "5. Simulate the execution of tasks MSAP3_01, MSAP3_02, MSAP3_03, and MSAP3_04.\n"
        "6. Verify the data integrity after each task simulation."
    )
    @pytest.mark.coverage_spec(
        [
            ("PDC-UC-GDP-SAS-2453", "Retrieving inputs from the SAS-PIP-STO"),
            ("PDC-UC-GDP-SAS-2454", "Transfer the outputs to SAS-PIP-STO"),
            (
                "PDC-UC-GDP-SAS-2914",
                "SAS product definition interface for data format validation",
            ),
        ]
    )
    def test_msap3_pipeline(self):
        idp_pfu_teff_sapp = self.faker.generate_fake_data(
            DataClassFactory.get_model("IDP_PFU_TEFF_SAPP")
        )
        idp_pfu_radius = self.faker.generate_fake_data(
            DataClassFactory.get_model("IDP_PFU_RADIUS_CLASSICAL")
        )

        external = {
            "IDP_PFU_TEFF_SAPP": idp_pfu_teff_sapp,
            "IDP_PFU_RADIUS_CLASSICAL": idp_pfu_radius,
        }

        self.sas = SasPipeline(self.hdf5, DataClassFactory.get_model("MSAP3"))
        self.sas.save_externals(external)

        # Simulate task MSAP3_01
        self.simulate_task("MSAP3_01")

        # Simulate task MSAP3_01
        self.simulate_task("MSAP3_02")

        # Simulate task MSAP3_01
        self.simulate_task("MSAP3_03")

        # Simulate task MSAP3_01
        self.simulate_task("MSAP3_04")

        # Verifying data integrity
        assert self.check_data("MSAP3_01")
        assert self.check_data("MSAP3_02")
        assert self.check_data("MSAP3_03")
        assert self.check_data("MSAP3_04")

    @pytest.mark.id("TI_PIPELINE_003")
    @pytest.mark.title("Test MSAP4 Pipeline Execution")
    @pytest.mark.description(
        "Test the execution of the MSAP4 pipeline, including external data preparation, task simulation, and data integrity verification."
    )
    @pytest.mark.inputs(
        "Fake data generated for IDP_PFU_LOGG_SAPP; SAS pipeline initialized with MSAP4."
    )
    @pytest.mark.expected(
        "All simulated tasks (MSAP4_01, MSAP4_02, MSAP4_03, MSAP4_04, MSAP4_06) should pass, and data integrity should be verified."
    )
    @pytest.mark.steps(
        "1. Generate fake data for IDP_PFU_LOGG_SAPP.\n"
        "2. Store the generated fake data in SAS storage.\n"
        "3. Initialize a SasPipeline instance with MSAP4.\n"
        "4. Save the generated external data using `save_externals`.\n"
        "5. Simulate the execution of tasks MSAP4_01, MSAP4_02, MSAP4_03, MSAP4_04, MSAP4_06.\n"
        "6. Verify the data integrity after each task simulation."
    )
    @pytest.mark.coverage_spec(
        [
            ("PDC-UC-GDP-SAS-2503", "Retrieving inputs from the SAS-PIP-STO"),
            ("PDC-UC-GDP-SAS-2504", "Transfer the outputs to SAS-PIP-STO"),
            (
                "PDC-UC-GDP-SAS-2914",
                "SAS product definition interface for data format validation",
            ),
        ]
    )
    def test_msap4_pipeline(self):
        idp_pfu_logg_sapp = self.faker.generate_fake_data(
            DataClassFactory.get_model("IDP_PFU_LOGG_SAPP")
        )
        external = {"IDP_PFU_LOGG_SAPP": idp_pfu_logg_sapp}

        self.sas = SasPipeline(self.hdf5, DataClassFactory.get_model("MSAP4"))
        self.sas.save_externals(external)

        # Simulate task MSAP4_01
        self.simulate_task("MSAP4_01")

        # Simulate task MSAP4_02
        self.simulate_task("MSAP4_02")

        # Simulate task MSAP4_03
        self.simulate_task("MSAP4_03")

        # Simulate task MSAP4_04
        self.simulate_task("MSAP4_04")

        # Simulate task MSAP4_06
        self.simulate_task("MSAP4_06")

        # Verifying data integrity
        assert self.check_data("MSAP4_01")
        assert self.check_data("MSAP4_02")
        assert self.check_data("MSAP4_03")
        assert self.check_data("MSAP4_04")
        assert self.check_data("MSAP4_06")

    @pytest.mark.id("TI_PIPELINE_004")
    @pytest.mark.title("Test MSAP2 Pipeline Execution")
    @pytest.mark.description(
        "Test the execution of the MSAP2 pipeline, including external data preparation, task simulation, and data integrity verification."
    )
    @pytest.mark.inputs(
        "Fake data generated for IDP_PFU_[FE_H]_SAPP and LG_PFU_OBS_DATA; SAS pipeline initialized with MSAP2."
    )
    @pytest.mark.expected(
        "Task MSAP2_123 should pass, and data integrity should be verified."
    )
    @pytest.mark.steps(
        "1. Generate fake data for IDP_PFU_[FE_H]_SAPP and LG_PFU_OBS_DATA.\n"
        "2. Store the generated fake data in SAS storage.\n"
        "3. Initialize a SasPipeline instance with MSAP2.\n"
        "4. Save the generated external data using `save_externals`.\n"
        "5. Simulate the execution of task MSAP2_123.\n"
        "6. Verify the data integrity after the task simulation."
    )
    @pytest.mark.coverage_spec(
        [
            ("PDC-UC-GDP-SAS-2313", "Retrieving inputs from the SAS-PIP-STO"),
            ("PDC-UC-GDP-SAS-2314", "Transfer the outputs to SAS-PIP-STO"),
            (
                "PDC-UC-GDP-SAS-2914",
                "SAS product definition interface for data format validation",
            ),
        ]
    )
    def test_msap2_pipeline(self):
        idp_pfu_feh_sapp = self.faker.generate_fake_data(
            DataClassFactory.get_model("IDP_PFU_[FE_H]_SAPP")
        )
        lg_pfu_obs_data = self.faker.generate_fake_data(
            DataClassFactory.get_model("LG_PFU_OBS_DATA")
        )

        external = {
            "IDP_PFU_[FE_H]_SAPP": idp_pfu_feh_sapp,
            "LG_PFU_OBS_DATA": lg_pfu_obs_data,
        }

        self.sas = SasPipeline(self.hdf5, DataClassFactory.get_model("MSAP2"))
        self.sas.save_externals(external)

        # Simulate task MSAP2_123
        self.simulate_task("MSAP2_123")

        # Verifying data integrity
        assert self.check_data("MSAP2_123")

    @pytest.mark.id("TI_PIPELINE_005")
    @pytest.mark.title("Test MSAP5_1 Pipeline Execution")
    @pytest.mark.description(
        "Test the execution of the MSAP5_1 pipeline, including external data preparation, task simulation, and data integrity verification."
    )
    @pytest.mark.inputs(
        "Fake data generated for PDP_WP12_MOD_EVOL and PDP_WP12_MOD_OSC_FREQ; SAS pipeline initialized with MSAP5_1."
    )
    @pytest.mark.expected(
        "Tasks MSAP5_11 to MSAP5_15 should pass, and data integrity should be verified."
    )
    @pytest.mark.steps(
        "1. Generate fake data for PDP_WP12_MOD_EVOL and PDP_WP12_MOD_OSC_FREQ.\n"
        "2. Store the generated fake data in SAS storage.\n"
        "3. Initialize a SasPipeline instance with MSAP5_1.\n"
        "4. Save the generated external data using `save_externals`.\n"
        "5. Simulate the execution of tasks MSAP5_11 to MSAP5_15.\n"
        "6. Verify the data integrity after each task simulation."
    )
    @pytest.mark.coverage_spec(
        [
            ("PDC-UC-GDP-SAS-2553", "Retrieving inputs from the SAS-PIP-STO"),
            ("PDC-UC-GDP-SAS-2554", "Transfer the outputs to SAS-PIP-STO"),
            (
                "PDC-UC-GDP-SAS-2914",
                "SAS product definition interface for data format validation",
            ),
        ]
    )
    def test_msap5_1_pipeline(self):
        pdp_wp12_mod_evol = self.faker.generate_fake_data(
            DataClassFactory.get_model("PDP_WP12_MOD_EVOL")
        )
        pdp_wp12_mod_osc_freq = self.faker.generate_fake_data(
            DataClassFactory.get_model("PDP_WP12_MOD_OSC_FREQ")
        )

        external = {
            "PDP_WP12_MOD_EVOL": pdp_wp12_mod_evol,
            "PDP_WP12_MOD_OSC_FREQ": pdp_wp12_mod_osc_freq,
        }

        class_names = DataClassFactory.get_model("MSAP5_1")
        if isinstance(class_names, tuple):
            cls_pipeline = class_names[0]
        else:
            cls_pipeline = class_names

        self.sas = SasPipeline(self.hdf5, cls_pipeline)
        self.sas.save_externals(external)

        # Simulate task MSAP5_11
        self.simulate_task("MSAP5_11")

        # Simulate task MSAP5_12
        self.simulate_task("MSAP5_12")

        # Simulate task MSAP5_13
        self.simulate_task("MSAP5_13")

        # Simulate task MSAP5_14
        self.simulate_task("MSAP5_14")

        # Simulate task MSAP5_15
        self.simulate_task("MSAP5_15")

        # Verifying data integrity
        assert self.check_data("MSAP5_11")
        assert self.check_data("MSAP5_12")
        assert self.check_data("MSAP5_13")
        assert self.check_data("MSAP5_14")
        assert self.check_data("MSAP5_15")

    @pytest.mark.id("TI_PIPELINE_006")
    @pytest.mark.title("Test MSAP5_2 Pipeline Execution")
    @pytest.mark.description(
        "Test the execution of the MSAP5_2 pipeline, including external data preparation, task simulation, and data integrity verification."
    )
    @pytest.mark.inputs(
        "Fake data generated for IDP_PFU_L_IRFM and IDP_PFU_RADIUS_SAPP; SAS pipeline initialized with MSAP5_2."
    )
    @pytest.mark.expected(
        "Tasks MSAP5_21 to MSAP5_24 should pass, and data integrity should be verified."
    )
    @pytest.mark.steps(
        "1. Generate fake data for IDP_PFU_L_IRFM and IDP_PFU_RADIUS_SAPP.\n"
        "2. Store the generated fake data in SAS storage.\n"
        "3. Initialize a SasPipeline instance with MSAP5_2.\n"
        "4. Save the generated external data using `save_externals`.\n"
        "5. Simulate the execution of tasks MSAP5_21 to MSAP5_24.\n"
        "6. Verify the data integrity after each task simulation."
    )
    @pytest.mark.coverage_spec(
        [
            ("PDC-UC-GDP-SAS-2553", "Retrieving inputs from the SAS-PIP-STO"),
            ("PDC-UC-GDP-SAS-2554", "Transfer the outputs to SAS-PIP-STO"),
            (
                "PDC-UC-GDP-SAS-2914",
                "SAS product definition interface for data format validation",
            ),
        ]
    )
    def test_msap5_2_pipeline(self):
        idp_pfu_l_irfm = self.faker.generate_fake_data(
            DataClassFactory.get_model("IDP_PFU_L_IRFM")
        )
        idp_pfu_radius_sapp = self.faker.generate_fake_data(
            DataClassFactory.get_model("IDP_PFU_RADIUS_SAPP")
        )

        external = {
            "IDP_PFU_L_IRFM": idp_pfu_l_irfm,
            "IDP_PFU_RADIUS_SAPP": idp_pfu_radius_sapp,
        }

        class_names = DataClassFactory.get_model("MSAP5_2")
        if isinstance(class_names, tuple):
            cls_pipeline = class_names[0]
        else:
            cls_pipeline = class_names

        self.sas = SasPipeline(self.hdf5, cls_pipeline)
        self.sas.save_externals(external)

        # Simulate task MSAP5_21
        self.simulate_task("MSAP5_21")

        # Simulate task MSAP5_22
        self.simulate_task("MSAP5_22")

        # Simulate task MSAP5_23
        self.simulate_task("MSAP5_23")

        # Simulate task MSAP5_24
        self.simulate_task("MSAP5_24")

        # Verifying data integrity
        assert self.check_data("MSAP5_21")
        assert self.check_data("MSAP5_22")
        assert self.check_data("MSAP5_23")
        assert self.check_data("MSAP5_24")

    @pytest.mark.id("TI_PIPELINE_007")
    @pytest.mark.title("Test MSAP5_3 Pipeline Execution")
    @pytest.mark.description(
        "Test the execution of the MSAP5_3 pipeline, including task simulation and data integrity verification."
    )
    @pytest.mark.inputs("SAS pipeline initialized with MSAP5_3.")
    @pytest.mark.expected(
        "Tasks MSAP5_31, MSAP5_32, and MSAP5_34 should pass, and data integrity should be verified."
    )
    @pytest.mark.steps(
        "1. Initialize a SasPipeline instance with MSAP5_3.\n"
        "2. Simulate the execution of tasks MSAP5_31, MSAP5_32, and MSAP5_34.\n"
        "3. Verify the data integrity after each task simulation."
    )
    @pytest.mark.coverage_spec(
        [
            ("PDC-UC-GDP-SAS-2553", "Retrieving inputs from the SAS-PIP-STO"),
            ("PDC-UC-GDP-SAS-2554", "Transfer the outputs to SAS-PIP-STO"),
            (
                "PDC-UC-GDP-SAS-2914",
                "SAS product definition interface for data format validation",
            ),
        ]
    )
    def test_msap5_3_pipeline(self):

        class_names = DataClassFactory.get_model("MSAP5_3")
        if isinstance(class_names, tuple):
            cls_pipeline = class_names[0]
        else:
            cls_pipeline = class_names

        self.sas = SasPipeline(self.hdf5, cls_pipeline)

        # Simulate task MSAP5_31
        self.simulate_task("MSAP5_31")

        # Simulate task MSAP5_32
        self.simulate_task("MSAP5_32")

        # Simulate task MSAP5_34
        self.simulate_task("MSAP5_34")

        # Verifying data integrity
        assert self.check_data("MSAP5_31")
        assert self.check_data("MSAP5_32")
        assert self.check_data("MSAP5_34")
