# -*- coding: utf-8 -*-
# conftest.py
import json
import os

import pytest

pytest_session = None


def pytest_sessionstart(session):
    """
    Capture the session when pytest starts.
    """
    global pytest_session
    pytest_session = session


def pytest_collection_modifyitems(items):
    """
    Add a description to the collected tests from 'description' marker.
    """

    for item in items:

        # Fill ID
        id = item.get_closest_marker("id")
        if id:
            item.id = id.args[0]
        else:
            item.id = "No ID available."

        # Fill title
        title = item.get_closest_marker("title")
        if title:
            item.title = title.args[0]
        else:
            item.title = "No Title available."

        # Fill class description
        class_description = item.get_closest_marker("class_description")
        if class_description:
            item.class_description = class_description.args[0]
        else:
            item.class_description = "No description available."

        # Fill description
        description = item.get_closest_marker("description")
        if description:
            item.description = description.args[0]
        else:
            item.description = "No description available."

        # Fill inputs
        inputs = item.get_closest_marker("inputs")
        if inputs:
            item.inputs = inputs.args[0]
        else:
            item.inputs = "No inputs available."

        # Fill expected
        expected = item.get_closest_marker("expected")
        if expected:
            item.expected = expected.args[0]
        else:
            item.expected = "No expected available."

        # Fill steps
        steps = item.get_closest_marker("steps")
        if steps:
            item.steps = steps.args[0]
        else:
            item.expected = "No steps available."

        # Fill coverage_spec
        coverage_spec = item.get_closest_marker("coverage_spec")
        if coverage_spec:
            item.coverage_spec = coverage_spec.args
        else:
            item.coverage_spec = "No coverage_spec available."


@pytest.hookimpl(optionalhook=True)
def pytest_json_modifyreport(json_report):
    """
    Modify the generated JSON report to include the descriptions of the tests.
    """
    # Check if the pytest session is available
    if pytest_session is None:
        raise RuntimeError("La session pytest n'a pas été initialisée.")

    for test in json_report.get("tests", []):
        # Retreive nodeid of the test
        nodeid = test.get("nodeid")
        # Find the item related to the nodeid from collected tests
        for item in pytest_session.items:
            if item.nodeid == nodeid:
                # Add elements to JSON report
                test["id"] = getattr(item, "id", "No ID available.")
                test["title"] = getattr(item, "title", "No Title available.")
                test["class_description"] = getattr(
                    item, "class_description", "No description available."
                )
                test["description"] = getattr(
                    item, "description", "No description available."
                )
                test["inputs"] = getattr(
                    item, "inputs", "No inputs available."
                )
                test["expected"] = getattr(
                    item, "expected", "No expected available."
                )
                test["steps"] = getattr(item, "steps", "No steps available.")
                test["coverage_spec"] = getattr(
                    item, "coverage_spec", "No coverage_spec available."
                )
                break

    # Save the custom report
    filename = f'{json_report["created"]}.json'
    with open(filename, "w") as f:
        json.dump(json_report, f, default=str)
