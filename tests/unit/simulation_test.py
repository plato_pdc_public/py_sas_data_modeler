# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
from typing import Dict
from typing import List
from typing import Optional
from typing import Type

import numpy as np
import pytest
from pydantic import ConfigDict
from pydantic import Field

from py_sas_data_modeler.dm import BaseDataClass
from py_sas_data_modeler.simulation.fake_data import FakeData


# Sample Pydantic models for testing
class SimpleDataModel(BaseDataClass):
    id: int
    name: str
    value: float
    active: bool

    model_config = ConfigDict(frozen=True)  # Make the model immutable


class NestedDataModel(BaseDataClass):
    simple_data: SimpleDataModel
    tags: List[str] = Field(
        ..., min_length=3, max_length=3
    )  # Enforce exactly 3 items
    scores: List[float] = Field(..., min_length=3, max_length=3)

    model_config = ConfigDict(frozen=True)  # Make the model immutable


class OptionalDataModel(BaseDataClass):
    id: int
    description: Optional[str] = None
    rating: Optional[float] = None

    model_config = ConfigDict(frozen=True)  # Make the model immutable


@pytest.mark.class_description("Tests for the simulation module.")
class TestFaker:

    @pytest.fixture
    def fake_data_instance(self):
        """Fixture to create a FakeData instance with default settings."""
        return FakeData(use_numpy=True, has_optional=True)

    @pytest.fixture
    def fake_data_python(self):
        """Fixture to create a FakeData instance using Python native types."""
        return FakeData(use_numpy=False, has_optional=True)

    @pytest.mark.id("TU_SIMULATION_001")
    @pytest.mark.title("Test Generating Fake Data for Simple Pydantic Model")
    @pytest.mark.class_description(
        "Test the generation of fake data for a simple Pydantic model."
    )
    @pytest.mark.description(
        "This test ensures that the `generate_fake_data` method can generate valid fake data for a simple Pydantic model, with correct types for each field."
    )
    @pytest.mark.inputs(
        "A simple Pydantic model with fields: id (int), name (str), value (float), and active (bool)."
    )
    @pytest.mark.expected(
        "Generated data should match the expected types: int64 for id, string for name, float64 for value, and bool for active."
    )
    @pytest.mark.steps(
        "1. Call `generate_fake_data` with a simple Pydantic model. "
        "2. Verify that the generated data matches the expected types for each field."
    )
    def test_generate_simple_pydantic(self, fake_data_instance):
        """Test generating fake data for a simple Pydantic model."""
        fake_data_instance.generate_fake_data(SimpleDataModel)
        data = fake_data_instance.fake_data

        assert isinstance(data["id"], np.int64)
        assert isinstance(data["name"], str)
        assert isinstance(data["value"], np.float64)
        assert isinstance(data["active"], bool)

    @pytest.mark.id("TU_SIMULATION_002")
    @pytest.mark.title("Test Generating Fake Data for Nested Pydantic Model")
    @pytest.mark.class_description(
        "Test the generation of fake data for a nested Pydantic model."
    )
    @pytest.mark.description(
        "This test ensures that the `generate_fake_data` method can generate valid fake data for a nested Pydantic model, with correct data structures and sizes for nested fields."
    )
    @pytest.mark.inputs(
        "A nested Pydantic model with fields: simple_data (dict), tags (list of 3 items), and scores (list of 3 items)."
    )
    @pytest.mark.expected(
        "Generated data should match the expected types: dict for simple_data, list for tags and scores, and tags/scores should each contain 3 items."
    )
    @pytest.mark.steps(
        "1. Call `generate_fake_data` with a nested Pydantic model. "
        "2. Verify that the generated data matches the expected types for each nested field and the correct list lengths."
    )
    def test_generate_nested_pydantic(self, fake_data_instance):
        """Test generating fake data for a nested Pydantic model."""
        fake_data_instance.generate_fake_data(NestedDataModel)
        data = fake_data_instance.fake_data

        assert isinstance(data["simple_data"], dict)
        assert isinstance(data["tags"], list)
        assert isinstance(data["scores"], list)
        assert len(data["tags"]) == 3
        assert len(data["scores"]) == 3

    @pytest.mark.id("TU_SIMULATION_003")
    @pytest.mark.title(
        "Test Generating Data for Pydantic Model with Optional Fields"
    )
    @pytest.mark.class_description(
        "Test the generation of fake data for a Pydantic model that includes optional fields."
    )
    @pytest.mark.description(
        "This test ensures that the `generate_fake_data` method can handle optional fields in a Pydantic model. It verifies that the optional fields are included in the generated data."
    )
    @pytest.mark.inputs(
        "A Pydantic model with optional fields: description (str) and rating (float)."
    )
    @pytest.mark.expected(
        "Generated data should include the `id` field (int), and optionally include `description` and `rating` fields."
    )
    @pytest.mark.steps(
        "1. Call `generate_fake_data` with a Pydantic model containing optional fields. "
        "2. Verify that the `id` field is generated as expected, and that optional fields (description and rating) are present in the generated data."
    )
    def test_optional_fields_pydantic(self, fake_data_instance):
        """Test generating data for a Pydantic model with optional fields."""
        fake_data_instance.generate_fake_data(OptionalDataModel)
        data = fake_data_instance.fake_data

        assert isinstance(data["id"], np.int64)
        assert "description" in data
        assert "rating" in data

    @pytest.mark.id("TU_PYD_004")
    @pytest.mark.title("Test Generating Fake Data for List Type")
    @pytest.mark.class_description(
        "Test the generation of fake data for a list type, ensuring that each item is of the expected type."
    )
    @pytest.mark.description(
        "This test ensures that the `generate_fake_data` method can correctly generate a list of a specific type (in this case, a list of integers). It verifies that each element in the list is of type `np.int64`."
    )
    @pytest.mark.inputs(
        "A list type, specifically a list of integers (List[int])."
    )
    @pytest.mark.expected(
        "Generated data should be a list, and all elements in the list should be of type `np.int64`."
    )
    @pytest.mark.steps(
        "1. Call `_generate_default_value` with a list type (List[int]). "
        "2. Verify that the result is a list and that all items in the list are of type `np.int64`."
    )
    def test_generate_list_type(self, fake_data_instance):
        """Test generating fake data for a list type."""
        result = fake_data_instance._generate_default_value(List[int], {})
        assert isinstance(result, list)
        assert all(isinstance(item, np.int64) for item in result)

    @pytest.mark.id("TU_PYD_005")
    @pytest.mark.title("Test Generating Fake Data for Dictionary Type")
    @pytest.mark.class_description(
        "Test the generation of fake data for a dictionary type, ensuring that the keys and values are of the correct types."
    )
    @pytest.mark.description(
        "This test ensures that the `generate_fake_data` method can correctly generate a dictionary with specified key and value types. It verifies that the keys are of type `str` and the values are of type `np.int64`."
    )
    @pytest.mark.inputs(
        "A dictionary type, with keys of type `str` and values of type `np.int64`."
    )
    @pytest.mark.expected(
        "Generated data should be a dictionary with 3 items, where each key is of type `str` and each value is of type `np.int64`."
    )
    @pytest.mark.steps(
        "1. Call `_generate_fake_dict` with `str` as the key type and `np.int64` as the value type, specifying a size of 3. "
        "2. Verify that the result is a dictionary, it has exactly 3 items, and each key-value pair matches the specified types."
    )
    def test_generate_dict_type(self, fake_data_instance):
        """Test generating fake data for a dictionary type."""
        result = fake_data_instance._generate_fake_dict(str, int, 3, {})
        assert isinstance(result, dict)
        assert len(result) == 3
        assert all(
            isinstance(k, str) and isinstance(v, np.int64)
            for k, v in result.items()
        )

    @pytest.mark.id("TU_PYD_006")
    @pytest.mark.title("Test Using Python Native Types for Generating Data")
    @pytest.mark.class_description(
        "Test the generation of fake data using Python's native types, ensuring correct data type mapping."
    )
    @pytest.mark.description(
        "This test verifies that the `generate_fake_data` method can correctly generate data with Python's native types. It checks that the generated data matches the expected types: `int`, `float`, and `bool`."
    )
    @pytest.mark.inputs(
        "A Pydantic model using Python native types for its fields (int, float, and bool)."
    )
    @pytest.mark.expected(
        "Generated data should contain fields with Python's native types: `int`, `float`, and `bool`."
    )
    @pytest.mark.steps(
        "1. Call `generate_fake_data` with a Pydantic model using Python native types. "
        "2. Verify that the generated data contains fields with the correct types (`int`, `float`, `bool`)."
    )
    def test_use_python_types(self, fake_data_python):
        """Test using Python native types for generating data."""
        fake_data_python.generate_fake_data(SimpleDataModel)
        data = fake_data_python.fake_data

        assert isinstance(data["id"], int)
        assert isinstance(data["value"], float)
        assert isinstance(data["active"], bool)
