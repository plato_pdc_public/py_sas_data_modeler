# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
import pytest
from pydantic import ValidationError

from py_sas_data_modeler.dm.dataclass_management import BaseDataClass
from py_sas_data_modeler.dm.io_pipeline import Hdf5NodeEnum
from py_sas_data_modeler.dm.io_pipeline import InputSet
from py_sas_data_modeler.dm.io_pipeline import Pipeline
from py_sas_data_modeler.dm.io_pipeline import Product
from py_sas_data_modeler.dm.io_pipeline import Task
from py_sas_data_modeler.dm.io_pipeline import TaskCombination
from py_sas_data_modeler.exception import DataTypeError
from py_sas_data_modeler.exception import TaskNotFoundError


class NestedModel(BaseDataClass):
    nested_field: str


class SampleModel(BaseDataClass):
    field1: int
    field2: str
    nested: NestedModel


@pytest.mark.class_description(
    "Test suite for the BaseDataClass and its methods."
)
class TestBaseDataClass:

    # Test data
    @pytest.fixture
    def test_data(self):
        return {
            "field1": 42,
            "field2": "hello",
            "nested": {"nested_field": "world"},
        }

    @pytest.fixture
    def test_invalid_data(self):
        return {
            "field1": "not an int",  # Invalid type
            "field2": "hello",
            "nested": {"nested_field": "world"},
        }

    @pytest.fixture
    def test_missing_attr_data(self):
        return {"field2": "hello", "nested": {"nested_field": "world"}}

    @pytest.fixture
    def test_intance_nestedmodel(self):
        return NestedModel(nested_field="world")

    @pytest.fixture
    def sample_data(self):
        return SampleModel(
            field1=42,
            field2="hello",
            nested=NestedModel(nested_field="world"),
        )

    # Tests
    @pytest.mark.id("TU_DM_001")
    @pytest.mark.title("Test from_dict Method")
    @pytest.mark.class_description(
        "Tests the functionality of the from_dict method in SampleModel."
    )
    @pytest.mark.description(
        "This test checks if the from_dict method correctly converts a dictionary into a SampleModel object."
    )
    @pytest.mark.inputs("data: dictionary with fields field1, field2, nested.")
    @pytest.mark.expected(
        "The SampleModel object should be created with the correct field values."
    )
    @pytest.mark.steps(
        "1. Prepare input data as a dictionary.\n"
        "2. Call the from_dict method.\n"
        "3. Assert the field values of the created object."
    )
    def test_from_dict_success(self, test_data):
        instance = SampleModel.from_dict(test_data)

        assert instance.field1 == 42
        assert instance.field2 == "hello"
        assert instance.nested.nested_field == "world"

    @pytest.mark.id("TU_DM_002")
    @pytest.mark.title("Test from_dict with Invalid Data Type")
    @pytest.mark.class_description(
        "Tests the handling of invalid data types in the from_dict method."
    )
    @pytest.mark.description(
        "This test ensures that passing invalid data types to from_dict raises a ValidationError."
    )
    @pytest.mark.inputs(
        "test_invalid_data: dictionary with incorrect data types for the fields."
    )
    @pytest.mark.expected(
        "A ValidationError is raised, with a message indicating which field is invalid."
    )
    @pytest.mark.steps(
        "1. Prepare invalid input data.\n"
        "2. Call the from_dict method.\n"
        "3. Assert that a ValidationError is raised with the expected message."
    )
    def test_from_dict_invalid_type(self, test_invalid_data):
        with pytest.raises(
            ValidationError,
            match="field1",
        ):
            SampleModel.from_dict(test_invalid_data)

    @pytest.mark.id("TU_DM_003")
    @pytest.mark.title("Test to_dict method")
    @pytest.mark.class_description(
        "Tests the conversion of a SampleModel instance to a dictionary using the to_dict method."
    )
    @pytest.mark.description(
        "This test ensures that the to_dict method correctly converts an instance of SampleModel to the expected dictionary format."
    )
    @pytest.mark.inputs(
        "instance: SampleModel instance with nested fields and sample data."
    )
    @pytest.mark.expected(
        "The dictionary produced by model_dump should match the expected dictionary, with the same field names and values."
    )
    @pytest.mark.steps(
        "1. Create an instance of SampleModel with sample data.\n"
        "2. Call the model_dump method.\n"
        "3. Compare the result with the expected dictionary."
    )
    def test_to_dict_success(self, test_intance_nestedmodel):
        instance = SampleModel(
            field1=42, field2="hello", nested=test_intance_nestedmodel
        )

        expected_dict = {
            "field1": 42,
            "field2": "hello",
            "nested": {"nested_field": "world"},
        }
        assert instance.model_dump(by_alias=True) == expected_dict

    @pytest.mark.id("TU_DM_004")
    @pytest.mark.title("Test get_field_value method")
    @pytest.mark.class_description(
        "Tests the get_field_value method of the SampleModel class."
    )
    @pytest.mark.description(
        "This test checks that the get_field_value method correctly retrieves the values of fields in a SampleModel instance."
    )
    @pytest.mark.inputs(
        "instance: SampleModel instance with multiple fields including a nested model."
    )
    @pytest.mark.expected(
        "The values of 'field1', 'field2', and 'nested_field' should be 42, 'hello', and 'world', respectively."
    )
    @pytest.mark.steps(
        "1. Create an instance of SampleModel with known field values.\n"
        "2. Call the get_field_value method for each field.\n"
        "3. Verify that the returned values match the expected values."
    )
    def test_get_field_value_success(self, sample_data):
        assert sample_data.field1 == 42
        assert sample_data.field2 == "hello"
        assert sample_data.nested.nested_field == "world"

    @pytest.mark.id("TU_DM_005")
    @pytest.mark.title("Test list_fields method")
    @pytest.mark.class_description(
        "Tests the list_fields method of the SampleModel class."
    )
    @pytest.mark.description(
        "This test checks that the list_fields method correctly lists all the fields of a SampleModel instance."
    )
    @pytest.mark.inputs(
        "instance: SampleModel instance with multiple fields including nested models."
    )
    @pytest.mark.expected(
        "The fields list should contain 'field1', 'field2', and 'nested' as keys."
    )
    @pytest.mark.steps(
        "1. Create an instance of SampleModel.\n"
        "2. Call the list_fields method.\n"
        "3. Verify that 'field1', 'field2', and 'nested' are in the list of fields."
    )
    def test_list_fields(self, sample_data):
        fields = sample_data.list_fields()

        assert "field1" in fields
        assert "field2" in fields
        assert "nested" in fields

    @pytest.mark.id("TU_DM_006")
    @pytest.mark.title("Test invalid field access")
    @pytest.mark.class_description(
        "Tests the behavior when an invalid field is accessed in the SampleModel class."
    )
    @pytest.mark.description(
        "This test checks that attempting to access an invalid field raises an AttributeError with the correct message."
    )
    @pytest.mark.inputs("instance: SampleModel instance with valid fields.")
    @pytest.mark.expected(
        "An AttributeError should be raised with the message indicating the invalid field."
    )
    @pytest.mark.steps(
        "1. Create an instance of SampleModel.\n"
        "2. Attempt to access a field that doesn't exist.\n"
        "3. Verify that an AttributeError is raised."
    )
    def test_get_field_value_invalid_field(self, sample_data):
        with pytest.raises(
            AttributeError, match="has no attribute 'invalid_field'"
        ):
            sample_data.invalid_field

    @pytest.mark.id("TU_DM_007")
    @pytest.mark.title("Test missing required field in from_dict method")
    @pytest.mark.class_description(
        "Tests the behavior when a required field is missing in the input dictionary."
    )
    @pytest.mark.description(
        "This test checks that when a required field is missing from the input data, a ValidationError is raised."
    )
    @pytest.mark.inputs(
        "test_missing_attr_data: Dictionary with missing required field 'field1'."
    )
    @pytest.mark.expected(
        "A ValidationError should be raised with the message indicating the missing field 'field1'."
    )
    @pytest.mark.steps(
        "1. Provide a dictionary without 'field1'.\n"
        "2. Call from_dict method.\n"
        "3. Verify that a ValidationError is raised."
    )
    def test_from_dict_missing_required_field(self, test_missing_attr_data):
        with pytest.raises(
            ValidationError,
            match="field1",
        ):
            SampleModel.from_dict(test_missing_attr_data)


@pytest.mark.class_description("Test suite for InputSet and its behavior.")
class TestInputSet:

    @pytest.fixture
    def simple_required_input_sets(self):
        """Fixture for simple product hints."""
        return InputSet(
            mandatory_inputs=[
                Product(
                    name="test", implementation_class=str, is_required=True
                )
            ],
            optional_inputs=[],
        )

    @pytest.fixture
    def simple_required_and_optional_input_sets(self):
        """Fixture for simple product hints."""
        return InputSet(
            mandatory_inputs=[
                Product(
                    name="test", implementation_class=str, is_required=True
                )
            ],
            optional_inputs=[
                Product(
                    name="test", implementation_class=str, is_required=True
                )
            ],
        )

    @pytest.fixture
    def sample_inputs_task(self):
        product1 = Product(
            name="Input1", implementation_class=int, is_required=True
        )
        product2 = Product(
            name="Input2", implementation_class=float, is_required=False
        )
        return InputSet(
            mandatory_inputs=[product1], optional_inputs=[product2]
        )

    @pytest.fixture
    def sample_required_input_task(self):
        product1 = Product(
            name="Input1", implementation_class=int, is_required=True
        )
        return InputSet(mandatory_inputs=[product1])

    @pytest.mark.id("TU_DM_008")
    @pytest.mark.title("Test the parsing of simple product hints")
    @pytest.mark.class_description(
        "Tests the parsing of mandatory and optional input sets."
    )
    @pytest.mark.description(
        "This test ensures that when parsing simple input sets, the mandatory inputs are correctly identified and the optional inputs are empty."
    )
    @pytest.mark.inputs(
        "simple_required_input_sets: A SimpleRequiredInputSets instance with mandatory inputs."
    )
    @pytest.mark.expected(
        "The number of mandatory inputs should be 1, optional inputs should be 0, and total inputs should be 1."
    )
    @pytest.mark.steps(
        "1. Parse the simple input set.\n"
        "2. Verify the length of mandatory inputs.\n"
        "3. Verify the length of optional inputs.\n"
        "4. Check the total number of inputs."
    )
    def test_number_products_with_required(self, simple_required_input_sets):
        """Test the parsing of simple product hints."""
        assert len(simple_required_input_sets.mandatory_inputs) == 1
        assert len(simple_required_input_sets.optional_inputs) == 0
        assert simple_required_input_sets.nb_inputs == 1

    @pytest.mark.id("TU_DM_009")
    @pytest.mark.title(
        "Test the parsing of simple product hints with mandatory and optional inputs"
    )
    @pytest.mark.class_description(
        "Tests the parsing of both mandatory and optional input sets."
    )
    @pytest.mark.description(
        "This test ensures that when parsing input sets with both mandatory and optional inputs, they are correctly identified."
    )
    @pytest.mark.inputs(
        "simple_required_and_optional_input_sets: A SimpleRequiredAndOptionalInputSets instance with both mandatory and optional inputs."
    )
    @pytest.mark.expected(
        "The number of mandatory inputs should be 1, optional inputs should be 1, and total inputs should be 2."
    )
    @pytest.mark.steps(
        "1. Parse the input set.\n"
        "2. Verify the length of mandatory inputs.\n"
        "3. Verify the length of optional inputs.\n"
        "4. Check the total number of inputs."
    )
    def test_number_products_with_optional(
        self, simple_required_and_optional_input_sets
    ):
        """Test the parsing of simple product hints."""
        assert (
            len(simple_required_and_optional_input_sets.mandatory_inputs) == 1
        )
        assert (
            len(simple_required_and_optional_input_sets.optional_inputs) == 1
        )
        assert simple_required_and_optional_input_sets.nb_inputs == 2

    @pytest.mark.id("TU_DM_010")
    @pytest.mark.title(
        "Test input set initialization with mandatory and optional inputs"
    )
    @pytest.mark.class_description(
        "Tests the initialization of input sets with both mandatory and optional inputs."
    )
    @pytest.mark.description(
        "This test ensures that when initializing input sets, the number of mandatory and optional inputs are correctly identified."
    )
    @pytest.mark.inputs(
        "sample_inputs_task: A SampleInputsTask instance containing both mandatory and optional inputs."
    )
    @pytest.mark.expected(
        "The number of inputs should be 2. 1 mandatory and 1 optional input."
    )
    @pytest.mark.steps(
        "1. Initialize the input set.\n"
        "2. Verify the total number of inputs.\n"
        "3. Verify the length of mandatory inputs.\n"
        "4. Verify the length of optional inputs."
    )
    def test_inputset_initialization(self, sample_inputs_task):
        assert sample_inputs_task.nb_inputs == 2
        assert len(sample_inputs_task.mandatory_inputs) == 1
        assert len(sample_inputs_task.optional_inputs) == 1

    @pytest.mark.id("TU_DM_011")
    @pytest.mark.title("Test input set with only required inputs")
    @pytest.mark.class_description(
        "Tests the initialization of input sets containing only required inputs and no optional inputs."
    )
    @pytest.mark.description(
        "This test ensures that input sets with only mandatory inputs correctly show 0 optional inputs."
    )
    @pytest.mark.inputs(
        "sample_required_input_task: A SampleRequiredInputTask instance containing only mandatory inputs."
    )
    @pytest.mark.expected(
        "There should be 1 input (mandatory) and no optional inputs."
    )
    @pytest.mark.steps(
        "1. Initialize the input set.\n"
        "2. Verify the total number of inputs.\n"
        "3. Verify there are no optional inputs."
    )
    def test_inputset_no_optional(self, sample_required_input_task):
        assert sample_required_input_task.nb_inputs == 1
        assert len(sample_required_input_task.optional_inputs) == 0


@pytest.mark.class_description(
    "Test suite for the Pipeline class and its operations."
)
class TestPipeline:

    @pytest.fixture
    def simple_pipeline(self):
        """Fixture for simple pipeline hints."""
        product1 = Product(
            name="product1", implementation_class=str, is_required=True
        )
        product2 = Product(
            name="product2", implementation_class=str, is_required=True
        )
        product3 = Product(
            name="product3", implementation_class=str, is_required=True
        )
        product4 = Product(
            name="product4", implementation_class=str, is_required=True
        )

        inputs_set1 = InputSet(mandatory_inputs=[product1, product2])
        inputs_set2 = InputSet(mandatory_inputs=[product3, product4])
        task = Task(
            name="task1",
            input_sets=[inputs_set1, inputs_set2],
            mandatory_outputs=[],
        )
        task_comb = TaskCombination(tasks=[task])
        pipeline = Pipeline()
        pipeline.add_combination(task_combination=task_comb)
        return pipeline

    @pytest.mark.id("TU_DM_012")
    @pytest.mark.title("Test all tasks in the pipeline")
    @pytest.mark.class_description(
        "Tests the retrieval of all tasks, modules, and task combinations in a simple pipeline."
    )
    @pytest.mark.description(
        "This test ensures that the pipeline correctly returns the expected number of tasks, modules, and task combinations."
    )
    @pytest.mark.inputs(
        "simple_pipeline: A simple pipeline with tasks and modules."
    )
    @pytest.mark.expected(
        "1 task, 1 module named 'task1', and 1 task combination."
    )
    @pytest.mark.steps(
        "1. Initialize the pipeline.\n"
        "2. Verify the number of tasks.\n"
        "3. Verify the module names.\n"
        "4. Verify task combinations."
    )
    def test_all_tasks(self, simple_pipeline):
        assert len(simple_pipeline.all_tasks) == 1
        assert simple_pipeline.modules == ["task1"]
        assert len(simple_pipeline.task_combinations) == 1

    @pytest.mark.id("TU_DM_013")
    @pytest.mark.title("Test pipeline initialization")
    @pytest.mark.class_description(
        "Tests the initialization of a pipeline and verifies task combinations count."
    )
    @pytest.mark.description(
        "This test ensures that upon initialization, the pipeline has no task combinations."
    )
    @pytest.mark.inputs(
        "Pipeline: An empty pipeline with no pre-defined task combinations."
    )
    @pytest.mark.expected("0 task combinations in the pipeline.")
    @pytest.mark.steps(
        "1. Initialize the pipeline. 2. Verify that no task combinations exist."
    )
    def test_add_combination(self):
        product1 = Product(
            name="Input1", implementation_class=int, is_required=True
        )
        product2 = Product(
            name="Output1", implementation_class=str, is_required=True
        )
        input_set = InputSet(mandatory_inputs=[product1])
        task = Task(
            name="Task1", input_sets=[input_set], mandatory_outputs=[product2]
        )
        task_combination = TaskCombination(tasks=[task])

        pipeline = Pipeline()
        pipeline.add_combination(task_combination)

        assert len(pipeline.task_combinations) == 1

    @pytest.mark.id("TU_DM_014")
    @pytest.mark.title("Test pipeline initialization")
    @pytest.mark.class_description(
        "Tests the initialization of a pipeline and verifies task combinations count."
    )
    @pytest.mark.description(
        "This test ensures that upon initialization, the pipeline has no task combinations."
    )
    @pytest.mark.inputs(
        "Pipeline: An empty pipeline with no pre-defined task combinations."
    )
    @pytest.mark.expected("0 task combinations in the pipeline.")
    @pytest.mark.steps(
        "1. Initialize the pipeline.\n"
        "2. Verify that no task combinations exist."
    )
    def test_pipeline_initialization(self):
        pipeline = Pipeline()
        assert len(pipeline.task_combinations) == 0

    @pytest.mark.id("TU_DM_015")
    @pytest.mark.title("Test modules property of the pipeline")
    @pytest.mark.class_description(
        "Tests the 'modules' property of the pipeline, verifying that it lists the tasks properly."
    )
    @pytest.mark.description(
        "This test checks that the 'modules' property of a pipeline correctly returns the names of the tasks in the pipeline."
    )
    @pytest.mark.inputs(
        "A pipeline containing a task with a product in its input set."
    )
    @pytest.mark.expected(
        "The 'modules' property should return the task name in the pipeline."
    )
    @pytest.mark.steps(
        "1. Create a pipeline with a task.\n"
        "2. Add a task combination to the pipeline.\n"
        "3. Verify that the 'modules' property returns the correct task names."
    )
    def test_modules_property(self):
        product1 = Product(
            name="Input1", implementation_class=int, is_required=True
        )
        input_set = InputSet(mandatory_inputs=[product1])
        task = Task(name="Task1", input_sets=[input_set], mandatory_outputs=[])
        task_combination = TaskCombination(tasks=[task])

        pipeline = Pipeline()
        pipeline.add_combination(task_combination)

        assert pipeline.modules == ["Task1"]

    @pytest.mark.id("TU_DM_016")
    @pytest.mark.title("Test all_tasks property of the pipeline")
    @pytest.mark.class_description(
        "Tests the 'all_tasks' property of the pipeline, ensuring it returns all tasks correctly."
    )
    @pytest.mark.description(
        "This test checks that the 'all_tasks' property correctly returns the list of tasks in the pipeline."
    )
    @pytest.mark.inputs(
        "A pipeline containing a task with a product in its input set."
    )
    @pytest.mark.expected(
        "The 'all_tasks' property should return a list of all tasks in the pipeline, including their names."
    )
    @pytest.mark.steps(
        "1. Create a pipeline with a task.\n"
        "2. Add a task combination to the pipeline.\n"
        "3. Verify that the 'all_tasks' property returns the correct task list."
    )
    def test_all_tasks_property(self):
        product1 = Product(
            name="Input1", implementation_class=int, is_required=True
        )
        input_set = InputSet(mandatory_inputs=[product1])
        task = Task(name="Task1", input_sets=[input_set], mandatory_outputs=[])
        task_combination = TaskCombination(tasks=[task])

        pipeline = Pipeline()
        pipeline.add_combination(task_combination)

        assert len(pipeline.all_tasks) == 1
        assert pipeline.all_tasks[0].name == "Task1"

    @pytest.mark.id("TU_DM_017")
    @pytest.mark.title("Test task retrieval from the pipeline")
    @pytest.mark.class_description(
        "Tests the retrieval of a task by its name from the pipeline."
    )
    @pytest.mark.description(
        "This test checks that a task can be correctly retrieved by its name from the pipeline."
    )
    @pytest.mark.inputs(
        "A pipeline containing a task with a product in its input set."
    )
    @pytest.mark.expected(
        "The task should be retrieved successfully by its name from the pipeline."
    )
    @pytest.mark.steps(
        "1. Create a pipeline with a task.\n"
        "2. Add a task combination to the pipeline.\n"
        "3. Retrieve the task by its name using 'task()' method."
    )
    def test_task_retrieval(self):
        product1 = Product(
            name="Input1", implementation_class=int, is_required=True
        )
        input_set = InputSet(mandatory_inputs=[product1])
        task = Task(name="Task1", input_sets=[input_set], mandatory_outputs=[])
        task_combination = TaskCombination(tasks=[task])

        pipeline = Pipeline()
        pipeline.add_combination(task_combination)

        retrieved_task = pipeline.task("Task1")
        assert retrieved_task.name == "Task1"

    @pytest.mark.id("TU_DM_018")
    @pytest.mark.title("Test task not found exception")
    @pytest.mark.class_description(
        "Tests the case where a task is not found in the pipeline."
    )
    @pytest.mark.description(
        "This test verifies that a TaskNotFoundError is raised when trying to retrieve a non-existent task from the pipeline."
    )
    @pytest.mark.inputs("A pipeline without the specified task.")
    @pytest.mark.expected(
        "TaskNotFoundError should be raised when attempting to retrieve a non-existent task."
    )
    @pytest.mark.steps(
        "1. Create an empty pipeline.\n"
        "2. Try to retrieve a non-existent task by name using 'task()' method.\n"
        "3. Verify that TaskNotFoundError is raised."
    )
    def test_task_not_found(self):
        pipeline = Pipeline()
        with pytest.raises(TaskNotFoundError):
            pipeline.task("NonExistentTask")


@pytest.mark.class_description(
    "Test suite for the Hdf5NodeEnum class and its values."
)
class TestHdf5NodeEnum:

    @pytest.mark.id("TU_DM_019")
    @pytest.mark.title("Test Hdf5NodeEnum values")
    @pytest.mark.class_description(
        "Tests the string representation of enum values in Hdf5NodeEnum."
    )
    @pytest.mark.description(
        "This test verifies that the enum values of Hdf5NodeEnum return the correct string values when converted to a string."
    )
    @pytest.mark.inputs(
        "Enum values Hdf5NodeEnum.PRODUCTS and Hdf5NodeEnum.EXTERNALS."
    )
    @pytest.mark.expected(
        "Enum values should return '/PRODUCTS' and '/EXTERNALS' when converted to strings."
    )
    @pytest.mark.steps(
        "1. Access the enum values Hdf5NodeEnum.PRODUCTS and Hdf5NodeEnum.EXTERNALS.\n"
        "2. Convert them to string and verify their expected string representations."
    )
    def test_enum_values(self):
        assert str(Hdf5NodeEnum.PRODUCTS) == "/PRODUCTS"
        assert str(Hdf5NodeEnum.EXTERNALS) == "/EXTERNALS"


@pytest.mark.class_description(
    "Test suite for Product dataclass and its behavior."
)
class TestProduct:

    @pytest.fixture
    def data_product(self):
        return Product(
            name="ProductA", implementation_class=str, is_required=True
        )

    @pytest.mark.id("TU_DM_020")
    @pytest.mark.title("Test Product initialization and attribute access")
    @pytest.mark.class_description(
        "Tests the getter and setter of Product object's attributes."
    )
    @pytest.mark.description(
        "This test checks if the Product object's attributes are initialized correctly and can be accessed properly."
    )
    @pytest.mark.inputs(
        "Product object with name='ProductA', implementation_class=str, is_required=True."
    )
    @pytest.mark.expected(
        "Attributes should be 'ProductA', 'str', and True respectively."
    )
    @pytest.mark.steps(
        "1. Create a Product object using the fixture.\n"
        "2. Access and check the attributes name, implementation_class, and is_required."
    )
    def test_product_initialization(self, data_product):
        assert data_product.name == "ProductA"
        assert data_product.implementation_class == str
        assert data_product.is_required is True

    @pytest.mark.id("TU_DM_021")
    @pytest.mark.title("Test Product immutability (frozen attribute)")
    @pytest.mark.class_description(
        "Tests if the Product object's attributes are immutable after initialization."
    )
    @pytest.mark.description(
        "This test checks if attempting to modify the 'name' attribute of the Product object raises a ValidationError."
    )
    @pytest.mark.inputs(
        "Product object with name='ProductA', implementation_class=str, is_required=True."
    )
    @pytest.mark.expected(
        "ValidationError is raised when attempting to modify the 'name' attribute."
    )
    @pytest.mark.steps(
        "1. Create a Product object using the fixture.\n"
        "2. Attempt to change the 'name' attribute.\n"
        "3. Check that a ValidationError is raised."
    )
    def test_product_frozen(self, data_product):
        with pytest.raises(
            ValidationError,
            match="name",
        ):
            data_product.name = "NewName"


@pytest.mark.class_description(
    "Test suite for Task dataclass and its behavior."
)
class TestTask:

    @pytest.mark.id("TU_DM_022")
    @pytest.mark.title("Test Task Initialization")
    @pytest.mark.class_description(
        "Tests the initialization of Task objects with input sets and mandatory outputs."
    )
    @pytest.mark.description(
        "This test validates the successful creation of a Task object with mandatory inputs and outputs."
    )
    @pytest.mark.inputs(
        "Two products: Input1 (int), Output1 (str), an InputSet with Input1, and a Task with mandatory outputs."
    )
    @pytest.mark.expected(
        "Task object is initialized successfully with correct name, input sets, and mandatory outputs."
    )
    @pytest.mark.steps(
        "1. Create Product objects for inputs and outputs.\n"
        "2. Create an InputSet with the input product.\n"
        "3. Initialize the Task with input sets and mandatory outputs.\n"
        "4. Validate the task's attributes."
    )
    def test_task_initialization(self):
        product1 = Product(
            name="Input1", implementation_class=int, is_required=True
        )
        product2 = Product(
            name="Output1", implementation_class=str, is_required=True
        )
        input_set = InputSet(mandatory_inputs=[product1])
        task = Task(
            name="Task1", input_sets=[input_set], mandatory_outputs=[product2]
        )

        assert task.name == "Task1"
        assert len(task.input_sets) == 1
        assert len(task.mandatory_outputs) == 1


@pytest.mark.class_description(
    "Test suite for TaskCombination dataclass and its behavior."
)
class TestTaskCombination:

    @pytest.mark.id("TU_DM_023")
    @pytest.mark.title("Test TaskCombination Initialization")
    @pytest.mark.class_description(
        "Tests the initialization of TaskCombination objects with tasks."
    )
    @pytest.mark.description(
        "This test validates the successful creation of a TaskCombination object containing a Task with input sets and mandatory outputs."
    )
    @pytest.mark.inputs(
        "Two products: Input1 (int), Output1 (str), an InputSet with Input1, a Task with mandatory outputs, and a TaskCombination with that task."
    )
    @pytest.mark.expected(
        "TaskCombination object is initialized successfully with the correct tasks."
    )
    @pytest.mark.steps(
        "1. Create Product objects for inputs and outputs.\n"
        "2. Create an InputSet with the input product.\n"
        "3. Initialize the Task with input sets and mandatory outputs.\n"
        "4. Create the TaskCombination with the task.\n"
        "5. Validate that TaskCombination contains the expected task."
    )
    def test_taskcombination_initialization(self):
        product1 = Product(
            name="Input1", implementation_class=int, is_required=True
        )
        product2 = Product(
            name="Output1", implementation_class=str, is_required=True
        )
        input_set = InputSet(mandatory_inputs=[product1])
        task = Task(
            name="Task1", input_sets=[input_set], mandatory_outputs=[product2]
        )
        task_combination = TaskCombination(tasks=[task])

        assert len(task_combination.tasks) == 1
        assert task_combination.tasks[0].name == "Task1"
