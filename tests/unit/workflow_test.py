# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
import pytest

from py_sas_data_modeler.dm import DataClassFactory
from py_sas_data_modeler.exception import ModuleNotFoundError
from py_sas_data_modeler.io import HDF5StorageSasWorkflow
from py_sas_data_modeler.workflow import Pipeline
from py_sas_data_modeler.workflow import SasPipeline


@pytest.mark.class_description(
    "Tests for the SasPipeline class, verifying initialization, module creation, task parsing, and pipeline functionality."
)
class TestSasPipeline:

    @pytest.fixture(autouse=True)
    def setup(self, tmp_path_factory):
        tmp_dir = tmp_path_factory.mktemp("data")
        file = tmp_dir / "test.h5"
        self.storage = HDF5StorageSasWorkflow(file)
        self.cls = DataClassFactory.get_model("MSAP1")
        self.pipeline = SasPipeline(self.storage, self.cls)

    @pytest.mark.id("SAS_WORKFLOW_001")
    @pytest.mark.title("Test Pipeline Class Association")
    @pytest.mark.description(
        "This test ensures that the `cls` property of the pipeline matches the class passed during initialization."
    )
    @pytest.mark.inputs("Pipeline initialized with class MSAP1.")
    @pytest.mark.expected(
        "The pipeline class (`cls`) should match the provided class MSAP1."
    )
    @pytest.mark.steps(
        "1. Initialize the SasPipeline with class MSAP1."
        "2. Verify that the `cls` property of the pipeline matches the class MSAP1."
    )
    def test_cls(self):
        assert self.pipeline.cls == self.cls

    @pytest.mark.id("SAS_WORKFLOW_002")
    @pytest.mark.title("Test Storage Association")
    @pytest.mark.description(
        "This test verifies that the `sas_storage` property of the pipeline matches the storage passed during initialization."
    )
    @pytest.mark.inputs(
        "Pipeline initialized with an HDF5StorageSasWorkflow instance."
    )
    @pytest.mark.expected(
        "The pipeline's `sas_storage` should match the initialized storage."
    )
    @pytest.mark.steps(
        "1. Initialize the SasPipeline with HDF5StorageSasWorkflow."
        "2. Verify that the `sas_storage` property matches the provided storage."
    )
    def test_storage(self):
        assert self.pipeline.sas_storage == self.storage

    @pytest.mark.id("SAS_WORKFLOW_003")
    @pytest.mark.title("Test Module List")
    @pytest.mark.description(
        "This test ensures that the pipeline's modules list contains the correct modules."
    )
    @pytest.mark.inputs("Pipeline initialized with valid modules.")
    @pytest.mark.expected(
        "The pipeline's modules list should contain the modules 'MSAP1_01', 'MSAP1_02', 'MSAP1_07', 'MSAP1_08'."
    )
    @pytest.mark.steps(
        "1. Initialize the SasPipeline."
        "2. Verify that the `modules` list matches the expected modules."
    )
    def test_modules(self):
        assert (
            self.pipeline.modules.sort()
            == ["MSAP1_01", "MSAP1_02", "MSAP1_07", "MSAP1_08"].sort()
        )

    @pytest.mark.id("SAS_WORKFLOW_004")
    @pytest.mark.title("Test Tasks Existence")
    @pytest.mark.description(
        "This test verifies that the pipeline contains tasks after initialization."
    )
    @pytest.mark.inputs("Pipeline initialized with tasks.")
    @pytest.mark.expected("The pipeline should contain tasks.")
    @pytest.mark.steps(
        "1. Initialize the SasPipeline."
        "2. Verify that the pipeline contains tasks by checking the length of `tasks`."
    )
    def test_tasks(self):
        assert len(self.pipeline.tasks) > 0

    @pytest.mark.id("SAS_WORKFLOW_005")
    @pytest.mark.title("Test Pipeline Parsing")
    @pytest.mark.description(
        "This test ensures that the `_parse_pipeline()` method correctly parses the pipeline into a `Pipeline` object with the expected task combinations."
    )
    @pytest.mark.inputs("Pipeline initialized with tasks.")
    @pytest.mark.expected(
        "The `parse_pipeline` method should return a Pipeline object with matching task combinations."
    )
    @pytest.mark.steps(
        "1. Call the `_parse_pipeline()` method of the SasPipeline."
        "2. Verify that it returns a Pipeline object."
        "3. Verify that the task combinations in the parsed pipeline match the tasks of the pipeline."
    )
    def test_parse_pipeline(self):
        pipeline = self.pipeline._parse_pipeline()
        assert isinstance(pipeline, Pipeline)
        assert pipeline.task_combinations == self.pipeline.tasks

    @pytest.mark.id("SAS_WORKFLOW_006")
    @pytest.mark.title("Test Module Creation")
    @pytest.mark.description(
        "This test ensures that the pipeline can create a module with the correct name."
    )
    @pytest.mark.inputs("Pipeline initialized with a module name.")
    @pytest.mark.expected(
        "The created module should have the correct module name."
    )
    @pytest.mark.steps(
        "1. Call the `create_module()` method of the SasPipeline with a valid module name."
        "2. Verify that the created module has the expected module name."
    )
    def test_create_module(self):
        sas_modules = self.pipeline.create_module("MSAP1_01")
        assert sas_modules.module_name == "MSAP1_01"

    @pytest.mark.id("SAS_WORKFLOW_007")
    @pytest.mark.title("Test Invalid Module Creation")
    @pytest.mark.description(
        "This test ensures that the pipeline raises a `ModuleNotFoundError` when trying to create an invalid module."
    )
    @pytest.mark.inputs("Invalid module name.")
    @pytest.mark.expected(
        "A `ModuleNotFoundError` should be raised when attempting to create an invalid module."
    )
    @pytest.mark.steps(
        "1. Call the `create_module()` method of the SasPipeline with an invalid module name."
        "2. Verify that a `ModuleNotFoundError` is raised with the appropriate error message."
    )
    def test_create_invalid_module(self):
        with pytest.raises(
            ModuleNotFoundError, match="Module 'INVALID_MODULE' not found."
        ):
            self.pipeline.create_module("INVALID_MODULE")
