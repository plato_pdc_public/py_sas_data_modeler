# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
import os
from typing import Any
from typing import Union
from unittest.mock import MagicMock
from unittest.mock import Mock
from unittest.mock import patch

import h5py
import numpy as np
import pytest
from deepdiff import DeepDiff
from loguru import logger
from pydantic import Field

from py_sas_data_modeler.dm import BaseDataClass
from py_sas_data_modeler.dm import DataClassFactory
from py_sas_data_modeler.dm.io_pipeline import Hdf5NodeEnum
from py_sas_data_modeler.exception import NoExistingDatasetError
from py_sas_data_modeler.exception import ProductClassError
from py_sas_data_modeler.exception import ProductSaveError
from py_sas_data_modeler.exception import StoringInformationError
from py_sas_data_modeler.helper import spec_coverage
from py_sas_data_modeler.helper import Utils
from py_sas_data_modeler.io.hdf5 import HDF5Manager
from py_sas_data_modeler.io.hdf5 import HDF5StorageSasTask
from py_sas_data_modeler.io.hdf5 import HDF5StorageSasWorkflow
from py_sas_data_modeler.io.interface import FileMediator
from py_sas_data_modeler.io.interface import IStorageSasTask
from py_sas_data_modeler.io.interface import IStorageSasWorkflow
from py_sas_data_modeler.simulation import FakeData


class MockFieldClass(BaseDataClass):
    field_3: int
    field_4: str


class MockDataClassSchema(BaseDataClass):
    field_1: MockFieldClass
    field_2: str


@spec_coverage(
    "PLATO-IAS-PDC-TN-0005", "test_hard_links_for_pipeline_inputs_and_outputs"
)
@spec_coverage(
    "PLATO-IAS-PDC-TN-0005", "test_hard_links_for_pipeline_inputs_and_outputs"
)
class TestHDF5Manager:

    @pytest.fixture
    def temp_hdf5_file(self, tmp_path):
        """Fixture to create a temporary HDF5 file for testing."""
        hdf5_file_path = tmp_path / "test_file.hdf5"
        with h5py.File(hdf5_file_path, "w"):
            pass
        yield str(hdf5_file_path)
        if os.path.exists(hdf5_file_path):
            os.remove(hdf5_file_path)

    @pytest.fixture
    def hdf5_manager(self, temp_hdf5_file):
        """Fixture to create an instance of HDF5Manager with the temporary file."""
        return HDF5Manager(temp_hdf5_file)

    @pytest.mark.id("TU_IO_001")
    @pytest.mark.title("Test Encode None")
    @pytest.mark.class_description("Test encoding None as a string.")
    @pytest.mark.description(
        "This test ensures that the `encode_none` method of `HDF5Manager` correctly encodes `None` as a string 'None'."
    )
    @pytest.mark.inputs("None (Python's None object).")
    @pytest.mark.expected("String 'None'.")
    def test_encode_none(self):
        result = HDF5Manager.encode_none()
        expected = "None"
        assert result == expected

    @pytest.mark.id("TU_IO_002")
    @pytest.mark.title("Test Decode None")
    @pytest.mark.class_description(
        "Test decoding the string 'None' back to Python's None."
    )
    @pytest.mark.description(
        "This test ensures that the `decode_none` method of `HDF5Manager` correctly decodes the string 'None' to None."
    )
    @pytest.mark.inputs("String 'None'.")
    @pytest.mark.expected("None (Python's None object).")
    @pytest.mark.steps(
        "1. Call `HDF5Manager.decode_none('None')`. 2. Check that the result is `None`."
    )
    def test_decode_none(self):
        result = HDF5Manager.decode_none("None")
        excepted = None
        assert result == excepted

    @pytest.mark.id("TU_IO_003")
    @pytest.mark.title("Test is_group Method for Different Paths")
    @pytest.mark.class_description(
        "Test the `is_group` method of the `HDF5Manager` for various paths."
    )
    @pytest.mark.description(
        "This test ensures that the `is_group` method correctly identifies whether a path corresponds to an existing group in the HDF5 file."
    )
    @pytest.mark.inputs("Path to check (e.g., '/test_group').")
    @pytest.mark.expected(
        "True if path corresponds to an existing group, False otherwise."
    )
    @pytest.mark.steps(
        "1. Create a group in the HDF5 file. 2. Check if the path is a group using `HDF5Manager.is_group`."
    )
    @pytest.mark.parametrize(
        "path,expected",
        [
            ("/test_group", True),
            ("/non_existent", False),
        ],
    )
    def test_is_group(self, hdf5_manager, temp_hdf5_file, path, expected):
        """Test the is_group method for different paths."""
        with h5py.File(temp_hdf5_file, "a") as f:
            f.create_group("/test_group")
        with h5py.File(temp_hdf5_file, "r") as f:
            assert HDF5Manager.is_group(f, path) == expected

    @pytest.mark.id("TU_IO_004")
    @pytest.mark.title(
        "Test create_link_to_data Method with Required/Optional Flags"
    )
    @pytest.mark.class_description(
        "Test the `create_link_to_data` method to check its behavior with required and optional flags."
    )
    @pytest.mark.description(
        "This test verifies the functionality of the `create_link_to_data` method when the link is required or optional. It ensures proper exception handling when necessary."
    )
    @pytest.mark.inputs("is_required flag, should_raise flag.")
    @pytest.mark.expected(
        "Raises `StoringInformationError` if required, otherwise returns None."
    )
    @pytest.mark.steps(
        "1. Test when link is required and missing. 2. Test when link is optional and missing."
    )
    @pytest.mark.parametrize(
        "is_required,should_raise",
        [
            (True, True),
            (False, False),
        ],
    )
    def test_create_link_to_data(
        self, hdf5_manager, is_required, should_raise
    ):
        """Test creating links to data with required/optional flags."""
        if should_raise:
            with pytest.raises(StoringInformationError):
                hdf5_manager.create_link_to_data(
                    "/link", "/missing_group", is_required
                )
        else:
            result = hdf5_manager.create_link_to_data(
                "/link", "/missing_group", is_required
            )
            assert result is None

    @pytest.mark.id("TU_IO_005")
    @pytest.mark.title("Test Recursive Save and Load Functionality")
    @pytest.mark.class_description(
        "Test the recursive save and load functionality for hierarchical data structures."
    )
    @pytest.mark.description(
        "This test checks if nested data can be saved and loaded recursively in HDF5 format."
    )
    @pytest.mark.inputs("Data, HDF5 file path, and hierarchical structure.")
    @pytest.mark.expected(
        "The data should be correctly saved and loaded with deep matching."
    )
    @pytest.mark.steps(
        "1. Save hierarchical data structure recursively. 2. Load data and compare."
    )
    def test_recursive_save_and_load(self, hdf5_manager, temp_hdf5_file):
        """Test recursively saving and loading data."""
        data = {
            "group_1": {
                "dataset_1": np.array([1, 2, 3]),
                "subgroup": {"dataset_2": np.array([4, 5, 6])},
            }
        }
        with h5py.File(temp_hdf5_file, "a") as f:
            group = f.create_group("save_group")
            HDF5Manager.recursive_save(group, data)
        with h5py.File(temp_hdf5_file, "r") as f:
            loaded_data = HDF5Manager.recursive_load(f["save_group"])
            assert DeepDiff(loaded_data, data)

    @pytest.mark.id("TU_IO_006")
    @pytest.mark.title("Test is_data_stored_in Method")
    @pytest.mark.class_description(
        "Test the `is_data_stored_in` method to check if data is stored in a specified group."
    )
    @pytest.mark.description(
        "This test verifies if the `is_data_stored_in` method correctly identifies whether data is stored in a given group."
    )
    @pytest.mark.inputs("Group name and HDF5 file.")
    @pytest.mark.expected("Returns True if the group exists, False otherwise.")
    @pytest.mark.steps(
        "1. Test if an existing group returns True. 2. Test if a non-existent group returns False."
    )
    @pytest.mark.parametrize(
        "group_name,expected",
        [
            ("test_group", True),
            ("non_existent_group", False),
        ],
    )
    def test_is_data_stored_in(
        self, hdf5_manager, temp_hdf5_file, group_name, expected
    ):
        """Test checking if data is stored in a group."""
        with h5py.File(temp_hdf5_file, "a") as f:
            f.create_group("test_group")
        assert hdf5_manager.is_data_stored_in(group_name) == expected

    @pytest.mark.id("TU_IO_007")
    @pytest.mark.title("Test is_data_stored_in_group Method")
    @pytest.mark.class_description(
        "Test the `is_data_stored_in_group` method to check if data is stored in a specific group."
    )
    @pytest.mark.description(
        "This test verifies if the `is_data_stored_in_group` method correctly identifies whether data is stored in a specified group within a hierarchy of groups."
    )
    @pytest.mark.inputs("Group name, group hierarchy, and HDF5 file.")
    @pytest.mark.expected(
        "Returns True if the group exists in the specified hierarchy, False otherwise."
    )
    @pytest.mark.steps(
        "1. Test if a group exists in the correct hierarchy."
        "2. Test for non-existent group and incorrect hierarchy."
    )
    @pytest.mark.parametrize(
        "group_name,expected",
        [
            ("test2/test_group", True),
            ("non_existent_group", False),
            ("test_group", False),
        ],
    )
    def test_is_data_stored_in_group(
        self, hdf5_manager, temp_hdf5_file, group_name, expected
    ):
        """Test checking if data is stored in a group."""
        with h5py.File(temp_hdf5_file, "a") as f:
            f.create_group("test1/test2/test_group")
        assert hdf5_manager.is_data_stored_in(group_name, "test1") == expected

    @pytest.mark.id("TU_IO_008")
    @pytest.mark.title("Test save_and_load_data Method")
    @pytest.mark.class_description(
        "Test the `save_and_load_data` method to ensure correct saving and loading of different data types in an HDF5 file."
    )
    @pytest.mark.description(
        "This test verifies that various types of data (JSON strings, regular strings, empty arrays, non-string objects, None, booleans, and JSON bytes) are saved and loaded correctly using the `Hdf5Manager`."
    )
    @pytest.mark.inputs(
        "Various data types, including JSON strings, regular strings, empty arrays, non-string objects, booleans, and JSON bytes."
    )
    @pytest.mark.expected(
        "Returns the correct loaded data that matches the original saved data, after being saved to and loaded from the HDF5 file."
    )
    @pytest.mark.steps(
        "1. Test saving different types of data (strings, booleans, JSON) to an HDF5 file. "
        "2. Test loading the saved data and comparing it to the original data to verify the save/load functionality."
    )
    def test_save_and_load_data(self, hdf5_manager, temp_hdf5_file):
        """Test saving and loading different types of data."""

        class MockDataClassSchema(BaseDataClass):
            value: Any = Field(default=None)  # Define the field explicitly

        data_cases = {
            "json_strings": MockDataClassSchema(
                value=[b'{"key1": "value1"}', b'{"key2": "value2"}']
            ),
            "regular_strings": MockDataClassSchema(
                value=["string1", "string2"]
            ),
            "empty_array": MockDataClassSchema(value=[]),
            "non_string_objects": MockDataClassSchema(
                value=[123.8, 45.67, 3.5]
            ),
            "NONE": MockDataClassSchema(value=None),
            "boolean": MockDataClassSchema(value=[True, True, False, True]),
            "boolean_numpy": MockDataClassSchema(
                value=np.array([True, False, True, False], dtype=bool)
            ),
            "json_bytes": MockDataClassSchema(
                value=np.bytes_(b'{"key1": "value1", "key2": 2}')
            ),
        }

        for case_name, data in data_cases.items():

            with patch(
                "py_sas_data_modeler.dm.Hdf5Tree.find_key_path",
                return_value="test_case_" + case_name,
            ):
                # Save data
                hdf5_manager.save_data("test_case_" + case_name, data)

                # Load data
                loaded_data = hdf5_manager.load_data("test_case_" + case_name)
                assert (
                    DeepDiff(
                        Utils.normalize_types(loaded_data),
                        Utils.normalize_types(data.to_dict()),
                    )
                    == {}
                )

    @pytest.mark.id("TU_IO_009")
    @pytest.mark.title("Test is_stored Method")
    @pytest.mark.class_description(
        "Test the `is_stored` method to check if a product is stored in the PRODUCTS or EXTERNALS group."
    )
    @pytest.mark.description(
        "This test verifies if the `is_stored` method correctly determines if a product is stored in the PRODUCTS or EXTERNALS group."
    )
    @pytest.mark.inputs(
        "Product name and mocked HDF5 storage system with side effects for `is_data_stored_in` method."
    )
    @pytest.mark.expected(
        "Returns `True` if the product is stored in either PRODUCTS or EXTERNALS, `False` otherwise."
    )
    @pytest.mark.steps(
        "1. Mock the HDF5 storage system to simulate `is_data_stored_in` behavior. "
        "2. Test the `is_stored` method for different combinations of `stored_in_products` and `stored_in_externals`. "
        "3. Verify that the result matches the expected output."
    )
    @pytest.mark.parametrize(
        "stored_in_products,stored_in_externals,expected",
        [
            (True, False, True),
            (False, True, True),
            (False, False, False),
        ],
    )
    def test_is_stored(
        self, stored_in_products, stored_in_externals, expected
    ):
        """Test whether a product is stored in the PRODUCTS or EXTERNALS group."""
        mock_sas_hdf5 = MagicMock()
        mock_sas_hdf5.is_data_stored_in.side_effect = [
            stored_in_products,
            stored_in_externals,
        ]

        storage = HDF5StorageSasWorkflow("test_file.h5")
        storage._HDF5StorageSasWorkflow__sas_hdf5 = mock_sas_hdf5

        result = storage.is_stored("test_product")
        assert result == expected

    @pytest.mark.id("TU_IO_010")
    @pytest.mark.title("Test is_stored Method")
    @pytest.mark.class_description(
        "Test the `is_stored` method to check if a product is stored in the PRODUCTS or EXTERNALS group."
    )
    @pytest.mark.description(
        "This test verifies if the `is_stored` method correctly determines if a product is stored in the PRODUCTS or EXTERNALS group."
    )
    @pytest.mark.inputs(
        "Product name and mocked HDF5 storage system with side effects for `is_data_stored_in` method."
    )
    @pytest.mark.expected(
        "Returns `True` if the product is stored in either PRODUCTS or EXTERNALS, `False` otherwise."
    )
    @pytest.mark.steps(
        "1. Mock the HDF5 storage system to simulate `is_data_stored_in` behavior. "
        "2. Test the `is_stored` method for different combinations of `stored_in_products` and `stored_in_externals`. "
        "3. Verify that the result matches the expected output."
    )
    def test_recursive_load_group_with_attrs(self):
        """Test loading a group with attributes."""
        mock_group = MagicMock(spec=h5py.Group)
        mock_subgroup = MagicMock(spec=h5py.Group)
        mock_subgroup.attrs.items.return_value = [("attr1", "value1")]

        mock_group.items.return_value = [("group1", mock_subgroup)]

        result = HDF5Manager.recursive_load(mock_group)
        assert result == {"group1": {"attr1": "value1"}}

    @pytest.mark.id("TU_IO_011")
    @pytest.mark.title("Test recursive_load Raises Error")
    @pytest.mark.class_description(
        "Test the `recursive_load` method to ensure it raises an error when an unknown item is encountered."
    )
    @pytest.mark.description(
        "This test verifies that the `recursive_load` method raises a `NoExistingDatasetError` when trying to load an unknown item."
    )
    @pytest.mark.inputs(
        "Mocked HDF5 group containing an unknown item to simulate the error case."
    )
    @pytest.mark.expected(
        "Raises a `NoExistingDatasetError` when an unknown item is encountered in the group."
    )
    @pytest.mark.steps(
        "1. Mock an HDF5 group with an unknown item. "
        "2. Test the `recursive_load` method to check that it raises the correct error when an unknown item is encountered."
    )
    def test_recursive_load_raises_error(self):
        """Test that an error is raised when an unknown item is loaded."""
        mock_group = MagicMock(spec=h5py.Group)
        unknown_item = MagicMock()

        mock_group.items.return_value = [("unknown", unknown_item)]

        with pytest.raises(NoExistingDatasetError):
            HDF5Manager.recursive_load(mock_group)


@spec_coverage(
    "PLATO-IAS-PDC-ICD-0001",
    "test_input_output_locations",
    "test_file_naming_convention",
    "test_is_local_valid_dm",
)
@pytest.mark.class_description(
    "Test HDF5StorageSasTask class for various functionalities."
)
class TestHDF5StorageSasTask:
    @pytest.fixture
    def setup(self):
        dir_path = "test_directory"
        module_name = "MSAP1_02"
        os.makedirs(dir_path, exist_ok=True)

        # Initialize the HDF5StorageSasTask
        self.storage_task = HDF5StorageSasTask(dir_path, module_name)

        yield self.storage_task

        # Teardown
        for file in ["input/inputs.h5", "output/outputs.h5"]:
            try:
                filename = os.path.join(dir_path, file)
                if os.path.exists(filename):
                    os.remove(filename)
                directory_name = os.path.dirname(filename)
                if os.path.exists(directory_name):
                    os.rmdir(directory_name)
            except FileNotFoundError:
                pass
        if os.path.exists(dir_path):
            os.rmdir(dir_path)

    @pytest.mark.id("TU_IO_012")
    @pytest.mark.title("Test module_name Attribute")
    @pytest.mark.class_description(
        "Test the `module_name` attribute to verify its correct value."
    )
    @pytest.mark.description(
        "This test ensures that the `module_name` attribute is correctly set to 'MSAP1_02'."
    )
    @pytest.mark.inputs("Module setup with a `module_name` attribute.")
    @pytest.mark.expected("The `module_name` attribute should be 'MSAP1_02'.")
    @pytest.mark.steps(
        "1. Set up the module with the `module_name` attribute. "
        "2. Verify that the `module_name` is equal to 'MSAP1_02'."
    )
    def test_module_name(self, setup):
        assert setup.module_name == "MSAP1_02"

    @pytest.mark.id("TU_IO_013")
    @pytest.mark.title("Test dir_path Attribute")
    @pytest.mark.class_description(
        "Test the `dir_path` attribute to verify its correct value."
    )
    @pytest.mark.description(
        "This test ensures that the `dir_path` attribute is correctly set to 'test_directory'."
    )
    @pytest.mark.inputs("Module setup with a `dir_path` attribute.")
    @pytest.mark.expected(
        "The `dir_path` attribute should be 'test_directory'."
    )
    @pytest.mark.steps(
        "1. Set up the module with the `dir_path` attribute. "
        "2. Verify that the `dir_path` is equal to 'test_directory'."
    )
    def test_dir_path(self, setup):
        assert setup.dir_path == "test_directory"

    @pytest.mark.id("TU_IO_014")
    @pytest.mark.title("Test Input and Output Locations")
    @pytest.mark.class_description(
        "Test the file paths for input and output files to ensure they contain the correct directory structure."
    )
    @pytest.mark.description(
        "This test verifies that the input file path contains 'input/inputs.h5' and the output file path contains 'output/outputs.h5'."
    )
    @pytest.mark.inputs("Module setup with input and output file paths.")
    @pytest.mark.expected(
        "The input file path should include 'input/inputs.h5' and the output file path should include 'output/outputs.h5'."
    )
    @pytest.mark.steps(
        "1. Check if 'input/inputs.h5' is present in the input file path. "
        "2. Check if 'output/outputs.h5' is present in the output file path. "
        "3. Verify that the paths are correctly set for both input and output files."
    )
    def test_file_naming_convention(self, setup):
        assert os.path.basename(setup.inputs.filename) == "inputs.h5"
        assert os.path.basename(setup.outputs.filename) == "outputs.h5"

    @pytest.mark.id("TU_IO_015")
    @pytest.mark.title("Test Input and Output Locations")
    @pytest.mark.class_description(
        "Test the file paths for input and output files to ensure they contain the correct directory structure."
    )
    @pytest.mark.description(
        "This test verifies that the input file path contains 'input/inputs.h5' and the output file path contains 'output/outputs.h5'."
    )
    @pytest.mark.inputs("Module setup with input and output file paths.")
    @pytest.mark.expected(
        "The input file path should include 'input/inputs.h5' and the output file path should include 'output/outputs.h5'."
    )
    @pytest.mark.steps(
        "1. Check if 'input/inputs.h5' is part of the input file path. "
        "2. Check if 'output/outputs.h5' is part of the output file path. "
        "3. Ensure both input and output file paths are correctly structured."
    )
    def test_input_output_locations(self, setup):
        assert "input/inputs.h5" in setup.inputs.filename
        assert "output/outputs.h5" in setup.outputs.filename

    @pytest.mark.id("TU_IO_016")
    @pytest.mark.title("Test Load Inputs")
    @pytest.mark.class_description(
        "Test the `load_inputs` method to verify that it loads the correct data from the mock `load_data` method."
    )
    @pytest.mark.description(
        "This test ensures that the `load_inputs` method correctly loads input data and matches the expected object."
    )
    @pytest.mark.inputs(
        "Mocked `load_data` method and fake input data generated using `FakeData`."
    )
    @pytest.mark.expected(
        "The `load_inputs` method should return the expected object that matches the generated fake data."
    )
    @pytest.mark.steps(
        "1. Mock the `load_data` method to return fake data. "
        "2. Generate fake input data using `FakeData` and the `DataClassFactory`. "
        "3. Call the `load_inputs` method and verify that the result matches the expected object."
    )
    @patch("py_sas_data_modeler.io.hdf5.HDF5Manager.load_data")
    def test_load_inputs(self, mock_load_data, setup):
        fake_data = FakeData(use_numpy=False, has_optional=False)
        obj = fake_data.generate_fake_data(
            DataClassFactory.get_model("INPUTS_MSAP1_02")[0]
        )
        dico = fake_data.fake_data
        mock_load_data.return_value = dico
        result = setup.load_inputs()
        assert result == obj

    @pytest.mark.id("TU_IO_017")
    @pytest.mark.title("Test Load Outputs")
    @pytest.mark.class_description(
        "Test the `load_outputs` method to verify that it loads the correct output data from the mock `load_data` method."
    )
    @pytest.mark.description(
        "This test ensures that the `load_outputs` method correctly loads output data and matches the expected object."
    )
    @pytest.mark.inputs(
        "Mocked `load_data` method and fake output data generated using `DataClassFactory`."
    )
    @pytest.mark.expected(
        "The `load_outputs` method should return the expected object that matches the generated output data."
    )
    @pytest.mark.steps(
        "1. Mock the `load_data` method to return fake output data. "
        "2. Generate fake output data using `DataClassFactory` and the appropriate models. "
        "3. Call the `load_outputs` method and verify that the result matches the expected object."
    )
    @patch("py_sas_data_modeler.io.hdf5.HDF5Manager.load_data")
    @pytest.mark.description("Test loading of output data from HDF5.")
    def test_load_outputs(self, mock_load_data, setup):
        obj = DataClassFactory.get_model("OUTPUTS_MSAP1_02")(
            IDP_SAS_FLARES_FLAG=DataClassFactory.get_model(
                "IDP_SAS_FLARES_FLAG"
            )(merged_flag=[1], time=[24000.0]),
            ADP_SAS_FLARES_FLAG=DataClassFactory.get_model(
                "ADP_SAS_FLARES_FLAG"
            )(flare_flag=[1.0], time=[24000.0]),
        )
        mock_load_data.return_value = obj.to_dict()
        result = setup.load_outputs()
        assert result == obj

    @pytest.mark.id("TU_IO_018")
    @pytest.mark.title("Test Save Inputs")
    @pytest.mark.class_description(
        "Test the `save_inputs` method to verify that it correctly saves input data to HDF5."
    )
    @pytest.mark.description(
        "This test ensures that the `save_inputs` method correctly interacts with the mocked `save_data` method to save input data."
    )
    @pytest.mark.inputs(
        "Mocked `save_data` method and mock product to be saved."
    )
    @pytest.mark.expected(
        "The `save_inputs` method should invoke the `save_data` method once with the correct input data."
    )
    @pytest.mark.steps(
        "1. Mock the `save_data` method to simulate saving. "
        "2. Call the `save_inputs` method with a mock product. "
        "3. Verify that the `save_data` method was called once."
    )
    @patch("py_sas_data_modeler.io.hdf5.HDF5Manager.save_data")
    @pytest.mark.description("Test saving of input data to HDF5.")
    def test_save_inputs(self, mock_save_data, setup):
        mock_product = MagicMock()
        setup.save_inputs(mock_product)
        mock_save_data.assert_called_once()

    @pytest.mark.id("TU_IO_019")
    @pytest.mark.title("Test Save Outputs")
    @pytest.mark.class_description(
        "Test the `save_outputs` method to verify that it correctly saves output data to HDF5."
    )
    @pytest.mark.description(
        "This test ensures that the `save_outputs` method correctly interacts with the mocked `save_data` method to save output data."
    )
    @pytest.mark.inputs(
        "Mocked `save_data` method, mock product, and generated output data using `DataClassFactory`."
    )
    @pytest.mark.expected(
        "The `save_outputs` method should invoke the `save_data` method once with the correct output data."
    )
    @pytest.mark.steps(
        "1. Mock the `save_data` method to simulate saving. "
        "2. Generate mock output data using `DataClassFactory`. "
        "3. Call the `save_outputs` method with the mock product. "
        "4. Verify that the `save_data` method was called once."
    )
    @patch("py_sas_data_modeler.io.hdf5.HDF5Manager.save_data")
    def test_save_outputs(self, mock_save_data, setup):
        class MockDataClass(BaseDataClass):
            value: Any

        obj = DataClassFactory.get_model("OUTPUTS_MSAP1_02")(
            IDP_SAS_FLARES_FLAG=DataClassFactory.get_model(
                "IDP_SAS_FLARES_FLAG"
            )(merged_flag=[1], time=[24000.0]),
            ADP_SAS_FLARES_FLAG=DataClassFactory.get_model(
                "ADP_SAS_FLARES_FLAG"
            )(flare_flag=[1.0], time=[24000.0]),
        )
        mock_product = MagicMock(spec=MockDataClass)
        mock_product.to_dict.return_value = obj.to_dict()
        setup.save_outputs(mock_product)
        mock_save_data.assert_called_once()

    @pytest.mark.id("TU_IO_020")
    @pytest.mark.title("Test Is Stored")
    @pytest.mark.class_description(
        "Test the `is_stored` method to check if a product is stored in HDF5."
    )
    @pytest.mark.description(
        "This test verifies if the `is_stored` method correctly checks whether a product is stored in HDF5."
    )
    @pytest.mark.inputs(
        "Mocked `is_stored` method and product name to be checked."
    )
    @pytest.mark.expected(
        "The `is_stored` method should return `True` if the product is stored, otherwise `False`."
    )
    @pytest.mark.steps(
        "1. Mock the `is_stored` method to simulate the product being stored. "
        "2. Call the `is_stored` method with a sample product name. "
        "3. Verify that the method returns the correct result (True in this case)."
    )
    @patch("py_sas_data_modeler.io.hdf5.HDF5StorageSasTask.is_stored")
    def test_is_stored(self, mock_is_stored, setup):
        mock_is_stored.return_value = True
        assert setup.is_stored("product_name") is True

    @pytest.mark.id("TU_IO_021")
    @pytest.mark.title("Test Validation of Data Model for HDF5")
    @pytest.mark.class_description(
        "Test the `is_validate_dm` method to verify the validation of the data model for HDF5 inputs."
    )
    @pytest.mark.description(
        "This test ensures that the `is_validate_dm` method correctly validates the data model of inputs loaded from HDF5."
    )
    @pytest.mark.inputs(
        "Mocked `load_inputs` method to simulate loading inputs and validation of the data model."
    )
    @pytest.mark.expected(
        "The `is_validate_dm` method should return `True` if the data model is valid."
    )
    @pytest.mark.steps(
        "1. Mock the `load_inputs` method to simulate loading data. "
        "2. Call the `is_validate_dm` method to validate the data model. "
        "3. Verify that the method returns `True` to indicate the model is valid."
    )
    @patch("py_sas_data_modeler.io.hdf5.HDF5StorageSasTask.load_inputs")
    def test_is_local_valid_dm(self, mock_load_inputs, setup):
        mock_load_inputs.return_value = MagicMock()
        assert setup.is_validate_dm() is True

    @pytest.mark.id("TU_IO_022")
    @pytest.mark.title("Test Failure to Validate Data Model for HDF5")
    @pytest.mark.class_description(
        "Test the failure scenario of the `is_validate_dm` method when validation of the data model for HDF5 fails."
    )
    @pytest.mark.description(
        "This test ensures that the `is_validate_dm` method correctly handles the failure case where loading the inputs or validating the data model fails."
    )
    @pytest.mark.inputs(
        "Mocked `load_inputs` method with a side effect to simulate loading failure."
    )
    @pytest.mark.expected(
        "The `is_validate_dm` method should return `False` when the validation fails due to loading failure."
    )
    @pytest.mark.steps(
        "1. Mock the `load_inputs` method to simulate a loading failure using a side effect. "
        "2. Call the `is_validate_dm` method to validate the data model. "
        "3. Verify that the method returns `False` due to the failure."
    )
    @patch("py_sas_data_modeler.io.hdf5.HDF5StorageSasTask.load_inputs")
    def test_is_local_valid_dm_failure(self, mock_load_inputs, setup):
        mock_load_inputs.side_effect = Exception("Load failed")
        assert setup.is_validate_dm() is False


@spec_coverage(
    "PLATO-IAS-PDC-ICD-0001",
    "test_is_global_valid_dm",
    "test_external_product_storage_path",
    "test_internal_product_storage_path",
)
@pytest.mark.class_description(
    "Test HDF5StorageSasWorkflow class for various functionalities."
)
class TestHDF5StorageSasWorkflow:

    @pytest.fixture
    def setup(self, tmpdir):
        filename = tmpdir.join("test_file.h5")
        hdf5_storage = HDF5StorageSasWorkflow(str(filename))
        return hdf5_storage

    @pytest.fixture
    def mock_hdf5_manager(self):
        return Mock(spec=HDF5Manager)

    @pytest.fixture
    def storage(self, mock_hdf5_manager):
        with patch(
            "py_sas_data_modeler.io.hdf5.HDF5Manager",
            return_value=mock_hdf5_manager,
        ):
            return HDF5StorageSasWorkflow("test_file.h5")

    @pytest.mark.id("TU_IO_024")
    @pytest.mark.title("Test Filename Property for MSAP1 Pipeline")
    @pytest.mark.class_description(
        "Test the `filename` property for MSAP1 pipeline to ensure the correct filename is retrieved from the HDF5Manager."
    )
    @pytest.mark.description(
        "This test ensures that the `filename` property correctly retrieves the filename from the `HDF5Manager` when using MSAP1 pipeline tasks."
    )
    @pytest.mark.inputs("Mocked HDF5Manager with a specified filename.")
    @pytest.mark.expected(
        "The `filename` property should return the correct filename, as set in the mocked `HDF5Manager`."
    )
    @pytest.mark.steps(
        "1. Mock the `HDF5Manager` to set a filename. "
        "2. Test the `filename` property of the storage. "
        "3. Verify that the `filename` property returns the correct filename from the mocked manager."
    )
    def test_filename_property(self, storage, mock_hdf5_manager):
        """
        Test MSAP1 pipeline:
        - Vérifie les étapes de traitement pour les tâches MSAP1_02, MSAP1_01, MSAP1_07, MSAP1_08.
        - Teste l'intégrité des données en entrée et sortie.
        """
        # Test that filename property returns the correct filename from HDF5Manager
        mock_hdf5_manager.filename = "test_file.h5"
        assert storage.filename == "test_file.h5"

    @pytest.mark.id("TU_IO_025")
    @pytest.mark.title("Test Module Name Setter and Getter")
    @pytest.mark.class_description(
        "Test the setter and getter for `module_name` in the storage system."
    )
    @pytest.mark.description(
        "This test ensures that the setter and getter for the `module_name` attribute correctly set and retrieve the module name."
    )
    @pytest.mark.inputs("Module name to be set in the storage system.")
    @pytest.mark.expected(
        "The `module_name` setter should correctly set the value and the getter should return the same value."
    )
    @pytest.mark.steps(
        "1. Set the `module_name` attribute of the storage system. "
        "2. Retrieve the `module_name` using the getter. "
        "3. Verify that the getter returns the correct value set by the setter."
    )
    def test_module_name_setter_and_getter(self, storage):
        # Test setting and getting module_name
        storage.module_name = "test_module"
        assert storage.module_name == "test_module"

    @pytest.mark.id("TU_IO_026")
    @pytest.mark.title("Test Is Stored Method - False Case")
    @pytest.mark.class_description(
        "Test the `is_stored` method to check if a product is not stored in the HDF5 storage."
    )
    @pytest.mark.description(
        "This test verifies that the `is_stored` method returns `False` when the product is not stored in the HDF5 storage system."
    )
    @pytest.mark.inputs(
        "Product name and mocked HDF5 storage system with `is_data_stored_in` returning `False`."
    )
    @pytest.mark.expected(
        "Returns `False` if the product is not stored in the HDF5 storage system."
    )
    @pytest.mark.steps(
        "1. Mock the HDF5 storage system to simulate `is_data_stored_in` returning `False`. "
        "2. Test the `is_stored` method for a product that is not stored. "
        "3. Verify that the result is `False`."
    )
    def test_is_stored_false(self, storage, mock_hdf5_manager):
        # Test when data is not stored
        mock_hdf5_manager.is_data_stored_in.return_value = False
        assert not storage.is_stored("some_product")

    @pytest.mark.id("TU_IO_027")
    @pytest.mark.title("Test Create Link Method")
    @pytest.mark.class_description(
        "Test the `create_link` method to ensure it creates a link between the source and destination paths."
    )
    @pytest.mark.description(
        "This test verifies that the `create_link` method correctly calls the underlying HDF5 manager to create a link between the source and destination paths."
    )
    @pytest.mark.inputs(
        "Source path, destination path, and boolean flag indicating if the link should be created."
    )
    @pytest.mark.expected(
        "The HDF5 manager's `create_link_to_data` method is called once with the correct parameters."
    )
    @pytest.mark.steps(
        "1. Call the `create_link` method with source path, destination path, and a boolean flag. "
        "2. Verify that the HDF5 manager's `create_link_to_data` method is called once with the correct arguments."
    )
    def test_create_link(self, storage, mock_hdf5_manager):
        # Test creating a link
        storage.create_link("src_path", "dest_path", True)
        mock_hdf5_manager.create_link_to_data.assert_called_once_with(
            "src_path", "dest_path", True
        )

    @pytest.mark.id("TU_IO_028")
    @pytest.mark.title("Test Save Product as Object Internal Method")
    @pytest.mark.class_description(
        "Test the `_save_product_as_obj` method for saving internal products without a `module_name`."
    )
    @pytest.mark.description(
        "This test verifies that the `_save_product_as_obj` method correctly saves an internal product when no `module_name` is provided, by calling the HDF5 manager with the correct path."
    )
    @pytest.mark.inputs(
        "Data to be saved as an internal product, with mock patches for `get_key_from`, `is_internal_product`, and `is_external_product` methods."
    )
    @pytest.mark.expected(
        "The HDF5 manager's `save_data` method is called once with the correct path and data for internal products."
    )
    @pytest.mark.steps(
        "1. Mock the methods `get_key_from`, `is_internal_product`, and `is_external_product` to simulate the behavior for internal products. "
        "2. Call the `_save_product_as_obj` method with the mock data. "
        "3. Verify that the HDF5 manager's `save_data` method is called with the correct path for internal products."
    )
    def test_save_product_as_obj_internal(self, storage, mock_hdf5_manager):
        # Test saving an internal product without a module_name
        data = Mock(spec=BaseDataClass)

        with (
            patch(
                "py_sas_data_modeler.dm.Hdf5Tree.get_key_from",
                return_value="internal_product",
            ),
            patch(
                "py_sas_data_modeler.helper.Utils.is_internal_product",
                return_value=True,
            ),
            patch(
                "py_sas_data_modeler.helper.Utils.is_external_product",
                return_value=False,
            ),
        ):
            storage._save_product_as_obj(data)
            mock_hdf5_manager.save_data.assert_called_once_with(
                f"{Hdf5NodeEnum.PRODUCTS.value}/INTERNAL_PRODUCT", data
            )

    @pytest.mark.id("TU_IO_029")
    @pytest.mark.title("Test Save Product as Object External Method")
    @pytest.mark.class_description(
        "Test the `_save_external_as_obj` method for saving external products without a `module_name`."
    )
    @pytest.mark.description(
        "This test verifies that the `_save_external_as_obj` method correctly saves an external product when no `module_name` is provided, by calling the HDF5 manager with the correct path."
    )
    @pytest.mark.inputs(
        "Data to be saved as an external product, with mock patches for `get_key_from`, `is_internal_product`, and `is_external_product` methods."
    )
    @pytest.mark.expected(
        "The HDF5 manager's `save_data` method is called once with the correct path and data for external products."
    )
    @pytest.mark.steps(
        "1. Mock the methods `get_key_from`, `is_internal_product`, and `is_external_product` to simulate the behavior for external products. "
        "2. Call the `_save_external_as_obj` method with the mock data. "
        "3. Verify that the HDF5 manager's `save_data` method is called with the correct path for external products."
    )
    def test_save_product_as_obj_external(self, storage, mock_hdf5_manager):
        # Test saving an external product without a module_name
        data = Mock(spec=BaseDataClass)
        with (
            patch(
                "py_sas_data_modeler.dm.Hdf5Tree.get_key_from",
                return_value="external_product",
            ),
            patch(
                "py_sas_data_modeler.helper.Utils.is_internal_product",
                return_value=False,
            ),
            patch(
                "py_sas_data_modeler.helper.Utils.is_external_product",
                return_value=True,
            ),
        ):
            storage._save_external_as_obj(data)
            mock_hdf5_manager.save_data.assert_called_once_with(
                f"{Hdf5NodeEnum.EXTERNALS.value}/EXTERNAL_PRODUCT", data
            )

    @pytest.mark.id("TU_IO_029")
    @pytest.mark.title("Test Save Native Product Internal Method")
    @pytest.mark.class_description(
        "Test the `_save_product_natif` method for saving a native internal product."
    )
    @pytest.mark.description(
        "This test verifies that the `_save_product_natif` method correctly saves a native internal product by calling the HDF5 manager with the appropriate path."
    )
    @pytest.mark.inputs(
        "Key representing the internal product and mock data, with a patch for `is_internal_product` to simulate an internal product."
    )
    @pytest.mark.expected(
        "The HDF5 manager's `save_data` method is called once with the correct path for internal products."
    )
    @pytest.mark.steps(
        "1. Mock the `is_internal_product` method to simulate an internal product. "
        "2. Call the `_save_product_natif` method with the given key and mock data. "
        "3. Verify that the HDF5 manager's `save_data` method is called once with the correct path for internal products."
    )
    def test_save_product_natif_internal(self, storage, mock_hdf5_manager):
        # Test saving native internal product
        key = "internal_product"
        data = Mock(spec=BaseDataClass)
        with patch(
            "py_sas_data_modeler.helper.Utils.is_internal_product",
            return_value=True,
        ):
            storage._save_product_natif(key, data)
            mock_hdf5_manager.save_data.assert_called_once_with(
                Hdf5NodeEnum.PRODUCTS.value, data
            )

    @pytest.mark.id("TU_IO_030")
    @pytest.mark.title("Test Save Native Product External Method")
    @pytest.mark.class_description(
        "Test the `_save_external_natif` method for saving a native external product."
    )
    @pytest.mark.description(
        "This test verifies that the `_save_external_natif` method correctly saves a native external product by calling the HDF5 manager with the appropriate path and key."
    )
    @pytest.mark.inputs(
        "Key representing the external product and mock data, with patches for `is_internal_product` and `is_external_product` to simulate an external product."
    )
    @pytest.mark.expected(
        "The HDF5 manager's `save_data` method is called once with the correct path and key for external products."
    )
    @pytest.mark.steps(
        "1. Mock the `is_internal_product` method to simulate a non-internal product. "
        "2. Mock the `is_external_product` method to simulate an external product. "
        "3. Call the `_save_external_natif` method with the given key and mock data. "
        "4. Verify that the HDF5 manager's `save_data` method is called once with the correct path for external products and the expected key."
    )
    def test_save_product_natif_external(self, storage, mock_hdf5_manager):
        # Test saving native external product
        key = "external_product"
        data = Mock(spec=BaseDataClass)
        with patch(
            "py_sas_data_modeler.helper.Utils.is_internal_product",
            return_value=False,
        ):
            with patch(
                "py_sas_data_modeler.helper.Utils.is_external_product",
                return_value=True,
            ):
                storage._save_external_natif(key, data)
                mock_hdf5_manager.save_data.assert_called_once_with(
                    Hdf5NodeEnum.EXTERNALS.value, {key: data}
                )

    @pytest.mark.id("TU_IO_031")
    @pytest.mark.title("Test Save Product Method")
    @pytest.mark.class_description(
        "Test the `save_product` method to ensure that a product is correctly saved using the provided data."
    )
    @pytest.mark.description(
        "This test verifies that the `save_product` method correctly interacts with the HDF5 manager's `save_data` method when saving a product."
    )
    @pytest.mark.inputs(
        "Data dictionary containing the product information and mock data. Patches are used to simulate `is_custom_object` behavior."
    )
    @pytest.mark.expected(
        "The HDF5 manager's `save_data` method is called once, indicating the product was saved."
    )
    @pytest.mark.steps(
        "1. Mock the `is_custom_object` method to simulate a non-custom object. "
        "2. Call the `save_product` method with the given data dictionary. "
        "3. Verify that the HDF5 manager's `save_data` method is called once to save the product."
    )
    def test_save_product(self, storage, mock_hdf5_manager):
        # Test saving a product using the save_product method
        data = {"IDP_SAS": Mock(spec=BaseDataClass)}
        with patch(
            "py_sas_data_modeler.helper.Utils.is_custom_object",
            return_value=False,
        ):
            storage.save_product(data)
            mock_hdf5_manager.save_data.assert_called_once()

    @pytest.mark.id("TU_IO_032")
    @pytest.mark.title("Test Save Product Invalid Data")
    @pytest.mark.class_description(
        "Test that an error is raised when invalid product data is provided to the `save_product` method."
    )
    @pytest.mark.description(
        "This test verifies that the `save_product` method raises a `ProductSaveError` when invalid data is passed."
    )
    @pytest.mark.inputs("Invalid product data (string in this case).")
    @pytest.mark.expected(
        "A `ProductSaveError` is raised with a specific error message indicating the invalid data."
    )
    @pytest.mark.steps(
        "1. Pass invalid product data (non-dict, incorrect type) to the `save_product` method. "
        "2. Verify that the `ProductSaveError` is raised. "
        "3. Check the error message to ensure it matches the expected format."
    )
    def test_save_product_invalid_data(self, storage):
        # Test that InvalidProductError is raised for invalid product data
        invalid_data = "Invalid data type"  # This should not be a valid input
        with pytest.raises(ProductSaveError) as exc_info:
            storage.save_product(invalid_data)

        assert (
            str(exc_info.value)
            == f"An error occurred while saving the product {invalid_data}"
        )

    @pytest.mark.id("TU_IO_033")
    @pytest.mark.title("Test Load Inputs")
    @pytest.mark.class_description(
        "Test the loading of input data using the `load_inputs` method in the `storage` class."
    )
    @pytest.mark.description(
        "This test ensures that the `load_inputs` method correctly loads input data and returns a valid result."
    )
    @pytest.mark.inputs(
        "Module name and mocked HDF5 storage system to simulate loading input data."
    )
    @pytest.mark.expected(
        "The method should return a valid object when the input data is successfully loaded from the HDF5 system."
    )
    @pytest.mark.steps(
        "1. Register a mock schema for inputs using `DataClassFactory`. "
        "2. Mock the HDF5 manager's `load_data` to return a predefined dictionary of input data. "
        "3. Use `patch` to mock `find_key_path` to simulate finding the correct path. "
        "4. Verify that the result of `load_inputs` is not `None`."
    )
    def test_load_inputs(self, storage, mock_hdf5_manager):
        class InputsModuleSchema(MockFieldClass):
            pass

        DataClassFactory.register_dataclass(
            "INPUTS_MODULE", InputsModuleSchema
        )
        # Test loading inputs
        mock_hdf5_manager.load_data.return_value = {
            "field_3": 4,
            "field_4": "hello",
        }
        with patch(
            "py_sas_data_modeler.dm.Hdf5Tree.find_key_path",
            return_value="",
        ):
            result = storage.load_inputs("MODULE")
        assert result is not None

    @pytest.mark.id("TU_IO_034")
    @pytest.mark.title("Test Load Outputs")
    @pytest.mark.class_description(
        "Test the loading of output data using the `load_outputs` method in the `storage` class."
    )
    @pytest.mark.description(
        "This test ensures that the `load_outputs` method correctly loads output data and returns a valid result."
    )
    @pytest.mark.expected(
        "The method should return a valid object when the output data is successfully loaded from the HDF5 system."
    )
    @pytest.mark.steps(
        "1. Register a mock schema for outputs using `DataClassFactory`. "
        "2. Mock the HDF5 manager's `load_data` to return a predefined dictionary of output data. "
        "3. Use `patch` to mock `find_key_path` to simulate finding the correct path. "
        "4. Verify that the result of `load_outputs` is not `None`."
    )
    def test_load_outputs(self, storage, mock_hdf5_manager):
        # Test loading outputs
        class OutputsModuleSchema(MockFieldClass):
            pass

        DataClassFactory.register_dataclass(
            "OUTPUTS_MODULE", OutputsModuleSchema
        )
        mock_hdf5_manager.load_data.return_value = {
            "field_3": 4,
            "field_4": "hello",
        }
        with patch(
            "py_sas_data_modeler.dm.Hdf5Tree.find_key_path",
            return_value="",
        ):
            result = storage.load_outputs("MODULE")
        assert result is not None


@spec_coverage("PLATO-IAS-PDC-TN-0005", "test_datatransfer")
@pytest.mark.class_description(
    "Tests for the FileMediator class, ensuring proper data transfer between SAS and task storage."
)
class TestFileMediator:

    @pytest.fixture
    def sas_storage(self):
        """Fixture to mock IStorageSasWorkflow."""
        return MagicMock(spec=IStorageSasWorkflow)

    @pytest.fixture
    def task_storage(self):
        """Fixture to mock IStorageSasTask."""
        return MagicMock(spec=IStorageSasTask)

    @pytest.fixture
    def mediator(self, sas_storage, task_storage):
        """Fixture to create a FileMediator instance."""
        return FileMediator(sas_storage=sas_storage, task_storage=task_storage)

    @pytest.mark.id("TU_IO_035")
    @pytest.mark.title("Test Transfer Inputs to Task")
    @pytest.mark.class_description(
        "Test the `transfer_inputs_to_task` method to ensure correct data transfer from SAS storage to task storage."
    )
    @pytest.mark.description(
        "This test verifies that the `transfer_inputs_to_task` method correctly loads input data from SAS storage and transfers it to task storage."
    )
    @pytest.mark.inputs(
        "Mocked input data loaded from SAS storage for a specific module."
    )
    @pytest.mark.expected(
        "The method should load inputs correctly from SAS storage and transfer them to task storage using the appropriate methods."
    )
    @pytest.mark.steps(
        "1. Mock the `load_inputs` method of `sas_storage` to return predefined input data. "
        "2. Call the `transfer_inputs_to_task` method of the mediator. "
        "3. Verify that `load_inputs` was called with the correct module name. "
        "4. Ensure that `save_inputs` was called with the correct data in `task_storage`."
    )
    def test_transfer_inputs_to_task(
        self, mediator, sas_storage, task_storage
    ):
        """Test the transfer_inputs_to_task method."""
        module_name = "module_1"
        data = {"input_1": 42}

        # Mock the load_inputs method to return test data
        sas_storage.load_inputs.return_value = data

        # Run the transfer method
        mediator.transfer_inputs_to_task(module_name)

        # Assert that load_inputs was called with correct module name
        sas_storage.load_inputs.assert_called_once_with(module_name)

        # Assert that save_inputs was called with the loaded data
        task_storage.save_inputs.assert_called_once_with(data)
