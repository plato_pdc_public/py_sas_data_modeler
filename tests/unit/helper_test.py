# -*- coding: utf-8 -*-
# PySasDataModeler - Data classes for SAS Data Model
# Copyright (C) 2024 - 2025 - Centre National d'Etudes Spatiales
# This file is part of PySasDataModeler <https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler>
# SPDX-License-Identifier: Apache-2.0
import numpy as np
import pytest
from deepdiff import DeepDiff

from py_sas_data_modeler.exception import IncompatibleDataTypeError
from py_sas_data_modeler.exception import InvalidDataTypeError
from py_sas_data_modeler.helper import Utils


@pytest.mark.class_description(
    "Test suite for utility functions in the Utils class."
)
class TestUtils:
    @pytest.mark.id("TU_HELPER_001")
    @pytest.mark.title("Test Convert to Numpy")
    @pytest.mark.class_description(
        "Tests conversion of various input types to numpy arrays."
    )
    @pytest.mark.description(
        "This test checks that various data types are correctly converted to numpy arrays, including nested lists, dictionaries, booleans, strings, and integers."
    )
    @pytest.mark.inputs(
        "A variety of input types including nested lists, floats, dictionaries, booleans, strings, and numpy arrays."
    )
    @pytest.mark.expected(
        "The input data should be correctly converted to a numpy array with the expected dtype."
    )
    @pytest.mark.steps(
        "1. Provide input values of different types.\n"
        "2. Convert the input using Utils.convert_to_numpy.\n"
        "3. Validate that the result matches the expected output numpy array."
    )
    @pytest.mark.parametrize(
        "input_value, expected_output",
        [
            # Test for nested lists of integers
            (
                [[1, 2, 3], [4, 5, 6]],
                np.array([[1, 2, 3], [4, 5, 6]], dtype=int),
            ),
            # Test for nested lists of floats
            (
                [[1.1, 2.2], [3.3, 4.4]],
                np.array([[1.1, 2.2], [3.3, 4.4]], dtype=np.float64),
            ),
            # Test for list of dictionaries
            (
                [{"a": 1}, {"b": 2}],
                np.array(['{"a": 1}', '{"b": 2}'], dtype=np.object_),
            ),
            # Test for list of booleans
            ([True, False, True], np.array([True, False, True], dtype=bool)),
            # Test for list of strings
            (["a", "b", "c"], np.array(["a", "b", "c"], dtype=np.object_)),
            # Test for numpy array of integers
            (np.array([1, 2, 3]), np.array([1, 2, 3])),
        ],
    )
    def test_convert_to_numpy(self, input_value, expected_output):
        if isinstance(expected_output, pytest.raises.Exception):
            with expected_output:
                Utils.convert_to_numpy(input_value)
        else:
            result = Utils.convert_to_numpy(input_value)
            assert np.array_equal(result, expected_output)

    @pytest.mark.id("TU_HELPER_002")
    @pytest.mark.title("Test Normalize Types")
    @pytest.mark.class_description(
        "Tests normalization of numpy types to Python types in a dictionary."
    )
    @pytest.mark.description(
        "This test validates the conversion of numpy types like integers, bytes, bool, and strings into their corresponding Python types."
    )
    @pytest.mark.inputs(
        "Dictionaries containing numpy arrays of integers, bytes, booleans, and strings."
    )
    @pytest.mark.expected(
        "The dictionary values should be converted to their appropriate Python types."
    )
    @pytest.mark.steps(
        "1. Provide input dictionaries with numpy array values.\n"
        "2. Normalize types using Utils.normalize_types.\n"
        "3. Validate that the resulting dictionary matches the expected output."
    )
    @pytest.mark.parametrize(
        "input_dict, expected_dict",
        [
            # Test conversion of numpy integers to Python integers
            ({"a": np.array([1, 2, 3])}, {"a": [1, 2, 3]}),
            # Test conversion of numpy bytes to string
            ({"b": np.array([b"abc", b"def"])}, {"b": ["abc", "def"]}),
            # Test nested dictionary with mixed types
            (
                {"a": {"b": np.array([1]), "c": b"hello"}},
                {"a": {"b": [1], "c": "hello"}},
            ),
            # Test conversion of numpy bool to Python bool
            ({"a": np.array([True, False, True])}, {"a": [True, False, True]}),
            # Test conversion of numpy string/bool to Python strin/bool
            (
                {"a": np.array(["True", "False", "True"])},
                {"a": ["True", "False", "True"]},
            ),
        ],
    )
    def test_normalize_types(self, input_dict, expected_dict):
        result = Utils.normalize_types(input_dict)
        assert DeepDiff(result, expected_dict) == {}

    @pytest.mark.id("TU_HELPER_003")
    @pytest.mark.title("Test Is External Product")
    @pytest.mark.class_description(
        "Tests whether a product name is classified as external."
    )
    @pytest.mark.description(
        "This test validates the classification of product names as external based on predefined rules."
    )
    @pytest.mark.inputs("Product names in string format.")
    @pytest.mark.expected(
        "Returns True for certain product names and False for others."
    )
    @pytest.mark.steps(
        "1. Provide product names.\n"
        "2. Call Utils.is_external_product.\n"
        "3. Verify the expected boolean output."
    )
    @pytest.mark.parametrize(
        "product_name, expected_output",
        [
            ("DP1_SAMPLE", True),
            ("IDP_PFU_TEST", True),
            ("LG_PFU_ANOTHER", True),
            ("IDP_EAS_SAMPLE", True),
            ("PDP_WP12_SAMPLE", True),
            ("IDP_SAS_SAMPLE", False),
        ],
    )
    def test_is_external_product(self, product_name, expected_output):
        assert Utils.is_external_product(product_name) == expected_output

    @pytest.mark.id("TU_HELPER_004")
    @pytest.mark.title("Test Is Internal Product")
    @pytest.mark.class_description(
        "Tests whether a product name is classified as internal."
    )
    @pytest.mark.description(
        "This test validates the classification of product names as internal based on predefined rules."
    )
    @pytest.mark.inputs("Product names in string format.")
    @pytest.mark.expected(
        "Returns True for certain product names and False for others."
    )
    @pytest.mark.steps(
        "1. Provide product names.\n"
        "2. Call Utils.is_internal_product.\n"
        "3. Verify the expected boolean output."
    )
    @pytest.mark.parametrize(
        "product_name, expected_output",
        [
            ("IDP_SAS_TEST", True),
            ("ADP_SAS_ANOTHER", True),
            ("DP3_SAS_EXAMPLE", True),
            ("DP4_SAS_SAMPLE", True),
            ("DP5_SAS_TEST", True),
            ("DP1_SAS_SAMPLE", False),
        ],
    )
    def test_is_internal_product(self, product_name, expected_output):
        assert Utils.is_internal_product(product_name) == expected_output

    @pytest.mark.id("TU_HELPER_005")
    @pytest.mark.title("Test Is Custom Object")
    @pytest.mark.class_description(
        "Tests whether a variable is classified as a custom object."
    )
    @pytest.mark.description(
        "This test validates the classification of variables as custom objects or not."
    )
    @pytest.mark.inputs(
        "Various types of variables (int, float, string, list, dict, object)."
    )
    @pytest.mark.expected(
        "Returns True for custom objects and False for other types."
    )
    @pytest.mark.steps(
        "1. Provide variables of different types.\n"
        "2. Call Utils.is_custom_object.\n"
        "3. Verify the expected boolean output."
    )
    @pytest.mark.parametrize(
        "variable, expected_output",
        [
            (1, False),
            (3.14, False),
            ("string", False),
            ([], False),
            ({}, False),
            (object(), True),
        ],
    )
    def test_is_custom_object(self, variable, expected_output):
        assert Utils.is_custom_object(variable) == expected_output

    @pytest.mark.id("TU_HELPER_006")
    @pytest.mark.title("Test Convert to Numpy Mixed Data Type")
    @pytest.mark.class_description(
        "Test that mixed data types in a list raise an exception during conversion."
    )
    @pytest.mark.description(
        "This test ensures that when a list with incompatible data types (e.g., int and string) is passed to `convert_to_numpy`, an error is raised."
    )
    @pytest.mark.inputs(
        "List with mixed data types (e.g., int, string, float)."
    )
    @pytest.mark.expected(
        "Raises an IncompatibleDataTypeError with a message specifying the mixed types."
    )
    @pytest.mark.steps(
        "1. Provide a list with mixed types.\n"
        "2. Call `Utils.convert_to_numpy`.\n"
        "3. Verify the exception is raised."
    )
    def test_convert_to_numpy_mixed_data_type(self):
        # This input has mixed types (int and string), which is not compatible.
        mixed_data = [1, "string", 3.0]

        with pytest.raises(IncompatibleDataTypeError) as exc_info:
            Utils.convert_to_numpy(mixed_data)

        # Assert that the error message contains detailed type information
        assert str(exc_info.value) == (
            f"The list contains incompatible mixed data types: {mixed_data} - "
            f"type: {type(mixed_data)} - types found: {[type(item) for item in mixed_data]}"
        )

    @pytest.mark.id("TU_HELPER_007")
    @pytest.mark.title("Test Convert to Numpy Byte Arrays")
    @pytest.mark.class_description(
        "Test that a list of NumPy byte arrays is correctly converted to a single concatenated byte array."
    )
    @pytest.mark.description(
        "This test ensures that a list of NumPy byte arrays is flattened and concatenated into a single NumPy array."
    )
    @pytest.mark.inputs("List of NumPy byte arrays.")
    @pytest.mark.expected("A single concatenated NumPy array of bytes.")
    @pytest.mark.steps(
        "1. Create a list of byte arrays.\n"
        "2. Call `Utils.convert_to_numpy`.\n"
        "3. Verify the result is a concatenated NumPy array."
    )
    def test_convert_to_numpy_byte_arrays(self):
        # Create a list of NumPy arrays with dtype bytes
        byte_array_1 = np.array([b"test1", b"data1"], dtype=np.bytes_)
        byte_array_2 = np.array([b"test2", b"data2"], dtype=np.bytes_)
        byte_array_list = [byte_array_1, byte_array_2]

        # Call the convert_to_numpy function
        result = Utils.convert_to_numpy(byte_array_list)

        # Expected result is the concatenated flattened array
        expected_result = np.concatenate(
            [byte_array_1.flatten(), byte_array_2.flatten()]
        )

        # Assertions
        assert isinstance(result, np.ndarray)
        # Check that the dtype starts with 'S', which represents string (bytes) in NumPy
        assert result.dtype.kind == "S"
        assert np.array_equal(result, expected_result)

    @pytest.mark.id("TU_HELPER_008")
    @pytest.mark.title("Test Convert None values to np.nan within array")
    @pytest.mark.class_description(
        "Test a None value in an array that is transformed to np.nan."
    )
    @pytest.mark.inputs("a numpy array with a None.")
    @pytest.mark.expected(
        "a numpy array where the None value is replaced by np.nan."
    )
    @pytest.mark.steps(
        "1. Create a list of float arrays with a None.\n"
        "2. Convert the list to a numpy array with a np.nan.\n"
        "3. Verify the lists are equals."
    )
    def test_convert_to_numpy_missing_data(self):
        array = [1.0, None, 3.0]
        result = Utils.convert_to_numpy(array, np.nan)
        expected_result = np.array([1.0, np.nan, 3.0], dtype=np.float64)
        assert np.allclose(result, expected_result, equal_nan=True)
