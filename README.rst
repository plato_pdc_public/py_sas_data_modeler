.. highlight:: shell

===============================
PySasDataModeler
===============================

.. image:: https://img.shields.io/badge/Maintained%3F-yes-green.svg
   :target: https://git.ias.u-psud.fr/plato_pdc/plato_wp37/skeleton-python-binary/-/network/main
.. image:: https://img.shields.io/badge/Apache v2.0-green

Reading, Writing and Validating SAS products


Stable release
--------------

To install PySasDataModeler, run this command in your terminal:

.. code-block:: console

    $ pip install git+https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler.git

This is the preferred method to install PySasDataModeler, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for PySasDataModeler can be downloaded from the `Gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler.git

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler/tarball/main

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ make  # install in the system root
    $ make user # or Install for non-root usage


.. _Gitlab repo: https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler
.. _tarball: https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler/tarball/main



Development
-----------

Please install graphviz to generate SVG profile of the code

.. code-block:: console

    sudo apt-get install graphviz

Then install the PySasDataModeler

.. code-block:: console

    $ git clone py_sas_data_modeler.git
    $ cd py_sas_data_modeler
    $ make prepare-dev
    $ source .py_sas_data_modeler
    $ make install-dev


To get more information about the preconfigured tasks:

.. code-block:: console

        $ make help

Usage
-----

To use PySasDataModeler in a project::

    import py_sas_data_modeler



Run tests
---------

.. code-block:: console

        $make tests


Documentation
-------------
The documentation is automatically deployed on https://plato_pdc.io.ias.u-psud.fr/plato_wp37/py_sas_data_modeler based on main branch

Container
---------
A container is automatically created on https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler/container_registry


Author
------
👤 **Jean-Christophe Malapert**



🤝 Contributing
---------------
Contributions, issues and feature requests are welcome!<br />Feel free to check [issues page](https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler/issues). You can also take a look at the [contributing guide](https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler/blob/main/CONTRIBUTING.rst)


📝 License
----------
This project is [Apache License V2.0](https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler/blob/main/LICENSE) licensed.
