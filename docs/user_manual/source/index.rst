Welcome to py_sas_data_modeler's documentation!
===========================================================

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   history
   introduction
   applicable_docs
   glossary
   conventions
   purpose
   external_view
   operations_environment
   operations_basics
   operation_manual
   reference_manual
   tutorial
   legal

.. _test:

Test report
===========

`Test report <_static/report.html>`_

.. figure:: _static/histo.*
   :alt: Benchmark

.. _coverage:

Coverage report
===============

`Test coverage <_static/coverage/index.html>`_


Code profiling
==============

.. figure:: _static/combined.*
   :alt: Acceptance test profiling


================
Analytical Index
================
* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
