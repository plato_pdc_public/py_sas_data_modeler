======================
Operations environment
======================

General
-------

The PySasDataModeler software operates within the SAS framework. Users will require the SAS framework to ensure optimal performance and usability.



Hardware configuration
----------------------

    - A storage to save the results in the HDF5

Software configuration
----------------------

   - Compatible with macOS or Linux distributions.


Operational constraints
-----------------------

   - Python >= 3.10
