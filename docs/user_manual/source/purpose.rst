=======================
Purpose of the Software
=======================

The PySasDataModeler software is designed to facilitate the management and processing of tasks and products
within an HDF5-based workflow system. Its intended uses include:

- **Capabilities**: The software allows users to model, validate, and manipulate complex data pipelines using Python dataclasses. It supports the creation of auto-generated business classes based on JSON schema definitions, enabling seamless object creation and serialization of various products and pipelines.

- **Operating Improvements**: By optimizing the storage of information within HDF5 files through the use of symbolic links, the software reduces redundancy and improves data accessibility. This leads to more efficient handling of large datasets and simplifies the workflow for both developers and scientific module creators.

- **Expected Benefits**: Users can expect enhanced productivity in managing data pipelines, reduced complexity in data serialization/deserialization, and improved validation processes. This tool serves two primary user profiles: SAS workflow developers and scientific module developers, providing them with an abstraction layer for data storage and validation mechanisms.


========
Concepts
========

Modeling of scientific module
=============================

This model outlines a scientific module consisting of inputs and outputs. Each input and output represents
either an internal or external product related to the SAS. The inputs can include a collection of mandatory
and optional products. Occasionally, an input may be a set of input groups, with each group containing both
mandatory and optional products.

.. automodule:: py_sas_data_modeler.dm.io_pipeline


Selecting the set of input groups
=================================

The following class describes the automatic stratgy to select the right input group.

.. autoclass:: py_sas_data_modeler.workflow.InputSelectionStrategy
    :members:
    :undoc-members:
    :show-inheritance:


Design Patterns
===============

DataClassFactory Design Pattern
-------------------------------

The `DataClassFactory` class serves as a Singleton factory responsible for managing
the registration and retrieval of dataclass types and their associated aliases.
This design pattern provides several benefits, making it a valuable addition to
any application that utilizes dataclasses.

Key Features and Benefits
~~~~~~~~~~~~~~~~~~~~~~~~~

1. **Centralized Management**: The factory acts as a centralized registry,
   allowing easy registration and retrieval of dataclasses. This avoids
   scattering the dataclass definitions throughout the codebase, enhancing
   maintainability.

2. **Single Instance (Singleton)**: The Singleton pattern ensures that
   only one instance of the `DataClassFactory` exists throughout the application.
   This guarantees consistent access to the factory, preventing potential issues
   related to multiple instances and making it easier to manage the registration
   process.

3. **Dynamic Retrieval**: The factory allows for dynamic retrieval of dataclass
   types based on string keys. This enables the application to work with various
   dataclasses without hardcoding their references, promoting flexibility and
   extensibility.

4. **Alias Support**: By supporting aliases, the factory enables different
   string representations for the same dataclass. This can simplify code and make
   it more intuitive, especially in scenarios where multiple terminologies may
   exist for similar concepts.

5. **Instance Creation**: The factory provides a method for creating instances
   of dataclasses from dictionaries. This simplifies the process of initializing
   dataclasses with data from external sources, such as JSON or database records,
   ensuring that the data is properly mapped to the dataclass attributes.

6. **Error Handling**: The factory incorporates robust error handling, raising
   specific exceptions like `UnknownDataclassError` and `DataclassInstanceCreationError`.
   This allows for clear identification of issues when attempting to retrieve or create
   dataclass instances, making debugging and error resolution easier.

.. autoclass:: py_sas_data_modeler.dm.DataClassFactory
    :members:
    :undoc-members:
    :show-inheritance:


Hdf5Tree Design Pattern
-----------------------

The `Hdf5Tree` class is a Singleton wrapper around the `DependencyTree`, designed
to manage a hierarchical structure of dependencies between dataclasses. It specifically
focuses on handling HDF5-like data structures, making it a valuable component for
applications that deal with complex nested data formats.

Key Features and Benefits
~~~~~~~~~~~~~~~~~~~~~~~~~

1. **Singleton Instance**: The `Hdf5Tree` follows the Singleton pattern, ensuring
   that only one instance exists throughout the application. This prevents
   redundancy and simplifies management of the dependency tree.

2. **Dependency Management**: The class provides a structured way to manage
   dependencies between dataclasses, which is crucial for applications that need
   to understand and navigate complex data relationships.

3. **Tree Creation**: The factory facilitates the creation of a dependency tree
   for any given dataclass type. This makes it easier to visualize and handle the
   relationships among various dataclasses, ensuring that all dependencies are
   captured effectively.

4. **Key Retrieval**: The `get_key_from` method allows for easy retrieval of
   keys associated with specific dataclasses in the dependency tree. This
   simplifies the process of identifying relationships and interactions between
   different components of the system.

5. **Hierarchical Path Discovery**: The `find_key_path` method enables users
   to discover the hierarchical path to a specific key within the dependency tree.
   This is particularly useful for navigating complex structures, as it provides
   a clear representation of how data elements are interconnected.

6. **Error Handling**: The class includes robust error handling, raising specific
   exceptions like `KeyNotFoundError` and `PathNotFoundError`. This allows developers
   to easily identify and address issues when searching for keys or paths, enhancing
   the overall reliability of the application.


.. autoclass:: py_sas_data_modeler.dm.Hdf5Tree
    :members:
    :undoc-members:
    :show-inheritance:


Architecture
============

Packages view
-------------

.. image:: _static/packages_architecture.png
   :alt: Packages view


classes view
------------

.. image:: _static/classes_architecture.png
   :alt: Classes view
