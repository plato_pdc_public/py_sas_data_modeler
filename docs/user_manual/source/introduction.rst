============
Introduction
============


Purpose
-------

.. automodule:: py_sas_data_modeler


Context
-------

The SAS framework is responsible for determining stellar parameters to facilitate exoplanet detection.
It consists of five pipelines, each composed of multiple scientific modules, generating over 150 products
for each star in a limited time. All products and the input/output structures of the pipelines are
described by a JSON schema. As the input/output structures and data evolve over time, updating them can
become complex in terms of time and validation. The PySasDataModeler software addresses this challenge.
