================
Reference manual
================

Introduction
------------

The :term:`SUM` provide or reference,a quick‐reference card or page for using the software, which summarises
frequently used function keys, control sequences, formats, commands, or other aspects of software use.*


Module overview
---------------


Inputs/outputs
~~~~~~~~~~~~~~

.. automodule:: py_sas_data_modeler.io


Data model
~~~~~~~~~~

.. automodule:: py_sas_data_modeler.dm
   :members:
   :undoc-members:
   :show-inheritance:


Workflow
~~~~~~~~

.. automodule:: py_sas_data_modeler.workflow
   :members:
   :undoc-members:
   :show-inheritance:

Simulation
~~~~~~~~~~

.. automodule:: py_sas_data_modeler.simulation


Error messages
~~~~~~~~~~~~~~

.. automodule:: py_sas_data_modeler.exception


HDF5 Format
-----------

.. automodule:: py_sas_data_modeler.io.hdf5
   :members:
   :undoc-members:
   :show-inheritance:
