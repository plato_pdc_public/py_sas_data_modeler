=============================
External view of the software
=============================

This picture describes the overview of the software and its interactions for the processing of a given star

.. uml::

    package "Workflow" {
        [Workflow] as wf
        wf --> [Global HDF5] : connects to
    }

    package "Scientific Modules" {
        [Scientific Modules] as sm
        sm --> [inputs.h5] : reads from
        sm --> [outputs.h5] : writes to
    }

    wf --> sm : interacts with
