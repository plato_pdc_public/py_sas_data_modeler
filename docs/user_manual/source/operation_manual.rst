=================
Operations manual
=================

General
-------

1. **Pipeline**

   - **Elementary Operations:**

      - :ref:`install`

         - Personnel: IDOC developer
         - Timeline: when installing dependencies

      - :ref:`uc1`

         - Personnel: IDOC developer
         - Timeline: Before running the SAS pipeline

      - :ref:`uc2`

         - Personnel: IDOC developer
         - Timeline: Before running the SAS pipeline

      - :ref:`uc3`

         - Personnel: IDOC developer
         - Timeline: Before running a SAS module

      - :ref:`uc4`

         - Personnel: IDOC developer
         - Timeline: Before running a SAS module

      - :ref:`uc6`

         - Personnel: IDOC developer
         - Timeline: After executing a SAS module

      - :ref:`uc7`

         - Personnel: IDOC developer
         - Timeline: After the processing of a star


2. **Scientific module**

   - **Elementary Operations:**

      - :ref:`uc5`

         - Personnel: Scientific module developer
         - Timeline: During the scientific module processing

      - :ref:`uc8`

         - Personnel: Scientific module developer
         - Timeline: During the scientific module processing

      - :ref:`uc9`

         - Personnel: Scientific module developer
         - Timeline: During the scientific module processing




.. _install:

Set‐up and initialisation
-------------------------

This section provides instructions on how to set up and initialize the `py_sas_data_modeler`
package. Follow these steps to ensure that the package is correctly configured and ready for
use in your SAS data modeling workflows.

To install `py_sas_data_modeler`, you can either install it from PyPI or clone the repository if it's hosted on a private repository.

To install from PyPI:

.. code-block:: bash

   pip install https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler

To install from a local repository:

.. code-block:: bash

   git clone https://git.ias.u-psud.fr/plato_pdc_public/py_sas_data_modeler
   cd py_sas_data_modeler
   make



Getting started
---------------

.. _import-classes-workflow:

For SAS workflow developers:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: python
   :caption: Import classes for SAS workflow developers

   # import DataClassFactory to retrieve easily a dataclass by its product name (ex: IDP_SAS_...)
   from py_sas_data_modeler.dm import DataClassFactory

   # import FileMediator to transfer data from Global HDF5 to/from processing HDF5
   from py_sas_data_modeler.io import FileMediator

   # import HDF5StorageSasTask for handling the HDF5 under scientific module responsabilities
   from py_sas_data_modeler.io import HDF5StorageSasTask

   # import HDF5StorageSasTask for handling the HDF5 under SAS pipeline responsabilities
   from py_sas_data_modeler.io import HDF5StorageSasWorkflow

   # import SasPipeline to manages inputs/outputs
   from py_sas_data_modeler.workflow import SasPipeline

.. _import-classes-module:

For SAS scientific module developers:
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

.. code-block:: python
   :caption: Import classes for SAS scientific module developers

   # import DataClassFactory to retrieve easily a dataclass by its product name (ex: IDP_SAS_...)
   from py_sas_data_modeler.dm import DataClassFactory

   # import HDF5StorageSasTask for handling the HDF5 under scientific module responsabilities
   from py_sas_data_modeler.io import HDF5StorageSasTask


.. _uc1:

UC1 : Add External Products
----------------------------

**Description:**

This operation involves adding external products into the HDF5 file of the workflow. This external products will be used
as inputs of scientific module in the HDF5 by a system of symbolic links

**Normal Operations:**

- :ref:`import-classes-workflow`
- Create the the external product objects from the external products.
- Import rhe product objects into the designated HDF5 structure.

**Normal Termination:**

- The operation completes successfully when external products are saved without errors. A confirmation message is logged.

**Error Conditions:**

- If the external product files are not found or are improperly formatted, an error is raised.

**Recovery Runs:**

- Users can retry the operation after correcting any issues with the external product files.

.. code-block:: python
   :caption: Import one external product as object in the HDF5

   # Create external data
   cls = DataClassFactory.get_model('DP1_LC_MERGED')
   dp1_obj = cls(time=[1.0], flux=[1.0], quarter=[1.0])

   # Create a SAS storage for the SAS workflow
   hdf5 = HDF5StorageSasWorkflow("/tmp/test.h5")

   # Save the external product into the SAS storage
   sas = SasPipeline(hdf5)
   sas.save_product(dp1_obj)

.. code-block:: python
   :caption: Import one external product as dictionnary

   # Create dictionnary:
   data = {
      'time'=[0.1],
      'flux'=[0.1],
      'quarter'=[0.1]
   }

   # Get the class
   cls = DataClassFactory.get_model('DP1_LC_MERGED')

   # Instanciate the object
   dp1_obj = cls.from_dict(data)

   # Create a SAS storage for the SAS workflow
   hdf5 = HDF5StorageSasWorkflow("/tmp/test.h5")

   # Save the external product into the SAS storage
   sas = SasPipeline(hdf5)
   sas.save_product(data)


.. code-block:: python
   :caption: Import external products

   # Create objets with the method you want
   ...

   # Create a dictionnary with the product_name as key
   data = {
      'DP1_LC_MERGED': dp1_obj
   }

   # Create a SAS storage for the SAS workflow
   hdf5 = HDF5StorageSasWorkflow("/tmp/test.h5")

   # Save the external products into the SAS storage
   sas = SasPipeline(hdf5)
   sas.save_products(data)


.. _uc2:

UC2 : Add Q-1 Products
----------------------

**Description:**

This operation is responsible for adding Q-1 products to the HDF5 file of the workflow.

.. note::

   Not implemented

**Normal Operations:**

- :ref:`import-classes-workflow`
- Similar to adding external products, but specifically for Q-1 products.

**Normal Termination:**

- Successful addition of Q-1 products is logged.

**Error Conditions:**

- Missing or incorrectly formatted Q-1 product files trigger an error.

**Recovery Runs:**

- Users can reattempt the addition after rectifying any identified issues.

.. code:: python
   :caption: Import Q-1 products


.. _uc3:

UC3 : Determine Pipeline Inputs
-------------------------------

**Description:**

Automatically determines the inputs required for a given pipeline and adds them to the HDF5 file.

.. note::

   The selection of the set of inputs is defined py the InputSelectionStrategy class

**Normal Operations:**

- :ref:`uc1`
- The system scans the pipeline data model and identifies the necessary input products.
- These inputs are then added to the HDF5 structure as symbolic links

**Normal Termination:**

- Inputs are successfully added, and a log entry confirms the action.

**Error Conditions:**

- If inputs cannot be identified due to configuration issues, an error is raised.

**Recovery Runs:**

- Users can modify the pipeline configuration and rerun the input determination.

.. code-block:: python
   :caption: Determine inputs and link them to the inputs of the module

   # Get the pipeline (e.g MSAP1, MSAP2, MSAP3, MSAP4, MSAP5)
   cls = DataClassFactory.get_model('MSAP1')

   # Set the pipeline in SasPipeline
   sas.cls = cls

   # Create a scientific module of the pipeline
   msap1_02 = sas.create_module('MSAP1_02')

   # Add inputs to this scientific module
   msap1_02.add_inputs()

.. note::

   Create a symbolic link from MSAP inputs to /external or /products. For some SAS products, symbolic
   link is not possible because the product is updated.

.. _uc4:

UC4 - Transfer Inputs to Scientific Module
-------------------------------------------

**Description:**

Transfers the determined inputs into the scientific module for processing.

**Normal Operations:**

- :ref:`uc3`
- Inputs from the HDF5 are retrieved and passed to the scientific module.

**Normal Termination:**

- Successful transfer of inputs is logged, indicating readiness for processing.

**Error Conditions:**

- Failure in input retrieval or transfer raises an error.

**Recovery Runs:**

- Developer can verify the HDF5 content and retry the transfer.

.. code-block:: python
   :caption: Transfer inputs data from workflow storage to scientific module storage

   # Create storage for scientific module
   task_hdf5 = HDF5StorageSasTask('/tmp/', 'MSAP1_02')

   # Create the Transfer object between the two storages
   file_transfer = FileMediator(
      sas_storage=hdf5, task_storage=task_hdf5
   )

   # Transfer the input data of MSAP1_01 to the scientific module storage
   file_transfer.transfer_inputs_to_task('MSAP1_02')

.. _uc5:

UC5 : Process Data by Scientific Module
----------------------------------------

**Description:**

The scientific module processes the data using the provided inputs.

**Normal Operations:**

- :ref:`import-classes-module`
- The scientific module reads the inputs and performs its data generation tasks.

**Normal Termination:**

- Successful completion of data processing is logged, and generated outputs are stored.

**Error Conditions:**

- If the processing fails (e.g., due to invalid data), an error is raised.

**Recovery Runs:**

- Users can review the inputs and rectify any issues before re-running the processing.

.. code-block:: python
   :caption: Load a given product, and save outputs

   # Create storage for scientific module
   task_hdf5 = HDF5StorageSasTask('/tmp/', 'MSAP1_02')

   # Load a given product
   dp1 = task_hdf5.load_product('DP1_LC_MERGED')

   # or load all products
   inputs = task_hdf5.load_inputs()

   # some processing ...

   # Load class for output
   outputs_cls =  DataClassFactory.get_model('OUTPUTS_MSAP1_02')
   idp_sas_flares_flag =  DataClassFactory.get_model('IDP_SAS_FLARES_FLAG')
   adp_sas_flares_flag =  DataClassFactory.get_model('ADP_SAS_FLARES_FLAG')
   idp_flares = idp_sas_flares_flag(merged_flag=[1], time=[1.0])
   adp_flares = adp_sas_flares_flag(flare_flag=[1.0], time=[1.0])
   outputs = outputs_cls(IDP_SAS_FLARES_FLAG=idp_flares, ADP_SAS_FLARES_FLAG=adp_flares)

   # Save outputs
   task_hdf5.save_outputs(outputs)

.. code-block:: python
   :caption: Load input products, and save outputs

   # Load a given product
   inputs = task_hdf5.load_inputs()

   # some processing ...

   # Load class for output
   outputs_cls =  DataClassFactory.get_model('OUTPUTS_MSAP1_02')

   # Instanciate it by using the object or with from_dict method
   outputs = outputs.from_dict(data_dict)

   # Save outputs
   task_hdf5.save_outputs(outputs)

   # Validation
   task_hdf5.is_validate_dm()

.. _uc6:

UC6 : Transfer Outputs to Global HDF5
-------------------------------------

**Description:**

Transfers outputs generated by the scientific module to the global HDF5.

**Normal Operations:**

- :ref:`uc4`
- Outputs from the module are saved into the designated global HDF5 structure.

**Normal Termination:**

- A successful transfer of outputs is logged.

**Error Conditions:**

- If outputs cannot be saved due to issues with the HDF5 structure, an error is raised.

**Recovery Runs:**

- Users can check the output data and retry the transfer.

.. code-block:: python

   file_transfer.retrieve_outputs_from_task('MSAP1_02')


.. _uc7:

UC7 : Final Validation of HDF5
------------------------------

**Description:**
Validates the final state of the HDF5 file at the end of the workflow.

**Normal Operations:**
- The HDF5 is checked for integrity and consistency.

**Normal Termination:**
- A successful validation is logged, confirming that the HDF5 structure is correct.

**Error Conditions:**
- Validation errors indicate structural issues within the HDF5, raising an exception.

**Recovery Runs:**
- Users can analyze validation logs, fix issues, and revalidate.

.. code-block:: python
   :caption: Validate the structure of the final HDF5

   sas.cls = None
   sas.is_validate_dm()


.. _uc8:

UC8 : List Output Parameters for a Scientific Module
----------------------------------------------------

**Description:**

This use case describes the process of listing output parameters from a specific scientific module
to understand the expected results after data processing.

**Normal Operations:**

- Users specify the scientific module of interest.
- The system queries the module for its output parameters, which may include names, types, and descriptions of the expected outputs.

**Normal Termination:**

-The operation completes successfully when the output parameters are retrieved and displayed or logged.
A message confirms the successful retrieval of output parameters.

**Error Conditions:**

- If the specified scientific module does not exist or fails to return output parameters, an error is raised.

**Recovery Runs:**

- Users can verify the module name and attempt the retrieval again after correcting any issues.

.. code-block:: python

   tasks = sas.tasks
   # Then iter on tasks


.. _uc9:

UC9 : Control the log level of py_sas_data_modeler
--------------------------------------------------

**Description:**

This use case describes how to control the level info data dumped out into log messages when using py_sas_data_modeler as a COTS.

**Normal Operations:**

.. code-block:: python
   :caption: Control log level

   from loguru import logger
   import inspect
   import sys
   logger.remove()
   logger.add(
      sys.stdout,
      level="WARNING",
      filter="py_sas_data_modeler.dm.dataclass_management",
      format=str(dict(inspect.signature(logger.add).parameters.items())['format']).replace('message}', 'message:.1024}...').replace('format=', '')[1:-1]
   )


**Normal Termination:**

-The log level of py_sas_data_modeler is set to warning.

**Error Conditions:**

- N/A

**Recovery Runs:**

- N/A
