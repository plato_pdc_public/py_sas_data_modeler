{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "b8e217dc",
   "metadata": {},
   "source": [
    "# Tutorial 2"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "d54e43ef",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "In this tutorial, we will guide you through the process of managing input and output data for scientific modules using PySasDataModeler. You will learn how to load data from HDF5 storage, create and save output products, and validate the data structure to ensure consistency within your scientific workflows. This step-by-step guide is designed for scientific module developers working with HDF5-based storage systems."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "66f8ebe4",
   "metadata": {},
   "outputs": [],
   "source": [
    "from jupyterlab_h5web import H5Web"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "32381a57",
   "metadata": {},
   "source": [
    "## Import the pySasDataModeler classes for scientific module developers\n",
    "In this section, we import the necessary classes from the PySasDataModeler package. These classes will help you load data, manage storage, and create outputs for your scientific module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "25b1239f-e4db-4aa3-99e4-4e77bf04b9d8",
   "metadata": {},
   "outputs": [],
   "source": [
    "from py_sas_data_modeler.dm import DataClassFactory\n",
    "from py_sas_data_modeler.io import HDF5StorageSasTask"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "118cc97b",
   "metadata": {},
   "source": [
    "* DataClassFactory: Used to dynamically retrieve data classes based on their identifiers.\n",
    "* HDF5StorageSasTask: Manages the storage of input and output data for scientific tasks using HDF5 files."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "aa179b0f",
   "metadata": {},
   "source": [
    "## Define the scientific module storage\n",
    "We create an instance of HDF5StorageSasTask to manage the data storage for a specific scientific module, in this case, MSAP1_02."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "7c3c8e08-f48e-45ae-a87f-b80d79a8de86",
   "metadata": {},
   "outputs": [],
   "source": [
    "from py_sas_data_modeler.io import HDF5StorageSasTask\n",
    "task_hdf5 = HDF5StorageSasTask('/tmp/', 'MSAP1_02')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "faf5ab8f",
   "metadata": {},
   "source": [
    "* HDF5StorageSasTask: Defines the storage location (/tmp/) and the module name (MSAP1_02)."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "cf15aeef",
   "metadata": {},
   "source": [
    "## Loading data\n",
    "There are two primary methods for loading data from the scientific module storage:"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68aad035",
   "metadata": {},
   "source": [
    "1 - Load a specific product: You can load a specific product by providing its name."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "08d1e628-f9fc-423d-b930-8d8be1bd5825",
   "metadata": {},
   "outputs": [],
   "source": [
    "dp1 = task_hdf5.load_product('DP1_LC_MERGED')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4e81a217",
   "metadata": {},
   "source": [
    "* load_product: Loads the DP1_LC_MERGED data product from the HDF5 file."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e31b00f7",
   "metadata": {},
   "source": [
    "2 - Load all products: You can also load all input products available in the storage at once."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "dd59f28b",
   "metadata": {},
   "outputs": [],
   "source": [
    "inputs = task_hdf5.load_inputs()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6f72e4d0",
   "metadata": {},
   "source": [
    "- load_inputs: Loads all available input products from the storage."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "522a267e",
   "metadata": {},
   "source": [
    "## Creating outputs\n",
    "Next, we define the outputs that the scientific module will generate. This involves retrieving specific output data classes and instantiating them with appropriate data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "a48ab0a9-03b6-4ae6-b197-bdd0775cc5b2",
   "metadata": {},
   "outputs": [],
   "source": [
    "outputs_cls =  DataClassFactory.get_model('OUTPUTS_MSAP1_02')\n",
    "idp_sas_flares_flag =  DataClassFactory.get_model('IDP_SAS_FLARES_FLAG')\n",
    "adp_sas_flares_flag =  DataClassFactory.get_model('ADP_SAS_FLARES_FLAG')\n",
    "idp_flares = idp_sas_flares_flag(merged_flag=[1], time=[1.0])\n",
    "adp_flares = adp_sas_flares_flag(flare_flag=[1.0], time=[1.0])\n",
    "outputs = outputs_cls(IDP_SAS_FLARES_FLAG=idp_flares, ADP_SAS_FLARES_FLAG=adp_flares)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bbae1cdf",
   "metadata": {},
   "source": [
    "* DataClassFactory.get_dataclass: Retrieves data classes for both intermediate and auxiliary data products (IDP and ADP).\n",
    "* idp_flares and adp_flares: Instances of the output data classes, populated with sample data for flares.\n",
    "* outputs: An instance of the OUTPUTS_MSAP1_02 class containing both IDP and ADP flags."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5ba7de5e",
   "metadata": {},
   "source": [
    "## Saving outputs\n",
    "Once the outputs are created, we save them into the HDF5 storage using the save_outputs method."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "c0cdd8de",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[32m2025-01-09 11:55:17.190\u001b[0m | \u001b[1mINFO    \u001b[0m | \u001b[36mpy_sas_data_modeler.io.hdf5\u001b[0m:\u001b[36msave_outputs\u001b[0m:\u001b[36m1002\u001b[0m - \u001b[1mSuccessfully saved outputs for module 'MSAP1_02' with data: IDP_SAS_FLARES_FLAG=IdpSasFlaresFlagSchemaDM(merged_flag=[1], time=[1.0]) ADP_SAS_FLARES_FLAG=AdpSasFlaresFlagSchemaDM(flare_flag=[1.0], time=[1.0]).\u001b[0m\n"
     ]
    }
   ],
   "source": [
    "task_hdf5.save_outputs(outputs)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "699fdf6f",
   "metadata": {},
   "source": [
    "* save_outputs: Saves the outputs object, which contains all the generated output data, into the HDF5 storage."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "5f6fcb3f",
   "metadata": {},
   "source": [
    "## Validating the data structure"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "9eb52d14",
   "metadata": {},
   "source": [
    "Finally, we validate the structure of the data stored in the HDF5 file to ensure that it complies with the expected data model."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "552b7e4a",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[32m2024-10-30 17:04:15.331\u001b[0m | \u001b[1mINFO    \u001b[0m | \u001b[36mpy_sas_data_modeler.io.hdf5\u001b[0m:\u001b[36mis_validate_dm\u001b[0m:\u001b[36m846\u001b[0m - \u001b[1mData model validation successful for module 'MSAP1_02'.\u001b[0m\n"
     ]
    },
    {
     "data": {
      "text/plain": [
       "True"
      ]
     },
     "execution_count": 8,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "task_hdf5.is_validate_dm()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4c55a7f3",
   "metadata": {},
   "source": [
    "* is_validate_dm: Validates that the data structure stored in the HDF5 file is consistent with the data model defined by the pipeline."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "cc5d32a9-f58e-4c61-b35a-96490d6a3121",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/x-hdf5": "/tmp/output/outputs.h5",
      "text/plain": [
       "<jupyterlab_h5web.widget.H5Web object>"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "H5Web('/tmp/output/outputs.h5')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6ccb5e01",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "In this notebook, we demonstrated how to:\n",
    "\n",
    "* Load data from HDF5 storage for a specific scientific module.\n",
    "* Create output data products by dynamically loading their data classes.\n",
    "* Save the output data back into the storage and validate the data structure."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "60a32e79-311c-4e85-8897-ab26b4cfe86b",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
