{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "39da3a09",
   "metadata": {},
   "source": [
    "## Introduction\n",
    "In this tutorial, we will explore how to use PySasDataModeler to manage scientific pipelines and data transfers using HDF5 files. We will walk through the process of instantiating external data, setting up and executing a SAS pipeline, and transferring data from SAS workflow to scientific processing modules. This guide will help you understand the key concepts for efficiently handling complex data in a scientific modeling environment."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 1,
   "id": "66f8ebe4",
   "metadata": {},
   "outputs": [],
   "source": [
    "from jupyterlab_h5web import H5Web"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "2f9ea5d6",
   "metadata": {},
   "source": [
    "## Import PySasDataModeler classes\n",
    "First, we need to import the relevant classes from the py_sas_data_modeler package. These classes will handle data management, file storage, and workflows in our pipeline processing system."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 2,
   "id": "25b1239f-e4db-4aa3-99e4-4e77bf04b9d8",
   "metadata": {},
   "outputs": [],
   "source": [
    "from py_sas_data_modeler.dm import DataClassFactory\n",
    "from py_sas_data_modeler.io import FileMediator\n",
    "from py_sas_data_modeler.io import HDF5StorageSasTask\n",
    "from py_sas_data_modeler.io import HDF5StorageSasWorkflow\n",
    "from py_sas_data_modeler.workflow import SasPipeline"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "dedfde60",
   "metadata": {},
   "source": [
    "Here’s a breakdown of the imports:\n",
    "\n",
    "* DataClassFactory: Responsible for retrieving specific data classes based on identifiers.\n",
    "* FileMediator: Handles file transfers between different storage systems (SAS storage and scientific module storage).\n",
    "* HDF5StorageSasTask and HDF5StorageSasWorkflow: Manage input/output data storage in HDF5 format.\n",
    "* SasPipeline: Manages the execution of the SAS pipeline and allows adding products to the workflow."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "429501a7",
   "metadata": {},
   "source": [
    "## Instanciate External data\n",
    "We use the DataClassFactory to instantiate external data products by specifying their identifiers. In this case, we retrieve the data class for DP1_LC_MERGED."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 3,
   "id": "44db3099-bef5-434e-beaa-803953627ddc",
   "metadata": {},
   "outputs": [],
   "source": [
    "cls = DataClassFactory.get_model('DP1_LC_MERGED')\n",
    "dp1_obj = cls(time=[1.0], flux=[1.0], quarter=[1.0])"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "91346ac2",
   "metadata": {},
   "source": [
    "* DP1_LC_MERGED: This is a specific data product class that we are retrieving.\n",
    "* The object dp1_obj is created with sample data, representing external data that will be saved in the SAS storage later."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d15bc44",
   "metadata": {},
   "source": [
    "## Create SasWorkflow instance and save external data\n",
    "We create an instance of HDF5StorageSasWorkflow, which will act as the storage handler for the SAS workflow, and then use it to save the external data we created."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 4,
   "id": "4b9488c2",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[32m2025-01-09 11:53:43.660\u001b[0m | \u001b[1mINFO    \u001b[0m | \u001b[36mpy_sas_data_modeler.workflow\u001b[0m:\u001b[36msave_external\u001b[0m:\u001b[36m562\u001b[0m - \u001b[1mProduct of type 'Dp1LcMergedSchemaDM' has been successfully saved.\u001b[0m\n"
     ]
    }
   ],
   "source": [
    "hdf5 = HDF5StorageSasWorkflow(\"/tmp/test.h5\")\n",
    "sas = SasPipeline(hdf5)\n",
    "sas.save_external(dp1_obj)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e0de48da",
   "metadata": {},
   "source": [
    "* HDF5StorageSasWorkflow: Stores data in an HDF5 file located at /tmp/test.h5.\n",
    "* SasPipeline: Represents the SAS workflow. The save_product method is used to save the dp1_obj product into the storage."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 5,
   "id": "c614d9e1-d193-414c-b64b-8e6e7259669a",
   "metadata": {},
   "outputs": [],
   "source": [
    "#H5Web('/tmp/test.h5')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "1fe6c3ca",
   "metadata": {},
   "source": [
    "## Create a MSAP1_02 instance and add the inputs to it\n",
    "Now, we create a module instance for the scientific pipeline MSAP1 and add inputs to the module MSAP1_02."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 6,
   "id": "603ad8da-1430-4931-a667-932b9cdf46d9",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[32m2025-01-09 11:53:49.031\u001b[0m | \u001b[33m\u001b[1mWARNING \u001b[0m | \u001b[36mpy_sas_data_modeler.workflow\u001b[0m:\u001b[36madd_inputs\u001b[0m:\u001b[36m447\u001b[0m - \u001b[33m\u001b[1mMultiple input sets detected for module 'MSAP1_02'. Attempting to find the best one.\u001b[0m\n",
      "Handle inputs: mandatory_inputs=[Product(name='DP1_LC_MERGED', implementation_class=<class 'py_sas_data_modeler.dm.sas.Dp1LcMergedSchemaDM'>, is_required=True), Product(name='IDP_SAS_FLARES_FLAG', implementation_class=<class 'py_sas_data_modeler.dm.sas.IdpSasFlaresFlagSchemaDM'>, is_required=True), Product(name='ADP_SAS_FLARES_FLAG', implementation_class=<class 'py_sas_data_modeler.dm.sas.AdpSasFlaresFlagSchemaDM'>, is_required=True)] optional_inputs=[] - False for MSAP1_02\n",
      "NNNNNN: /EXTERNALS/DP1_LC_MERGED to MSAP1/MSAP1_02/inputs_msap1_02/DP1_LC_MERGED\n",
      "\u001b[32m2025-01-09 11:53:49.040\u001b[0m | \u001b[31m\u001b[1mERROR   \u001b[0m | \u001b[36mpy_sas_data_modeler.workflow\u001b[0m:\u001b[36m_link_input_set\u001b[0m:\u001b[36m379\u001b[0m - \u001b[31m\u001b[1mMandatory input IDP_SAS_FLARES_FLAG is missing. Please add it before linking.\u001b[0m\n",
      "Handle inputs: mandatory_inputs=[Product(name='DP1_LC_MERGED', implementation_class=<class 'py_sas_data_modeler.dm.sas.Dp1LcMergedSchemaDM'>, is_required=True)] optional_inputs=[Product(name='IDP_SAS_FLARES_FLAG', implementation_class=<class 'py_sas_data_modeler.dm.sas.IdpSasFlaresFlagSchemaDM'>, is_required=False), Product(name='ADP_SAS_FLARES_FLAG', implementation_class=<class 'py_sas_data_modeler.dm.sas.AdpSasFlaresFlagSchemaDM'>, is_required=False)] - True for MSAP1_02\n",
      "NNNNNN: /EXTERNALS/DP1_LC_MERGED to MSAP1/MSAP1_02/inputs_msap1_02/DP1_LC_MERGED\n",
      "\u001b[32m2025-01-09 11:53:49.043\u001b[0m | \u001b[33m\u001b[1mWARNING \u001b[0m | \u001b[36mpy_sas_data_modeler.workflow\u001b[0m:\u001b[36m_link_input_set\u001b[0m:\u001b[36m386\u001b[0m - \u001b[33m\u001b[1mOptional input IDP_SAS_FLARES_FLAG not found in the HDF5. Skipping link creation.\u001b[0m\n",
      "\u001b[32m2025-01-09 11:53:49.045\u001b[0m | \u001b[33m\u001b[1mWARNING \u001b[0m | \u001b[36mpy_sas_data_modeler.workflow\u001b[0m:\u001b[36m_link_input_set\u001b[0m:\u001b[36m386\u001b[0m - \u001b[33m\u001b[1mOptional input ADP_SAS_FLARES_FLAG not found in the HDF5. Skipping link creation.\u001b[0m\n",
      "\u001b[32m2025-01-09 11:53:49.045\u001b[0m | \u001b[1mINFO    \u001b[0m | \u001b[36mpy_sas_data_modeler.workflow\u001b[0m:\u001b[36madd_inputs\u001b[0m:\u001b[36m465\u001b[0m - \u001b[1mSuccessfully linked input set: mandatory_inputs=[Product(name='DP1_LC_MERGED', implementation_class=<class 'py_sas_data_modeler.dm.sas.Dp1LcMergedSchemaDM'>, is_required=True)] optional_inputs=[Product(name='IDP_SAS_FLARES_FLAG', implementation_class=<class 'py_sas_data_modeler.dm.sas.IdpSasFlaresFlagSchemaDM'>, is_required=False), Product(name='ADP_SAS_FLARES_FLAG', implementation_class=<class 'py_sas_data_modeler.dm.sas.AdpSasFlaresFlagSchemaDM'>, is_required=False)] for Task MSAP1_02\u001b[0m\n"
     ]
    }
   ],
   "source": [
    "cls = DataClassFactory.get_model('MSAP1')\n",
    "sas.cls = cls\n",
    "msap1_02 = sas.create_module('MSAP1_02')\n",
    "msap1_02.add_inputs()"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "a00827f8",
   "metadata": {},
   "source": [
    "* DataClassFactory.get_dataclass('MSAP1'): Retrieves the MSAP1 pipeline class.\n",
    "* SasPipeline.cls: The pipeline class is assigned to the SAS pipeline (sas).\n",
    "* sas.create_module('MSAP1_02'): Creates a specific scientific module (MSAP1_02) within the MSAP1 pipeline.\n",
    "* msap1_02.add_inputs(): Adds the required inputs for this module, allowing it to process data."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 7,
   "id": "628b8d93-94a0-4f69-9432-68de0cac7778",
   "metadata": {},
   "outputs": [],
   "source": [
    "#H5Web('/tmp/test.h5')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "0839981f",
   "metadata": {},
   "source": [
    "## Transfert inputs of MSAP1_02 from the SAS storage to scientific module storage\n",
    "We create a new HDF5 storage for the scientific module MSAP1_02 and transfer the input data from the SAS storage to the scientific module storage using the FileMediator."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 8,
   "id": "fa6e51ae-bd69-4150-b5fc-3b02192416ea",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "\u001b[32m2025-01-09 11:53:56.925\u001b[0m | \u001b[1mINFO    \u001b[0m | \u001b[36mpy_sas_data_modeler.io.hdf5\u001b[0m:\u001b[36msave_inputs\u001b[0m:\u001b[36m976\u001b[0m - \u001b[1mSAVE inputs: DP1_LC_MERGED=Dp1LcMergedSchemaDM(time=[1.0], flux=[1.0], quarter=[1.0]) IDP_SAS_FLARES_FLAG=None ADP_SAS_FLARES_FLAG=None\u001b[0m\n",
      "\u001b[32m2025-01-09 11:53:56.928\u001b[0m | \u001b[1mINFO    \u001b[0m | \u001b[36mpy_sas_data_modeler.io.interface\u001b[0m:\u001b[36mtransfer_inputs_to_task\u001b[0m:\u001b[36m111\u001b[0m - \u001b[1mSuccessfully transferred inputs to task 'MSAP1_02'.\u001b[0m\n"
     ]
    }
   ],
   "source": [
    "task_hdf5 = HDF5StorageSasTask('/tmp/', 'MSAP1_02')\n",
    "file_transfer = FileMediator(\n",
    "   sas_storage=hdf5, task_storage=task_hdf5\n",
    ")\n",
    "file_transfer.transfer_inputs_to_task('MSAP1_02')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "4d8634b9",
   "metadata": {},
   "source": [
    "* HDF5StorageSasTask: Creates storage for the scientific module MSAP1_02 in the specified path (/tmp/).\n",
    "* FileMediator: Acts as the intermediary to transfer data between the SAS storage (hdf5) and the task-specific storage (task_hdf5).\n",
    "* transfer_inputs_to_task: This method transfers the input data required by MSAP1_02 from the SAS storage to the scientific module storage."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 9,
   "id": "18fccf83-ccf5-4699-86e9-7a627e7d4e5c",
   "metadata": {},
   "outputs": [
    {
     "data": {
      "application/x-hdf5": "/tmp/input/inputs.h5",
      "text/plain": [
       "<jupyterlab_h5web.widget.H5Web object>"
      ]
     },
     "execution_count": 9,
     "metadata": {},
     "output_type": "execute_result"
    }
   ],
   "source": [
    "H5Web('/tmp/input/inputs.h5')"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "bea343c2",
   "metadata": {},
   "source": [
    "## Conclusion\n",
    "In this notebook, we demonstrated how to:\n",
    "\n",
    "* Instantiate external data products and save them in the SAS workflow storage.\n",
    "* Create and configure scientific pipeline modules.\n",
    "* Transfer data between SAS workflow storage and scientific module storage using file mediation."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "e21b8390-6ca5-4105-8991-9c9e4dde40c5",
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
