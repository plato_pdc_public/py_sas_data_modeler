===========
Conventions
===========

The following stylistics conventions, and command syntax conventions are used in the document:

Important Notes
----------------
.. note::

   This section highlights important points that the user should pay attention to while using the software.

Python Code Illustrations
-------------------------
.. code-block:: python

   # This section includes Python code examples to illustrate usage and syntax.

   def example_function(param1, param2):
       """
       Example function that demonstrates how to use the parameters.
       """
       return param1 + param2
