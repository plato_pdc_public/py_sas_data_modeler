======================================================
Software unit testing and software integration testing
======================================================

The SUITP describes the responsibility and schedule information for the software unit
testing and integration testing, detailed as follows.

Organization
------------

+-----------------------+--------------------------+
| Role                  | Person                   |
+=======================+==========================+
| Project Manager       | Hervé Ballans            |
+-----------------------+--------------------------+
| Test Manager          | Hervé Ballans            |
| - DevOps Engineers    | Marc Dexet               |
| - Developers          | Jean-Christophe Malapert |
+-----------------------+--------------------------+
| Configuration Manager | Hervé Ballans            |
+-----------------------+--------------------------+
| Product Assurance     | Marc Dexet               |
+-----------------------+--------------------------+

The organization of software unit testing and integration testing activities for the `py_sas_data_modeler`
project is structured as follows:

- **Roles**:

  -     **Test Manager**: versees the overall testing strategy, ensures proper test case coverage, monitors automated
        test results in GitLab CI, and resolves escalated issues.
  -     **Developers**: Write and maintain unit tests, implement fixes for issues discovered during automated testing,
        and ensure their code integrates well with the overall system.
  -     **DevOps Engineers**: Manage and maintain the GitLab CI/CD pipelines, ensure the availability of test environments,
        and troubleshoot CI-related issues.
  -     **Product Assurance**: Verifies that automated test results meet quality standards and confirms that critical paths
        are tested as required.
  -     **Project Manager**: Coordinates between development and testing teams, ensuring alignment with project timelines
        and test milestones.
  -     **Configuration Manager**: Manages the identification, control, and versioning of configuration items, ensuring all
        changes to software, systems, and related artifacts are tracked, controlled, and consistent across the project.


- **Reporting Channels**:

  -     Test results are automatically generated and stored in GitLab CI.
  -     The **Test Manager** reviews the reports, analyzes trends, and communicates with the Project Management team to track
        progress and address issues.
  -     Any issues detected in CI jobs are escalated to the **DevOps Engineers** for pipeline or environment-related concerns
        or to **Developers** for software bugs.

- **Levels of Authority for Resolving Problems**:

  -     **Test Manager**: Has the authority to adjust test schedules, allocate additional resources, or escalate issues to
        senior management if a critical issue arises. The Test Manager is responsible for overseeing the entire testing strategy
        and resolving major testing issues.
  -     **Developers**: Have the authority to resolve software-related issues discovered during unit testing but must escalate
        complex or unresolved issues to the Test Manager. Developers focus on implementing fixes during the development process. 
  -     **DevOps Engineers**: Have the authority to troubleshoot and resolve CI/CD pipeline-related issues, ensure the availability
        of test environments, and escalate infrastructure-related issues to senior technical staff if needed.
  -     **Product Assurance**: Oversees compliance with quality standards and recommends mitigation actions for testing issues.
        Product Assurance has the authority to ensure that critical test paths are properly validated, but issues related to the
        quality of test results may be escalated to the Test Manager.
  -     **Project Manager**: Coordinates between the development and testing teams, ensuring alignment with project timelines and
        test milestones. While they do not directly resolve technical issues, they have the authority to adjust the project schedule
        and escalate issues to senior management for higher-level decision-making.
  -     **Configuration Manager**: Manages the identification, control, and versioning of configuration items, ensuring all changes
        to software, systems, and related artifacts are tracked and controlled. If configuration discrepancies or issues arise, they
        have the authority to resolve them by enforcing configuration control processes and escalating to senior technical staff if necessary.

- **Relationships to other activities**:

  - The software unit testing and integration testing activities are closely tied to the **Project Management**
    (for scheduling), **Development** (for providing the software under test), **Configuration Management**
    (to manage the software versions), and **Product Assurance** (to maintain quality control).

Master schedule
---------------

The master schedule for the software unit testing and integration testing activities follows the overall software
development plan, with test milestones outlined as follows:

-   **Reference to the Master Schedule**: The test activities are aligned with the software development schedule,
    as detailed in the Software Development Plan (SDP). This includes key development milestones and corresponding test phases.

-   **Additional Test Milestones**:

    -   **Unit Test Preparation**: The unit test environment is prepared in the first month of development.
    -   **Unit Testing Phase**: Starts at the end of the first development sprint, with tests executed after every
        development iteration.
    -   **Integration Testing**: Begins after unit tests are completed, during the second development sprint, and
        continues through the final integration stage.

- **Schedule for each Testing Task**:

    - **Unit Testing Tasks**: 1-2 weeks per sprint for new functionality.
    - **Integration Testing**: Conducted at the end of each sprint after unit testing is completed.
    - **Test Milestone Reviews**: At the end of each sprint, a test review will assess whether all requirements are met.

- **Period of use for Test Facilities**:
  The test facilities, including both hardware and software environments, will be in use throughout the testing phases:
  unit testing (ongoing in each sprint) and integration testing (ongoing post-unit testing until completion).


Resource summary
----------------

The following resources are required to perform the software unit and integration testing activities:

- **Staff**:

  - **Test Manager**: A dedicated CI job is triggered automatically with each tag deployment on both **main** and **ci** branches. The
    manager checks the test case coverage, monitors automated test results in GitLab CI, and resolves escalated issues
  - **Developers**: 1 developer per module to provide code and bug fixes based on test feedback.
  - **Product Assurance Staff**: 1 engineer responsible for ensuring quality standards are met.

- **Hardware**:

  - **Test Servers**: IAS or local servers for test environment setup and execution.
  - **Workstations**: Workstations for developers and QA engineers to perform manual testing and review test results.

- **Software Tools**:

  - **Testing Framework**: `pytest` for unit and integration testing.
  - **CI Tools**: GitLab CI/CD for automated testing and continuous integration.
  - **Code Coverage Tools**: `coverage.py` for tracking test coverage.
  - **Test Management Tools**: Jira or a similar tool for managing test tasks, milestones, and issues.

Responsibilities
----------------

The specific responsibilities associated with each role are as follows:

-   **Test Engineer**: Manages the overall testing process, defines test strategies, prepares schedules, and ensures
    the timely delivery of test results. Resolves escalated issues and manages testing risks.
-   **Developers**: Responsible for writing and maintaining unit tests, fixing defects identified in the tests, and
    providing support during integration testing.
-   **QA Engineers**: Execute test cases, document results, and ensure that tests are completed according to schedule.
    Responsible for reporting on test progress and issues.
-   **Product Assurance**: Ensures all tests are performed according to the quality assurance processes and maintains
    test documentation.
-   **Configuration Management**: Ensures the correct configuration of testing environments and manages the versioning
    of software under test.

Tools, techniques and methods
-----------------------------

The tools, techniques, and methods used for software unit testing and integration testing include:

-   **Testing Tools**: `pytest`, `mock`, and `unittest` frameworks for automated unit and integration testing.
-   **CI/CD Tools**: GitLab CI for automating test execution and reporting.
-   **Code Quality and Coverage Tools**: `pytest-cov`, `flake8`, and `pylint` in local for checking code quality and
    measuring test coverage and sonarqube on the IAS server.
-   **Test Execution Tools**: Custom scripts for executing tests in specific environments or configurations.
-   **Test Reporting Tools**: Tools integrated into GitLab for continuous feedback on test results and issues.

Personnel and personnel training requirements
---------------------------------------------

The following training requirements are identified for the personnel involved in software unit and integration testing:

- **Test Engineers**: Should be familiar with Python testing frameworks (`pytest`, `unittest`), test automation, and GitLab CI tools.
- **Developers**: Need to be trained in unit testing best practices, using mocking frameworks, and reviewing and addressing test feedback.
- **Product Assurance**: Should have a solid understanding of software quality assurance processes and test execution best practices.
- **Configuration Management**: Should have knowledge of software versioning tools and environment configuration management.

Risks and contingencies
-----------------------

The primary risks associated with software unit and integration testing are:

-   **Unforeseen Software Bugs**: There may be software bugs that emerge during testing, causing delays. A contingency plan
    is in place to allocate additional resources for bug fixing.
-   **Integration Failures**: Issues may arise when integrating new modules. These will be resolved by developers, and
    additional testing will be conducted if necessary.
-   **Test Environment Issues**: Failures in test environment configuration could delay testing. Redundant environments
    and backup systems are available to mitigate this risk.


Control procedures for software unit testing / integration testing
------------------------------------------------------------------

The SUITP outlines the management procedures for controlling software unit and integration testing activities:

1. **Problem Reporting and Resolution**:

   -    Any issues found during testing will be logged into the issue tracker (e.g., Jira), and immediate action will be taken
        by the responsible teams to resolve them.
   -    Issues are categorized based on severity and are prioritized for resolution.

2. **Deviation and Waiver Policy**:

   -    In case deviations from the planned test approach are necessary, approval must be obtained from the Test Manager and
        Product Assurance.
   -    Waivers may be granted for non-critical tests, but a formal assessment is required.

3. **Control Procedures**:

   - Regular meetings and status reports will ensure the test activities remain on schedule.
   - Deviation reports are generated when milestones are at risk of being missed, and corrective actions are planned.
