======================================================
Software unit testing and integration testing approach
======================================================

The SUITP describes the approach to be utilized for the software unit testing and integration testing, detailed as follows.

Unit/integration testing strategy
---------------------------------

The testing strategy for unit and integration testing of the software is defined to ensure that the software functions
as expected under various conditions. The strategy includes the following key elements:

1.  **Unit Testing**: Focuses on individual components of the software (functions, classes, modules) to ensure that each
    part behaves as expected in isolation.
2.  **Integration Testing**: Focuses on interactions between components and their integration into larger systems to verify
    that the components work together as expected.
3.  **Continuous Testing**: Unit and integration tests are integrated into the continuous integration pipeline, ensuring
    that tests are executed frequently and issues are detected early.
4.  **Test Automation**: Tests will be automated wherever possible to ensure reproducibility and efficiency, leveraging
    tools like pytest and GitLab CI/CD for seamless integration.


Unit testing approach
---------------------------------

1.  **Test Framework**: We will use pytest as the test framework for all unit testing activities.
2.  **Test Structure**: Unit tests will be organized into test classes, with one test method for each unit of functionality
    to be validated. Each unit test will focus on a specific function or method.
3.  **Test Marking**: The following custom pytest.mark annotations will be used.

 -  **@pytest.mark.class_description**: Provides a detailed description of the test class_description

 .. code-block:: python

    @pytest.mark.class_description(
        "Test suite for the BaseDataClass and its methods."
    )
    class TestBaseDataClass:
        @pytest.mark.description(
            "Test instance object creation with from_dict method."
        )
        def test_from_dict_success(self):
            #test code

 -  **@pytest.mark.description**: Provides a detailed description of the purpose and behavior of individual tests.

 .. code-block:: python

    @pytest.mark.description("Test the behavior of the 'encode_none' function when handling None inputs.")
    def test_encode_none():
        # Test code...


 -  **@pytest.mark.parametrize**:  decorator is used to parametrize tests, allowing a function to be run multiple times
    with different sets of input data. This helps ensure the behavior of the software is correctly tested under various conditions
    without duplicating test code.

 .. code-block:: python

    @pytest.mark.parametrize(
        "input_value, expected_output",
        [
            # Test for nested lists of integers
            (
                [[1, 2, 3], [4, 5, 6]],
                np.array([[1, 2, 3], [4, 5, 6]], dtype=int),
            ),
            # Test for nested lists of floats
            (
                [[1.1, 2.2], [3.3, 4.4]],
                np.array([[1.1, 2.2], [3.3, 4.4]], dtype=np.float64),
            ),
            # Test for list of dictionaries
            (
                [{"a": 1}, {"b": 2}],
                np.array(['{"a": 1}', '{"b": 2}'], dtype=np.object_),
            ),
            # Test for list of booleans
            ([True, False, True], np.array([True, False, True], dtype=bool)),
            # Test for list of strings
            (["a", "b", "c"], np.array(["a", "b", "c"], dtype=np.object_)),
            # Test for numpy array of integers
            (np.array([1, 2, 3]), np.array([1, 2, 3])),
        ],
    )
    @pytest.mark.description(
        "Test conversion of various types of input (e.g., lists, arrays) to numpy arrays."
    )
    def test_convert_to_numpy(self, input_value, expected_output):
        # test code


Integration testing approach
----------------------------

1.  **Test Framework**: Integration tests will also use pytest, but will be run with specific configurations that involve \
    the interaction between different modules of the system.
2.  **Test Structure**: Integration tests will involve multiple components interacting with each other, such as connecting \
    the data storage system with input/output processing modules.
3.  **Test Marking**: Integration tests will make use of custom marks such as:

 - **@pytest.mark.class_description**: Provides a detailed description of the test class_description

 .. code-block:: python

    @pytest.mark.class_description(
        "Test suite for the BaseDataClass and its methods."
    )
    class TestBaseDataClass:
        @pytest.mark.description(
            "Test instance object creation with from_dict method."
        )
        def test_from_dict_success(self):
            #test code

 - **@pytest.mark.description**: Provides a detailed description of the purpose and behavior of individual tests.

 .. code-block:: python

    @pytest.mark.description("Test the behavior of the 'encode_none' function when handling None inputs.")
    def test_encode_none():
        # Test code...


 -  **@pytest.mark.parametrize**:  decorator is used to parametrize tests, allowing a function to be run multiple times
    with different sets of input data. This helps ensure the behavior of the software is correctly tested under various conditions
    without duplicating test code.

 .. code-block:: python

    @pytest.mark.parametrize(
        "input_value, expected_output",
        [
            # Test for nested lists of integers
            (
                [[1, 2, 3], [4, 5, 6]],
                np.array([[1, 2, 3], [4, 5, 6]], dtype=int),
            ),
            # Test for nested lists of floats
            (
                [[1.1, 2.2], [3.3, 4.4]],
                np.array([[1.1, 2.2], [3.3, 4.4]], dtype=np.float64),
            ),
            # Test for list of dictionaries
            (
                [{"a": 1}, {"b": 2}],
                np.array(['{"a": 1}', '{"b": 2}'], dtype=np.object_),
            ),
            # Test for list of booleans
            ([True, False, True], np.array([True, False, True], dtype=bool)),
            # Test for list of strings
            (["a", "b", "c"], np.array(["a", "b", "c"], dtype=np.object_)),
            # Test for numpy array of integers
            (np.array([1, 2, 3]), np.array([1, 2, 3])),
        ],
    )
    @pytest.mark.description(
        "Test conversion of various types of input (e.g., lists, arrays) to numpy arrays."
    )
    def test_convert_to_numpy(self, input_value, expected_output):
        # test code

 -  **@pytest.mark.spec_coverage**: Like in unit tests, this mark will ensure that integration tests are properly aligned
    with the relevant specifications.

 .. code-block:: python

    @pytest.mark.integration
    @pytest.mark.spec_coverage("SPEC-0012", "Test the integration of the task processor with HDF5 storage")
    def test_integration_task_processing():
        # Integration test code here...


Branching Strategy and CI/CD Workflow
-------------------------------------

1.  **Main Branch**: This branch represents the stable version of the software, which is production-ready. It is where the
    automated tests must pass, and code quality should meet the required standards. CI/CD is set up to automatically run tests
    on this branch to ensure that all changes are validated before being merged.
2.  **CI Branch**: This branch is dedicated to testing specific feature branches through the CI pipeline. It allows continuous
    integration testing, helping ensure that the code integrates well before merging into the main branch. It is specifically used
    for validating new changes before they are included in the main release.
3. **Feature/Development Branches**: These branches are used for ongoing development and feature additions. Developers create
    these branches to work on specific tasks or modules. Once changes are made, they are tested through the CI pipeline before
    being merged into the ci branch for further validation.

Tasks and items under test
--------------------------

The software under test includes both individual components and their integrations. The tasks and items to be tested are as follows:

1. **Unit Testing Tasks**:
   - Testing individual functions and methods for correctness.
   - Verifying data processing logic, especially for edge cases.
   - Validating error handling and exception raising mechanisms.

2. **Integration Testing Tasks**:
   - Ensuring that different modules interact as expected (e.g., data flow between HDF5Handler and MSAP classes).
   - Validating the correct setup and execution of pipelines and task combinations.

Test items include:

- **HDF5 file handling and I/O**: Ensuring that data is correctly saved and loaded from HDF5 files.
- **Pipeline execution**: Verifying that different task combinations execute in the correct order.
- **Data validation**: Ensuring that each simulated product is well validated according to the PSM data model
- **Data transformations**: Ensuring that data transformations between modules are correctly handled.
- **Error handling**: Verifying that appropriate exceptions are raised in case of incorrect inputs or operations.

Features to be tested
---------------------

The features to be tested include:

1.  **Data Handling**: Verifying the correct parsing, transformation, and saving of various data structures,
    such as arrays, dictionaries, and JSON-like objects, particularly in the context of hierarchical HDF5 datasets and groups.
2.  **Pipeline Execution**: Ensuring the proper orchestration and execution of tasks within the pipeline, including the
    resolution of task dependencies, correct execution order, and handling of dynamic inputs and outputs between modules.
3.  **Data Model Validation**: Validating that the structure and content of HDF5 files comply with the predefined data model
    specified by PSM, ensuring consistent organization and data integrity throughout the workflow.
4.  **Product Validation**: Verifying that generated data products meet the specifications defined in the data model,
    ensuring compatibility and consistency across various workflows, and confirming that products are correctly linked to
    their respective tasks.
5.  **Data Transfer**: Testing the seamless transfer of data between different storage systems—both SAS workflow and
    module-specific storage—ensuring that data flow and integration within the pipeline are properly managed.
6.  **Error and Exception Handling**: Verifying that the system behaves as expected when invalid data is encountered or
    unexpected operations occur, ensuring appropriate error messages and recovery mechanisms are in place.
7.  **Integration with External Storage**: Validating that HDF5 interactions (saving, loading, and path discovery) function
    correctly, ensuring smooth integration with external storage systems and compatibility with different storage setups.
8.  **Logging**: Ensuring that the logging system captures appropriate debug information, error messages, and system events,
    providing traceability and support for troubleshooting during execution.

References to the applicable documentation for these features will be made from the software documentation and user manuals.

Features not to be tested
-------------------------

The features and significant combinations not to be tested include:

1.  **Third-party library internals**: Internal functions of external libraries like `numpy` or `h5py` are assumed to be
    correctly implemented and will not be tested.
2.  **Unlikely edge cases**: Certain rare or exceptional conditions that are unlikely to be encountered in typical usage,
    such as extreme performance scenarios or failure modes in external systems.

Test pass ‐ fail criteria
-------------------------

The following criteria will be used to determine whether tests have passed or failed:

1.  **Test Pass**: A test is considered to pass if the actual output matches the expected output, and no errors or
    exceptions are raised during the test execution.
2. **Test Fail**: A test is considered to fail if:

   - The actual output does not match the expected output.
   - Unhandled exceptions or errors are raised.
   - The software behaves unexpectedly or deviates from the specifications.

   A test failure will trigger investigation, debugging, and potential fixes before re-running the tests.


Manually and automatically generated code
------------------------------------------

The testing approach will address separately the activities for manually and automatically generated code:

1. **Manually Generated Code**:

   -    Tests will focus on verifying the correctness of individual functions, classes, and modules manually written
        by the developers. This includes validation against specifications and ensuring that the code behaves as expected
        for a range of inputs.

2. **Automatically Generated Code**:

   -    The tests for automatically generated code (such as code produced by Jinja templates or data model generation tools)
        will focus on verifying the correctness of the generated structures, ensuring that the output complies with predefined
        formats and adheres to the required standards.
