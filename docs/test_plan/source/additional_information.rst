=========================================
Software test plan additional information
=========================================

The following additional information shall be provided:

- test procedures to test cases traceability matrix;
- test cases to test procedures traceability matrix;
- test scripts;
- detailed test procedures.

NOTE 1 This information can be given in separate
appendices.
