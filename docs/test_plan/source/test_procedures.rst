=============================================
Software unit and integration test procedures
=============================================

General
-------

- The SUITP shall provide a identification of software unit and integration test procedures.
- For each identified test procedure, the SUITP shall provide the information given in <10.2>.

Organization of each identified test procedure
----------------------------------------------

TP_01_testing_server_ias
========================

The purpose of each test procedure is to verify that specific software units or integration points behave as
expected. Each test case, such as loading and saving pipeline information, will be referenced under their
corresponding procedures.

**Procedure steps:**

1.  **Log**: Results, incidents, and any events are logged automatically through GitLab CI, where the pipeline status
    and logs are accessible for review.
2.  **Set up**: The GitLab CI pipeline automatically triggers test execution. The setup includes creating and configuring
    necessary test resources like the HDF5 file and any mock data required (e.g., fake data generation).
3.  **Start**: Once the environment is set up, the testing process starts via CI, executing the relevant test functions
    with appropriate parameters and configurations.
4.  **Proceed**: The tests are executed, running through different scenarios defined by parameters like sas_pipeline
    (MSAP1, MSAP2, etc.), covering unit and integration tests. Test execution continues automatically, checking data processing
    and storage as defined in the test cases.
5.  **Test Result Acquisition**: Results are captured by pytest’s reporting tools (e.g., pytest-html or benchmark) and stored
    in CI logs. This allows easy tracking of success or failure for each test, along with performance metrics.
6.  **Shut Down**: In case of unexpected interruptions, the CI system may halt or skip the execution, logging the failure to
    ensure that the issue is flagged for review.
7.  **Restart**: If necessary, tests can be restarted from failure points using CI’s retry mechanisms, ensuring that the
    procedure can resume without manually resetting the environment.
8.  **Wrap Up**: After execution, the GitLab CI pipeline ensures that the testing environment is cleaned up, and results
    are packaged for review, including any logs, coverage reports, and test results.

TP_02_testing_local
===================

TP_03_code_quality_coverage
===========================

sonarqube
