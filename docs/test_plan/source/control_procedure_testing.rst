==================================================================
Control procedures for software unit testing / integration testing
==================================================================

The SUITP describes the management procedures that are applied during software unit and integration testing.
The following aspects are covered:

1. **Problem Reporting and Resolution**:

   -    **Issue Tracking**: Any problems encountered during unit or integration testing are logged into an issue
        tracking system (e.g., Jira, GitLab Issues). Each issue is assigned a severity level and categorized (e.g.,
        bug, enhancement, test failure).
   -    **Reporting Process**: Test engineers and developers are responsible for identifying and reporting issues.
        The Test Manager oversees the reporting process and ensures that all issues are tracked to resolution.
   -    **Resolution Process**: Once an issue is reported, the relevant team (developers, QA, etc.) investigates
        the cause of the issue. If a bug is identified in the code, it is fixed by the development team, and tests
        are re-executed to verify the fix.
   -    **Escalation**: If an issue cannot be resolved within a reasonable time frame or if it impedes further
        testing, the issue is escalated to senior management or the Product Assurance team for immediate attention.

2. **Deviation and Waiver Policy**:

   -    **Deviation Handling**: In the event that deviations from the planned testing approach or schedule are
        necessary (e.g., due to unforeseen issues or changes in scope), the Test Manager must approve any deviation
        request. The reason for the deviation is documented and communicated to all stakeholders.
   -    **Waiver Requests**: A waiver can be requested for specific test cases or milestones if they cannot be
        completed for valid reasons (e.g., due to resource constraints or environment limitations). A waiver request
        must be formally reviewed and approved by the Test Manager, and the reasons for granting the waiver must be documented.
   -    **Approval Process**: Deviation and waiver requests are submitted to the Test Manager, who evaluates the
        impact on overall project timelines and quality. Approval from the Product Assurance team may be required for
        critical deviations or waivers.

3. **Control Procedures**:

   -    **Test Control Procedures**: Regular monitoring and reporting ensure that testing activities stay on track.
        This includes daily status meetings or check-ins where any blockers are discussed, and corrective actions are taken.
   -    **Test Reporting**: The Test Manager is responsible for compiling test results, tracking the progress of testing
        activities, and producing periodic reports. Test results are reviewed to ensure that the planned test coverage is
        achieved and to identify any gaps or issues that need further attention.
   -    **Corrective Actions**: If a test milestone is at risk of being missed, corrective actions are taken, including
        reallocating resources, adjusting schedules, or revising the test strategy. Regular meetings with development, QA,
        and configuration management teams help ensure the timely resolution of issues.
   -    **Metrics and Analysis**: Key test metrics (such as test coverage, pass/fail rates, defect density) are regularly
        analyzed to gauge the effectiveness of the testing effort. If the metrics indicate potential risks to product quality
        or test coverage, adjustments are made to improve the testing process.
   -    **Audit and Review**: Periodic audits and reviews of the testing process ensure adherence to the defined procedures.
        Any issues identified during audits are addressed in the next test cycle to prevent recurrence.
