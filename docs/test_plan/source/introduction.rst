============
Introduction
============

This document outlines the Software Unit and Integration Test Plan (SUITP) for the py_sas_data_modeler project, focusing
on ensuring the software meets its functional, performance, and quality requirements.

The purpose of this test plan is to detail the responsibilities, schedules, resources, and procedures for conducting unit
and integration testing activities. These testing efforts are essential to validate individual components, verify their
integration, and ensure the overall reliability of the software.

This test plan applies to version 1.2.0 of the py_sas_data_modeler software. It aligns with the overall software
development lifecycle, ensuring compatibility with the project’s milestones and quality standards.


==================================
Applicable and reference documents
==================================

The SUITP refers to the following applicable and reference documents that guide the generation of the testing procedures:

+-----------+----------------------------------------+--------------------------+
| Reference | Title                                  | Code                     |
+===========+========================================+==========================+
|  RD.1     | Software                               | ECSS-E-ST-40C6           |
+-----------+----------------------------------------+--------------------------+
|  RD.2     | Software product assurance             | ECSS-Q-ST-80             |
+-----------+----------------------------------------+--------------------------+
|  RD.3     | PMC-SGS System Development Plan        | PLATO-MPSSR-PMC-PLN-0001 |
+-----------+----------------------------------------+--------------------------+

These documents provide guidelines and frameworks for software testing, quality control, and the verification process for the software being developed.

========================================
Terms, definitions and abbreviated terms
========================================

For clarity, the following terms, definitions, and abbreviations are used throughout this SUITP:

.. glossary::
    :sorted:

    SUITP
        Software Unit and Integration Test Plan

    SUT
        Software Under Test – the `py_sas_data_modeler` project

    CI
        Continuous Integration – the process of integrating code changes and running automated tests frequently (e.g., using GitLab CI).

    HDF5
        Hierarchical Data Format version 5 – a file format used for managing complex data structures in `py_sas_data_modeler`.

    Test case
        A set of conditions or variables under which a tester determines whether a system or part of an application is working correctly.

    Mocking
        Creating mock objects to simulate the behavior of real objects in tests.
