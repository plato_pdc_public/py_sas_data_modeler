# -*- coding: utf-8 -*-
import json
import os
from collections import defaultdict
from datetime import date

import toml
from jinja2 import Environment
from jinja2 import FileSystemLoader
from weasyprint import HTML

# Project root
project_root = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


# Load the toml configuration
def load_project_metadata():
    """Loads the project metadata from the pyproject.toml file."""
    try:
        pyproject_path = os.path.join(project_root, "pyproject.toml")
        with open(pyproject_path, "r") as file:
            pyproject_content = toml.load(file)
        project_tool = pyproject_content.get("tool", {})
        return project_tool.get("poetry", {})
    except Exception as e:
        print(f"Error loading pyproject.toml file: {e}")
        return {}


project_metadata = load_project_metadata()


def organize_tests_by_directory(test_results):
    """Organizes the test results by directory and category (acceptance/unit)."""
    if not test_results or "tests" not in test_results:
        print("No test results available or 'tests' key missing in data.")
        return defaultdict(lambda: defaultdict(lambda: defaultdict(list)))

    organized_tests = defaultdict(
        lambda: defaultdict(lambda: defaultdict(list))
    )

    for item in test_results["tests"]:
        # Check if 'nodeid' and 'outcome' exist in each item
        if "nodeid" not in item or "outcome" not in item:
            print(f"Missing 'nodeid' or 'outcome' in test item: {item}")
            continue

        # Determine if the test belongs to 'acceptance' or 'unit'
        category = "acceptance" if "acceptance" in item["nodeid"] else "unit"

        # Extract the file name (may vary depending on the exact JSON format)
        test_file = item["nodeid"].split("::")[0].split("/")[-1]
        class_name = item["nodeid"].split("::")[1]

        # Add the test result to the organization
        organized_tests[category][test_file][class_name].append(
            {
                "nodeid": item["nodeid"].split("::")[-1],
                "outcome": item["outcome"],
                "id": item.get("id", "No id provided"),
                "title": item.get("title", "No title provided"),
                "description": item.get(
                    "description", "No description provided"
                ),
                "class_description": item.get(
                    "class_description", "No class description provided"
                ),
                "inputs": item.get("inputs", "No inputs provided"),
                "expected": item.get("expected", "No expected provided"),
                "expected": item.get("expected", "No expected provided"),
                "steps": item.get("steps", "No steps provided"),
                "coverage_spec": item.get("coverage_spec", "N/A"),
            }
        )

    return organized_tests


def generate_pdf_report(json_report_path, output_pdf_path):
    """Generates a PDF report based on the test results JSON file."""
    try:
        # Load the test results
        with open(json_report_path, "r") as f:
            json_report = json.load(f)

        # Extract necessary results
        tests = json_report.get("tests", [])
        summary = {
            "project_name": project_metadata.get("name", "unknown"),
            "version": project_metadata.get("version", "0.0.0"),
            "tested_by": project_metadata.get("authors", [{}])[0],
            "date": date.today(),
            "total_tests": len(tests),
            "passed": sum(1 for t in tests if t["outcome"] == "passed"),
            "failed": sum(1 for t in tests if t["outcome"] == "failed"),
            "skipped": sum(1 for t in tests if t["outcome"] == "skipped"),
        }

        # Organize tests by directory and category
        test_hierarchy = organize_tests_by_directory({"tests": tests})

        # Simulate bug reports for section 4
        # bugs = [
        #     {"id": 1, "description": "Critical error in analysis module", "priority": "High", "status": "Open"},
        #     {"id": 2, "description": "Compatibility issue with system X", "priority": "Medium", "status": "In Progress"}
        # ]
        bugs = []

        # Load and render the HTML template
        env = Environment(loader=FileSystemLoader(project_root))
        template = env.get_template(
            os.path.join("scripts", "test_result", "report_template.html")
        )
        context = {
            "created": json_report["created"],
            "test_hierarchy": test_hierarchy,
            "bugs": bugs,  # Add the bug report list to the context
            **summary,
        }
        rendered_html = template.render(context)

        # Convert HTML to PDF
        base_url = os.path.join(
            project_root, "docs", "source", "_static", "logo.jpeg"
        )
        HTML(string=rendered_html, base_url=base_url).write_pdf(
            output_pdf_path
        )
        print(f"PDF report generated successfully: {output_pdf_path}")

    except Exception as e:
        print(f"Error generating PDF report: {e}")


# Path to the report files
report_json_path = os.path.join(
    project_root, "scripts", "test_result", "report.json"
)
report_pdf_path = os.path.join(
    project_root, "scripts", "test_result", "test_report.pdf"
)

# Generate the PDF report
generate_pdf_report(report_json_path, report_pdf_path)
