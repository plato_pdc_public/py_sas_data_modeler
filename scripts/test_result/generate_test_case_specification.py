# -*- coding: utf-8 -*-
import ast
import json
import os
from collections import defaultdict
from datetime import date
from textwrap import wrap

import toml
from jinja2 import Environment
from jinja2 import FileSystemLoader

# Project root
project_root = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))


def wrap_spec(value, first_columnn, column_width, total_width=100):
    if not value or value == "No coverage_spec available.":
        return " " * (total_width - 3) + " |"

    specs = []
    for coverage in value:
        for item in coverage:
            specs.append(item[0])
    ids = ", ".join(specs)
    return f"{ids.ljust(column_width)}|"


def wrap_text_with_borders(value, column_width, total_width=77):
    """
    Wrap text to fit within a specific column width and ensure table borders are aligned.

    :param value: The text to wrap.
    :param column_width: The width of the column to fit the text into.
    :param total_width: The total width of the table row.
    :return: Wrapped text with borders added.
    """
    if not value:
        return "| " + " " * (total_width - 3) + "|"

    wrapped_lines = wrap(value, width=column_width)
    bordered_lines = [
        f"| {line.ljust(column_width)} |" for line in wrapped_lines
    ]
    return "\n".join(bordered_lines)


def wrap_text_with_col(value, first_columnn, column_width, total_width=100):

    if not value:
        return "| " + " " * (total_width - 3) + "|"

    lines = value.splitlines("\n")
    wrapped_lines = []
    for line in lines:
        wrapped_lines.extend(wrap(line, width=column_width))  # Wrap each line

    # Format the wrapped lines with the column layout
    bordered_lines = [f"{wrapped_lines[0].ljust(column_width)}|"]
    bordered_lines.extend(
        f"|{' ' * first_columnn}|{line.ljust(column_width)}|"
        for line in wrapped_lines[1:]
    )
    return "\n".join(bordered_lines)


# Load the toml configuration
def load_project_metadata():
    """Loads the project metadata from the pyproject.toml file."""
    try:
        pyproject_path = os.path.join(project_root, "pyproject.toml")
        with open(pyproject_path, "r") as file:
            pyproject_content = toml.load(file)
        project_tool = pyproject_content.get("tool", {})
        return project_tool.get("poetry", {})
    except Exception as e:
        print(f"Error loading pyproject.toml file: {e}")
        return {}


project_metadata = load_project_metadata()


def organize_tests_by_directory(test_results):
    """Organizes the test results by directory and category (acceptance/unit)."""
    if not test_results or "tests" not in test_results:
        print("No test results available or 'tests' key missing in data.")
        return defaultdict(lambda: defaultdict(lambda: defaultdict(list)))

    organized_tests = defaultdict(
        lambda: defaultdict(lambda: defaultdict(list))
    )

    for item in test_results["tests"]:
        # Check if 'nodeid' and 'outcome' exist in each item
        if "nodeid" not in item or "outcome" not in item:
            print(f"Missing 'nodeid' or 'outcome' in test item: {item}")
            continue

        # Determine if the test belongs to 'acceptance' or 'unit'
        category = "acceptance" if "acceptance" in item["nodeid"] else "unit"

        # Extract the file name (may vary depending on the exact JSON format)
        test_file = item["nodeid"].split("::")[0].split("/")[-1]
        class_name = item["nodeid"].split("::")[1]

        # Add the test result to the organization
        organized_tests[category][test_file][class_name].append(
            {
                "nodeid": item["nodeid"].split("::")[-1],
                "outcome": item["outcome"],
                "id": item.get("id", "No id provided"),
                "title": item.get("title", "No title provided"),
                "description": item.get(
                    "description", "No description provided"
                ),
                "class_description": item.get(
                    "class_description", "No class description provided"
                ),
                "inputs": item.get("inputs", "No inputs provided"),
                "expected": item.get("expected", "No expected provided"),
                "expected": item.get("expected", "No expected provided"),
                "steps": item.get("steps", "No steps provided"),
                "coverage_spec": item.get("coverage_spec", "N/A"),
            }
        )

    return organized_tests


def generate_rst_test_case(json_report_path):
    """Generates a RST test case for test plan."""
    try:
        # Load the test results
        with open(json_report_path, "r") as f:
            json_report = json.load(f)

        # Extract necessary results
        tests = json_report.get("tests", [])

        # Organize tests by directory and category
        test_hierarchy = organize_tests_by_directory({"tests": tests})

        # Simulate bug reports for section 4
        # bugs = [
        #     {"id": 1, "description": "Critical error in analysis module", "priority": "High", "status": "Open"},
        #     {"id": 2, "description": "Compatibility issue with system X", "priority": "Medium", "status": "In Progress"}
        # ]
        bugs = []

        # Load and render the HTML template
        env = Environment(loader=FileSystemLoader(project_root))
        env.filters["wrap_border_text"] = wrap_text_with_borders
        env.filters["wrap_text_with_col"] = wrap_text_with_col
        env.filters["wrap_spec"] = wrap_spec
        template = env.get_template(
            os.path.join(
                "scripts", "test_result", "test_case_specification.rst_tpl"
            )
        )
        context = {
            "created": json_report["created"],
            "test_hierarchy": test_hierarchy,
            "bugs": bugs,  # Add the bug report list to the context
        }
        rendered_rst = template.render(context)

        # Convert to RST
        output_file = os.path.join(
            project_root,
            "docs",
            "test_plan",
            "source",
            "test_case_specification.rst",
        )
        with open(output_file, "w") as file:
            file.write(rendered_rst)

        print(f"RST generated successfully: {output_file}")

    except Exception as e:
        print(f"Error generating RST test case: {e}")


# Path to the report files
report_json_path = os.path.join(
    project_root, "scripts", "test_result", "report.json"
)

# Generate the PDF report
generate_rst_test_case(report_json_path)
