from py_sas_data_modeler.dm.hdf5 import HDF5Delivery
from py_sas_data_modeler.simulation import FakeData
from py_sas_data_modeler.io import HDF5CompactHandler
fake_data = FakeData(use_numpy=False)
data = fake_data.generate_fake_data(HDF5Delivery)
#print(fake_data.fake_data)
handler = HDF5CompactHandler("test.h5")
handler.save(fake_data.fake_data)
