# -*- coding: utf-8 -*-
import inspect
import json

from py_sas_data_modeler import dm
from py_sas_data_modeler import io


def extract_test_coverage(module):
    coverage = dict()

    for name, obj in inspect.getmembers(module):
        if inspect.isfunction(obj):
            doc_id = getattr(obj, "_doc_id", None)
            specs = getattr(obj, "_specs", None)

            if doc_id in coverage:
                coverage[doc_id].append(specs)
            else:
                coverage[doc_id] = list()

    return coverage


coverage = list()

result = extract_test_coverage(io.hdf5.HDF5Manager)
coverage.append((result))

result = extract_test_coverage(io.hdf5.HDF5StorageSasWorkflow)
coverage.append((result))

result = extract_test_coverage(io.hdf5.HDF5StorageSasTask)
coverage.append((result))

result = extract_test_coverage(io.interface.FileMediator)
coverage.append((result))

result = extract_test_coverage(dm.dataclass_management.DataClassFactory)
coverage.append((result))

result = extract_test_coverage(dm.dataclass_management.DataClassFactory)
coverage.append((result))


print(json.dumps(coverage, indent=4))
